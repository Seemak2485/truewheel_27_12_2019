package com.truewheels.user.util;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.truewheels.user.R;
import com.truewheels.user.activities.AddSYSActivity;

import java.util.ArrayList;

public class CheckBoxDialogHelper implements CompoundButton.OnCheckedChangeListener {

    private CheckBox aCbWeekends, aCbWeekDays, aCbWholeWeek, aCBMonday, aCbTuesday, aCbWednesday, aCbThursday, aCbFriday, aCbSaturday, aCbSunday;
    private AlertDialog mAlertDialog;
    private ArrayList<String> mArrayList = new ArrayList<>();

    public void showDialog(Context context, final EditText editText, final ArrayList<String> arrayList, boolean[] iArray) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = ((AddSYSActivity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_custom_dialog_available_days, null);
        dialogBuilder.setView(dialogView);

        Button aBCancel = (Button) dialogView.findViewById(R.id.bCancel);
        Button aBOk = (Button) dialogView.findViewById(R.id.bOk);
        aCbWeekends = (CheckBox) dialogView.findViewById(R.id.cbOnlyWeekend);
        aCbWeekDays = (CheckBox) dialogView.findViewById(R.id.cbOnlyWeekdays);
        aCbWholeWeek = (CheckBox) dialogView.findViewById(R.id.cbWholeWeek);
        aCBMonday = (CheckBox) dialogView.findViewById(R.id.cbMonday);
        aCbTuesday = (CheckBox) dialogView.findViewById(R.id.cbTuesday);
        aCbWednesday = (CheckBox) dialogView.findViewById(R.id.cbWednesday);
        aCbThursday = (CheckBox) dialogView.findViewById(R.id.cbThursday);
        aCbFriday = (CheckBox) dialogView.findViewById(R.id.cbFriday);
        aCbSaturday = (CheckBox) dialogView.findViewById(R.id.cbSaturday);
        aCbSunday = (CheckBox) dialogView.findViewById(R.id.cbSunday);

        if (iArray[0])
            aCbWeekDays.setChecked(true);
        if (iArray[1])
            aCbWholeWeek.setChecked(true);
        if (iArray[2])
            aCbWholeWeek.setChecked(true);
        if (iArray[3])
            aCBMonday.setChecked(true);
        if (iArray[4])
            aCbTuesday.setChecked(true);
        if (iArray[5])
            aCbWednesday.setChecked(true);
        if (iArray[6])
            aCbThursday.setChecked(true);
        if (iArray[7])
            aCbFriday.setChecked(true);
        if (iArray[8])
            aCbSaturday.setChecked(true);
        if (iArray[9])
            aCbSunday.setChecked(true);

        aCbWeekends.setOnCheckedChangeListener(this);
        aCbWeekDays.setOnCheckedChangeListener(this);
        aCbWholeWeek.setOnCheckedChangeListener(this);
        aCBMonday.setOnCheckedChangeListener(this);
        aCbTuesday.setOnCheckedChangeListener(this);
        aCbWednesday.setOnCheckedChangeListener(this);
        aCbThursday.setOnCheckedChangeListener(this);
        aCbFriday.setOnCheckedChangeListener(this);
        aCbSaturday.setOnCheckedChangeListener(this);
        aCbSunday.setOnCheckedChangeListener(this);

        aBOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String aSelectedDays = "";
                if (mArrayList.size() > 0) {
                    for (String aInt : mArrayList) {
                        aSelectedDays = aSelectedDays + ",  " + arrayList.get(Integer.parseInt(aInt));
                    }
                }
                if (!TextUtils.isEmpty(aSelectedDays)) {
                    aSelectedDays = aSelectedDays.substring(3);
                }
                editText.setText(aSelectedDays);
                mAlertDialog.dismiss();
            }
        });

        aBCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog = dialogBuilder.create();
        mAlertDialog.show();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            switch (compoundButton.getId()) {
                case R.id.cbOnlyWeekend:
                    clearAllChecks();
                    aCbWeekends.setChecked(true);
                    mArrayList.add("1");
                    break;
                case R.id.cbOnlyWeekdays:
                    clearAllChecks();
                    aCbWeekDays.setChecked(true);
                    mArrayList.add("0");
                    break;
                case R.id.cbWholeWeek:
                    clearAllChecks();
                    aCbWholeWeek.setChecked(true);
                    mArrayList.add("2");
                    break;
                case R.id.cbMonday:
                    clearWeekChecks();
                    aCBMonday.setChecked(true);
                    mArrayList.add("3");
                    break;
                case R.id.cbTuesday:
                    clearWeekChecks();
                    mArrayList.add("4");
                    aCbTuesday.setChecked(true);
                    break;
                case R.id.cbWednesday:
                    clearWeekChecks();
                    aCbWednesday.setChecked(true);
                    mArrayList.add("5");
                    break;
                case R.id.cbThursday:
                    mArrayList.add("6");
                    clearWeekChecks();
                    aCbThursday.setChecked(true);
                    break;
                case R.id.cbFriday:
                    mArrayList.add("7");
                    clearWeekChecks();
                    aCbFriday.setChecked(true);
                    break;
                case R.id.cbSaturday:
                    clearWeekChecks();
                    mArrayList.add("8");
                    aCbSaturday.setChecked(true);
                    break;
                case R.id.cbSunday:
                    mArrayList.add("9");
                    clearWeekChecks();
                    aCbSunday.setChecked(true);
                    break;
            }
        }
    }

    private void clearAllChecks() {
        mArrayList.clear();
        aCBMonday.setChecked(false);
        aCbTuesday.setChecked(false);
        aCbWednesday.setChecked(false);
        aCbThursday.setChecked(false);
        aCbFriday.setChecked(false);
        aCbSaturday.setChecked(false);
        aCbSunday.setChecked(false);
        aCbWeekDays.setChecked(false);
        aCbWeekends.setChecked(false);
        aCbWholeWeek.setChecked(false);
    }

    private void clearWeekChecks() {
        if (mArrayList.contains("0"))
            mArrayList.remove("0");
        if (mArrayList.contains("1"))
            mArrayList.remove("1");
        if (mArrayList.contains("2"))
            mArrayList.remove("2");
        aCbWeekDays.setChecked(false);
        aCbWeekends.setChecked(false);
        aCbWholeWeek.setChecked(false);
    }
}
