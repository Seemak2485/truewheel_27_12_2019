package com.truewheels.user.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtil {
    public static boolean isValidEmail(String target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isValidPanCard(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{5}[0-9]{4}[A-Z]{1}");
        Matcher matcher = pattern.matcher(target);
        return matcher.matches();
    }

    public static boolean isValidLiscenceNo(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{13}");
        Matcher matcher = pattern.matcher(target);
        return matcher.matches();
    }

    public static boolean isValidVehicleNo(String target) {
        String aTarget = target.substring(4);
        aTarget = aTarget.substring(0, aTarget.length() - 4);
        boolean isValid= false;
        for (int i = 0; i != aTarget.length(); ++i) {
            if (Character.isLetter(aTarget.charAt(i))) {
                isValid = true;
                break;
            }
        }
        if (!isValid)
            return isValid;
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{2}[A-Z0-9]{1}[0-9]{4}");


        Matcher matcher = pattern.matcher(target);
        if (matcher.matches())
            return true;
        else
            return isValidVehicleNo2(target);
    }

    public static boolean isValidVehicleNo2(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{2}[A-Z0-9]{2}[0-9]{4}");
        Matcher matcher = pattern.matcher(target);
        if (matcher.matches())
            return matcher.matches();
        else
            return isValidVehicleNo3(target);
    }

    public static boolean isValidVehicleNo3(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{2}[A-Z0-9]{3}[0-9]{4}");
        Matcher matcher = pattern.matcher(target);
        if (matcher.matches())
            return matcher.matches();
        else
            return isValidVehicleNo4(target);
    }

    public static boolean isValidVehicleNo4(String target) {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9]{2}[A-Z0-9]{4}[0-9]{4}");
        Matcher matcher = pattern.matcher(target);
        if (matcher.matches())
        return matcher.matches();
         else
        return isValidVehicleNo5(target);
    }

    public  static boolean isValidVehicleNo5(String target)
    {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9][A-Z]{2}[0-9]{4}");
        Matcher matcher = pattern.matcher(target);

        if (matcher.matches())
        return matcher.matches();
        else
            return isValidVehicleNo6(target);
    }

    public  static boolean isValidVehicleNo6(String target)
    {
        Pattern pattern = Pattern.compile("[A-Z]{2}[0-9][A-Z]{3}[0-9]{4}");
        Matcher matcher = pattern.matcher(target);
        return matcher.matches();
    }
}
