package com.truewheels.user.util;

import android.content.Context;

import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.truewheels.user.constants.Constants;

import java.util.HashMap;


public class PaytmUtil {
    PaytmPGService Service = null;


    public void placeOrderOnPaytm(Context mContext, String iUserId, String iOrderId, String iAmount, PaytmPaymentTransactionCallback iCallBack, String iCheckSum) {
     //   Service = PaytmPGService.getStagingService();
//or
        Service = PaytmPGService.getProductionService();

//Create new order Object having all order information.
        HashMap<String, String> paramMap = new HashMap<String, String>();
        paramMap.put("MID", Constants.MID);
      //  paramMap.put("MerchantKey","oxvZasOkA4zdKm!c");
        paramMap.put("CHANNEL_ID", Constants.CHANNEL_ID);
        paramMap.put("INDUSTRY_TYPE_ID", Constants.INDUSTRY_TYPE_ID);
        paramMap.put("WEBSITE", Constants.WEBSITE);
        paramMap.put("CUST_ID",iUserId );

        paramMap.put("ORDER_ID", iOrderId);
        Double amt = Double.valueOf(iAmount);
        amt = (Math.round(amt*100.0)) / 100.0;
        paramMap.put("TXN_AMOUNT", iAmount);
        paramMap.put("CALLBACK_URL", "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+iOrderId);
        paramMap.put("CHECKSUMHASH", iCheckSum);

        PaytmOrder Order = new PaytmOrder(paramMap);
//Create new Merchant Object having all merchant configuration.
        //PaytmMerchant Merchant = new PaytmMerchant(Constants.CHECKSUM_GENERATION_URL, Constants.CHECKSUM_VERIFICATION_URL);

//Set PaytmOrder and PaytmMerchant objects. Call this method and set both objects before starting transaction.
        Service.initialize(Order, null);

//Start the Payment Transaction. Before starting the transaction ensure that initialize method is called.
        Service.startPaymentTransaction(mContext, true, true, iCallBack);
    }

    private String getCallbackUrl(String iOrderId) {
        String aVal = Constants.CALLBACK_URL + iOrderId;
        return aVal;
    }
}
