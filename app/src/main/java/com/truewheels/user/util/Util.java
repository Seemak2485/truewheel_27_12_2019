package com.truewheels.user.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.truewheels.user.BuildConfig;
import com.truewheels.user.TrueWheelApp;

import java.util.concurrent.atomic.AtomicInteger;

public class Util {
    public static AtomicInteger aInteger = new AtomicInteger(41);

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && view != null/* && inputMethodManager.isAcceptingText()*/) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && view != null/* && inputMethodManager.isAcceptingText()*/) {
            inputMethodManager.showSoftInput(view, 1);
        }
    }

    public static void showToast(String message) {
        if (TextUtils.isEmpty(message)) {
            Log.e("TAG", "Trying to show Toast Empty Toast message.");
            return;
        }
        Toast.makeText(TrueWheelApp.getInstance(), message, Toast.LENGTH_SHORT).show();
    }

    public static void shareApp(Activity activity) {
        try {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "TrueWheel");
            String message = "\nHi, welcome to the online Wheel community.\n\n";
            message = message + "https://play.google.com/store/apps/details?id=com.truewheels.user \n\n";
            intent.putExtra(Intent.EXTRA_TEXT, message);
            activity.startActivity(Intent.createChooser(intent, "choose one"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openWhatsApp(Activity activity, String smsNumber, String message, ErrorCallback errorCallback) {
        //String smsNumber = "917030701191"; // E164 format without '+' sign
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setAction(Intent.ACTION_VIEW);
        intent.setPackage("com.whatsapp");
        String url = "https://api.whatsapp.com/send?phone=" + smsNumber + "&text=" + message;
        intent.setData(Uri.parse(url));
        if (intent.resolveActivity(activity.getPackageManager()) == null) {
            errorCallback.perform();
            return;
        }
        activity.startActivity(intent);
    }

    public interface ErrorCallback {
        void perform();
    }

    public static void openBrowser(Activity activity, String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://"))
            url = "http://" + url;
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            activity.startActivity(intent);
        } catch (Exception e) {
            Util.showToast("Error");
            e.printStackTrace();
        }
    }

    public static void openPlaystore(Activity activity) {
        String url = "market://details?id=" + BuildConfig.APPLICATION_ID;
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            activity.startActivity(intent);
        } catch (Exception e) {
            Util.showToast("Error");
            e.printStackTrace();
        }
    }

    public static int getAtomicInt() {
        return aInteger.incrementAndGet();
    }
}
