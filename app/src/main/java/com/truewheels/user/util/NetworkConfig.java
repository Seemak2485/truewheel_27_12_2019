package com.truewheels.user.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.truewheels.user.TrueWheelApp;

public class NetworkConfig {

    private static NETWORK_TYPE networkType;

    private enum NETWORK_TYPE {
        WIFI, MOBILE, NOT_CONNECTED
    }

    private static void checkConnectivity() {
        Context context = TrueWheelApp.getInstance().getApplicationContext();
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = null;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
        }

        if (activeNetwork != null) { // connected to the internet
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    networkType = NETWORK_TYPE.WIFI;
                    break;
                default:
                    networkType = NETWORK_TYPE.MOBILE;
                    break;
            }
        } else {
            networkType = NETWORK_TYPE.NOT_CONNECTED;
        }
    }

    public static boolean isConnectedToInternet() {
        checkConnectivity();
        return networkType != NETWORK_TYPE.NOT_CONNECTED;
    }
}
