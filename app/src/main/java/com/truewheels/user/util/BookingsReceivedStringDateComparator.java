package com.truewheels.user.util;

import com.truewheels.user.server.model.parkings.booking_made.BookingMadeItem;
import com.truewheels.user.server.model.parkings.booking_received.BookingRecievedItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

public class BookingsReceivedStringDateComparator implements Comparator<BookingRecievedItem> {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME);

    public int compare(BookingRecievedItem lhs, BookingRecievedItem rhs) {
        try {
            return dateFormat.parse(rhs.getBookingDateTime()).compareTo(dateFormat.parse(lhs.getBookingDateTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}