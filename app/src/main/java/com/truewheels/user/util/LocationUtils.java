package com.truewheels.user.util;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;

import com.truewheels.user.R;

/**
 * Defines app-wide constants and utilities
 */
public final class LocationUtils {
    // Debugging tag for the application
    public static final String APPTAG = "DeliveryLog";
    // Name of shared preferences repository that stores persistent state
    public static final String SHARED_PREFERENCES = "com.DeliveryLog.SHARED_PREFERENCES";

    /*
     * Define a request code to send to Google Play services This code is
     * returned in Activity.onActivityResult
     */
    public final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    /*
     * KiaskSDKConstants for location update parameters
     */
    // Milliseconds per second
    public static final int MILLISECONDS_PER_SECOND = 1000;
    // The update interval
    public static final int UPDATE_INTERVAL_IN_SECONDS = 5;
    // A fast interval ceiling
    public static final int FAST_CEILING_IN_SECONDS = 1;
    // Update interval in milliseconds
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * UPDATE_INTERVAL_IN_SECONDS;
    // A fast ceiling of update intervals, used when the app is visible
    public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = MILLISECONDS_PER_SECOND * FAST_CEILING_IN_SECONDS;
    // Create an empty string for initializing strings
    public static final String EMPTY_STRING = new String();

    /**
     * Get the latitude and longitude from the Location object returned by
     * Location Services.
     *
     * @param currentLocation A Location object containing the current location
     * @return The latitude and longitude of the current location, or null if no
     * location is available.
     */
    public static String getLatLng(Context context, Location currentLocation) {
        // If the location is valid
        if (currentLocation != null) {
            // Return the latitude and longitude as strings
            return context.getString(R.string.latitude_longitude, currentLocation.getLatitude(), currentLocation.getLongitude());
        } else {
            // Otherwise, return the empty string
            return EMPTY_STRING;
        }
    }


    public void openMapsForLocation(Context iContext, Double iLatitude, Double iLongitude) {

//        String data = String.format("geo:0,0?q=", iLatitude + "," + iLongitude);
        String format = "google.navigation:q=" + String.valueOf(iLatitude) + "," + String.valueOf(iLongitude);

        // Create a Uri from an intent string. Use the result to create an Intent.
        Uri gmmIntentUri = Uri.parse(format);

        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);

        // Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps");

        if (mapIntent.resolveActivity(iContext.getPackageManager()) != null) {
            // Attempt to start an activity that can handle the Intent
            iContext.startActivity(mapIntent);
        }
    }
}