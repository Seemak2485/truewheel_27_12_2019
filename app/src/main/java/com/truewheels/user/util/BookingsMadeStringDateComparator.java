package com.truewheels.user.util;

import com.truewheels.user.server.model.parkings.booking_made.BookingMadeItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

public class BookingsMadeStringDateComparator implements Comparator<BookingMadeItem> {
    SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME);

    public int compare(BookingMadeItem lhs, BookingMadeItem rhs) {
        try {
            return dateFormat.parse(rhs.getBookingDateTime()).compareTo(dateFormat.parse(lhs.getBookingDateTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}