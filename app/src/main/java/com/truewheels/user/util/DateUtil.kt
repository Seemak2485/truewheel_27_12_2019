package com.truewheels.user.util

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


class DateUtil {

    companion object {
        const val FORMAT_READABLE_DATE = "d MMM yyyy"
        const val FORMAT_READABLE_DATE_TIME = "d MMM yyyy hh:mm:ss aaa"
        const val FORMAT_READABLE_DAY_OF_WEEK = "EEE"
        const val FORMAT_READABLE_DATE_TIME_SHORT = "d MMM yyyy hh:mm aaa"
        const val FORMAT_SQL_DATE = "yyyy-MM-dd"
        const val FORMAT_SQL_DATE_TIME = "yyyy-MM-dd HH:mm:ss"
        const val FORMAT_SQL_SLASH_DATE_TIME = "MM/dd/yyyy hh:mm:ss aaa"
        const val FORMAT_READABLE_TIME = "hh : mm aaa"
        const val FORMAT_24_HOUR_TIME = "HH : mm"

        fun getCurrentDate(pattern: String): String = SimpleDateFormat(pattern, Locale.getDefault())
                .format(Date())

        fun convertFormat(currentFormat: String, desiredFormat: String, date: String): String {
            val dateFormat = SimpleDateFormat(currentFormat, Locale.getDefault())
            val readableDateFormat = SimpleDateFormat(desiredFormat, Locale.getDefault())
            val dateObj: Date = dateFormat.parse(date)
            return readableDateFormat.format(dateObj)
        }

        fun compareDateWithSystemDate(date: String, pattern: String): Int {
            val dateTime = SimpleDateFormat(pattern, Locale.getDefault())
                    .parse(date)
            val currentDateTimeStr = SimpleDateFormat(pattern, Locale.getDefault())
                    .format(Date())
            val currentDateTime = SimpleDateFormat(pattern, Locale.getDefault())
                    .parse(currentDateTimeStr)

            return dateTime.compareTo(currentDateTime)
        }

        fun compareUserDates(iStartDate: String, iEndDate: String, pattern: String): Int {
            val startDateTime = SimpleDateFormat(pattern, Locale.getDefault())
                    .parse(iStartDate)
            val endDateTime = SimpleDateFormat(pattern, Locale.getDefault())
                    .parse(iEndDate)

            return endDateTime.compareTo(startDateTime)
        }

        fun compareDates(startDate: String, endDate: String, pattern: String): Int {
            val startDateTime = SimpleDateFormat(pattern, Locale.getDefault())
                    .parse(startDate)
            val endDateTime = SimpleDateFormat(pattern, Locale.getDefault())
                    .parse(endDate)

            return endDateTime.compareTo(startDateTime)
        }

        fun compareStartAndEndDates(startTime: String, endTime: String, parkingTime: String, parkingEndTime: String, pattern: String): Int {
            val aStartTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, startTime);
            val aEndTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, endTime);
            val aParkingStartTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, parkingTime);
            val aParkingEndTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, parkingEndTime);
            var isNextDay = false;
            if (aStartTime.substring(0, 2).toInt() >= aEndTime.substring(0, 2).toInt()) {
                if (aStartTime.substring(0, 2).toInt() == aEndTime.substring(0, 2).toInt()) {
                    isNextDay = aStartTime.substring(aStartTime.length - 2, aStartTime.length).toInt() >= aEndTime.substring(aEndTime.length - 2, aEndTime.length).toInt()
                } else
                    isNextDay = true;
            } else
                isNextDay = false;

            if (isNextDay) {
                if (checkDifferentDayTime(aStartTime, aEndTime, aParkingStartTime) && checkDifferentDayTime(aStartTime, aEndTime, aParkingEndTime))
                    return 1
            } else {
                if (checkSameDayTime(aStartTime, aEndTime, aParkingStartTime) && checkSameDayTime(aStartTime, aEndTime, aParkingEndTime))
                    return 1
            }

            return -1
        }

        private fun checkDifferentDayTime(aStartTime: String, aEndTime: String, aParkingTime: String): Boolean {
            var aStartHour = aStartTime.substring(0, 2).toInt()
            var aEndHour = aEndTime.substring(0, 2).toInt()
            var aParkingHour = aParkingTime.substring(0, 2).toInt()

            var aStartMin = aStartTime.substring(aStartTime.length - 2, aStartTime.length).toInt()
            var aEndMin = aEndTime.substring(aEndTime.length - 2, aEndTime.length).toInt()
            var aParkingMin = aParkingTime.substring(aParkingTime.length - 2, aParkingTime.length).toInt()

            var isValid = false
            if (aParkingHour >= aStartHour) {
                if (aParkingHour == aStartHour) {
                    if (aParkingMin >= aStartMin)
                        isValid = true
                } else
                    isValid = true
            }

            var isValidEnd = false
            if (aEndHour >= aParkingHour) {
                if (aEndHour == aParkingHour) {
                    if (aEndMin >= aParkingMin)
                        isValidEnd = true
                } else
                    isValidEnd = true
            }


            return (isValid || isValidEnd)
        }

        private fun checkSameDayTime(aStartTime: String, aEndTime: String, aParkingTime: String): Boolean {
            var aStartHour = aStartTime.substring(0, 2).toInt()
            var aEndHour = aEndTime.substring(0, 2).toInt()
            var aParkingHour = aParkingTime.substring(0, 2).toInt()

            var aStartMin = aStartTime.substring(aStartTime.length - 2, aStartTime.length).toInt()
            var aEndMin = aEndTime.substring(aEndTime.length - 2, aEndTime.length).toInt()
            var aParkingMin = aParkingTime.substring(aParkingTime.length - 2, aParkingTime.length).toInt()

            var isValid = false
            if (aParkingHour >= aStartHour) {
                if (aParkingHour == aStartHour) {
                    if (aParkingMin >= aStartMin)
                        isValid = true
                } else
                    isValid = true
            }

            var isValidEnd = false
            if (aEndHour >= aParkingHour) {
                if (aEndHour == aParkingHour) {
                    if (aEndMin >= aParkingMin)
                        isValidEnd = true
                } else
                    isValidEnd = true
            }


            return (isValid && isValidEnd)
        }

        fun getDate(pattern: String, date: String): Date {
            val dateFormat = SimpleDateFormat(pattern, Locale.getDefault())
            return dateFormat.parse(date)
        }

        fun getDateAsString(pattern: String, date: Date): String {
            val dateFormat: DateFormat = SimpleDateFormat(pattern, Locale.getDefault())
            return dateFormat.format(date.time)
        }

        fun getDifferenceInSeconds(startDate: Date, endDate: Date): Long {
            val duration = endDate.time - startDate.time
            return TimeUnit.MILLISECONDS.toSeconds(duration)
        }

        fun getDifferenceInSeconds(startDate: String, endDate: String, pattern: String): Long {
            val startDateObj = getDate(pattern, startDate)
            val endDateObj = getDate(pattern, endDate)
            return getDifferenceInSeconds(startDateObj, endDateObj)
        }

        fun getDifferenceInSeconds(startTime: String, parkingStartTime: String, parkingEndTime: String, pattern: String): Long {
            val aStartTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, startTime);
            val aParkingStartTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, parkingStartTime);
            val aParkingEndTime = DateUtil.convertFormat(pattern, DateUtil.FORMAT_24_HOUR_TIME, parkingEndTime);
            var isNextDay = false;
            if (aStartTime.substring(0, 2).toInt() >= aParkingEndTime.substring(0, 2).toInt()) {
                if (aStartTime.substring(0, 2).toInt() == aParkingEndTime.substring(0, 2).toInt()) {
                    isNextDay = aStartTime.substring(aStartTime.length - 2, aStartTime.length).toInt() >= aParkingEndTime.substring(aParkingEndTime.length - 2, aParkingEndTime.length).toInt()
                } else
                    isNextDay = true;
            } else
                isNextDay = false;


            if (isNextDay) {
                if (checkDifferentDayTime(aParkingStartTime, aParkingEndTime, aStartTime)) {
                    val startDateObj = getDate(pattern, startTime)
                    val endDateObj = getDate(pattern, parkingEndTime)
                    if (getDifferenceInSeconds(startDateObj, endDateObj) > 30 * 60)
                        return -1
                    else
                        return 1
                }
            } else {
                if (checkSameDayTime(aParkingStartTime, aParkingEndTime, aStartTime)) {
                    val startDateObj = getDate(pattern, startTime)
                    val endDateObj = getDate(pattern, parkingEndTime)
                    if (getDifferenceInSeconds(startDateObj, endDateObj) > 30 * 60)
                        return -1
                    else
                        return 1
                }
            }
            return -1
        }

        fun getDateDifference(currentFormat: String, date: String): Boolean {
            val dateFormat = SimpleDateFormat(currentFormat, Locale.getDefault())
            val dateObj: Date = dateFormat.parse(date)
            val aTimeStamp = dateObj.time;

            if ((System.currentTimeMillis() - aTimeStamp) > 10800000)
                return false;
            else
                return true;
        }
    }

}