package com.truewheels.user.server.model.profile

import com.google.gson.annotations.SerializedName

open class UserVehicles(

        @field:SerializedName("User_id")
        val userId: Int? = null,

        @field:SerializedName("listVehicle")
        val listVehicle: List<ListVehicleItem>? = null
)