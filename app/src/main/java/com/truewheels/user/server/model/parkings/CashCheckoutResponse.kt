package com.truewheels.user.server.model.parkings

import com.google.gson.annotations.SerializedName
import com.truewheels.user.server.model.GenericResponse

data class CashCheckoutResponse(

        @field:SerializedName("Booking_Id")
        val bookingId: String? = null,

        @field:SerializedName("OrderId")
        val orderId: String? = null,

        @field:SerializedName("Parking_id")
        val parkingId: String? = null,

        @field:SerializedName("PaymentMode")
        val paymentMode: String? = null,

        @field:SerializedName("Txn_Amount")
        val txn_amount: String? = null,

        @field:SerializedName("Txn_Id")
        val txn_id: String? = null
) : GenericResponse()