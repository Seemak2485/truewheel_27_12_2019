package com.truewheels.user.server.model.parkings

import com.google.gson.annotations.SerializedName
import com.truewheels.user.server.model.GenericResponse

/**
 * Created by Saurabh Kataria on 10-05-2018.
 */
data class PublicCheckoutBookingResponse(
        @field:SerializedName("Bill Details")
        val bill_Details: ArrayList<CheckoutBookingResponse>? = null
) : GenericResponse()
