package com.truewheels.user.server.services;

import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.SearchRequest;
import com.truewheels.user.server.model.SearchResponse;
import com.truewheels.user.server.model.map_distance_matrix.MapDistanceMatrixResponse;
import com.truewheels.user.server.model.parkings.FetchPrivateParkingResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface LocationService {
    @POST("api/SearchLocation")
    Call<BaseResponse<SearchResponse>> fetchPublicParking(@Body SearchRequest request);

    @POST("api/GetPrivateParkingSYS")
    @FormUrlEncoded
    Call<BaseResponse<FetchPrivateParkingResponse>> fetchPrivateParking(@Field("Latitude") String Latitude,
                                                                        @Field("Longitude") String Longitude);

    @GET
    Call<MapDistanceMatrixResponse> fetchMapDistanceMatrix(@Url String url,
                                                           @Query("origins") String origins,
                                                           @Query("destinations") String destinations,
                                                           @Query("key") String key);
}