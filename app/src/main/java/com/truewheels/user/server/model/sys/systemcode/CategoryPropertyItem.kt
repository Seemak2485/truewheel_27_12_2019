package com.truewheels.user.server.model.sys.systemcode

import com.google.gson.annotations.SerializedName

data class CategoryPropertyItem(

        @field:SerializedName("PropertyDescription")
        val propertyDescription: String,

        @field:SerializedName("PropertyName")
        val propertyName: String,

        @field:SerializedName("PropertyCode")
        val propertyCode: String
)