package com.truewheels.user.server.model;

import java.io.Serializable;

public class ParkingLocationData implements Serializable {
    private String Parking_id;
    private String lattitude;
    private String longitude;
    private String No_Of_Space_Avaiable;
    private String CarBasicCharge;
    private String BikeBasicCharge;
    private String BookingAvailableFlag;
    private String Distance;
    private String space_name;



    private String space_type;

    public String getParking_id() {
        return Parking_id;
    }

    public void setParking_id(String parking_id) {
        Parking_id = parking_id;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNo_Of_Space_Avaiable() {
        return No_Of_Space_Avaiable;
    }

    public void setNo_Of_Space_Avaiable(String no_Of_Space_Avaiable) {
        No_Of_Space_Avaiable = no_Of_Space_Avaiable;
    }

    public String getCarBasicCharge() {
        return CarBasicCharge;
    }

    public void setCarBasicCharge(String carBasicCharge) {
        CarBasicCharge = carBasicCharge;
    }

    public String getBikeBasicCharge() {
        return BikeBasicCharge;
    }

    public void setBikeBasicCharge(String bikeBasicCharge) {
        BikeBasicCharge = bikeBasicCharge;
    }

    public String getBookingAvailableFlag() {
        return BookingAvailableFlag;
    }

    public void setBookingAvailableFlag(String bookingAvailableFlag) {
        BookingAvailableFlag = bookingAvailableFlag;
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }
    public String getSpace_type() {
        return space_type;
    }

    public String getSpace_name() {
        return space_name;
    }

    public void setSpace_type(String space_type) {
        this.space_type = space_type;
    }
    public void setSpace_name(String space_name) {
        this.space_name = space_name;
    }
}
