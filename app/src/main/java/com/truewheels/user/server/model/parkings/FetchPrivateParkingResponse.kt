package com.truewheels.user.server.model.parkings

import com.google.gson.annotations.SerializedName

data class FetchPrivateParkingResponse(

        @field:SerializedName("Parkings")
        val parkings: List<ParkingsItem?>
)