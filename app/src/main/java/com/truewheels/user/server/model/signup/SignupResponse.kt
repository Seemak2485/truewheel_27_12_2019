package com.truewheels.user.server.model.signup

import com.google.gson.annotations.SerializedName
import com.truewheels.user.server.model.GenericResponse

data class SignupResponse(

        @field:SerializedName("FullName")
        val fullName: String? = null,

        @field:SerializedName("User_ID")
        val userID: String? = null
) : GenericResponse()