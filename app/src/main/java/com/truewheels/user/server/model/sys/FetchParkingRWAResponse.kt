package com.truewheels.user.server.model.sys

import com.google.gson.annotations.SerializedName
import com.truewheels.user.server.ParkingsItemRWA

data class FetchParkingRWAResponse(

        @field:SerializedName("All RWA Parking List")
        val parkings: List<ParkingsItemRWA>)
