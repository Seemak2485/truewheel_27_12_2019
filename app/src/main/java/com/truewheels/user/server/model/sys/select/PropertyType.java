package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.ACADEMIC_INSTITUTION;
import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.COMMERCIAL_PARKING_LOT;
import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.HOME;
import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.OFFICE_BUSINESS;
import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.PLACE_OF_WORSHIP;
import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.PRIVATE_RESIDENCE;
import static com.truewheels.user.server.model.sys.select.PropertyType.PropertyTypeEnum.RETAIL;

public class PropertyType {
    public static enum PropertyTypeEnum {

        ACADEMIC_INSTITUTION("PT_AI"),
        COMMERCIAL_PARKING_LOT("PT_CPL"),
        HOME("PT_HM"),
        OFFICE_BUSINESS("PT_OFC"),
        PLACE_OF_WORSHIP("PT_POW"),
        PRIVATE_RESIDENCE("PT_PR"),
        RETAIL("PT_R");

        private final String text;

        PropertyTypeEnum(final String text) {
            this.text = text;
        }

        public String val() {
            return text;
        }
    }

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

    /*static {
        map.put(ACADEMIC_INSTITUTION.val(), "Academic Institution");
        map.put(COMMERCIAL_PARKING_LOT.val(), "Commercial Parking lot");
        map.put(HOME.val(), "Home");
        map.put(OFFICE_BUSINESS.val(), "Office/Business");
        map.put(PLACE_OF_WORSHIP.val(), "Place of worship");
        map.put(PRIVATE_RESIDENCE.val(), "Private Residence");
        map.put(RETAIL.val(), "Retail");

        for (Map.Entry<String, String> entry : map.entrySet())
            swapped.put(entry.getValue(), entry.getKey());
    }*/

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}