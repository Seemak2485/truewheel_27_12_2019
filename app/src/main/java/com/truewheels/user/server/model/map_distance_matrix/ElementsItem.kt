package com.truewheels.user.server.model.map_distance_matrix

import com.google.gson.annotations.SerializedName

data class ElementsItem(

        @field:SerializedName("duration")
        val duration: TextValue? = null,

        @field:SerializedName("distance")
        val distance: TextValue? = null,

        @field:SerializedName("status")
        val status: String? = null
)