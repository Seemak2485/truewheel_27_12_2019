package com.truewheels.user.server.model

import com.google.gson.annotations.SerializedName

data class EarningRespone(

        @SerializedName("Booking Charges")
        var bookingCharges: String? = null,

        @SerializedName("TW Libilities")
        var commision: String? = null,

        @SerializedName("Coupon Discount")
        var couponDiscount: String? = null,


        @SerializedName("You Earned")
        var libilities: String? = null

) : GenericResponse()