package com.truewheels.user.server.model;

public class SearchRequest {
    private String parkingClass = "PC_A";
    private double Lat;
    private double Long;
    private boolean PopularParking;
    private int Distance = 3;

    public String getParkingClass() {
        return parkingClass;
    }

    public void setParkingClass(String parkingClass) {
        this.parkingClass = parkingClass;
    }

    public double getLat() {
        return Lat;
    }

    public void setLat(double lat) {
        Lat = lat;
    }

    public double getLong() {
        return Long;
    }

    public void setLong(double aLong) {
        Long = aLong;
    }

    public boolean isPopularParking() {
        return PopularParking;
    }

    public void setPopularParking(boolean popularParking) {
        PopularParking = popularParking;
    }

    public int getDistance() {
        return Distance;
    }

    public void setDistance(int distance) {
        Distance = distance;
    }
}
