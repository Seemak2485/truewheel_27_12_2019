package com.truewheels.user.server.model.profile

data class VehicleDetailsPojo(

        var vehicleID: Int = -1,
        var vehicleNumber: String = "",
        var vehicleType: String = "",
        var vehicleModel: String = ""
)