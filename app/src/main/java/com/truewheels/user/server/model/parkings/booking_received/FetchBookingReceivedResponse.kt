package com.truewheels.user.server.model.parkings.booking_received

import com.google.gson.annotations.SerializedName

data class FetchBookingReceivedResponse(

        @field:SerializedName("Booking Recieved")
        val bookingRecieved: List<BookingRecievedItem>? = null
)