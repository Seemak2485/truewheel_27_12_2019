package com.truewheels.user.server.model;

public class IsRegisterResponse {

    String Success;
    String Email;
    String Mobile;
    String Username;
    String LastLogin;
    String UserId;
    String Alternate_Email_Id;

    public String getSuccess() {
        return Success;
    }

    public void setSuccess(String success) {
        Success = success;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String username) {
        Username = username;
    }

    public String getLastLogin() {
        return LastLogin;
    }

    public void setLastLogin(String lastLogin) {
        LastLogin = lastLogin;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getAlternate_Email_Id() {
        return Alternate_Email_Id;
    }

    public void setAlternate_Email_Id(String alternate_Email_Id) {
        Alternate_Email_Id = alternate_Email_Id;
    }
}
