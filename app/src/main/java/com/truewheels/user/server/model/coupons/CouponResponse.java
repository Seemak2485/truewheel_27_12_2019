package com.truewheels.user.server.model.coupons;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Saurabh Kataria on 23-04-2018.
 */

public class CouponResponse {

    @SerializedName("AvailableCouponToUser")
    private ArrayList<CouponsModel> availableCouponToUser = new ArrayList<>();

    public ArrayList<CouponsModel> getAvailableCouponToUser() {
        return availableCouponToUser;
    }
}
