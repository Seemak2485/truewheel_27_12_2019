package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.AIRPORT_PARKING;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.ALLOCATED_CAR_PARKING_SPACE;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.MCD;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.METRO;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.MULTI_LEVEL;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.OTHERS;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.PERMISSION_TO_BLOCK_DRIVEWAY;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.PRIVATE_DRIVEWAY;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.PRIVATE_FIELD;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.PRIVATE_GARAGE;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.PRIVATE_PARKING_LOT;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.PUBLIC_GARAGE;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.SHARED_DRIVEWAY;
import static com.truewheels.user.server.model.sys.select.SpaceType.SpaceTypeEnum.SINGLE_LEVEL;


public class SpaceType {

    public static enum SpaceTypeEnum {

        ALLOCATED_CAR_PARKING_SPACE("ST_ACPS"),
        AIRPORT_PARKING("ST_AP"),
        MCD("ST_MCD"),
        MULTI_LEVEL("ST_MLPL"),
        METRO("ST_MTR"),
        OTHERS("ST_O"),
        PERMISSION_TO_BLOCK_DRIVEWAY("ST_PBD"),
        PRIVATE_DRIVEWAY("ST_PD"),
        PRIVATE_FIELD("ST_PF"),
        PUBLIC_GARAGE("ST_PG"),
        PRIVATE_PARKING_LOT("ST_PPL"),
        PRIVATE_GARAGE("ST_PVG"),
        SHARED_DRIVEWAY("ST_SD"),
        SINGLE_LEVEL("ST_SLPL");

        private final String text;

        SpaceTypeEnum(final String text) {
            this.text = text;
        }

        public String val() {
            return text;
        }
    }

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

    /*static {
        map.put(ALLOCATED_CAR_PARKING_SPACE.val(), "Allocated Car Parking Space");
        map.put(AIRPORT_PARKING.val(), "Airport");
        map.put(MCD.val(), "MCD Parking");
        map.put(MULTI_LEVEL.val(), "Multi level");
        map.put(METRO.val(), "Metro");
        map.put(OTHERS.val(), "Others");
        map.put(PERMISSION_TO_BLOCK_DRIVEWAY.val(), "Permission to block driveway");
        map.put(PRIVATE_DRIVEWAY.val(), "Private driveway");
        map.put(PRIVATE_FIELD.val(), "Private Field");
        map.put(PUBLIC_GARAGE.val(), "Public Garage");
        map.put(PRIVATE_PARKING_LOT.val(), "Private Parking lot");
        map.put(PRIVATE_GARAGE.val(), "Private Garage");
        map.put(SHARED_DRIVEWAY.val(), "Shared Driveway");
        map.put(SINGLE_LEVEL.val(), "Single level");

        for (Map.Entry<String, String> entry : map.entrySet())
            swapped.put(entry.getValue(), entry.getKey());
    }*/

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}