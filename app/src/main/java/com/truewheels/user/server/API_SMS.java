package com.truewheels.user.server;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.readystatesoftware.chuck.ChuckInterceptor;
import com.truewheels.user.TrueWheelApp;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

@SuppressWarnings("SpellCheckingInspection")
public class API_SMS {
   // public static final String USER_NAME = "demoashuhtp";
    public static final String USER_NAME = "truewheel";
    //public static final String PASSWORD = "dmashhtp";
    public static final String PASSWORD = "Truewheel19";
    //public static final String SENDER = "TRUWHL";
    public static final String SENDER = "TruWhl";
    public static final String STYPE = "normal";

    private static final String BASE_URL1 = "http://www.myvaluefirst.com/smpp/";
    private static final String BASE_URL ="http://stacking.org/index.php/smsapi/";
    private static Retrofit retrofit;

    public static Retrofit getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                    .setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .writeTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(1, TimeUnit.MINUTES)
                    .addInterceptor(new ChuckInterceptor(TrueWheelApp.getInstance().getApplicationContext()))
                    .addInterceptor(interceptor).build();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

  /*  public interface SMSService {
        @GET("sendsms")
        Call<ResponseBody> sendSMS(@Query("username") String user,
                                   @Query("password") String pass,
                                   @Query("udh") String sender,
                                   @Query("to") String phone,
                                   @Query("text") String text,
                                   @Query("from") String priority
                                   );
    }*/

    public interface SMSService {
        @GET("http://staticking.org/index.php/smsapi/httpapi/")
        Call<ResponseBody> sendSMS(@Query("uname") String user,
                                   @Query("password") String pass,
                                   @Query("sender") String sender,
                                   @Query("receiver") String phone,
                                   @Query("route") String text,
                                   @Query("msgtype") String priority,
                                   @Query("sms") String stype);
    }
}