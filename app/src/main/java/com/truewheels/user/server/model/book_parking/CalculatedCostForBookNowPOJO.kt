package com.truewheels.user.server.model.book_parking

data class CalculatedCostForBookNowPOJO(
        var totalAmount: String = "",
        var dateIn: String = "",
        var dateOut: String = "",
        var subscriptionMode: String = "",
        var vehicleNo: String = "",
        var vehicleType: String = "",
        var baseAmount: String = "",
        var bookingTDSAmount: String = "",
        var serviceCharge: String = "",
        var rysTDSAmount: String = "",
        var payableToRYSAmount: String = ""
)