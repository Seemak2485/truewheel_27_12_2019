package com.truewheels.user.server.model.map_distance_matrix

import com.google.gson.annotations.SerializedName

data class MapDistanceMatrixResponse(

        @field:SerializedName("destination_addresses")
        val destinationAddresses: List<String>? = null,

        @field:SerializedName("rows")
        val rows: List<RowsItem>? = null,

        @field:SerializedName("origin_addresses")
        val originAddresses: List<String>? = null,

        @field:SerializedName("status")
        val status: String? = null
)