package com.truewheels.user.server.model.parkings.fetch_public_parking_detail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PublicParkingDetailItem(

        @field:SerializedName("No_of_space_bike")
        val noOfSpaceBike: String? = null,

        @field:SerializedName("CarMonthlyNightCharge")
        var carMonthlyNightCharge: String? = null,

        @field:SerializedName("Description")
        val description: String? = null,

        @field:SerializedName("CarMonthlyCharge")
        var carMonthlyCharge: String? = null,

        @field:SerializedName("lattitude")
        val lattitude: String? = null,

        @field:SerializedName("BikeNightCharge")
        var bikeNightCharge: String? = null,

        @field:SerializedName("BikeBasicCharge")
        var bikeBasicCharge: String? = null,

        @field:SerializedName("CarMonthlyDayCharge")
        var carMonthlyDayCharge: String? = null,

        @field:SerializedName("BikeMonthlyCharge")
        var bikeMonthlyCharge: String? = null,

        @field:SerializedName("BikeMonthlyNightCharge")
        var bikeMonthlyNightCharge: String? = null,

        @field:SerializedName("No_of_space")
        val noOfSpace: String? = null,

        @field:SerializedName("TimeFrom")
        val timeFrom: String = "",

        @field:SerializedName("TimeTo")
        val timeTo: String = "",

        @field:SerializedName("BookingAvailableFlag")
        val bookingAvailableFlag: String? = null,

        @field:SerializedName("CarNightCharge")
        var carNightCharge: String? = null,

        @field:SerializedName("Space_type")
        val spaceType: String? = null,

        @field:SerializedName("BikeMonthlyDayCharge")
        var bikeMonthlyDayCharge: String? = null,

        @field:SerializedName("Property_type")
        val propertyType: String? = null,

        @field:SerializedName("parking_id")
        val parkingId: Int? = null,

        @field:SerializedName("CarBasicCharge")
        var carBasicCharge: String? = null,

        @field:SerializedName("propertyaddress")
        val propertyaddress: String? = null,

        @field:SerializedName("AverageRating")
        val averageRating: String? = null,

        @field:SerializedName("PropertyVerifiedStatus")
        val propertyVerifiedStatus: String? = null,

        @field:SerializedName("HelmetCharge")
        val helmetCharge: String? = null,

        @field:SerializedName("longitude")
        val longitude: String? = null
) : Serializable