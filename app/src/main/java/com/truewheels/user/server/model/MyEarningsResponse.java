package com.truewheels.user.server.model;

import com.google.gson.annotations.SerializedName;

public class MyEarningsResponse {

    public boolean Success;
    public String Message;
    @SerializedName("Booking Charges")
    public String bookingCharges;

    @SerializedName("TW Libilities")
    public String commision;

    @SerializedName("Coupon Discount")
    public String couponDiscount;

    @SerializedName("Message")
    public String message;

    @SerializedName("You Earned")
    public String libilities;
}
