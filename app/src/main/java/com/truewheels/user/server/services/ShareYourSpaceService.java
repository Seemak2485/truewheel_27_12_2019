package com.truewheels.user.server.services;

import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.GenericResponse;
import com.truewheels.user.server.model.sys.FetchParkingRWAResponse;
import com.truewheels.user.server.model.sys.select.FetchSYSParkingResponse;
import com.truewheels.user.server.model.sys.systemcode.SystemCodeResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ShareYourSpaceService {
    @GET("api/GetAllRWAParkingList")
    Call<BaseResponse<FetchParkingRWAResponse>> fetchRWAParkingList();

    @GET("api/GetAllRYSByOwnerID")
    Call<BaseResponse<FetchSYSParkingResponse>> fetchParkingList(@Query("ownerId") String ownerId);

    @GET("api/GetAllSystemCodes")
    Call<BaseResponse<SystemCodeResponse>> fetchSystemCodes();

    @POST("api/Insert_RestSpace_RegParkingArea")
    @FormUrlEncoded
    Call<BaseResponse<GenericResponse>> addParking(@Field("owner_id") String owner_id,
                                                   @Field("Space_type") String Space_type,
                                                   @Field("Property_type") String Property_type,
                                                   @Field("No_of_space") String No_of_space,
                                                   @Field("No_Of_Space_Avaiable_bike") String noOfSpaceAvailableBike,
                                                   @Field("PropertyAddress") String PropertyAddress,
                                                   @Field("PropertyPinCode") String PropertyPinCode,
                                                   @Field("PropertyLandMark") String PropertyLandMark,
                                                   @Field("OwnerComments") String OwnerComments,
                                                   @Field("lattitude") String latitude,
                                                   @Field("longitude") String longitude,
                                                   @Field("state") String state,
                                                   @Field("timeFrom") String timeFrom,
                                                   @Field("timeTo") String timeTo,
                                                   @Field("BikeBasicCharge") String bikeBasicCharge,
                                                   @Field("CarBasicCharge") String carBasicCharge,
                                                   @Field("open24hours") String open24hours,
                                                   @Field("descriptionSelected") String descriptionSelected,
                                                   @Field("daysSelected") String daysSelected,
                                                   @Field("facilitySelected") String facilitySelected,
                                                   @Field("parkingSpaceName") String parkingSpaceName,
                                                   @Field("bikedailycharge") String bikedailycharge,
                                                   @Field("bikemonthycharge") String bikeMonthlyCharge,
                                                   @Field("CarDailyCharge") String carDailyCharge,
                                                   @Field("carmonthlycharge") String carMonthlyCharge,
                                                   @Field("Hourly_Pricing") String hourlyPricing,
                                                   @Field("Parking_Available_Date") String parkingAvailableDate,
                                                   @Field("DaysProduct") int DaysProduct,
                                                   @Field("Parentparking")int parentParking,
                                                   @Field("Parking_support_no") String parkingSupportNo);

    @POST("api/Update_SYS_RegParkingArea")
    @FormUrlEncoded
    Call<BaseResponse<GenericResponse>> updateParking(@Field("owner_id") String owner_id,
                                                      @Field("Space_type") String Space_type,
                                                      @Field("Property_type") String Property_type,
                                                      @Field("No_of_space") String No_of_space,
                                                      @Field("No_Of_Space_Avaiable_bike") String noOfSpaceAvailableBike,
                                                      @Field("PropertyAddress") String PropertyAddress,
                                                      @Field("PropertyPinCode") String PropertyPinCode,
                                                      @Field("PropertyLandMark") String PropertyLandMark,
                                                      @Field("OwnerComments") String OwnerComments,
                                                      @Field("lattitude") String latitude,
                                                      @Field("longitude") String longitude,
                                                      @Field("Status") String status,
                                                      @Field("State") String state,
                                                      @Field("TimeFrom") String timeFrom,
                                                      @Field("TimeTo") String timeTo,
                                                      @Field("BikeBasicCharge") String bikeBasicCharge,
                                                      @Field("CarBasicCharge") String carBasicCharge,
                                                      @Field("open24hours") String open24hours,
                                                      @Field("descriptionSelected") String descriptionSelected,
                                                      @Field("daysSelected") String daysSelected,
                                                      @Field("facilitySelected") String facilitySelected,
                                                      @Field("parkingSpaceName") String parkingSpaceName,
                                                      @Field("bikedailycharge") String bikedailycharge,
                                                      @Field("bikemonthycharge") String bikeMonthlyCharge,
                                                      @Field("CarDailyCharge") String carDailyCharge,
                                                      @Field("carmonthlycharge") String carMonthlyCharge,
                                                      @Field("Hourly_Pricing") String hourlyPricing,
                                                      @Field("Parking_Available_Date") String parkingAvailableDate,
                                                      @Field("parking_id") String parkingId,
                                                      @Field("Parentparking")int parentParking,
                                                      @Field("DaysProduct") int DaysProduct,
                                                      @Field("Parking_support_no") String parkingSupportNo);

    @POST("api/Activate_RestSpace")
    Call<BaseResponse> activateParking(@Query("ownerId") String ownerId,
                                       @Query("parkingId") String parkingId);

    @POST("api/Deactivate_RestSpace")
    Call<BaseResponse> deactivateParking(@Query("ownerId") String ownerId,
                                         @Query("parkingId") String parkingId);
}