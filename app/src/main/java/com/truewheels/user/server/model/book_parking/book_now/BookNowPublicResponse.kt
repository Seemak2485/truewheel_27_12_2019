package com.truewheels.user.server.model.book_parking.book_now

import com.google.gson.annotations.SerializedName

data class BookNowPublicResponse(

        @field:SerializedName("bookingID")
        val bookingID: String? = null,

        @field:SerializedName("Attendant_Phone_No")
        val attendantPhoneNo: String? = null,

        @field:SerializedName("Owner_Address")
        val ownerAddress: String = "",

        @field:SerializedName("Message")
        val message: String? = null,

        @field:SerializedName("Paid Amount")
        val paidAmount: String? = null,

        @field:SerializedName("Owner Name ")
        val ownerName: String? = null,

        @field:SerializedName("Payment Transaction ID")
        val paymentTransactionID: String = "",

        @field:SerializedName("Owner Number")
        val ownerNumber: String? = null,

        @field:SerializedName("Success")
        val success: Boolean? = null,

        @field:SerializedName("Attendant_Name")
        val attendantName: String? = null
)