package com.truewheels.user.server.model;

public class LocationData {
    String ParkingClass;
    String Destination;
    double lat;
    double lng;

    public String getParkingClass() {
        return ParkingClass;
    }

    public void setParkingClass(String parkingClass) {
        ParkingClass = parkingClass;
    }

    public String getDestination() {
        return Destination;
    }

    public void setDestination(String destination) {
        Destination = destination;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "LocationData{" +
                "ParkingClass='" + ParkingClass + '\'' +
                ", Destination='" + Destination + '\'' +
                '}';
    }
}
