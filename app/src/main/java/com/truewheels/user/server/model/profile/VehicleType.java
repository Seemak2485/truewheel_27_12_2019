package com.truewheels.user.server.model.profile;

import java.util.LinkedHashMap;
import java.util.Map;

public class VehicleType {

    private static class SingletonHelper {
        private static final Map<String, String> map = new LinkedHashMap<>();
        private static final Map<String, String> swapped = new LinkedHashMap<>();

        static {
            map.put("2", "Two Wheeler");
            //map.put("3", "Three Wheeler");
            map.put("4", "Four Wheeler");

            for (Map.Entry<String, String> e : map.entrySet()) {
                swapped.put(e.getValue(), e.getKey());
            }
        }
    }

    public static Map<String, String> getMap() {
        return SingletonHelper.map;
    }

    public static Map<String, String> getSwappedMap() {
        return SingletonHelper.swapped;
    }
}