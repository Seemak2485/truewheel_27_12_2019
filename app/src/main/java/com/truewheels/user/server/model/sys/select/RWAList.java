package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.ParkingsItemRWA;
import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RWAList {

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

    public static void setMap(List<ParkingsItemRWA> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (ParkingsItemRWA categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getParkingId().toString(), categoryPropertyItem.getParkingName());
            swapped.put(categoryPropertyItem.getParkingName(), categoryPropertyItem.getParkingId().toString());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}