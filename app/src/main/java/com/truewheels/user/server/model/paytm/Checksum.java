package com.truewheels.user.server.model.paytm;


import com.google.gson.annotations.SerializedName;

public class Checksum {
    @SerializedName("checksum")
    private String checksum;

    @SerializedName("ORDER_ID")
    private String orderId;

    @SerializedName("payt_STATUS")
    private String paytStatus;

    public Checksum(String checksum, String orderId, String paytStatus) {
        this.checksum = checksum;
        this.orderId = orderId;
        this.paytStatus = paytStatus;
    }

    public String getChecksumHash() {
        return checksum;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getPaytStatus() {
        return paytStatus;
    }
}
