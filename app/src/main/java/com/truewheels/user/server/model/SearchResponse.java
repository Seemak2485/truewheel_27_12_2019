package com.truewheels.user.server.model;

import java.util.ArrayList;

public class SearchResponse extends BaseResponse {
    private ArrayList<ParkingLocationData> parkingList;

    public ArrayList<ParkingLocationData> getParkingList() {
        return parkingList;
    }

    public void setParkingList(ArrayList<ParkingLocationData> parkingList) {
        this.parkingList = parkingList;
    }
}
