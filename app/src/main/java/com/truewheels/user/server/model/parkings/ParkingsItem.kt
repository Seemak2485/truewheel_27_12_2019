package com.truewheels.user.server.model.parkings

import com.google.gson.annotations.SerializedName

data class ParkingsItem(

        @field:SerializedName("bikemonthlycharge")
        val bikemonthlycharge: String? = null,

        @field:SerializedName("lattitude")
        val lattitude: String? = null,

        @field:SerializedName("CarDailyCharge")
        val carDailyCharge: String? = null,

        @field:SerializedName("BikeBasicCharge")
        val bikeBasicCharge: String? = null,

        @field:SerializedName("CarBasicCharge")
        val carBasicCharge: String? = null,

        @field:SerializedName("carmonthlycharge")
        val carmonthlycharge: String? = null,

        @field:SerializedName("ErrorMessage")
        val errorMessage: String? = null,

        @field:SerializedName("Success")
        val success: Boolean? = null,

        @field:SerializedName("Distance")
        val distance: String? = null,

        @field:SerializedName("bikedailycharge")
        val bikedailycharge: String? = null,

        @field:SerializedName("longitude")
        val longitude: String? = null,

        @field:SerializedName("Parking_Id")
        val parkingId: Int? = null,

        @field:SerializedName("Space_type")
        val spacetype: String? = null,

        @field:SerializedName("Park_Space_Name")
        val spacename: String? = null
)