package com.truewheels.user.server.services;

import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.GenericResponse;
import com.truewheels.user.server.model.book_parking.CalculateCostResponse;
import com.truewheels.user.server.model.book_parking.book_now.BookNowPublicResponse;
import com.truewheels.user.server.model.book_parking.book_now.BookNowResponse;
import com.truewheels.user.server.model.parkings.CashCheckoutResponse;
import com.truewheels.user.server.model.parkings.CheckoutBookingResponse;
import com.truewheels.user.server.model.parkings.FetchRWAParkingResponse;
import com.truewheels.user.server.model.parkings.PublicCheckoutBookingResponse;
import com.truewheels.user.server.model.parkings.fetch_private_parking_detail.FetchParkingDetailsResponse;
import com.truewheels.user.server.model.parkings.fetch_public_parking_detail.FetchPublicParkingDetailResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ParkingService {
    @POST("api/GetRWAParkingByParkingID")
    Call<BaseResponse<FetchRWAParkingResponse>> fetchRWAParking(@Query("parkingId") String parkingId);

    @POST("api/GetIndividualPublicParkingDetails")
    Call<BaseResponse<FetchPublicParkingDetailResponse>> fetchPublicParkingDetails(@Query("Parking_id") String parkingId);

    @POST("api/GetIndividualParkingDetails")
    Call<BaseResponse<FetchParkingDetailsResponse>> fetchParkingDetails(@Query("parking_id") String parkingId);

    @POST("api/GetEstimatedCost")
    @FormUrlEncoded
    Call<BaseResponse<CalculateCostResponse>> calculateCost(@Field("Parking_ID") String parkingId,
                                                            @Field("BookedIntime") String BookedIntime,
                                                            @Field("BookedOutTime") String BookedOutTime,
                                                            @Field("Vehicle_Type") String Vehicle_Type,
                                                            @Field("MonthlySuscription") String subscription,
                                                            @Field("Coupon_Id") String couponId);

    @POST("api/SetPayment")
    @FormUrlEncoded
    Call<BaseResponse<BookNowResponse>> bookNow(@Field("parkingId") String parkingId,
                                                @Field("login_userId") String userId,
                                                @Field("bookedInTime") String bookedInTime,
                                                @Field("bookedOutDatetime") String bookedOutDatetime,
                                                @Field("vehicalNo") String vehicalNo,
                                                @Field("vehicleType") String vehicleType,
                                                @Field("paymentMode") String paymentMode,
                                                @Field("paymentAmount") String paymentAmount,
                                                @Field("monthlySubscription") String subscription,
                                                @Field("BaseAmount") String baseAmount,
                                                @Field("Coupon_id") String Coupon_id,
                                                @Field("BookingTDSAmount") String BookingTDSAmount,
                                                @Field("TWServiceCharge") String TWServiceCharge,
                                                @Field("RYSTDSAmount") String RYSTDSAmount,
                                                @Field("TotalAmount") String TotalAmount,
                                                @Field("PayableToRYSAmount") String PayableToRYSAmount);

    @POST("api/SetPayment")
    @FormUrlEncoded
    Call<BaseResponse<BookNowResponse>> bookNowPaytm(@Field("parkingId") String parkingId,
                                                     @Field("login_userId") String userId,
                                                     @Field("bookedInTime") String bookedInTime,
                                                     @Field("bookedOutDatetime") String bookedOutDatetime,
                                                     @Field("vehicalNo") String vehicalNo,
                                                     @Field("vehicleType") String vehicleType,
                                                     @Field("paymentMode") String paymentMode,
                                                     @Field("paymentAmount") String paymentAmount,
                                                     @Field("monthlySubscription") String subscription,
                                                     @Field("Transaction_ID") String transaction_ID,
                                                     @Field("BaseAmount") String baseAmount,
                                                     @Field("BookingTDSAmount") String BookingTDSAmount,
                                                     @Field("TWServiceCharge") String TWServiceCharge,
                                                     @Field("RYSTDSAmount") String RYSTDSAmount,
                                                     @Field("TotalAmount") String TotalAmount,
                                                     @Field("PayableToRYSAmount") String PayableToRYSAmount,
                                                     @Field("Coupon_id") String Coupon_id,
                                                     @Field("BANKNAME") String BANKNAME,
                                                     @Field("BANKTXNID") String BANKTXNID,
                                                     @Field("GATEWAYNAME") String GATEWAYNAME,
                                                     @Field("MID") String MID,
                                                     @Field("ORDERID") String ORDERID,
                                                     @Field("PROMO_CAMP_ID") String PROMO_CAMP_ID,
                                                     @Field("PROMO_RESPCODE") String PROMO_RESPCODE,
                                                     @Field("PROMO_STATUS") String PROMO_STATUS,
                                                     @Field("RESPMSG") String RESPMSG,
                                                     @Field("TXNAMOUNT") String TXNAMOUNT,
                                                     @Field("TXNID") String TXNID,
                                                     @Field("TXNTYPE") String TXNTYPE,
                                                     @Field("RESPCODE") String RESPCODE);

    @POST("api/Booking_Cancel")
    Call<BaseResponse<GenericResponse>> cancelBooking(@Query("BookedID") String bookedID,
                                                      @Query("CancellationReason") String cancellationReason,
                                                      @Query("whocancelledID") String whoCancelledID);

    @POST("api/UpdateCheckOut")
    Call<BaseResponse<CheckoutBookingResponse>> checkoutBooking(@Query("bookingId") String bookingId,
                                                                @Query("CheckInOut") String CheckInOut,
                                                                @Query("whodidcheckoutID") String whoDidCheckoutID);

    @POST("api/PrivateParkingINandOUTforMonthly")
    Call<BaseResponse<CheckoutBookingResponse>> inOutMonthly(@Query("bookingId") String bookingId   );


    @POST("api/GetBillDetailsOnApp")
    Call<BaseResponse<PublicCheckoutBookingResponse>> getBill(@Query("BookingId") String bookingId);

    @POST("api/TransactionRequest")
    Call<BaseResponse<CashCheckoutResponse>> paytmRequest(@Query("Booking_Id") String bookingId,
                                                          @Query("orderId") String orderId,
                                                          @Query("parking_Id") String parkingId,
                                                          @Query("cust_Id") String custId,
                                                          @Query("txn_Amount") String txnAmount);

    @FormUrlEncoded
    @POST("api/CashTransactionRequest")
    Call<BaseResponse<CashCheckoutResponse>> cashRequest(@Field("Booking_Id") String bookingId,
                                                         @Field("OrderId") String orderId,
                                                         @Field("Parking_Id") String parkingId,
                                                         @Field("ParkingType") String parkingType,
                                                         @Field("PaymentMode") String paymentMode,
                                                         @Field("Txn_Amount") String txnAmount);

    @POST("api/PostCheckOutInsertPaymentModeByPO")
    Call<BaseResponse<CashCheckoutResponse>> collectCashRequest(@Query("Booked_Id") String bookingId,
                                                                @Query("TotalAmount") String totalAmount);

    @POST("api/Booking_Support")
    Call<BaseResponse<GenericResponse>> supportForBooking(@Query("BookedID") String BookedID,
                                                          @Query("SupportTitle") String SupportTitle,
                                                          @Query("Support_Description") String Support_Description,
                                                          @Query("SupportRequiredID") String SupportRequiredID);

    @POST("api/BookForPublicParking")
    @FormUrlEncoded
    Call<BaseResponse<BookNowPublicResponse>> bookNowPublic(@Field("login_userId") String userId,
                                                            @Field("parkingId") String parkingId,
                                                            @Field("vehicalNo") String vehicalNo,
                                                            @Field("BarCode") String BarCode,
                                                            @Field("vehicleWheels") String vehicleWheels,
                                                            @Field("MonthlySubscription") String MonthlySubscription,
                                                            @Field("Helmet") int Helmet,
                                                            @Field("Coupon_Id") String Coupon_Id);


}