package com.truewheels.user.server.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class ClusterMarkerItem implements ClusterItem {
    private final LatLng mPosition;
    private String mTitle;
    private String mSnippet;
    private ParkingLocationData parkingLocationData;
    private boolean isPublic;
    private boolean isPublicAvailable;
    private boolean isRWAAvailable;

    public ClusterMarkerItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public ClusterMarkerItem(double lat, double lng, String title, String snippet) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
    }

    public void setParkingLocationData(ParkingLocationData parkingLocationData) {
        this.parkingLocationData = parkingLocationData;
    }

    public ParkingLocationData getParkingLocationData() {
        return parkingLocationData;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isPublicAvailable() {
        return isPublicAvailable;
    }

    public void setPublicAvailable(boolean aPublicAvailable) {
        isPublicAvailable = aPublicAvailable;
    }

    public boolean isRWAAvailable() {
        return isRWAAvailable;
    }

    public void setRWAAvailable(boolean aRWAAvailable) {
        isRWAAvailable = aRWAAvailable;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }
}