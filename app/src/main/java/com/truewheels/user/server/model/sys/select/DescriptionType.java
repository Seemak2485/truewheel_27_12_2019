package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.ARRANGE_TRANSFER;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.ATM;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.CCTV;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.COVERED_PARKING;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.ELECTRICITY;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.GUARD_ON_SITE;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.PERMIT_REQUIRED;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.SECURITY_GATE;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.SECURITY_KEY;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.UNDERGROUND;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.WATER_SUPPLY;
import static com.truewheels.user.server.model.sys.select.DescriptionType.DescriptionTypeEnum.WIFI;

public class DescriptionType {
    public static enum DescriptionTypeEnum {

        ARRANGE_TRANSFER("DT_AT"),
        ATM("DT_ATM"),
        CCTV("DT_CCTV"),
        COVERED_PARKING("DT_CP"),
        ELECTRICITY("DT_Elec"),
        GUARD_ON_SITE("DT_GS"),
        PERMIT_REQUIRED("DT_PR"),
        SECURITY_GATE("DT_SG"),
        SECURITY_KEY("DT_SK"),
        UNDERGROUND("DT_UG"),
        WATER_SUPPLY("DT_Water"),
        WIFI("DT_WIFI");

        private final String text;

        DescriptionTypeEnum(final String text) {
            this.text = text;
        }

        public String val() {
            return text;
        }
    }

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

   /* static {
        map.put(ARRANGE_TRANSFER.val(), "Arrange Transfer");
        map.put(ATM.val(), "ATM Nearby");
        map.put(CCTV.val(), "CCTV");
        map.put(COVERED_PARKING.val(), "Covered Parking");
        map.put(ELECTRICITY.val(), "Electricity");
        map.put(GUARD_ON_SITE.val(), "Guard on site");
        map.put(PERMIT_REQUIRED.val(), "Permit Required");
        map.put(SECURITY_GATE.val(), "Security Gate");
        map.put(SECURITY_KEY.val(), "Security Key");
        map.put(UNDERGROUND.val(), "Underground");
        map.put(WATER_SUPPLY.val(), "Water Supply");
        map.put(WIFI.val(), "WiFi");

        for (Map.Entry<String, String> entry : map.entrySet())
            swapped.put(entry.getValue(), entry.getKey());
    }*/

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}