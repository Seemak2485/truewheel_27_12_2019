package com.truewheels.user.server.model;

public class SignUpResponse {
    private boolean Success;
    private String Message;
    private String Transation;

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTransation() {
        return Transation;
    }

    public void setTransation(String transation) {
        Transation = transation;
    }
}
