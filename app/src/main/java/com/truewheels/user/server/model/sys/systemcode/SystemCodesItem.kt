package com.truewheels.user.server.model.sys.systemcode

import com.google.gson.annotations.SerializedName

data class SystemCodesItem(

        @field:SerializedName("Category")
        val category: String,

        @field:SerializedName("CategoryProperty")
        val categoryProperty: List<CategoryPropertyItem>
)