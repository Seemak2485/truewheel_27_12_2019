package com.truewheels.user.server.model.map_distance_matrix

import com.google.gson.annotations.SerializedName

data class RowsItem(

        @field:SerializedName("elements")
        val elements: List<ElementsItem>? = null
)