package com.truewheels.user.server.model.paytm;

import com.google.gson.annotations.SerializedName;
import com.truewheels.user.server.model.BaseResponse;

public class PaytmResponse extends BaseResponse {

    public String ORDERID;
    @SerializedName("mid")
    public String MID;
    public String TXNID;
    public String TXNAMOUNT;
    public String PAYMENTMODE;
    public String CURRENCY;
    public String TXNDATE;

    @SerializedName("resultInfo")
    public ResultInfo resultInfo;


    public String RESPMSG;
    public String GATEWAYNAME;
    public String BANKTXNID;
    public String BANKNAME;
    public String CHECKSUMHASH;
    public String REFUNDAMOUNT;
    public String REFID;
    public String CARD_ISSUER;
    public String TOTALREFUNDAMT;
    public String REFUNDDATE;
    public String REFUNDTYPE;
    public String REFUNDID;
    public String ErrorCode;
    public String ErrorMsg;

   public class ResultInfo {
        @SerializedName("resultCode")
        String RESPCODE;
        @SerializedName("resultStatus")
        String STATUS;
        @SerializedName("resultMsg")
        String RESPMSG;
        public String getRESPCODE() {
            return RESPCODE;
        }

        public void setRESPCODE(String RESPCODE) {
            this.RESPCODE = RESPCODE;
        }

        public String getSTATUS() {
            return STATUS;
        }

        public void setSTATUS(String STATUS) {
            this.STATUS = STATUS;
        }
    }



//    {"ORDERID":"31961525978568876", "MID":"GANEIS18597724354172",
// "TXNID":"20180511111212800110168329200013798", "TXNAMOUNT":"60.00",
// "PAYMENTMODE":"PPI", "CURRENCY":"INR",
// "TXNDATE":"2018-05-11 00:26:11.0", "STATUS":"TXN_SUCCESS",
// "RESPCODE":"01", "RESPMSG":"Txn Success", "GATEWAYNAME":"WALLET", "BANKTXNID":"",
// "BANKNAME":"WALLET", "CHECKSUMHASH":"vuQ/MuileYqFPaVq3qEsmz3BInziZEPFHaRUiu7/v8nK8cxfOXTI4qW3CH+nRqP59bX8E6Egvwmcc8fiy8jFMg3PUKV43R71pO1u6y2Rj/8="}
}