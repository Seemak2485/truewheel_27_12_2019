package com.truewheels.user.server.model.parkings

import com.google.gson.annotations.SerializedName
import com.truewheels.user.server.model.GenericResponse

data class CheckoutBookingResponse(

        @field:SerializedName("Status")
        val status: Boolean? = null,

        @field:SerializedName("Total_Days")
        val totalDays: String? = null,

        @field:SerializedName("Extra_Hour")
        val extraHour: String? = null,

        @field:SerializedName("ExtraDays")
        val extraDays: String? = null,

        @field:SerializedName("Extra_month")
        val extraMonth: String? = null,

        @field:SerializedName("Total_Min")
        val totalMin: String? = null,

        @field:SerializedName("TotalAmount")
        val totalAmount: String? = null,

        @field:SerializedName("Base_Amount")
        val baseAmount: String? = null,

        @field:SerializedName("Total_Hour")
        val totalHour: String? = null,

        @field:SerializedName("ExtraAmount")
        val extraAmount: String? = null,

        @field:SerializedName("TXNAMOUNT")
        val totalTxnAmount: String? = null,

        @field:SerializedName("AlreadyCheckOutBy")
        val checkedOutBy: String? = null,

        @field:SerializedName("whoDidCheckout")
        val whoDidCheckout: String? = null,

        @field:SerializedName("Amount")
        val amount: String? = null,

        @field:SerializedName("CheckOut")
        val checkOutPublic: String = "",

        @field:SerializedName("CheckIn")
        val checkInPublic: String = "",

        @field:SerializedName("ORDERID")
        val ORDERID: String = "",

        @field:SerializedName("BANKTXNID")
        val BANKTXNID: String = "",

        @field:SerializedName("REFUNDAMOUNT")
        val REFUNDAMOUNT: String = "",

        @field:SerializedName("REFUNDID")
        val REFUNDID: String = "",

        @field:SerializedName("Refund")
        val Refund: String = "",

        @field:SerializedName("RESPCODE")
        val RESPCODE: String = "",

        @field:SerializedName("RESPMSG")
        val RESPMSG: String = "",

        @field:SerializedName("TXNDATE")
        val TXNDATE: String = "",

        @field:SerializedName("STATUS")
        val STATUS: String = "",

        @field:SerializedName("Paid")
        val Paid: String = "",

        @field:SerializedName("Extra_Min")
        val extraMin: String? = null
) : GenericResponse()