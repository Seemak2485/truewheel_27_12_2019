package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class StateType {

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}