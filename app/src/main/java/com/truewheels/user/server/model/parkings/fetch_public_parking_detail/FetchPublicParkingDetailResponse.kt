package com.truewheels.user.server.model.parkings.fetch_public_parking_detail

import com.google.gson.annotations.SerializedName

data class FetchPublicParkingDetailResponse(

        @field:SerializedName("ParkingDetail")
        val parkingDetail: List<PublicParkingDetailItem>
)