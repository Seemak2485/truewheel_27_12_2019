package com.truewheels.user.server.model.book_parking

import com.google.gson.annotations.SerializedName

data class CalculateCostResponse(

        @field:SerializedName("BookingTDSAmount")
        val bookingTDSAmount: String,

        @field:SerializedName("TotalMinutes")
        val totalMinutes: String,

        @field:SerializedName("Message")
        val message: String,

        @field:SerializedName("Totalhours")
        val totalHours: String,

        @field:SerializedName("TotalAmount")
        val totalAmount: String,

        @field:SerializedName("Success")
        val success: Boolean? = null,

        @field:SerializedName("RYSTDSAmount")
        val rysTDSAmount: String,

        @field:SerializedName("TWServiceCharge")
        val serviceCharge: String,

        @field:SerializedName("BaseAmount")
        val baseAmount: String,

        @field:SerializedName("TotalDays")
        val totalDays: String,

        @field:SerializedName("Totalmonths")
        val totalMonths: String,

        @field:SerializedName("Coupon Discount")
        val couponDiscount: String,

        @field:SerializedName("PayableToRYSAmount")
        val payableToRYSAmount: String
)