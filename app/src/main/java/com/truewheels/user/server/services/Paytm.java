package com.truewheels.user.server.services;


import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.paytm.Checksum;
import com.truewheels.user.server.model.paytm.PaytmResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Paytm {

    //    @FormUrlEncoded
    @POST("api/GenerateCheckSum")
    Call<BaseResponse<Checksum>> getChecksum(
            @Query("orderid") String orderId,
            @Query("cust_id") String custId,
            @Query("txn_amount") String txnAmount
    );

    @POST("api/VerifyCheckSum")
    Call<BaseResponse<Checksum>> verifyChecksum(
            @Query("orderid") String orderId,
            @Query("cust_id") String custId,
            @Query("paytmChecksum") String paytmChecksum,
            @Query("txn_amount") String txnAmount
    );

    @FormUrlEncoded
    @POST("api/TransactionResponse")
    Call<BaseResponse<PaytmResponse>> transactionResponse(
            @Field("BANKNAME") String BANKNAME,
            @Field("BANKTXNID") String BANKTXNID,
            @Field("Booking_Id") String Booking_Id,
            @Field("GATEWAYNAME") String GATEWAYNAME,
            @Field("MID") String MID,
            @Field("ORDERID") String ORDERID,
            @Field("PAYMENTMODE") String PAYMENTMODE,
            @Field("Parking_Id") String Parking_Id,
            @Field("PROMO_CAMP_ID") String PROMO_CAMP_ID,
            @Field("PROMO_STATUS") String PROMO_STATUS,
            @Field("RESPMSG") String RESPMSG,
            @Field("PROMO_RESPCODE") String PROMO_RESPCODE,
            @Field("STATUS") String STATUS,
            @Field("TXNAMOUNT") String TXNAMOUNT,
            @Field("TXNDATE") String TXNDATE,
            @Field("TXNID") String TXNID,
            @Field("TXNTYPE") String TXNTYPE,
            @Field("Customer_Id") String Customer_Id
    );

    @FormUrlEncoded
    @POST("api/Refund")
    Call<BaseResponse<PaytmResponse>> initiateRefund(
            @Field("ORDERID") String ORDERID,
            @Field("CUSTID") String CUSTID,
            @Field("TXNID") String TXNID,
            @Field("REFUNDAMOUNT") String REFUNDAMOUNT,
            @Field("TXNTYPE") String TXNTYPE,
            @Field("REFID") String REFID,
            @Field("CHECKSUM") String CHECKSUM
    );
}
