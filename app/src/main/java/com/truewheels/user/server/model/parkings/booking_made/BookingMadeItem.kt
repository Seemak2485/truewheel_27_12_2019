package com.truewheels.user.server.model.parkings.booking_made

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BookingMadeItem(

        @field:SerializedName("parking_address")
        val parkingAddress: String? = null,

        @field:SerializedName("TWServiceCharge")
        val serviceCharge: String? = null,

        @field:SerializedName("Total_Days")
        val totalDays: String? = null,


        @field:SerializedName("Parking_Space_Name")
        val parkingSpaceName: String? = null,

        @field:SerializedName("VehicleType")
        val vehicleType: String? = null,

        @field:SerializedName("Base_Amount")
        val baseAmount: String? = null,

        @field:SerializedName("Total_month")
        val totalMonth: String? = null,

        @field:SerializedName("VehicalNumber")
        val vehicalNumber: String? = null,

        @field:SerializedName("Total_Hour")
        val totalHour: String? = null,

        @field:SerializedName("BookedOutTime")
        val bookedOutTime: String = "",

        @field:SerializedName("Success")
        val success: Boolean? = null,

        @field:SerializedName("Full_Name")
        val fullName: String? = null,

        @field:SerializedName("MonthlySubscription")
        val monthlySubscription: String? = null,

        @field:SerializedName("BookingDateTime")
        val bookingDateTime: String = "",

        @field:SerializedName("Total_Min")
        val totalMin: String? = null,

        @field:SerializedName("BookingStatus")
        val bookingStatus: String? = null,

        @field:SerializedName("Booked_Id")
        val bookedId: Int? = null,

        @field:SerializedName("BookedIntime")
        val bookedIntime: String = "",

        @field:SerializedName("CheckedOutDateTime")
        val checkedOutDateTime: String = "",

        @field:SerializedName("lattitude")
        val lattitude: String = "",

        @field:SerializedName("longitude")
        val longitude: String = "",

        @field:SerializedName("barcode")
        val barCode: String = "",

        @field:SerializedName("PAYMENTMODE")
        val paymentMode: String? = null,

        @field:SerializedName("ParkingType")
        val parkingType: String = "",

        @field:SerializedName("TXNAMOUNT")
        val txnAmount: String = "",
        @field:SerializedName("TotalAmount")
        val totalAmount: String = "",
        @field:SerializedName("RESPCODE")
        val RESPCODE: String = "",

        @field:SerializedName("RESPMSG")
        val RESPMSG: String = "",

        @field:SerializedName("TXNDATE")
        val TXNDATE: String = "",
        @field:SerializedName("CHECKSUMHASH")
        val CHECKSUMHASH: String = "",

        @field:SerializedName("TXNID")
        val TXNID: String = "",
        @field:SerializedName("TXNTYPE")
        val TXNTYPE: String = "",

        @field:SerializedName("Payment_ID")
        var Payment_ID: String = "",

        @field:SerializedName("STATUS")
        val STATUS: String = "",

        @field:SerializedName("ORDERID")
        val orderId: String = "",

        @field:SerializedName("CheckOut")
        val checkOutPublic: String = "",

        @field:SerializedName("CheckIn")
        val checkInPublic: String = "",

        @field:SerializedName("CouponDiscount")
        val couponDiscount: String = "",

        @field:SerializedName("AlreadyCheckOutBy")
        val checkedOutBy: String? = null,

        @field:SerializedName("WhoDidCheckout")
        val whoDidCheckout: String? = null,

        @field:SerializedName("Phone_No1")
        val phoneNo1: Long? = null
) : Serializable