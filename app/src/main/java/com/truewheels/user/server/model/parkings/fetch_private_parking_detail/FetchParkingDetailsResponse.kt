package com.truewheels.user.server.model.parkings.fetch_private_parking_detail

import com.google.gson.annotations.SerializedName

data class FetchParkingDetailsResponse(

        @field:SerializedName("ParkingDetail")
        val parkingDetail: List<PrivateParkingDetailItem?>
)