package com.truewheels.user.server.model.parkings.booking_received

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BookingRecievedItem(
        @field:SerializedName("parking_address")
        val parkingAddress: String? = null,

        @field:SerializedName("Total_Days")
        val totalDays: String? = null,

        @field:SerializedName("Parking_Space_Name")
        val parkingSpaceName: String? = null,

        @field:SerializedName("VehicleType")
        val vehicleType: String? = null,

        @field:SerializedName("Base_Amount")
        val baseAmount: String? = null,

        @field:SerializedName("Total_month")
        val totalMonth: String? = null,

        @field:SerializedName("VehicalNumber")
        val vehicalNumber: String? = null,

        @field:SerializedName("Total_Hour")
        val totalHour: String? = null,

        @field:SerializedName("BookedOutTime")
        val bookedOutTime: String = "",

        @field:SerializedName("Success")
        val success: Boolean? = null,

        @field:SerializedName("Full_Name")
        val fullName: String? = null,

        @field:SerializedName("MonthlySubscription")
        val monthlySubscription: String? = null,

        @field:SerializedName("BookingDateTime")
        val bookingDateTime: String = "",

        @field:SerializedName("Total_Min")
        val totalMin: String? = null,

        @field:SerializedName("BookingStatus")
        val bookingStatus: String? = null,

        @field:SerializedName("Booked_Id")
        val bookedId: Int? = null,

        @field:SerializedName("CheckedOutDateTime")
        val checkedOutDateTime: String = "",

        @field:SerializedName("BookedIntime")
        val bookedIntime: String = "",

        @field:SerializedName("AverageOwnerRating")
        val averageRating: String = "",


        @field:SerializedName("PayableToRYSAmount")
        val payableToRYSAmount: String = "",

        @field:SerializedName("PAYMENTMODE")
        val paymentMode: String = "",

        @field:SerializedName("AlreadyCheckOutBy")
        val checkedOutby: String? = null,

        @field:SerializedName("TWServiceCharge")
        val serviceCharge: String? = null,

        @field:SerializedName("TotalAmount")
        val totalAmount: String? = null,

        @field:SerializedName("CouponDiscount")
        val couponDiscount: String? = null,

        @field:SerializedName("Phone_No1")
        val phoneNo1: Long? = null
) : Serializable