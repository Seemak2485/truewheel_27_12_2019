package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.FRIDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.MONDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.ONLY_WEEKDAYS;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.ONLY_WEEKENDS;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.SATURDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.SUNDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.THURSDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.TUESDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.WEDNESDAY;
import static com.truewheels.user.server.model.sys.select.AvailableDayType.AvailableDayTypeEnum.WHOLE_WEEK;


public class AvailableDayType {
    public static enum AvailableDayTypeEnum {

        ONLY_WEEKDAYS("D_OWD"),
        ONLY_WEEKENDS("D_OWE"),
        WHOLE_WEEK("D_WW"),
        MONDAY("D_MO"),
        TUESDAY("D_TU"),
        WEDNESDAY("D_WE"),
        THURSDAY("D_TH"),
        FRIDAY("D_FR"),
        SATURDAY("D_SA"),
        SUNDAY("D_SU");

        private final String text;

        AvailableDayTypeEnum(final String text) {
            this.text = text;
        }

        public String val() {
            return text;
        }
    }

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();
    private static Map<String, Integer> mapValue = new HashMap<>();

    static {
        map.put(ONLY_WEEKDAYS.val(), "Only Weekdays");
        map.put(ONLY_WEEKENDS.val(), "Only Weekends");
        map.put(WHOLE_WEEK.val(), "Whole week");
        map.put(MONDAY.val(), "Monday");
        map.put(TUESDAY.val(), "Tuesday");
        map.put(WEDNESDAY.val(), "Wednesday");
        map.put(THURSDAY.val(), "Thursday");
        map.put(FRIDAY.val(), "Friday");
        map.put(SATURDAY.val(), "Saturday");
        map.put(SUNDAY.val(), "Sunday");

        mapValue.put(MONDAY.val(), 2);
        mapValue.put(TUESDAY.val(), 3);
        mapValue.put(WEDNESDAY.val(), 5);
        mapValue.put(THURSDAY.val(), 7);
        mapValue.put(FRIDAY.val(), 11);
        mapValue.put(SATURDAY.val(), 13);
        mapValue.put(SUNDAY.val(), 17);
        mapValue.put(ONLY_WEEKDAYS.val(), 2 * 3 * 5 * 7 * 11);
        mapValue.put(ONLY_WEEKENDS.val(), 13 * 17);
        mapValue.put(WHOLE_WEEK.val(), 2 * 3 * 5 * 7 * 11 * 13 * 17);

        for (Map.Entry<String, String> entry : map.entrySet())
            swapped.put(entry.getValue(), entry.getKey());
    }

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        if (map.size() != 0)
            return;

        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }

    public static Map<String, Integer> getMapValue() {
        return mapValue;
    }
}