package com.truewheels.user.server

import com.google.gson.annotations.SerializedName

data class ParkingsItemRWA(


        @field:SerializedName("ParkingName")
        val parkingName: String? = null,

        @field:SerializedName("ParkingId")
        val parkingId: Int? = null
)