package com.truewheels.user.server.model.profile

import com.google.gson.annotations.SerializedName

data class ListVehicleItem(

        @field:SerializedName("Vehicle_ID")
        val VehicleID: Int,

        @field:SerializedName("Vehicle_No")
        val vehicleNo: String? = null,

        @field:SerializedName("Vehicle_Type")
        val vehicleType: String? = null,

        @field:SerializedName("Vehicle_Model")
        val vehicleModel: String? = null
)