package com.truewheels.user.server.model.coupons;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Saurabh Kataria on 23-04-2018.
 */

public class CouponsModel {
    @SerializedName("Id")
    private String id;

    @SerializedName("CouponNo")
    private String CouponNo;

    @SerializedName("CouponDiscription")
    private String CouponDiscription;

    @SerializedName("CouponStartDate")
    private String CouponStartDate;

    @SerializedName("CouponEndDate")
    private String CouponEndDate;

    @SerializedName("CategoryValue")
    private String CategoryValue;

    @SerializedName("Category")
    private String Category;

    @SerializedName("DateCreated")
    private String DateCreated;

    @SerializedName("CreatedBy")
    private String CreatedBy;

    @SerializedName("IsActive")
    private boolean isActive;

    @SerializedName("Status")
    private boolean status;

    @SerializedName("Error")
    private String error;

    public String getId() {
        return id;
    }

    public String getCouponNo() {
        return CouponNo;
    }

    public String getCouponDiscription() {
        return CouponDiscription;
    }

    public String getCouponStartDate() {
        return CouponStartDate;
    }

    public String getCouponEndDate() {
        return CouponEndDate;
    }

    public String getCategoryValue() {
        return CategoryValue;
    }

    public String getCategory() {
        return Category;
    }

    public String getDateCreated() {
        return DateCreated;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public boolean isActive() {
        return isActive;
    }

    public boolean isStatus() {
        return status;
    }

    public String getError() {
        return error;
    }
}
