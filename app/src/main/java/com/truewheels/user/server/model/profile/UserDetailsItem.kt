package com.truewheels.user.server.model.profile

import com.google.gson.annotations.SerializedName

data class UserDetailsItem(

        @field:SerializedName("Full_Name")
        val fullName: String? = null,

        @field:SerializedName("Driving_License")
        val drivingLicense: String? = null,

        @field:SerializedName("Phone_No2")
        val phoneNo2: String? = null,

        @field:SerializedName("DOB")
        val dOB: String? = null,

        @field:SerializedName("Email_id")
        val emailId: String? = null,

        @field:SerializedName("PAN")
        val pAN: String? = null,

        @field:SerializedName("Phone_No1")
        val phoneNo1: String? = null
) : UserVehicles()