package com.truewheels.user.server.services;

import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.app_version.LatestAppVersionResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AppService {
    @GET("api/AppVersion")
    Call<BaseResponse<LatestAppVersionResponse>> fetchLatestAppVersion();
}