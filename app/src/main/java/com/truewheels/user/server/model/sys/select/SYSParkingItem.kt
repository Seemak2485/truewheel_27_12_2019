package com.truewheels.user.server.model.sys.select

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SYSParkingItem(

        @field:SerializedName("Open24Hour")
        val open24Hour: String? = null,

        @field:SerializedName("parking_address")
        val parkingAddress: String? = null,

        @field:SerializedName("PopularParking")
        val popularParking: String? = null,

        @field:SerializedName("city")
        val city: String? = null,

        @field:SerializedName("owner_id")
        val ownerId: String? = null,

        @field:SerializedName("Parking_Owner_Comment")
        val ownerComments: String? = null,

        @field:SerializedName("BikeBasicCharge")
        val bikeBasicCharge: String? = null,

        @field:SerializedName("carmonthlycharge")
        val carmonthlycharge: String? = null,

        @field:SerializedName("Success")
        val success: Boolean? = null,

        @field:SerializedName("Parking_id")
        val parkingId: Int? = null,

        @field:SerializedName("No_Of_Space_Avaiable")
        val noOfSpaceAvaiable: String? = null,

        @field:SerializedName("Space_Type")
        val spaceType: String? = null,

        @field:SerializedName("street")
        val street: String? = null,

        @field:SerializedName("Property_Type")
        val propertyType: String? = null,

        @field:SerializedName("state")
        val state: String? = null,

        @field:SerializedName("CarBasicCharge")
        val carBasicCharge: String? = null,

        @field:SerializedName("DaysSelected")
        val daysSelected: String? = null,

        @field:SerializedName("longitude")
        val longitude: String? = null,

        @field:SerializedName("Parking_Space_Name")
        val parkSpaceName: String? = null,

        @field:SerializedName("Status")
        val status: String? = null,

        @field:SerializedName("DescriptionType")
        val descriptionType: String? = null,

        @field:SerializedName("DescriptionSelected")
        val descriptionSelected: String? = null,

        /*@field:SerializedName("DateTimeTo")
        val dateTimeTo: String? = null,

        @field:SerializedName("DateTimeFrom")
        val dateTimeFrom: String? = null,*/

        @field:SerializedName("lattitude")
        val lattitude: String? = null,

        @field:SerializedName("CarDailyCharge")
        val carDailyCharge: String? = null,

        @field:SerializedName("bikedailycharge")
        val bikedailycharge: String? = null,

        /* @field:SerializedName("carweeklycharge")
         val carweeklycharge: String? = null,

         @field:SerializedName("bikeweeklycharge")
         val bikeweeklycharge: String? = null,
 */

        @field:SerializedName("Pin_Code")
        val propertyPinCode: String? = null,

        @field:SerializedName("ParkingClass")
        val parkingClass: String? = null,

        @field:SerializedName("Space_Availablility")
        val spaceAvailablility: String? = null,

        @field:SerializedName("Landmark")
        val propertyLandMark: String? = null,

        @field:SerializedName("ParkingRating")
        val parkingRating: String? = null,

        @field:SerializedName("Active")
        var active: String,

        @field:SerializedName("PropertyAddress")
        val propertyAddress: String? = null,

        @field:SerializedName("bikemonthycharge")
        val bikemonthycharge: String? = null,

        @field:SerializedName("FacilitySelected")
        val facilitySelected: String? = null,

        @field:SerializedName("ErrorMessage")
        val errorMessage: String? = null,

        @field:SerializedName("Detail_ID")
        val detailID: String? = null,

        @field:SerializedName("FacilityType")
        val facilityType: String? = null,

        @field:SerializedName("No_Of_Space_Avaiable_bike")
        val noOfSpaceAvaiableBike: String? = null,

        @field:SerializedName("TimeFrom")
        val timeFrom: String? = null,

        @field:SerializedName("TimeTo")
        val timeTo: String? = null,

        @field:SerializedName("open24hours")
        val open24hours: String? = null,

        @field:SerializedName("Parking_Available_Date")
        val parkingAvailableDate: String = "",

        @field:SerializedName("Parking_support_no")
        val parkingSupportNo: String = "",

        @field:SerializedName("Hourly_Pricing")
        val hourlyPricing: String? = null

) : Serializable