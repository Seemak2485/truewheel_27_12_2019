package com.truewheels.user.server.model.app_version

import com.google.gson.annotations.SerializedName
import com.truewheels.user.server.model.GenericResponse

data class LatestAppVersionResponse(

        @field:SerializedName("CurrentLatestVersion")
        val currentLatestVersion: String? = null,

        @field:SerializedName("ShouldForceUpdate")
        val shouldForceUpdate: Boolean = false
) : GenericResponse()