package com.truewheels.user.server.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

open class GenericResponse(
        @field:SerializedName("Message")
        val message: String? = null,

        @field:SerializedName("Success")
        val success: Boolean? = null
) : Serializable
