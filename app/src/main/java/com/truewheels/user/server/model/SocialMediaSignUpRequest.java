package com.truewheels.user.server.model;

public class SocialMediaSignUpRequest {
    String firstName;
    String lastName;
    String emailId;
    String mobile;
    String fbId;
    String picUrl;
    /**
     * GG for google
     * FB for facebook
     */
    String signupModeID;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getSignupModeID() {
        return signupModeID;
    }

    public void setSignupModeID(String signupModeID) {
        this.signupModeID = signupModeID;
    }
}
