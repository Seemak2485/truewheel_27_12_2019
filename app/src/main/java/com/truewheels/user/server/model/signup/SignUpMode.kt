package com.truewheels.user.server.model.signup

enum class SignUpMode(val value: String) {
    FACEBOOK("FB"),
    GOOGLE("GG")
}