package com.truewheels.user.server.model.sys.select

import com.google.gson.annotations.SerializedName

data class FetchSYSParkingResponse(

        @field:SerializedName("Parkings")
        val parkings: List<SYSParkingItem>? = null
)