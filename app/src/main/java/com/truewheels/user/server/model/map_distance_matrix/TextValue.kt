package com.truewheels.user.server.model.map_distance_matrix

import com.google.gson.annotations.SerializedName

data class TextValue(

        @field:SerializedName("text")
        val text: String? = null,

        @field:SerializedName("value")
        val value: Int? = null
)