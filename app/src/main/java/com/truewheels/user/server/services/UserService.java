package com.truewheels.user.server.services;

import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.ContactUsRequest;
import com.truewheels.user.server.model.EarningRespone;
import com.truewheels.user.server.model.IsRegisterResponse;
import com.truewheels.user.server.model.SignUpResponse;
import com.truewheels.user.server.model.add_delete_vehicle.AddDeleteVehicleResponse;
import com.truewheels.user.server.model.coupons.CouponResponse;
import com.truewheels.user.server.model.parkings.booking_made.FetchBookingMadeResponse;
import com.truewheels.user.server.model.parkings.booking_received.FetchBookingReceivedResponse;
import com.truewheels.user.server.model.profile.FetchVehiclesResponse;
import com.truewheels.user.server.model.profile.ProfileResponse;
import com.truewheels.user.server.model.signup.SignupResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserService {
    @GET("api/IsRegistered")
    Call<BaseResponse<IsRegisterResponse>> isRegistered(@Query("Phone_no") String phone);

    @POST("api/updateprofileinApp")
    @FormUrlEncoded
    Call<BaseResponse<SignUpResponse>> updateProfile(@Field("userId") String userId,
                                                     @Field("Full_Name") String Full_Name,
                                                     @Field("emailID") String emailID,
                                                     @Field("dob") String dob,
                                                     @Field("alternateNo") String alternateNo,
                                                     @Field("drivingLicense") String drivingLicense,
                                                     @Field("panNo") String panNo,
                                                     @Field("Phone_No1") String Phone_No1,
                                                     @Field("Vehicle_No1") String Vehicle_No1,
                                                     @Field("Vehicle_No2") String Vehicle_No2,
                                                     @Field("Vehicle_No3") String Vehicle_No3,
                                                     @Field("Vehicle_No4") String Vehicle_No4,
                                                     @Field("Vehicle_No5") String Vehicle_No5,
                                                     @Field("Vehicle_Model1") String Vehicle_Model1,
                                                     @Field("Vehicle_Model2") String Vehicle_Model2,
                                                     @Field("Vehicle_Model3") String Vehicle_Model3,
                                                     @Field("Vehicle_Model4") String Vehicle_Model4,
                                                     @Field("Vehicle_Model5") String Vehicle_Model5,
                                                     @Field("Vehicle_Type1") String Vehicle_Type1,
                                                     @Field("Vehicle_Type2") String Vehicle_Type2,
                                                     @Field("Vehicle_Type3") String Vehicle_Type3,
                                                     @Field("Vehicle_Type4") String Vehicle_Type4,
                                                     @Field("Vehicle_Type5") String Vehicle_Type5);

    @POST("api/OnProfileClick")
    Call<BaseResponse<ProfileResponse>> fetchProfile(@Query("User_id") String userId);

    @POST("api/VehicleInfo")
    Call<BaseResponse<FetchVehiclesResponse>> fetchVehicles(@Query("User_id") String userId);

    @POST("api/GetBookingRecievedRYSByOwnerID")
    Call<BaseResponse<FetchBookingReceivedResponse>> fetchBookingReceived(@Query("ownerId") String ownerId);

    @POST("api/GetBookingMadeRYSByOwnerID")
    Call<BaseResponse<FetchBookingMadeResponse>> fetchBookingMade(@Query("ownerId") String ownerId);

    @POST("api/SignUpApp")
    @FormUrlEncoded
    Call<BaseResponse<SignupResponse>> signUp(@Field("Full_Name") String Full_Name,
                                              @Field("Phone_No1") String Phone_No1,
                                              @Field("Email_Id") String Email_Id,
                                              @Field("Alternate_No") String Alternate_No,
                                              @Field("ismobileverified") String isMobileVerified);

    @POST("api/SignupAPIModel4FBNGG")
    @FormUrlEncoded
    Call<BaseResponse<SignUpResponse>> socialMediaSignUp(@Field("firstName") String firstName,
                                                         @Field("lastName") String lastName,
                                                         @Field("emailId") String emailId,
                                                         @Field("mobile") String mobile,
                                                         @Field("fbId") String fbId,
                                                         @Field("picUrl") String picUrl,
                                                         @Field("Other_id") String otherId,
                                                         @Field("signUpModeID") String signUpModeID);

    @POST("api/UsersVehicleDetails")
    @FormUrlEncoded
    Call<BaseResponse<AddDeleteVehicleResponse>> addDeleteVehicle(@Field("Id") String vehicleID,
                                                                  @Field("User_Id") String userId,
                                                                  @Field("flag") String flag, /*1 for Add, 0 for Delete*/
                                                                  @Field("Vehicle_No1") String vehicleNo,
                                                                  @Field("Vehicle_Model1") String vehicleModel,
                                                                  @Field("Vehicle_Type1") String vehicleType);

    @POST("api/contactUs")
    Call<BaseResponse> contactUs(@Body ContactUsRequest request);

    @POST("api/InserUpdateUserRatingCommentApp")
    @FormUrlEncoded
    Call<BaseResponse<SignUpResponse>> ratePublicParking(@Field("ParkingId") String parkingId,
                                                         @Field("UserId") String userId,
                                                         @Field("Booking_Id") String bookingId,
                                                         @Field("Comments") String comments,
                                                         @Field("Rating") String rating,
                                                         @Field("Feedback") String feedback,
                                                         @Field("isRated") boolean isRated);

    @POST("api/InserUpdateOwnerRatingCommentAgainstUserApp")
    Call<BaseResponse<SignUpResponse>> rateParking(@Query("Id") int id,
                                                   @Query("parkingId") int parkingId,
                                                   @Query("userId") int userId,
                                                   @Query("userRating") int rating,
                                                   @Query("userComment") String comments,
                                                   @Query("ownerId") int ownerId,
                                                   @Query("ownerRating") int ownerRating,
                                                   @Query("ownerCommentd") String ownerCommentd,
                                                   @Query("Booking_Id") String bookingId)
            ;

    //    @POST("api/CouponGeneratedAutomated")
    @POST("api/GetUserAvailableCoupon")
    Call<BaseResponse<CouponResponse>> getCouponsForUser(@Query("userId") String userId,
                                                         @Query("parkingId") String parkingId);

    @FormUrlEncoded
    @POST("api/CouponApplied")
    Call<BaseResponse<CouponResponse>> couponApplied(@Field("CouponNo") String CouponNo,
                                                     @Field("Owner_Id") String Owner_Id,
                                                     @Field("Parking_Id") String Parking_Id,
                                                     @Field("UserId") String userId,
                                                     @Field("Booking_Id") String Booking_Id,
                                                     @Field("Id") String Id);

    @POST("api/MYEarning")
    Call<BaseResponse<EarningRespone>> getMyEarnings(@Query("user_id") String userId);
}