package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.truewheels.user.server.model.sys.select.FacilityType.FacilityTypeEnum.DENT_FIX;
import static com.truewheels.user.server.model.sys.select.FacilityType.FacilityTypeEnum.DRY_WASH;
import static com.truewheels.user.server.model.sys.select.FacilityType.FacilityTypeEnum.INSTALLATION;
import static com.truewheels.user.server.model.sys.select.FacilityType.FacilityTypeEnum.PUNCTURE_AIR;
import static com.truewheels.user.server.model.sys.select.FacilityType.FacilityTypeEnum.SERVICING;
import static com.truewheels.user.server.model.sys.select.FacilityType.FacilityTypeEnum.WATER_WASH;


public class FacilityType {
    public static enum FacilityTypeEnum {

        DENT_FIX("FT_DF"),
        DRY_WASH("FT_DW"),
        INSTALLATION("FT_Ins"),
        PUNCTURE_AIR("FT_PA"),
        SERVICING("FT_Serv"),
        WATER_WASH("FT_WW");

        private final String text;

        FacilityTypeEnum(final String text) {
            this.text = text;
        }

        public String val() {
            return text;
        }
    }

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

    /*static {
        map.put(DENT_FIX.val(), "Dent Fix");
        map.put(DRY_WASH.val(), "Dry wash");
        map.put(INSTALLATION.val(), "Installation");
        map.put(PUNCTURE_AIR.val(), "Puncture Air");
        map.put(SERVICING.val(), "Servicing");
        map.put(WATER_WASH.val(), "Water wash");

        for (Map.Entry<String, String> entry : map.entrySet())
            swapped.put(entry.getValue(), entry.getKey());
    }*/

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}