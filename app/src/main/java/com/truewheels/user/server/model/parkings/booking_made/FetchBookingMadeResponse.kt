package com.truewheels.user.server.model.parkings.booking_made

import com.google.gson.annotations.SerializedName

data class FetchBookingMadeResponse(

        @field:SerializedName("Booking Made")
        val bookingMade: List<BookingMadeItem>? = null
)