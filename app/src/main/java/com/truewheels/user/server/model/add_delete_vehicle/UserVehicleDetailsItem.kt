package com.truewheels.user.server.model.add_delete_vehicle

import com.google.gson.annotations.SerializedName

data class UserVehicleDetailsItem(

        @field:SerializedName("Id")
        val vehicleId: String,

        @field:SerializedName("Vehicle_No1")
        val vehicleNo1: String? = null,

        @field:SerializedName("Vehicle_Type1")
        val vehicleType1: String? = null,

        @field:SerializedName("Vehicle_Model1")
        val vehicleModel1: String? = null
)