package com.truewheels.user.server.model.profile

import com.google.gson.annotations.SerializedName

data class FetchVehiclesResponse(

        @field:SerializedName("UserDetails")
        val userDetails: List<UserVehicles>
)