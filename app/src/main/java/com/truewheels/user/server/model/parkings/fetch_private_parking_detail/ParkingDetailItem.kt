package com.truewheels.user.server.model.parkings.fetch_private_parking_detail

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PrivateParkingDetailItem(

        @field:SerializedName("parking_address")
        val parkingAddress: String? = null,

        @field:SerializedName("Parking_Owner_Comment")
        val parkingOwnerComment: String = "",

        @field:SerializedName("DescriptionSelected")
        val descriptionSelected: String? = null,

        @field:SerializedName("Parking_Space_Name")
        val parkingSpaceName: String = "",

        @field:SerializedName("CarDailyCharge")
        var carDailyCharge: String? = null,

        @field:SerializedName("BikeBasicCharge")
        var bikeBasicCharge: String? = null,

        @field:SerializedName("carmonthlycharge")
        var carmonthlycharge: String? = null,

        @field:SerializedName("Parking_id")
        val parkingId: Int? = null,

        @field:SerializedName("No_Of_Space_Avaiable")
        val noOfSpaceAvaiable: String = "0",

        @field:SerializedName("bikemonthycharge")
        var bikemonthycharge: String? = null,

        @field:SerializedName("TimeFrom")
        val timeFrom: String = "",

        @field:SerializedName("TimeTo")
        val timeTo: String = "",

        @field:SerializedName("FacilitySelected")
        val facilitySelected: String? = null,

        @field:SerializedName("CarBasicCharge")
        var carBasicCharge: String? = null,

        @field:SerializedName("DaysSelected")
        val daysSelected: String? = null,

        @field:SerializedName("bikedailycharge")
        var bikedailycharge: String? = null,

        @field:SerializedName("VerifiedStatus")
        var verified: String? = null,

        @field:SerializedName("DaysProduct")
        var daysProduct: Int = -1,

        @field:SerializedName("AverageRating")
        val averageRating: String? = null,

        @field:SerializedName("PropertyVerifiedStatus")
        val propertyVerifiedStatus: String? = null,

        @field:SerializedName("No_Of_Space_Avaiable_bike")

        val noOfSpaceAvaiableBike: String = "0"
) : Serializable