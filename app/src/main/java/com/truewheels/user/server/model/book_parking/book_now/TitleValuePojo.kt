package com.truewheels.user.server.model.book_parking.book_now

data class TitleValuePojo(
        var title: String = "",
        var value: String = ""
)