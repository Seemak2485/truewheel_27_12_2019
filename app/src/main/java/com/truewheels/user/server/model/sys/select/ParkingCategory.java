package com.truewheels.user.server.model.sys.select;

import com.truewheels.user.server.model.sys.systemcode.CategoryPropertyItem;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.truewheels.user.server.model.sys.select.ParkingCategory.ParkingCategoryEnum.ALL;
import static com.truewheels.user.server.model.sys.select.ParkingCategory.ParkingCategoryEnum.GOLD;
import static com.truewheels.user.server.model.sys.select.ParkingCategory.ParkingCategoryEnum.PLAIN_PARKING;
import static com.truewheels.user.server.model.sys.select.ParkingCategory.ParkingCategoryEnum.PLATINUM;
import static com.truewheels.user.server.model.sys.select.ParkingCategory.ParkingCategoryEnum.SILVER;


public class ParkingCategory {
    public static enum ParkingCategoryEnum {

        ALL("PC_A"),
        GOLD("PC_G"),
        PLATINUM("PC_P"),
        PLAIN_PARKING("PC_PP"),
        SILVER("PC_S");

        private final String text;

        ParkingCategoryEnum(final String text) {
            this.text = text;
        }

        public String val() {
            return text;
        }
    }

    private static Map<String, String> map = new LinkedHashMap<>();
    private static Map<String, String> swapped = new LinkedHashMap<>();

    /*static {
        map.put(ALL.val(), "All");
        map.put(GOLD.val(), "Gold");
        map.put(PLATINUM.val(), "Platinum");
        map.put(PLAIN_PARKING.val(), "Plain parking");
        map.put(SILVER.val(), "Silver");

        for (Map.Entry<String, String> entry : map.entrySet())
            swapped.put(entry.getValue(), entry.getKey());
    }*/

    public static void setMap(List<CategoryPropertyItem> categoryPropertyItemList) {
        map.clear();
        swapped.clear();

        for (CategoryPropertyItem categoryPropertyItem : categoryPropertyItemList) {
            map.put(categoryPropertyItem.getPropertyCode(), categoryPropertyItem.getPropertyDescription());
            swapped.put(categoryPropertyItem.getPropertyDescription(), categoryPropertyItem.getPropertyCode());
        }
    }

    public static Map<String, String> getMap() {
        return map;
    }

    public static Map<String, String> getSwappedMap() {
        return swapped;
    }
}