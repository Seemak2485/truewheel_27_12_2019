package com.truewheels.user.server.model.add_delete_vehicle

import com.google.gson.annotations.SerializedName

data class AddDeleteVehicleResponse(

        @field:SerializedName("UserVehicleDetails")
        val userVehicleDetails: List<UserVehicleDetailsItem>? = null
)