package com.truewheels.user.views;


import android.content.Context;
import androidx.appcompat.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * As the anchor of a ImageView is the center of the image,
 * if we use a image like a pin, the marker will apparently move
 * because the bottom center of the image must be in the center of the map.
 * To avoid this, we can move the image creating an anchored image view.
 */

public class AnchoredImageView extends AppCompatImageView {
    public AnchoredImageView(Context context) {
        super(context);
    }

    public AnchoredImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AnchoredImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        setTranslationY(-h / 2);
    }
}