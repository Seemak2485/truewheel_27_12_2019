package com.truewheels.user.constants;

import com.google.android.gms.maps.model.LatLng;

public class Constants {
    public static final boolean IS_PROD = false;
    public static final String EXTRA_ADDRESS_RESULT_RECEIVER = "EXTRA_ADDRESS_RESULT_RECEIVER";
    public static final String LOCATION_DATA_EXTRA = "LOCATION_DATA_EXTRA";
    public static final int ADDRESS_FETCH_SUCCESS_RESULT = 521;
    public static final int ADDRESS_FETCH_FAILURE_RESULT = 594;
    public static final String EXTRA_ADDRESS_OBJECT_KEY = "EXTRA_ADDRESS_OBJECT_KEY";
    public static final LatLng CENTER_NCR_LATLNG = new LatLng(28.661038, 77.091361);
   public static final String BASE_URL = "http://www.truewheel.in";
  //public static final String BASE_URL = "http://www.truewheelsuat.in";
   //public static final String BASE_URL = "https://truewheels.azurewebsites.net";
    /**
     * radius distance in meter.
     */
    public static final long DISTANCE = 80 * 1000;

    public static final String MAP_DISTANCE_MATRIX_URL = "https://maps.googleapis.com/maps/api/distancematrix/json";
    public static final String MAP_DISTANCE_MATRIX_KEY = "AIzaSyAL5QEt8YGTjKAPLD5v1evSCyqDYIuNebg";

    public static final String CHECKSUM_GENERATION_URL = "http://www.truewheelsuat.in/api/GenerateCheckSum";
    public static final String CHECKSUM_VERIFICATION_URL = "http://www.truewheelsuat.in/api/VerifyCheckSum";


    // Staging Details
    public static final String WEBSITE = "APPPROD";
    public static final String CHANNEL_ID = "WAP";
    //public static final String PAYTMURL="https://securegw.paytm.in/theia/processTransaction?orderid=";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";
   // public static final String CALLBACK_URL = "https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=";
    public static final String INDUSTRY_TYPE_ID = "Retail109";
    public static final String MID = "GANEIS76857833356724";
    //public static final String MID = "GANEIS18597724354172";

    //     Production Details
    /*public static final String WEBSITE = "APPPROD";
    public static final String CHANNEL_ID = "WAP";
    public static final String CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=";
    public static final String INDUSTRY_TYPE_ID = "Retail109";
    public static final String MID = "GANEIS76857833356724";*/
}
