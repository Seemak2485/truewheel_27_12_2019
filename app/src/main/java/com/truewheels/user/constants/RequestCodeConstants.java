package com.truewheels.user.constants;

public class RequestCodeConstants {
    public static final int REQUEST_CODE_PLACE_AUTOCOMPLETE = 102;
    public static final int REQUEST_CODE_CHECK_GPS_SETTINGS = 103;
    public static final int REQUEST_CODE_EDIT_SYS = 104;
    public static final int REQUEST_CODE_FINISH_ON_COMPLETETION = 105;

    public static final int REQUEST_CODE_PERMISSION_FINE_LOCATION = 205;

    public static final int REQUEST_CODE_GOOGLE_LOGIN = 505;
    public static final int REQUEST_CODE_OTP = 547;

}
