package com.truewheels.user;

import androidx.multidex.MultiDexApplication;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class TrueWheelApp extends MultiDexApplication {
    private static TrueWheelApp instance;

    public static TrueWheelApp getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        try {
            Fabric.with(this, new Crashlytics());
        } catch (Error e) {
            e.printStackTrace();
        }
    }
}
