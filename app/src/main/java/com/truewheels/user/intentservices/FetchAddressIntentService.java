package com.truewheels.user.intentservices;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.truewheels.user.constants.Constants;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class FetchAddressIntentService extends IntentService {

    private static final String TAG = FetchAddressIntentService.class.getName();
    protected ResultReceiver mReceiver;
    private Geocoder geocoder;

    public FetchAddressIntentService() {
        super("FetchAddressIntentService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        geocoder = new Geocoder(this, Locale.getDefault());
        Log.i(TAG, "Starting service to Fetch address");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String errorMessage = "";

        // Get the location passed to this service through an extra.
        LatLng location = intent.getParcelableExtra(Constants.LOCATION_DATA_EXTRA);
        Log.i(TAG, "==location== new address requested for latlng : " + location);
        mReceiver = intent.getParcelableExtra(Constants.EXTRA_ADDRESS_RESULT_RECEIVER);

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    1);

        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            Log.w(TAG, "Address from API");
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            Log.v(TAG, errorMessage + ". " +
                    "Latitude = " + location.latitude +
                    ", Longitude = " +
                    location.longitude, illegalArgumentException);
        } catch (NullPointerException e) {
            e.printStackTrace();
            return;
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            deliverResultToReceiver(Constants.ADDRESS_FETCH_FAILURE_RESULT, null, location);
        } else {
            Address address = addresses.get(0);

            address.setLatitude(location.latitude);
            address.setLongitude(location.longitude);
            deliverResultToReceiver(Constants.ADDRESS_FETCH_SUCCESS_RESULT, address, location);
        }
    }

    private void deliverResultToReceiver(int resultCode, Address address, LatLng location) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.EXTRA_ADDRESS_OBJECT_KEY, address);
        bundle.putParcelable(Constants.LOCATION_DATA_EXTRA, location);
        if (mReceiver != null) {
            mReceiver.send(resultCode, bundle);
        } else {
            Log.e(TAG, "trying to send result while mReceiver is null");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy()--");
        mReceiver = null;
    }


}