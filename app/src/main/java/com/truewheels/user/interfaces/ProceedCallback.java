package com.truewheels.user.interfaces;

public interface ProceedCallback {

    void process();
}
