package com.truewheels.user.interfaces;

public interface SmsListener {
    public void messageReceived(String messageText);
}
