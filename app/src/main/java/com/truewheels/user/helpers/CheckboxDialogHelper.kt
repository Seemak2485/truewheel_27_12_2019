package com.truewheels.user.helpers

import android.content.Context
import androidx.appcompat.app.AlertDialog
import android.widget.EditText
import java.util.*

class CheckboxDialogHelper {

    companion object {
        fun createCheckboxDialog(context: Context, editText: EditText, arrayList: ArrayList<String>, isCheckedArr: BooleanArray) {
            val tempCheckedIndexArr = ArrayList<Int>()

            val alertBuilder = AlertDialog.Builder(context)
            alertBuilder.setTitle("Select ")
            alertBuilder.setMultiChoiceItems(arrayList.toTypedArray(), isCheckedArr, { _, which, _ ->
                if (tempCheckedIndexArr.contains(which))
                    tempCheckedIndexArr.remove(which)
                else
                    tempCheckedIndexArr.add(which)
            }).setPositiveButton("OK", { _, _ ->
                var selectedDetails = ""

                isCheckedArr.indices
                        .filter { isCheckedArr[it] }
                        .forEach {
                            selectedDetails += ",  " + arrayList[it]
                        }

                if (selectedDetails != "") {
                    selectedDetails = selectedDetails.substring(3)
                }
                editText.setText(selectedDetails)

            }).setNegativeButton("Cancel", { _, _ ->
                tempCheckedIndexArr.forEach {
                    isCheckedArr[it] = !isCheckedArr[it]
                }
            })

            alertBuilder.create().show()
        }
    }
}
