package com.truewheels.user.helpers

import android.content.Context
import android.view.View
import androidx.appcompat.app.AlertDialog
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.truewheels.user.activities.AddSYSActivity
import java.util.*

class ListDialogHelper {

    companion object {
        fun createListDialog(context: Context, editText: EditText, arrayList: ArrayList<String>) {

            val charSeqArr = arrayList.toArray(arrayOfNulls<CharSequence>(arrayList.size))

            val alertBuilder = AlertDialog.Builder(context)
                    .setTitle("Select ")
                    .setItems(charSeqArr, { _, i ->
                        editText.setText(arrayList[i])
                         })

            alertBuilder.create().show()
        }


        fun createListDialog(context: Context, editText: EditText, arrayList: ArrayList<String>,t1: TextInputLayout) {

            val charSeqArr = arrayList.toArray(arrayOfNulls<CharSequence>(arrayList.size))

            val alertBuilder = AlertDialog.Builder(context)
                    .setTitle("Select ")
                    .setItems(charSeqArr, { _, i ->
                        editText.setText(arrayList[i])
                        if(editText.text.contains("Resident Welfare")) {
                            AddSYSActivity.isRWA = true
                            t1.visibility= View.VISIBLE
                        }
                        else {
                            AddSYSActivity.isRWA = false
                            t1.visibility= View.GONE
                        }
                    })

            alertBuilder.create().show()
        }
    }
}
