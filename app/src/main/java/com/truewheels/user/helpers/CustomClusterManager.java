package com.truewheels.user.helpers;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.maps.android.clustering.ClusterManager;

/**
 * Created by Saurabh Kataria on 21-05-2018.
 */

public class CustomClusterManager extends ClusterManager {
    public CustomClusterManager(Context context, GoogleMap map) {
        super(context, map);
    }
}
