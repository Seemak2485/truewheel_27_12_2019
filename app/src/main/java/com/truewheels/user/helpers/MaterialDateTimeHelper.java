package com.truewheels.user.helpers;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Color;
import android.widget.EditText;

import androidx.fragment.app.FragmentActivity;

import com.truewheels.user.util.DateUtil;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;

public class MaterialDateTimeHelper {

    public static void createDatePickerDialog(FragmentActivity activity, String title, final EditText editText, boolean isMinNow) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        date = DateUtil.Companion.convertFormat(DateUtil.FORMAT_SQL_DATE, DateUtil.FORMAT_READABLE_DATE, date);
                        editText.setText(date);
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        //Same for tpd
        dpd.setAccentColor(Color.parseColor("#009688"));
        dpd.setTitle(title);
        if (isMinNow) dpd.setMinDate(now);
        else dpd.setMaxDate(now);

        dpd.show(activity.getSupportFragmentManager(), "Datepickerdialog");
    }

    public static void createDatePickerDialog(FragmentActivity activity, String title, final EditText editText, int yearGap) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                        String date = "" + year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        date = DateUtil.Companion.convertFormat(DateUtil.FORMAT_SQL_DATE, DateUtil.FORMAT_READABLE_DATE, date);
                        editText.setText(date);
                    }
                },
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        //Same for tpd
        dpd.setAccentColor(Color.parseColor("#009688"));
        dpd.setTitle(title);

        Calendar maxDateCalendar = Calendar.getInstance();
        maxDateCalendar.set(Calendar.YEAR, now.get(Calendar.YEAR) - yearGap);
        dpd.setMaxDate(maxDateCalendar);

        dpd.show(activity.getSupportFragmentManager(), "Datepickerdialog");
    }

    public static void createTimePickerDialog(FragmentActivity activity, String title, final EditText editText) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                        String tempHour = String.valueOf(hourOfDay);
                        if (tempHour.length() == 1) tempHour = "0" + tempHour;
                        String tempMinute = String.valueOf(minute);
                        if (tempMinute.length() == 1) tempMinute = "0" + tempMinute;
                        String time = tempHour + " : " + tempMinute;
                        editText.setText(DateUtil.Companion.convertFormat(
                                DateUtil.FORMAT_24_HOUR_TIME,
                                DateUtil.FORMAT_READABLE_TIME,
                                time));
                    }
                },
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false);

        tpd.setAccentColor(Color.parseColor("#009688"));
        tpd.setTitle(title);

        tpd.show(activity.getSupportFragmentManager(), "Datepickerdialog");
    }
}
