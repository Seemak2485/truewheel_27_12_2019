package com.truewheels.user.helpers;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.truewheels.user.R;
import com.truewheels.user.activities.MainActivity;
import com.truewheels.user.constants.Constants;
import com.truewheels.user.constants.RequestCodeConstants;
import com.truewheels.user.enums.TriggerSearchApiReason;
import com.truewheels.user.interfaces.ProceedCallback;
import com.truewheels.user.server.API;
import com.truewheels.user.server.model.ClusterMarkerItem;
import com.truewheels.user.server.model.ParkingLocationData;
import com.truewheels.user.server.model.map_distance_matrix.MapDistanceMatrixResponse;
import com.truewheels.user.server.services.LocationService;
import com.truewheels.user.util.Util;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.truewheels.user.enums.TriggerSearchApiReason.DEFAULT_TRIGGER;
import static com.truewheels.user.enums.TriggerSearchApiReason.DO_NOT_TRIGGER;
import static com.truewheels.user.enums.TriggerSearchApiReason.LOCATION_ENABLED_BTN_CLICK;
import static com.truewheels.user.enums.TriggerSearchApiReason.MAP_DRAGGED_BY_USER;

public class GoogleMapHelper implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private Activity activity;
    private ClusterManager<ClusterMarkerItem> clusterManager;
    private GoogleMap map;
    private View helperView;
    private View mapView;
    private boolean shouldCluster;
    private TriggerSearchApiReason triggerSearchApiReason = LOCATION_ENABLED_BTN_CLICK;
    private OnMapInitialisedCallback onMapInitialisedCallback;
    private OnCameraIdleCallback onCameraIdleCallback;
    private OnNonClusterMarkerClickCallback onNonClusterMarkerClickCallback;
    private OnClusterItemInfoWindowClickCallback onClusterItemInfoWindowClickCallback;
    private boolean isPublicAvailable = false;

    private String mDistance = "";

    public GoogleMapHelper(Activity activity, View mapView) {
        this.activity = activity;
        this.mapView = mapView;
    }

    public GoogleMap getMap() {
        return map;
    }

    public ClusterManager<ClusterMarkerItem> getClusterManager() {
        return clusterManager;
    }

    public void setHelperView(View helperView) {
        this.helperView = helperView;
    }

    public void setTriggerSearchApiReason(TriggerSearchApiReason triggerSearchApiReason) {
        this.triggerSearchApiReason = triggerSearchApiReason;
    }

    public TriggerSearchApiReason getTriggerSearchApiReason() {
        return triggerSearchApiReason;
    }

    public void setShouldCluster(boolean shouldCluster) {
        this.shouldCluster = shouldCluster;
    }

    // CALLBACKS

    public void setOnMapInitialisedCallback(OnMapInitialisedCallback onMapInitialisedCallback) {
        this.onMapInitialisedCallback = onMapInitialisedCallback;
    }

    public void setOnCameraIdleCallback(OnCameraIdleCallback onCameraIdleCallback) {
        this.onCameraIdleCallback = onCameraIdleCallback;
    }

    public void setOnNonClusterMarkerClickCallback(OnNonClusterMarkerClickCallback onNonClusterMarkerClickCallback) {
        this.onNonClusterMarkerClickCallback = onNonClusterMarkerClickCallback;
    }

    public void setOnClusterItemInfoWindowClickCallback(OnClusterItemInfoWindowClickCallback onClusterItemInfoWindowClickCallback) {
        if (!shouldCluster) {
            Toast.makeText(activity, "Must set clustering first", Toast.LENGTH_SHORT).show();
            return;
        }
        this.onClusterItemInfoWindowClickCallback = onClusterItemInfoWindowClickCallback;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        helperView.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public boolean onTouch(final View view, final MotionEvent motionEvent) {
                if (simpleGestureDetector.onTouchEvent(motionEvent)) {
                    return true;
                } else if (motionEvent.getPointerCount() == 1) { // Single tap
                    mapView.dispatchTouchEvent(motionEvent); // Propagate the event to the map (Pan)
                    // single tap is also performed on ending of double_tap or pinch_zoom
                    // service API on single tap (only at - end of drag)
                    if (triggerSearchApiReason != DO_NOT_TRIGGER && motionEvent.getAction() == MotionEvent.ACTION_MOVE)
                        triggerSearchApiReason = MAP_DRAGGED_BY_USER;   // in drag motion,
                    // single tap is performed continuously

                } else if (scaleGestureDetector.onTouchEvent(motionEvent)) {
                    return true;
                }

                return true; // Consume all the gestures
            }

            // Gesture detector to manage double tap gestures
            private GestureDetector simpleGestureDetector = new GestureDetector(
                    activity, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onDoubleTap(MotionEvent e) {
                    map.animateCamera(CameraUpdateFactory.zoomIn()); // Fixed zoom in
                    // do not service Api on double tap
                    triggerSearchApiReason = DO_NOT_TRIGGER;
                    return true;
                }

                @Override
                public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                    // service Api on fling motion end
                    triggerSearchApiReason = MAP_DRAGGED_BY_USER;
                    return super.onFling(e1, e2, velocityX, velocityY);
                }
            });

            // Gesture detector to manage scale gestures
            private ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(
                    activity, new ScaleGestureDetector.SimpleOnScaleGestureListener() {
                @Override
                public boolean onScale(ScaleGestureDetector detector) { // Pinch zoom
                    float scaleFactor = detector.getScaleFactor();
                    map.moveCamera(CameraUpdateFactory.zoomBy( // Zoom the map without panning it
                            (map.getCameraPosition().zoom * scaleFactor
                                    - map.getCameraPosition().zoom) / 5));
                    // do not service Api on pinch zoom
                    triggerSearchApiReason = DO_NOT_TRIGGER;
                    return true;
                }
            });
        });

        if (shouldCluster)
            setUpClusterer();
        else {
            map.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                // Camera has stopped moving
                public void onCameraIdle() {
                    processCameraIdle();
                }
            });

            if (null != onNonClusterMarkerClickCallback) {
                map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        onNonClusterMarkerClickCallback.onMarkerClick(marker);
                        return true;
                    }
                });
            }
        }

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
                Util.showToast("We require this permission to show your location");
            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RequestCodeConstants.REQUEST_CODE_PERMISSION_FINE_LOCATION);
            return;
        }
        googleMap.setMyLocationEnabled(true);

        if (null != onMapInitialisedCallback) {
            onMapInitialisedCallback.onMapReady();
        }
    }

    private void processCameraIdle() {
        switch (triggerSearchApiReason) {
            case LOCATION_ENABLED_BTN_CLICK:
            case GOOGLE_PLACES_AUTOCOMPLETE_RESULT_CALLBACK:
                return;

            case MAP_DRAGGED_BY_USER:
                if (null != onCameraIdleCallback)
                    onCameraIdleCallback.onCameraIdle(map.getCameraPosition().target.latitude,
                            map.getCameraPosition().target.longitude);
                break;

            case DO_NOT_TRIGGER:
                triggerSearchApiReason = DEFAULT_TRIGGER;
                break;
        }
    }

    private void setUpClusterer() {
        clusterManager = new ClusterManager<>(activity, getMap());
        final CustomClusterRenderer renderer = new CustomClusterRenderer(activity, getMap(), clusterManager);
        renderer.setMinClusterSize(5);
        renderer.setAnimation(true);
        clusterManager.setRenderer(renderer);
        clusterManager.setOnClusterClickListener(new ClusterManager.OnClusterClickListener<ClusterMarkerItem>() {
            @Override
            public boolean onClusterClick(Cluster<ClusterMarkerItem> cluster) {
                // if true, do not move camera
                getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(getLatLongBounds(cluster), 50));
//                map.animateCamera(CameraUpdateFactory.zoomIn());
                return true;
            }
        });
        clusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<ClusterMarkerItem>() {
            @Override
            public boolean onClusterItemClick(final ClusterMarkerItem clusterMarkerItem) {
                // if true, click handling stops here and do not show info view, do not move camera
                // you can avoid this by calling:
                // renderer.getMarker(clusterItem).showInfoWindow();
                triggerSearchApiReason = DO_NOT_TRIGGER;
                getMap().animateCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(clusterMarkerItem.getParkingLocationData().getLattitude()),
                        Double.parseDouble(clusterMarkerItem.getParkingLocationData().getLongitude()))));
                ProceedCallback aCallback = new ProceedCallback() {
                    @Override
                    public void process() {
                        try {
                            clusterMarkerItem.getParkingLocationData().setDistance(mDistance);
                            renderer.getMarker(clusterMarkerItem).showInfoWindow();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                getCurrentDistance(clusterMarkerItem.getParkingLocationData(), aCallback);

//                getMap().moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(clusterMarkerItem.getParkingLocationData().getLattitude()),
//                        Double.parseDouble(clusterMarkerItem.getParkingLocationData().getLongitude())), 15));
                return true;
            }
        });
        clusterManager.setOnClusterItemInfoWindowClickListener(new ClusterManager.OnClusterItemInfoWindowClickListener<ClusterMarkerItem>() {
            @Override
            public void onClusterItemInfoWindowClick(ClusterMarkerItem clusterMarkerItem) {
                if (null != onClusterItemInfoWindowClickCallback)
                    onClusterItemInfoWindowClickCallback.onClusterItemInfoWindowClick(clusterMarkerItem);
            }
        });
        getMap().setOnCameraIdleListener(clusterManager);
//        getMap().setOnMarkerClickListener(this);
        getMap().setOnMarkerClickListener(clusterManager);
        getMap().setOnInfoWindowClickListener(clusterManager);
        clusterManager.getMarkerCollection()
                .setOnInfoWindowAdapter(new CustomInfoViewAdapter(LayoutInflater.from(activity), renderer));
        getMap().setInfoWindowAdapter(clusterManager.getMarkerManager());
    }

    private LatLngBounds getLatLongBounds(Cluster<ClusterMarkerItem> iCluster) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        for (ClusterMarkerItem aCluster : iCluster.getItems()) {
            LatLng latLng = new LatLng(Double.parseDouble(aCluster.getParkingLocationData().getLattitude()), Double.parseDouble(aCluster.getParkingLocationData().getLongitude()));
            builder.include(latLng);
        }

        LatLngBounds aBounds = builder.build();

        return aBounds;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        triggerSearchApiReason = DO_NOT_TRIGGER;
        return false;
    }

    private final class CustomClusterRenderer extends DefaultClusterRenderer<ClusterMarkerItem> implements GoogleMap.OnCameraIdleListener {
        @SuppressWarnings("FieldCanBeLocal")
        private final Context context;

        CustomClusterRenderer(Context context, GoogleMap map, ClusterManager<ClusterMarkerItem> clusterManager) {
            super(context, map, clusterManager);
            this.context = context;
        }

        @Override
        protected void onBeforeClusterItemRendered(ClusterMarkerItem item, MarkerOptions markerOptions) {
            int drawable;
            if (item.isPublicAvailable())
                drawable = R.drawable.map_icon_red;
            else if(item.isRWAAvailable())
                drawable = R.drawable.map_icon_blue;
            else
                drawable = R.drawable.map_icon_mcd;

            // final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
            final BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.fromResource(drawable);
            markerOptions.icon(markerDescriptor);
//            Marker aMarker = getMap().addMarker(markerOptions);
//            Log.d("TAG", "A cluster marker: " + aMarker);
//            ((MainActivity) activity).mMarkerMap.put(aMarker.getId(), item.getParkingLocationData());

        }

        @Override
        public void onCameraIdle() {
            processCameraIdle();
        }


    }

    private final class CustomInfoViewAdapter implements GoogleMap.InfoWindowAdapter {

        private final LayoutInflater mInflater;
        private final CustomClusterRenderer renderer;

        CustomInfoViewAdapter(LayoutInflater inflater, CustomClusterRenderer renderer) {
            this.mInflater = inflater;
            this.renderer = renderer;
        }

        @Override
        @SuppressLint("InflateParams")
        public View getInfoWindow(Marker marker) {
            final View popup = mInflater.inflate(R.layout.item_map_info_window, null);
            init(popup, marker);
            return popup;
        }

        @Override
        @SuppressLint("InflateParams")
        public View getInfoContents(Marker marker) {
            final View popup = mInflater.inflate(R.layout.item_map_info_window, null);
            init(popup, marker);
            return popup;
        }

        @SuppressLint("SetTextI18n")
        private void init(final View popup, Marker marker) {
            final ClusterMarkerItem clusterItem = renderer.getClusterItem(marker);
            final ParkingLocationData pd = clusterItem.getParkingLocationData();


            String tvContinueText = "Continue";

            if (!clusterItem.isPublic() || clusterItem.isPublicAvailable()) {
                tvContinueText = "Book Now";
            }

            ((TextView) popup.findViewById(R.id.tvContinue)).setText(tvContinueText);

            if(pd.getSpace_type()!=null&&pd.getSpace_type().contains("RWA"))
            {
                ((TextView) popup.findViewById(R.id.tvSpacName)).setVisibility(View.VISIBLE);
                ((TextView) popup.findViewById(R.id.tvSpacName)).setText(clusterItem.getParkingLocationData().getSpace_name());
                ((androidx.constraintlayout.widget.ConstraintLayout) popup.findViewById(R.id.parkinginfo)).setVisibility(View.GONE);
            }
else {
                ((androidx.constraintlayout.widget.ConstraintLayout) popup.findViewById(R.id.parkinginfo)).setVisibility(View.VISIBLE);
                ((TextView) popup.findViewById(R.id.tvSpacName)).setVisibility(View.GONE);
            if (pd.getCarBasicCharge().length() > 0) {
                if (clusterItem.isPublic()) {
                    ((TextView) popup.findViewById(R.id.tvCarBasic)).setText(pd.getCarBasicCharge());
                } else {
                    ((TextView) popup.findViewById(R.id.tvCarBasic)).setText("Rs. " + pd.getCarBasicCharge());
                }
            } else {
                ((TextView) popup.findViewById(R.id.tvCarBasic)).setText("NA");
            }

            if (pd.getBikeBasicCharge().length() > 0) {
                if (clusterItem.isPublic()) {
                    ((TextView) popup.findViewById(R.id.tvBikeBasic)).setText(pd.getBikeBasicCharge());
                } else {
                    ((TextView) popup.findViewById(R.id.tvBikeBasic)).setText("Rs. " + pd.getBikeBasicCharge());
                }
            } else {
                ((TextView) popup.findViewById(R.id.tvBikeBasic)).setText("NA");
            }

            TextView mTvDistance = ((TextView) popup.findViewById(R.id.tvCurrentParkingDistance));
            mTvDistance.setText(
                    String.format(Locale.getDefault(), "%.2f Km", Double.valueOf(pd.getDistance())
                    ));
        }
        }
    }

    private String getCurrentDistance(ParkingLocationData clusterItemParkingLocationData, ProceedCallback iCallback) {
        serviceMapDistanceMatrixApi(new LatLng(Double.parseDouble(clusterItemParkingLocationData.getLattitude()),
                Double.parseDouble(clusterItemParkingLocationData.getLongitude())), iCallback);
        return mDistance;
    }

    private void serviceMapDistanceMatrixApi(LatLng source, final ProceedCallback callback) {
        LatLng destination = ((MainActivity) activity).currentLatLng;
        API.getClient().create(LocationService.class).fetchMapDistanceMatrix(
                Constants.MAP_DISTANCE_MATRIX_URL,
                source.latitude + "," + source.longitude,
                destination.latitude + "," + destination.longitude,
                Constants.MAP_DISTANCE_MATRIX_KEY
        ).enqueue(new Callback<MapDistanceMatrixResponse>() {
            @Override
            public void onResponse(Call<MapDistanceMatrixResponse> call, Response<MapDistanceMatrixResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getStatus().equals("OK") && response.body().getRows().get(0)
                            .getElements().get(0).getStatus().equals("OK")) {
                        double dist = response.body()
                                .getRows().get(0)
                                .getElements().get(0)
                                .getDistance()
                                .getValue() / 1000.0;
                        mDistance = String.valueOf(dist);
                    }

                    callback.process();
                } else {
                    mDistance = "";
                    Util.showToast("Error Code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<MapDistanceMatrixResponse> call, Throwable t) {
                mDistance = "";
            }
        });
    }

    // INTERFACES

    public interface OnMapInitialisedCallback {
        void onMapReady();
    }

    public interface OnCameraIdleCallback {
        void onCameraIdle(double latitude, double longitude);
    }

    public interface OnClusterItemInfoWindowClickCallback {
        void onClusterItemInfoWindowClick(ClusterMarkerItem clusterMarkerItem);
    }

    public interface OnNonClusterMarkerClickCallback {
        void onMarkerClick(Marker marker);
    }
}
