package com.truewheels.user.helpers;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;

import org.jetbrains.annotations.Nullable;

public class AlertDialogHelper {

    public static void showInfoAlertDialog(Context context, String message) {
        showInfoAlertDialog(context, message, null);
    }

    public static void showInfoAlertDialog(Context context, String message, final SuccessCallback successCallback) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (null != successCallback)
                            successCallback.onSuccess();
                    }
                })
                .show();
    }

    public static void showAlertDialog(Context context, @Nullable String title, String message, final Callback callback) {
        showAlertDialog(context, title, message, "Confirm", "Back", callback);
    }

    public static void showAlertDialog(Context context, @Nullable String title, String message,
                                       String positiveTitle, String negativeTitle, final Callback callback) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        if (null != title)
            alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positiveTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onSuccess();
                    }
                })
                .setNegativeButton(negativeTitle, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onBack();
                    }
                })
                .show();
    }

    public interface Callback extends SuccessCallback {
        void onBack();
    }

    public interface SuccessCallback {
        void onSuccess();
    }

}