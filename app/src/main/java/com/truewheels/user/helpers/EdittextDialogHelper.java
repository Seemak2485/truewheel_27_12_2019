package com.truewheels.user.helpers;

import android.app.Activity;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.truewheels.user.R;

public class EdittextDialogHelper {

    public static void showEdittextDialog(Activity activity, String title, final Callback callback) {
        final View dialogView = activity.getLayoutInflater().inflate(R.layout.layout_edittext_dialog, null);
        final EditText editText = dialogView.findViewById(R.id.etDialog);
        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);

        editText.setText("");

        final AlertDialog alertdialog = new AlertDialog.Builder(activity)
                .setTitle(title)
                .setView(dialogView)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onSuccess(editText.getText().toString());
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        callback.onBack();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        callback.onBack();
                    }
                })
                .show();

        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    alertdialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                    handled = true;
                }
                return handled;
            }
        });
    }

    public interface Callback {
        void onSuccess(String message);

        void onBack();
    }
}
