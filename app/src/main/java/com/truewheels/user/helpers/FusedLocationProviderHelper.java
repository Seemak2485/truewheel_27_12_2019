package com.truewheels.user.helpers;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.truewheels.user.constants.RequestCodeConstants;
import com.truewheels.user.util.Util;

import org.jetbrains.annotations.NotNull;

public class FusedLocationProviderHelper {

    private Activity activity;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private FusedLocationCompleteCallback callback;

    @SuppressWarnings("unused")
    private FusedLocationProviderHelper() {
        // activity & callback must be set
    }

    public FusedLocationProviderHelper(@NotNull Activity activity,
                                       @NotNull FusedLocationCompleteCallback callback) {
        this.activity = activity;
        this.callback = callback;
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity);
    }

    /**
     * Gets the current location of the device
     */
    public void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (ContextCompat.checkSelfPermission(activity.getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(activity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        callback.onFusedLocationComplete(task);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mFusedLocationProviderClient.flushLocations();
                            }
                        }, 30 * 1000);
                    }
                });
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION))
                    Util.showToast("We require this permission to show your location");
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RequestCodeConstants.REQUEST_CODE_PERMISSION_FINE_LOCATION);
            }
        } catch (SecurityException e) {
            Log.e("Exception: ", e.getMessage());
        }
    }

    public interface FusedLocationCompleteCallback {

        void onFusedLocationComplete(@NonNull Task<Location> task);
    }
}
