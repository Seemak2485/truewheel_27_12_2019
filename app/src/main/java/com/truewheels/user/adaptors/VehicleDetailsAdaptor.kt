package com.truewheels.user.adaptors

import android.app.Activity
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.truewheels.user.R
import com.truewheels.user.activities.ProfileActivity
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.server.model.profile.VehicleDetailsPojo
import com.truewheels.user.viewholders.VehicleDetailsViewHolder

class VehicleDetailsAdaptor(val activity: Activity) : RecyclerView.Adapter<VehicleDetailsViewHolder>() {
    private lateinit var recyclerView: RecyclerView
    var dataList: ArrayList<VehicleDetailsPojo>? = null

    fun populate(dataList: ArrayList<VehicleDetailsPojo>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleDetailsViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_vehicle_detail_new_layout, parent, false)
        return VehicleDetailsViewHolder(view)
    }

    override fun onBindViewHolder(holder: VehicleDetailsViewHolder, position: Int) {
        val vehicleDetailsItem = dataList!![position]
        holder.populate(vehicleDetailsItem)
        holder.ivDelete.setOnClickListener {
            AlertDialogHelper.showAlertDialog(activity,
                    "Delete Entry",
                    "Are you sure you want to delete this entry?",
                    object : AlertDialogHelper.Callback {
                        override fun onSuccess() {
                            if (activity is ProfileActivity)
                                activity.serviceAddDeleteVehicleApi(
                                        false, dataList!![holder.adapterPosition])
                        }

                        override fun onBack() {
                            // do nothing
                        }
                    })
        }
    }

    override fun getItemCount(): Int = dataList?.size ?: 0

}