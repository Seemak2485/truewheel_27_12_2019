package com.truewheels.user.adaptors

import android.app.Activity
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.truewheels.user.R
import com.truewheels.user.activities.BookingReceivedDetailsActivity
import com.truewheels.user.helpers.RecyclerItemClickListener
import com.truewheels.user.server.model.parkings.booking_received.BookingRecievedItem
import com.truewheels.user.viewholders.BookingReceivedViewHolder

class BookingReceivedAdaptor(val activity: Activity) : RecyclerView.Adapter<BookingReceivedViewHolder>() {
    private var dataList: List<BookingRecievedItem>? = null

    fun populate(dataList: List<BookingRecievedItem>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingReceivedViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_bookings, parent, false)
        return BookingReceivedViewHolder(view, activity)
    }

    override fun onBindViewHolder(holder: BookingReceivedViewHolder, position: Int) {
        val bookingReceivedItem = dataList!![position]
        holder?.populate(bookingReceivedItem)
        holder?.itemView?.setOnClickListener { v ->
            val intent = Intent(v.context, BookingReceivedDetailsActivity::class.java)
            intent.putExtra("bookingReceivedDetailObj", bookingReceivedItem)
            activity.startActivity(intent)
        }

        holder?.rvBookingDetails?.addOnItemTouchListener(
                RecyclerItemClickListener(activity, holder.rvBookingDetails, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        holder.itemView?.performClick()
                    }

                    override fun onLongItemClick(view: View, position: Int) {
                        // do whatever
                    }
                })
        )

    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }
}
