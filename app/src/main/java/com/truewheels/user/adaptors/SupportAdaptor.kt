package com.truewheels.user.adaptors

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.truewheels.user.R
import com.truewheels.user.util.AnimationUtil
import com.truewheels.user.viewholders.SupportViewHolder

class SupportAdaptor : RecyclerView.Adapter<SupportViewHolder>() {
    private var dataList: List<String>? = null
    private lateinit var recyclerView: RecyclerView
    var selectedPos = -1
    private var previousPos = -1

    fun populate(dataList: List<String>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SupportViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_support, parent, false)
        return SupportViewHolder(view)
    }

    override fun onBindViewHolder(holder: SupportViewHolder, position: Int) {
        val title = dataList!![position]
        holder?.populate(title)
        holder?.itemView?.setOnClickListener {
            clearOtherMessages(holder.adapterPosition)
        }
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }

    private fun clearOtherMessages(position: Int) {
        if (selectedPos != position) {
            previousPos = selectedPos
            selectedPos = position
        }

        if (previousPos != -1) {
            val view = recyclerView.findViewHolderForAdapterPosition(previousPos)?.itemView
            val etSupportMessage: EditText = view!!.findViewById(R.id.etSupportMessage)
            if (etSupportMessage.visibility == View.VISIBLE) {
                etSupportMessage.setText("")
                AnimationUtil.collapse(etSupportMessage)
            }
        }

        val view = recyclerView.findViewHolderForAdapterPosition(selectedPos)?.itemView
        val etSupportMessage: EditText = view!!.findViewById(R.id.etSupportMessage)
        if (etSupportMessage.visibility == View.VISIBLE)
            AnimationUtil.collapse(etSupportMessage)
        else AnimationUtil.expand(etSupportMessage)
    }

    fun getSupportTitle(): String {
        return dataList?.get(selectedPos)!!
    }

    fun getSupportMessage(): String {
        val view = recyclerView.findViewHolderForAdapterPosition(selectedPos)?.itemView
        val etSupportMessage: EditText = view!!.findViewById(R.id.etSupportMessage)
        return etSupportMessage.text.toString()
    }
}
