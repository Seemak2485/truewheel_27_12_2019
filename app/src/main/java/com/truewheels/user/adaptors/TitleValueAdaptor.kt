package com.truewheels.user.adaptors

import android.app.Activity
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.truewheels.user.R
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.viewholders.TitleValueViewHolder

class TitleValueAdaptor(val activity: Activity, @LayoutRes val resource: Int) : RecyclerView.Adapter<TitleValueViewHolder>() {
    private var dataList: List<TitleValuePojo?>? = null

    fun populate(dataList: List<TitleValuePojo>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TitleValueViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(resource, parent, false)
        return TitleValueViewHolder(view)
    }

    override fun onBindViewHolder(holder: TitleValueViewHolder, position: Int) {
        holder?.populate(dataList!![position]!!)
    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }
}
