package com.truewheels.user.adaptors

import android.app.Activity
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.truewheels.user.R
import com.truewheels.user.activities.BookingMadeDetailsActivity
import com.truewheels.user.helpers.RecyclerItemClickListener
import com.truewheels.user.server.model.parkings.booking_made.BookingMadeItem
import com.truewheels.user.viewholders.MyBookingsViewHolder


class MyBookingsAdaptor(val activity: Activity) : RecyclerView.Adapter<MyBookingsViewHolder>() {
    private var dataList: List<BookingMadeItem>? = null

    fun populate(dataList: List<BookingMadeItem>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyBookingsViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_bookings, parent, false)
        return MyBookingsViewHolder(view, activity)
    }

    override fun onBindViewHolder(holder: MyBookingsViewHolder, position: Int) {
        val bookingReceivedItem = dataList!![position]
        holder?.populate(bookingReceivedItem)
        holder?.itemView?.setOnClickListener { v ->
            val intent = Intent(v.context, BookingMadeDetailsActivity::class.java)
            intent.putExtra("bookingMadeDetailObj", bookingReceivedItem)
            activity.startActivity(intent)
        }

        holder?.rvBookingDetails?.addOnItemTouchListener(
                RecyclerItemClickListener(activity, holder.rvBookingDetails, object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        holder.itemView?.performClick()
                    }

                    override fun onLongItemClick(view: View, position: Int) {
                        // do whatever
                    }
                })
        )

    }

    override fun getItemCount(): Int {
        return dataList?.size ?: 0
    }
}
