package com.truewheels.user.adaptors

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.activities.AddSYSActivity
import com.truewheels.user.activities.BaseActivity
import com.truewheels.user.constants.RequestCodeConstants
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.sys.select.SYSParkingItem
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.util.Util
import com.truewheels.user.viewholders.SYSParkingListViewHolder
import kotlinx.android.synthetic.main.item_sys_parking_list.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SYSParkingListAdaptor(val activity: BaseActivity) : RecyclerView.Adapter<SYSParkingListViewHolder>() {
    private var dataList: List<SYSParkingItem?>? = null
    private var shouldListenToCheckChange = true

    fun populate(dataList: List<SYSParkingItem>?) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SYSParkingListViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_sys_parking_list, parent, false)
        return SYSParkingListViewHolder(view)
    }

    override fun onBindViewHolder(holder: SYSParkingListViewHolder, position: Int) {
        val parkingItem = dataList!![position]!!
        holder?.populate(parkingItem)

        holder?.itemView?.setOnClickListener { v ->
            if (parkingItem.active == "N") {
                Util.showToast("Activate this parking to edit it")
                return@setOnClickListener
            } else if (parkingItem.active == "P") {
                Util.showToast("This parking has not been verified yet")
                return@setOnClickListener
            }
            val intent = Intent(v.context, AddSYSActivity::class.java)
            intent.putExtra("sysParkingItemObj", parkingItem)
            intent.putExtra("mode", "edit")
            activity.startActivityForResult(intent, RequestCodeConstants.REQUEST_CODE_EDIT_SYS)
        }

        holder?.cbIsActive?.setOnCheckedChangeListener { compoundBtn, isChecked ->
            if (!shouldListenToCheckChange) {
                shouldListenToCheckChange = true
                return@setOnCheckedChangeListener
            }

            val message = if (isChecked)
                "Great, you are activating your parking.\nNow you will receive booking at your parking site."
            else
                "Are you sure you want to Deactivate this parking?\nAfter deactivation you will not receive any booking till you reactivate the parking"

            AlertDialogHelper.showAlertDialog(compoundBtn.context, null, message, object : AlertDialogHelper.Callback {
                override fun onSuccess() {
                    serviceSYSActivationApi(holder.cbIsActive, isChecked, position)
                }

                override fun onBack() {
                    //back to previous state
                    shouldListenToCheckChange = false
                    holder.cbIsActive.isChecked = !isChecked
                }

            })
        }
    }

    override fun getItemCount(): Int = dataList?.size ?: 0

    override fun onViewRecycled(holder: SYSParkingListViewHolder) {
        super.onViewRecycled(holder!!)
        holder?.itemView?.cbIsActive?.setOnCheckedChangeListener(null)
    }

    private fun serviceSYSActivationApi(cbIsActive: CheckBox, activate: Boolean, position: Int) {
        val sysParkingItem: SYSParkingItem = dataList!![position]!!
        activity.showDialog()

        val requestCall: Call<BaseResponse<*>> = if (activate)
            API.getClient().create(ShareYourSpaceService::class.java).activateParking(
                    TVPreferences.getInstance().userId,
                    sysParkingItem.parkingId.toString())
        else
            API.getClient().create(ShareYourSpaceService::class.java).deactivateParking(
                    TVPreferences.getInstance().userId,
                    sysParkingItem.parkingId.toString())

        requestCall.enqueue(object : Callback<BaseResponse<*>> {
            override fun onResponse(call: Call<BaseResponse<*>>?, response: Response<BaseResponse<*>>) {
                activity.dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dataList!![position]!!.active = if (activate) "A" else "N"
                    Util.showToast(response.body()!!.error)
                } else {
                    shouldListenToCheckChange = false
                    cbIsActive.isChecked = !activate
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<*>>?, t: Throwable?) {
                activity.dismissDialog()
            }
        })
    }

}