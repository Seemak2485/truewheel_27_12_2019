package com.truewheels.user.adaptors;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.truewheels.user.R;
import com.truewheels.user.server.model.coupons.CouponsModel;

import java.util.ArrayList;

public class PrivateCouponAdapter extends RecyclerView.Adapter<PrivateCouponAdapter.CouponHolder> {

    private Context mContext;
    private ArrayList<CouponsModel> mCouponList = new ArrayList<>();
    private CouponListener mListener;

    public PrivateCouponAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void populate(ArrayList<CouponsModel> iList) {
        mCouponList = iList;
        notifyDataSetChanged();
    }

    @Override
    public CouponHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CouponHolder(LayoutInflater.from(mContext).inflate(R.layout.item_coupon, parent, false));
    }

    @Override
    public void onBindViewHolder(CouponHolder holder, int position) {
        holder.mTvCouponTitle.setText(mCouponList.get(position).getCouponNo());
        holder.mTvCouponDetail.setText(mCouponList.get(position).getCouponDiscription());
    }

    @Override
    public int getItemCount() {
        return mCouponList.size();
    }

    public class CouponHolder extends RecyclerView.ViewHolder {
        TextView mTvCouponTitle, mTvCouponDetail;
        RadioButton mRbCoupon;

        public CouponHolder(View itemView) {
            super(itemView);
            mRbCoupon = itemView.findViewById(R.id.rbCoupon);
            mTvCouponTitle = itemView.findViewById(R.id.tvCouponTitle);
            mTvCouponDetail = itemView.findViewById(R.id.tvCouponDetails);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onCouponSelected(mCouponList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface CouponListener {
        void onCouponSelected(CouponsModel iCoupon);
    }

    public void setOnCouponListener(CouponListener iListener) {
        mListener = iListener;
    }
}
