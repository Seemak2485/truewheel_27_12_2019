package com.truewheels.user;

import android.content.Context;
import android.content.SharedPreferences;

public class TVPreferences {
    private static final String NAME = "TV_PREFERENCES";
    private static final TVPreferences ourInstance = new TVPreferences();
    private final SharedPreferences mPreferences;

    private String FULL_NAME = "FULL_NAME";
    private String PHONE = "PHONE";
    private String EMAIL = "EMAIL";
    private String AUTH_TOKEN = "AUTH_TOKEN";
    private String USER_ID = "USER_ID";

    private String PARKING_PRICE_ID_TO_RATE = "PARKING_PRICE_ID_TO_RATE";
    private String IS_PROFILE_UPDATED = "IS_PROFILE_UPDATED";

    private TVPreferences() {
        mPreferences = TrueWheelApp.getInstance().getSharedPreferences(NAME, Context.MODE_PRIVATE);
    }

    public static TVPreferences getInstance() {
        return ourInstance;
    }

    public String getFullName() {
        return mPreferences.getString(FULL_NAME, null);
    }

    public void setFullName(String name) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(FULL_NAME, name);
        editor.apply();
    }

    public String getPhone() {
        return mPreferences.getString(PHONE, null);
    }

    public void setPhone(String phone) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PHONE, phone);
        editor.apply();
    }

    public String getEmail() {
        return mPreferences.getString(EMAIL, null);
    }

    public void setEmail(String email) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(EMAIL, email);
        editor.apply();
    }

    public String getAuthToken() {
        return mPreferences.getString(AUTH_TOKEN, null);
    }

    public void setAuthToken(String token) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(AUTH_TOKEN, token);
        editor.apply();
    }

    public boolean getIsProfileUpdated() {
        return mPreferences.getBoolean(IS_PROFILE_UPDATED, false);
    }

    public void setIsProfileUpdated(boolean is) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putBoolean(IS_PROFILE_UPDATED, is);
        editor.apply();
    }

    public String getUserId() {
        return mPreferences.getString(USER_ID, "-1");
    }

    public void setUserId(String is) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(USER_ID, is);
        editor.apply();
    }

    /*public String getParkingIdToRate() {
        return mPreferences.getString(PARKING_ID_TO_RATE, "");
    }

    public void setParkingIdToRate(String is) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PARKING_ID_TO_RATE, is);
        editor.apply();
    }

    public String getParkingPriceForIdToRate() {
        return mPreferences.getString(PARKING_PRICE_ID_TO_RATE, "");
    }

    public void setParkingPriceForIdToRate(String is) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString(PARKING_PRICE_ID_TO_RATE, is);
        editor.apply();
    }
*/
}
