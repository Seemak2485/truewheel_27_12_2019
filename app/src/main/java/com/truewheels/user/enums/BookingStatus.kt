package com.truewheels.user.enums

enum class BookingStatus(val status: String) {
    SCHEDULED("Scheduled"),
    IN_PROGRESS("In Progress"),
    CANCELLED("Cancelled"),
    PARKED_OUT("Parked Out"),
}