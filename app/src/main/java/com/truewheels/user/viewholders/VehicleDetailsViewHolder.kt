package com.truewheels.user.viewholders

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import com.truewheels.user.R
import com.truewheels.user.server.model.profile.VehicleDetailsPojo

class VehicleDetailsViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    private var vehicleID: Int = -1
    private val etVehicleNo = itemView?.findViewById(R.id.etVehicleNo) as EditText
    private val ivVehicleType = itemView?.findViewById(R.id.ivVehicleType) as ImageView
    val ivDelete = itemView?.findViewById(R.id.ivDelete) as ImageView
    //private val etVehicleModel = itemView?.findViewById(R.id.etVehicleModel) as EditText

    fun populate(vehicleDetailsPojo: VehicleDetailsPojo) {
        vehicleID = vehicleDetailsPojo.vehicleID
        etVehicleNo.setText(vehicleDetailsPojo.vehicleNumber)
        if (!vehicleDetailsPojo.vehicleType.isBlank()) {
            when (vehicleDetailsPojo.vehicleType) {
                "Four Wheeler" -> ivVehicleType.setImageResource(R.drawable.ic_directions_car)
                "Two Wheeler" -> ivVehicleType.setImageResource(R.drawable.ic_motorcycle_light)
            }
            ivVehicleType.tag = vehicleDetailsPojo.vehicleType
        }
        // etVehicleModel.setText(vehicleDetailsPojo.vehicleModel)
    }

}