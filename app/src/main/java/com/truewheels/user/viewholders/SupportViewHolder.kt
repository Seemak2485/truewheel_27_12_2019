package com.truewheels.user.viewholders

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.truewheels.user.R

class SupportViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    private val tvSupportTitle: TextView = itemView?.findViewById(R.id.tvSupportTitle) as TextView
    private val etSupportMessage: EditText = itemView?.findViewById(R.id.etSupportMessage) as EditText

    fun populate(title: String) {
        tvSupportTitle.text = title
    }
}