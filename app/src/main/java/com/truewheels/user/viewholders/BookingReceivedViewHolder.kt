package com.truewheels.user.viewholders

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.truewheels.user.R
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.enums.BookingStatus
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.server.model.parkings.booking_received.BookingRecievedItem
import com.truewheels.user.util.DateUtil

@SuppressLint("SetTextI18n")
class BookingReceivedViewHolder(itemView: View?, val activity: Activity) : RecyclerView.ViewHolder(itemView!!) {
    private val tvBookingId: TextView = itemView?.findViewById(R.id.tvBookingId) as TextView
    private val tvStatus: TextView = itemView?.findViewById(R.id.tvStatus) as TextView
    private val tvBookedParkingAddress: TextView = itemView?.findViewById(R.id.tvBookedParkingAddress) as TextView
    val rvBookingDetails: RecyclerView = itemView?.findViewById(R.id.rvBookingDetails) as RecyclerView
    private val tvCallForSupport: TextView = itemView?.findViewById(R.id.tvCallForSupport) as TextView
    private val tvNavigateNow: TextView = itemView?.findViewById(R.id.tvNavigateNow) as TextView

    init {
        itemView.apply {
            rvBookingDetails.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = TitleValueAdaptor(activity, R.layout.item_bold_title_value)
            }
        }

        tvCallForSupport.text = "Call Vehicle Owner"
        tvNavigateNow.visibility = View.GONE
    }

    @SuppressLint("SetTextI18n")
    fun populate(bookingReceivedItem: BookingRecievedItem) {
        bookingReceivedItem.apply {
            tvBookingId.text = bookedId.toString()
            tvStatus.text = bookingStatus
            when (bookingStatus) {
                BookingStatus.SCHEDULED.status -> {
                    tvStatus.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.holo_green_dark))
                    toggleVisibilityCallNavigate(true)
                }
                BookingStatus.CANCELLED.status, BookingStatus.PARKED_OUT.status -> {
                    tvStatus.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.holo_red_dark))
                    toggleVisibilityCallNavigate(false)
                }
                BookingStatus.PARKED_OUT.status -> {
                    tvStatus.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.holo_red_dark))
                    toggleVisibilityCallNavigate(true)
                }
                else -> {
                    tvStatus.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.black))
                    toggleVisibilityCallNavigate(true)
                }
            }
            tvBookedParkingAddress.text = parkingSpaceName + "\n" + parkingAddress

            tvCallForSupport.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:$phoneNo1")
                activity.startActivity(intent)
            }

            val dataList: ArrayList<TitleValuePojo> = arrayListOf()
            dataList.add(TitleValuePojo("Booking Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingDateTime)))
//            dataList.add(TitleValuePojo("End Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedOutTime)))
//            dataList.add(TitleValuePojo("Cash", "Rs. $baseAmount"))
            (rvBookingDetails.adapter as TitleValueAdaptor).populate(dataList)
        }
    }

    private fun toggleVisibilityCallNavigate(isVisible: Boolean) {
        if (isVisible) {
            tvCallForSupport.visibility = View.VISIBLE
            //tvNavigateNow.visibility = View.VISIBLE
        } else {
            tvCallForSupport.visibility = View.GONE
            //tvNavigateNow.visibility = View.GONE
        }
    }
}