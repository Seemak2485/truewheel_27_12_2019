package com.truewheels.user.viewholders

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.truewheels.user.R
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo

class TitleValueViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    private val tvTitle: TextView = itemView?.findViewById(R.id.tvTitle) as TextView
    private val tvValue: TextView = itemView?.findViewById(R.id.tvValue) as TextView

    fun populate(titleValuePojo: TitleValuePojo) {
        tvTitle.text = titleValuePojo.title
        tvValue.text = titleValuePojo.value
    }
}