package com.truewheels.user.viewholders

import android.annotation.SuppressLint
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.CheckBox
import android.widget.TextView
import com.truewheels.user.R
import com.truewheels.user.server.model.sys.select.SYSParkingItem

class SYSParkingListViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
    private val tvParingList: TextView = itemView?.findViewById(R.id.tvSysParkingName) as TextView
    private val tvSysParkingAddress: TextView = itemView?.findViewById(R.id.tvSysParkingAddress) as TextView
    private val tvSysParkingLandmark: TextView = itemView?.findViewById(R.id.tvSysParkingLandmark) as TextView
    val cbIsActive: CheckBox = itemView?.findViewById(R.id.cbIsActive) as CheckBox

    @SuppressLint("SetTextI18n")
    fun populate(parkingItem: SYSParkingItem) {
        tvParingList.text = parkingItem.parkSpaceName
        tvSysParkingAddress.text = parkingItem.parkingAddress
        tvSysParkingLandmark.text = parkingItem.propertyLandMark

        cbIsActive.isChecked = (parkingItem.active == "Y" || parkingItem.active == "A")
        cbIsActive.isEnabled = parkingItem.active != "P"
    }

}