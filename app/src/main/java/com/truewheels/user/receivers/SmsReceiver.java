package com.truewheels.user.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.truewheels.user.interfaces.SmsListener;

public class SmsReceiver extends BroadcastReceiver {

    private static SmsListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle data = intent.getExtras();

        Object[] pdus = (Object[]) data.get("pdus");

        try {
            for (int i = 0; i < pdus.length; i++) {
                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);

                //You must check here if the sender is your provider and not another one with same text.
                String sender = smsMessage.getDisplayOriginatingAddress();
                String messageBody = smsMessage.getMessageBody();
                String[] aSplited = messageBody.split("\\s+");
                String aOtp = aSplited[aSplited.length - 1];
                if (sender.contains("TruWhl")||sender.contains("TRUWHL")||sender.contains("TESTIN")) {
                    //Pass on the text to our listener.
                    mListener.messageReceived(aOtp);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void bindListener(SmsListener listener) {
        mListener = listener;
    }
}

