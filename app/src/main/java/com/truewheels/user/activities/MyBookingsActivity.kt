package com.truewheels.user.activities

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.MyBookingsAdaptor
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.parkings.booking_made.BookingMadeItem
import com.truewheels.user.server.model.parkings.booking_made.FetchBookingMadeResponse
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.BookingsMadeStringDateComparator
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.content_my_bookings.*
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class MyBookingsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_bookings)
        initToolbar()
        init()
        serviceFetchBookingMadeApi()
    }

    private fun init() {
        rvMyBookings.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = MyBookingsAdaptor(this@MyBookingsActivity)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
    }

    private fun serviceFetchBookingMadeApi() {
        showDialog()

        API.getClient().create(UserService::class.java).fetchBookingMade(
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<FetchBookingMadeResponse>> {
            override fun onResponse(call: retrofit2.Call<BaseResponse<FetchBookingMadeResponse>>?, response: Response<BaseResponse<FetchBookingMadeResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body()?.result!!.bookingMade!!.isEmpty()) {
                        tvEmptyBookingMade.visibility = View.VISIBLE
                        (rvMyBookings.adapter as MyBookingsAdaptor).populate(null)
                    } else {
                        tvEmptyBookingMade.visibility = View.GONE

                        (rvMyBookings.adapter as MyBookingsAdaptor)
                                .populate(sortList(response.body()?.result!!.bookingMade))
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: retrofit2.Call<BaseResponse<FetchBookingMadeResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun sortList(iBookingMade: List<BookingMadeItem>?): List<BookingMadeItem>? {
        Collections.sort(iBookingMade, BookingsMadeStringDateComparator())
        return iBookingMade
    }
}
