package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Handler

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import com.google.android.material.textfield.TextInputEditText
import com.google.zxing.WriterException
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.CouponAdapter
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.add_delete_vehicle.AddDeleteVehicleResponse
import com.truewheels.user.server.model.book_parking.book_now.BookNowPublicResponse
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.server.model.coupons.CouponResponse
import com.truewheels.user.server.model.coupons.CouponsModel
import com.truewheels.user.server.model.parkings.fetch_public_parking_detail.FetchPublicParkingDetailResponse
import com.truewheels.user.server.model.parkings.fetch_public_parking_detail.PublicParkingDetailItem
import com.truewheels.user.server.model.profile.FetchVehiclesResponse
import com.truewheels.user.server.model.profile.VehicleDetailsPojo
import com.truewheels.user.server.model.sys.select.*
import com.truewheels.user.server.model.sys.systemcode.SystemCodeResponse
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.Util
import com.truewheels.user.util.ValidationUtil
import kotlinx.android.synthetic.main.activity_public_parking_details.*
import kotlinx.android.synthetic.main.content_public_parking_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class PublicParkingDetailsActivity : BaseActivity(), CouponAdapter.CouponListener {

    private lateinit var vehicleDetails: ArrayList<VehicleDetailsPojo>
    var parkingDetailItem: PublicParkingDetailItem? = null
    var geoURI: String? = null
    private val vehicleToBeParkedArrayList: ArrayList<String> = ArrayList()
    private val helmetCountArrayList: ArrayList<String> = ArrayList()
    private val couponsArrayList: ArrayList<CouponsModel> = ArrayList()
    private val carParkingPlanList: ArrayList<String> = ArrayList()
    private val bikeParkingPlanList: ArrayList<String> = ArrayList()
    private var isCarVacancyEmpty: Boolean = false
    private var isBikeVacancyEmpty: Boolean = false
    private var isBikePlansEmpty: Boolean = false
    private var isCarPlansEmpty: Boolean = false
    private var mSelectedVehicleNo: String? = null
    private var mQrCodeString: String? = null
    private var mMonthlySubscription: String? = "0"
    private var mParkingList: String? = ""
    private var mVehicleWheels: String? = ""
    private var mCouponSelected: String? = ""
    private var mVehicleType: Int? = -1  // 0 for car and 1 for bike
    private var mHelmetCount: Int? = 0
    private var mQrEncoder: QRGEncoder? = null
    private var mBitmap: Bitmap? = null
    private var mCouponSelectedText: String? = ""
    private var mEtCouponText: TextInputEditText? = null

    private val ST_LDA = "ST_LDA"
    private val ST_NN = "ST_NNLKO"
    private var btnDone: Button? = null
    private var rvCouponList: RecyclerView? = null
    private var tvEmptyCoupons: TextView? = null
    private var aAdapter: CouponAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_public_parking_details)
        initToolbar()
        initListeners()

        initHelmetCountArray()

        if (StateType.getMap().isEmpty())
            serviceSystemCodeApi()
        else
            serviceFetchParkingDetailsApi()
    }

    private fun initHelmetCountArray() {
        helmetCountArrayList.add("1");
        helmetCountArrayList.add("2");
        helmetCountArrayList.add("3");
        helmetCountArrayList.add("4");
    }

    private fun initListeners() {
        mEtCouponText = findViewById(R.id.etSelectCoupon)

        ivDeletePublicCoupon.setOnClickListener(View.OnClickListener {
            mCouponSelected = ""
            mCouponSelectedText = ""
            mEtCouponText!!.setText("")
        })

        addNewVehicleInitialization()

        btnPublicParkingContinue.setOnClickListener {
            if (null == parkingDetailItem) {
                Util.showToast("Error fetching details. Please select this parking again")
                return@setOnClickListener
            }
            if (valid()) {
                if (parkingDetailItem!!.spaceType.equals(ST_LDA) || parkingDetailItem!!.spaceType.equals(ST_NN)) {
                    if (checkTime())
                        bookParking()
                    else {
                        AlertDialogHelper.showAlertDialog(this@PublicParkingDetailsActivity, "Alert!", "This parking will start at 7:00 am in the morning. Do you still want to continue?",
                                "Yes", "No", object : AlertDialogHelper.Callback {
                            override fun onSuccess() {
                                bookParking()
                            }

                            override fun onBack() {
                            }

                        })
                    }
                } else
                    bookParking()
            }
        }

        btnPublicParkingNavigateNow.setOnClickListener {
            if (null == geoURI) {
                Util.showToast("Error fetching details. Please select this parking again")
                return@setOnClickListener
            }
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(geoURI)))
        }

        tvParkingCharges.setOnClickListener(View.OnClickListener {
            val promptsView = LayoutInflater.from(this@PublicParkingDetailsActivity)
                    .inflate(R.layout.layout_charges_details, null)

            val alertDialogBuilder = AlertDialog.Builder(this@PublicParkingDetailsActivity)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            val btnClose = promptsView.findViewById<Button>(R.id.bClose)
            setCharges(promptsView)

            btnClose.setOnClickListener(View.OnClickListener {
                alertDialogBuilder.dismiss()
            })
            alertDialogBuilder.show()
        })

        setUpCouponSpinner()

        cbHelmet.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->

            if (cbHelmet.isChecked) {
                if (!mMonthlySubscription.equals("0")) {
                    val aAdb = AlertDialog.Builder(this)
                    aAdb.setTitle("Alert")
                            .setMessage("Helmet is not available for monthly parking. Do you still want to continue")
                            .setPositiveButton(R.string.yes) { dialogInterface, i ->
                                sParkingPlan.setSelection(0)
                                mHelmetCount = 1
                                dismissDialog()
                            }
                            .setNegativeButton(R.string.no) { dialogInterface, i ->
                                cbHelmet.isChecked = false
                                mHelmetCount = 0
                            }
                            .show()
                } else {
                    mHelmetCount = 1;
                }
            }
        })
    }

    private fun checkTime(): Boolean {
        val c = Calendar.getInstance()
        val timeOfDay = c.get(Calendar.HOUR_OF_DAY)

        var aStartTime = 7
        var aEndTime = 21

        try {
            if (!TextUtils.isEmpty(parkingDetailItem!!.bikeMonthlyNightCharge) && parkingDetailItem!!.bikeMonthlyNightCharge != "0")
                aStartTime = parkingDetailItem!!.bikeMonthlyNightCharge!!.toInt()
            if (!TextUtils.isEmpty(parkingDetailItem!!.carMonthlyNightCharge) && parkingDetailItem!!.carMonthlyNightCharge != "0")
                aEndTime = parkingDetailItem!!.carMonthlyNightCharge!!.toInt()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return (timeOfDay in aStartTime..aEndTime)
    }

    private fun valid(): Boolean {
        if (TextUtils.isEmpty(mSelectedVehicleNo)) {
            Util.showToast("Please select a vehicle to continue")
            return false
        }
//        else if (cbHelmet.isChecked && TextUtils.isEmpty(etHelmetCount.text)) {
//            Util.showToast("Please enter no. of helmets")
//            return false
//        }
//        else if (cbHelmet.isChecked && sHelmetCount.selectedItemPosition == 0) {
//            Util.showToast("Please enter no. of helmets")
//            return false
//        }
        else if ((isCarVacancyEmpty && mVehicleType == 0) || (isBikeVacancyEmpty && mVehicleType == 1)) {
            if (mVehicleType == 0)
                Util.showToast("No car vacancy left..")
            else
                Util.showToast("No bike vacancy left..")
            return false
        } else if ((isCarPlansEmpty && mVehicleType == 0) || (isBikePlansEmpty && mVehicleType == 1)) {
            if (mVehicleType == 0)
                Util.showToast("This parking doesn't have any car parking plans..")
            else
                Util.showToast("This parking doesn't have any bike parking plans..")
            return false
        }
        return true
    }

    private fun setParkingPlans() {
        carParkingPlanList.clear()
        bikeParkingPlanList.clear()

        if (!TextUtils.isEmpty(parkingDetailItem?.carMonthlyDayCharge))
            carParkingPlanList.add("Monthly Day")
        if (!TextUtils.isEmpty(parkingDetailItem?.carMonthlyNightCharge))
            carParkingPlanList.add("Monthly Night")
        if (!TextUtils.isEmpty(parkingDetailItem?.carMonthlyCharge))
            carParkingPlanList.add("Monthly Both")

        if (!TextUtils.isEmpty(parkingDetailItem?.bikeMonthlyDayCharge))
            bikeParkingPlanList.add("Monthly Day")
        if (!TextUtils.isEmpty(parkingDetailItem?.bikeMonthlyNightCharge))
            bikeParkingPlanList.add("Monthly Night")
        if (!TextUtils.isEmpty(parkingDetailItem?.bikeMonthlyCharge))
            bikeParkingPlanList.add("Monthly Both")
    }

    private fun setCharges(promptsView: View?) {
        val aCarBasicCharge = parkingDetailItem?.carBasicCharge

        val aBikeBasicCharge = parkingDetailItem?.bikeBasicCharge

        val aCarCharges = aCarBasicCharge!!.split(",")
        val aBikeCharges = aBikeBasicCharge!!.split(",")

        val aTvCarBasicOneHr = promptsView?.findViewById<TextView>(R.id.tvCarBasicOneHr)
        val aTvCarBasicSixHr = promptsView?.findViewById<TextView>(R.id.tvCarBasicSixHr)
        val aTvCarBasicFullDay = promptsView?.findViewById<TextView>(R.id.tvCarBasicFullDay)
        val aTvBikeBasicOneHr = promptsView?.findViewById<TextView>(R.id.tvBikeBasicOneHr)
        val aTvBikeBasicSixHr = promptsView?.findViewById<TextView>(R.id.tvBikeBasicSixHr)
        val aTvBikeBasicFullDay = promptsView?.findViewById<TextView>(R.id.tvBikeBasicFullDay)
        aTvCarBasicOneHr?.text = getCharges(aCarCharges, 0)
        aTvCarBasicSixHr?.text = getCharges(aCarCharges, 1)
        aTvCarBasicFullDay?.text = getCharges(aCarCharges, 2)
        aTvBikeBasicOneHr?.text = getCharges(aBikeCharges, 0)
        aTvBikeBasicSixHr?.text = getCharges(aBikeCharges, 1)
        aTvBikeBasicFullDay?.text = getCharges(aBikeCharges, 2)

        val aTvCarBasicNight = promptsView?.findViewById<TextView>(R.id.tvCarBasicFullNight)
        val aTvBikeBasicNight = promptsView?.findViewById<TextView>(R.id.tvBikeBasicFullNight)
        aTvCarBasicNight?.text = getCharges(parkingDetailItem?.carNightCharge)
        aTvBikeBasicNight?.text = getCharges(parkingDetailItem?.bikeNightCharge)

        val aTvCarMonthlyDay = promptsView?.findViewById<TextView>(R.id.tvCarMonthlyday)
        val aTvCarMonthlyNight = promptsView?.findViewById<TextView>(R.id.tvCarMonthlyNight)
        val aTvCarMonthlyBoth = promptsView?.findViewById<TextView>(R.id.tvCarMonthlyBoth)
        val aTvBikeMonthlyDay = promptsView?.findViewById<TextView>(R.id.tvBikeMonthlyday)
        val aTvBikeMonthlyNight = promptsView?.findViewById<TextView>(R.id.tvBikeMonthlyNight)
        val aTvBikeMonthlyBoth = promptsView?.findViewById<TextView>(R.id.tvBikeMonthlyBoth)
        aTvCarMonthlyDay?.text = getCharges(parkingDetailItem?.carMonthlyDayCharge)
        aTvCarMonthlyNight?.text = getCharges(parkingDetailItem?.carMonthlyNightCharge)
        aTvCarMonthlyBoth?.text = getCharges(parkingDetailItem?.carMonthlyCharge)
        aTvBikeMonthlyDay?.text = getCharges(parkingDetailItem?.bikeMonthlyDayCharge)
        aTvBikeMonthlyNight?.text = getCharges(parkingDetailItem?.bikeMonthlyNightCharge)
        aTvBikeMonthlyBoth?.text = getCharges(parkingDetailItem?.bikeMonthlyCharge)

        val aTvHelmetCHarge = promptsView?.findViewById<TextView>(R.id.tvHelmet)
        aTvHelmetCHarge?.text = getCharges(parkingDetailItem?.helmetCharge)
    }

    private fun getCharges(iCharges: List<String>, i: Int): String {
        if (iCharges.size > i && !TextUtils.isEmpty(iCharges.get(i)))
            return "Rs. " + iCharges.get(i)
        else
            return "NA"
    }

    private fun getCharges(iString: String?): String {
        if (iString != null && !TextUtils.isEmpty(iString))
            return "Rs. " + iString;
        else
            return "NA"
    }

    private fun setParkingCharges() {
        val aCarBasicCharge = parkingDetailItem?.carBasicCharge

        val aBikeBasicCharge = parkingDetailItem?.bikeBasicCharge

        val aCarCharges = aCarBasicCharge!!.split(",")
        val aBikeCharges = aBikeBasicCharge!!.split(",")

        val aTvCarBasicOneHr = findViewById<TextView>(R.id.tvCarBasicOneHr)
        val aTvCarBasicSixHr = findViewById<TextView>(R.id.tvCarBasicSixHr)
        val aTvCarBasicFullDay = findViewById<TextView>(R.id.tvCarBasicFullDay)
        val aTvBikeBasicOneHr = findViewById<TextView>(R.id.tvBikeBasicOneHr)
        val aTvBikeBasicSixHr = findViewById<TextView>(R.id.tvBikeBasicSixHr)
        val aTvBikeBasicFullDay = findViewById<TextView>(R.id.tvBikeBasicFullDay)
        aTvCarBasicOneHr?.text = getCharges(aCarCharges, 0)
        aTvCarBasicSixHr?.text = getCharges(aCarCharges, 1)
        aTvCarBasicFullDay?.text = getCharges(aCarCharges, 2)
        aTvBikeBasicOneHr?.text = getCharges(aBikeCharges, 0)
        aTvBikeBasicSixHr?.text = getCharges(aBikeCharges, 1)
        aTvBikeBasicFullDay?.text = getCharges(aBikeCharges, 2)

        val aTvCarBasicNight = findViewById<TextView>(R.id.tvCarBasicFullNight)
        val aTvBikeBasicNight = findViewById<TextView>(R.id.tvBikeBasicFullNight)
        aTvCarBasicNight?.text = getCharges(parkingDetailItem?.carNightCharge)
        aTvBikeBasicNight?.text = getCharges(parkingDetailItem?.bikeNightCharge)

        val aTvCarMonthlyDay = findViewById<TextView>(R.id.tvCarMonthlyday)
        val aTvCarMonthlyNight = findViewById<TextView>(R.id.tvCarMonthlyNight)
        val aTvCarMonthlyBoth = findViewById<TextView>(R.id.tvCarMonthlyBoth)
        val aTvBikeMonthlyDay = findViewById<TextView>(R.id.tvBikeMonthlyday)
        val aTvBikeMonthlyNight = findViewById<TextView>(R.id.tvBikeMonthlyNight)
        val aTvBikeMonthlyBoth = findViewById<TextView>(R.id.tvBikeMonthlyBoth)
        aTvCarMonthlyDay?.text = getCharges(parkingDetailItem?.carMonthlyDayCharge)
        aTvCarMonthlyNight?.text = getCharges(parkingDetailItem?.carMonthlyNightCharge)
        aTvCarMonthlyBoth?.text = getCharges(parkingDetailItem?.carMonthlyCharge)
        aTvBikeMonthlyDay?.text = getCharges(parkingDetailItem?.bikeMonthlyDayCharge)
        aTvBikeMonthlyNight?.text = getCharges(parkingDetailItem?.bikeMonthlyNightCharge)
        aTvBikeMonthlyBoth?.text = getCharges(parkingDetailItem?.bikeMonthlyCharge)

        val aTvHelmetCHarge = findViewById<TextView>(R.id.tvHelmet)
        aTvHelmetCHarge?.text = getCharges(parkingDetailItem?.helmetCharge)
    }

    private fun addNewVehicleInitialization() {
        tvAddNewVehiclePublic.setOnClickListener { v ->
            if (vehicleDetails.size >= 5) {
                Util.showToast("Cannot add more than 5 vehicles")
                return@setOnClickListener
            }
            val promptsView = LayoutInflater.from(v.context)
                    .inflate(R.layout.dialog_add_vehicle, null)

            val alertDialogBuilder = AlertDialog.Builder(v.context)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            val rbCar = promptsView.findViewById<RadioButton>(R.id.rbCar)
            val rbBike = promptsView.findViewById<RadioButton>(R.id.rbBike)
            val ivRadioCar = promptsView.findViewById<ImageView>(R.id.ivRadioCar)
            val ivRadioBike = promptsView.findViewById<ImageView>(R.id.ivRadioBike)
            val etDialogVehicleNo = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleNo)
            val etDialogVehicleModel = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleModel)
            val btnDialogAddVehicle = promptsView.findViewById<Button>(R.id.btnDialogAddVehicle)

            rbCar.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car)
                    rbBike.isChecked = false
                } else {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car_unchecked)
                }
            }
            rbBike.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_light)
                    rbCar.isChecked = false
                } else {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_unchecked)
                }
            }
            btnDialogAddVehicle.setOnClickListener {
                var isValid = true
                if (!rbCar.isChecked && !rbBike.isChecked) {
                    Util.showToast("Please select vehicle type")
                    isValid = false
                } else if (etDialogVehicleNo.text.toString().isEmpty()) {
                    Util.showToast("Please enter vehicle number")
                    isValid = false
                } else if (!ValidationUtil.isValidVehicleNo(etDialogVehicleNo.text.toString())) {
                    Util.showToast(getString(R.string.vehicle_number_prompt))
                    isValid = false
                }
//                else if (etDialogVehicleModel.getText().toString().isEmpty()) {
//                    Util.showToast("Please enter vehicle model");
//                    alertDialogBuilder.cancel()
//                    isValid = false
//                }

                if (isValid) {
                    serviceAddDeleteVehicleApi(true,
                            VehicleDetailsPojo(
                                    -1,
                                    etDialogVehicleNo.text.toString(),
                                    if (rbCar.isChecked) rbCar.text.toString() else rbBike.text.toString(),
                                    etDialogVehicleModel.text.toString()))
                    alertDialogBuilder.dismiss()
                }
            }
            alertDialogBuilder.show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun populate() {
        parkingDetailItem?.apply {
            tvParkingAddressPublic.text = propertyaddress
            if (null != intent.getStringExtra("distance")) {
                val distance: Double = intent.getStringExtra("distance").toDouble()
                tvParkingDistancePublic.text = String.format("%.2f KM away", distance)
            }
            try {
                if (null != spaceType && !spaceType.isEmpty()) {
                    tvParkingNamePublic.visibility = View.VISIBLE
                    tvParkingNamePublic.text = SpaceType.getMap().getValue(spaceType)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (!TextUtils.isEmpty(noOfSpace))
                tvVacantSpacesCar.text = noOfSpace
            else
                tvVacantSpacesCar.text = "0"

            if (!TextUtils.isEmpty(noOfSpaceBike))
                tvVacantSpacesBike.text = noOfSpaceBike
            else
                tvVacantSpacesBike.text = "0"

            isCarVacancyEmpty = (noOfSpace == "0") || (noOfSpace == "")
            isBikeVacancyEmpty = (noOfSpaceBike == "0") || (noOfSpaceBike == "")

            if (parkingDetailItem?.averageRating != null && !TextUtils.isEmpty(parkingDetailItem?.averageRating!!))
                rbParkingRating.rating = parkingDetailItem?.averageRating!!.toFloat()
            if (parkingDetailItem?.propertyVerifiedStatus != null && parkingDetailItem?.propertyVerifiedStatus.equals("Y"))
                tvVerifiedStatus.setText("Yes")
            else
                tvVerifiedStatus.setText("No")

        }
    }

    private fun serviceSystemCodeApi() {
        showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchSystemCodes()
                .enqueue(object : Callback<BaseResponse<SystemCodeResponse>> {
                    override fun onResponse(call: Call<BaseResponse<SystemCodeResponse>>?, response: Response<BaseResponse<SystemCodeResponse>>) {
                        dismissDialog()

                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                            response.body()?.result!!.apply {
                                for (systemCode in systemCodes) {
                                    when (systemCode?.category) {
                                        "StateType" -> StateType.setMap(systemCode.categoryProperty)
                                        "SpaceType" -> SpaceType.setMap(systemCode.categoryProperty)
                                        "PropertyType" -> PropertyType.setMap(systemCode.categoryProperty)
                                        "ParkingCategory" -> ParkingCategory.setMap(systemCode.categoryProperty)
                                        "Facility Type" -> FacilityType.setMap(systemCode.categoryProperty)
                                        "DescriptionType" -> DescriptionType.setMap(systemCode.categoryProperty)
                                        "DaysType" -> AvailableDayType.setMap(systemCode.categoryProperty)
                                    }
                                }
                            }

                            serviceFetchParkingDetailsApi()
                        } else {
                            if (null != response.body())
                                Util.showToast(response.body()!!.error)
                            else
                                Util.showToast("Error Code : ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<BaseResponse<SystemCodeResponse>>?, t: Throwable?) {
                        dismissDialog()
                    }
                })
    }

    private fun serviceFetchParkingDetailsApi() {
        showDialog()

        API.getClient().create(ParkingService::class.java).fetchPublicParkingDetails(
                intent.getStringExtra("parkingID")).enqueue(object : Callback<BaseResponse<FetchPublicParkingDetailResponse>> {
            override fun onResponse(call: Call<BaseResponse<FetchPublicParkingDetailResponse>>?, response: Response<BaseResponse<FetchPublicParkingDetailResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body()?.result!!.parkingDetail.size > 0)
                        response.body()?.result!!.parkingDetail[0].apply {
                            parkingDetailItem = this
                            geoURI = "${intent.getStringExtra("geoURI")} ($propertyaddress)"
                            if ("Y" == bookingAvailableFlag) {
                                btnPublicParkingContinue.visibility = View.VISIBLE
                                btnPublicParkingNavigateNow.visibility = View.GONE
                            } else {
                                btnPublicParkingContinue.visibility = View.GONE
                                sVehicleToBePublicParked.visibility = View.GONE
                                tvAddNewVehiclePublic.visibility = View.GONE
                                tvMonthlyDisclaimer.visibility = View.GONE
                                tvBookingDetails.visibility = View.GONE
                                vBottomDivider2.visibility = View.GONE
                                etSelectCoupon.visibility = View.GONE
                                ivDeletePublicCoupon.visibility = View.GONE
                                tvParkingCharges.visibility = View.INVISIBLE
                                clParkingDetails.visibility = View.VISIBLE

                                setParkingCharges()
                                btnPublicParkingNavigateNow.visibility = View.VISIBLE
                            }

                            if (parkingDetailItem!!.spaceType.equals(ST_LDA) || parkingDetailItem!!.spaceType.equals(ST_NN)) {
                                tvAvailableTimingsLabel.visibility = View.VISIBLE
                                tvAvailableTime.visibility = View.VISIBLE
                            }
                            if (TextUtils.isEmpty(parkingDetailItem?.carBasicCharge) && TextUtils.isEmpty(parkingDetailItem?.carNightCharge)
                                    && TextUtils.isEmpty(parkingDetailItem?.carMonthlyDayCharge)
                                    && TextUtils.isEmpty(parkingDetailItem?.carMonthlyNightCharge)
                                    && TextUtils.isEmpty(parkingDetailItem?.carMonthlyCharge)) {
                                isCarPlansEmpty = true;
                            }
                            if (TextUtils.isEmpty(parkingDetailItem?.bikeBasicCharge) && TextUtils.isEmpty(parkingDetailItem?.bikeNightCharge)
                                    && TextUtils.isEmpty(parkingDetailItem?.bikeMonthlyDayCharge)
                                    && TextUtils.isEmpty(parkingDetailItem?.bikeMonthlyNightCharge)
                                    && TextUtils.isEmpty(parkingDetailItem?.bikeMonthlyCharge)) {
                                isBikePlansEmpty = true;
                            }

                            populate()
                        }
                    initListeners()
                    serviceFetchVehiclesApi()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<FetchPublicParkingDetailResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun serviceAddDeleteVehicleApi(toAdd: Boolean, vehiclePojo: VehicleDetailsPojo) {
        showDialog()
        API.getClient().create(UserService::class.java).addDeleteVehicle(
                if (toAdd) "" else vehiclePojo.vehicleID.toString(),
                TVPreferences.getInstance().userId,
                if (toAdd) "1" else "0",
                vehiclePojo.vehicleNumber,
                vehiclePojo.vehicleModel,
                vehiclePojo.vehicleType
        ).enqueue(object : Callback<BaseResponse<AddDeleteVehicleResponse>> {
            override fun onResponse(call: Call<BaseResponse<AddDeleteVehicleResponse>>, response: Response<BaseResponse<AddDeleteVehicleResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    vehicleDetails = ArrayList()
                    vehicleToBeParkedArrayList.clear()

                    if (response.body() != null && response.body()?.result != null && response.body()?.result!!.userVehicleDetails?.size != 0) {
                        for ((vehicleID, vehicleNo, vehicleType, vehicleModel) in response.body()?.result!!.userVehicleDetails!!) {
                            if (vehicleNo!!.isEmpty())
                                continue
//                        if (isCarVacancyEmpty && "Four Wheeler" == vehicleType)
//                            continue
//                        if (isBikeVacancyEmpty && "Two Wheeler" == vehicleType)
//                            continue

                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleID.toInt(),
                                    vehicleNo,
                                    vehicleType!!,
                                    vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleNo)
                        }
                    }
                    initSpinners()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<AddDeleteVehicleResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    private fun initSpinners() {
        setupSpinner(sVehicleToBePublicParked, "SELECT VEHICLE", vehicleToBeParkedArrayList)

        sVehicleToBePublicParked.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                mMonthlySubscription = "0"
                if (position > 0) {
                    mSelectedVehicleNo = vehicleToBeParkedArrayList.get(position)
                    setParkingPlans()
                    if (vehicleDetails.get(position - 1).vehicleType.contains("Two", true)) {
                        if (TextUtils.isEmpty(parkingDetailItem?.helmetCharge) || parkingDetailItem?.helmetCharge == "0")
                            makeHelmetLayoutVisible(false)
                        else
                            makeHelmetLayoutVisible(true)
                        setupSpinner(sParkingPlan, "PARKING PLAN", bikeParkingPlanList)
                        mParkingList = "bike"
                        mVehicleType = 1
                    } else {
                        makeHelmetLayoutVisible(false)
                        setupSpinner(sParkingPlan, "PARKING PLAN", carParkingPlanList)
                        mParkingList = "car"
                        mVehicleType = 0
                    }
                } else {
                    mVehicleType = -1
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                return
            }
        }

//        sHelmetCount.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
//
//                when (position) {
//                    1 -> {
//                        mHelmetCount = 1;
//                    }
//                    2 -> {
//                        mHelmetCount = 2;
//                    }
//                    3 -> {
//                        mHelmetCount = 3;
//                    }
//                    4 -> {
//                        mHelmetCount = 4;
//                    }
//                    0 -> {
//                        mHelmetCount = 0;
//                    }
//                }
//            }
//
//            override fun onNothingSelected(parentView: AdapterView<*>?) {
//                return
//            }
//        }

        sParkingPlan.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {

                if (position != 0) {
                    if (mVehicleType == 0) {
                        if (carParkingPlanList.get(position).equals("Monthly Day")) {
                            mMonthlySubscription = "D"
                        } else if (carParkingPlanList.get(position).equals("Monthly Night")) {
                            mMonthlySubscription = "N"
                        } else if (carParkingPlanList.get(position).equals("Monthly Both")) {
                            mMonthlySubscription = "B"
                        } else {
                            mMonthlySubscription = "0"
                        }
                    } else if (mVehicleType == 1) {

                        if (bikeParkingPlanList.get(position).equals("Monthly Day")) {
                            mMonthlySubscription = "D"
                        } else if (bikeParkingPlanList.get(position).equals("Monthly Night")) {
                            mMonthlySubscription = "N"
                        } else if (bikeParkingPlanList.get(position).equals("Monthly Both")) {
                            mMonthlySubscription = "B"
                        } else {
                            mMonthlySubscription = "0"
                        }
                    }
                    if (cbHelmet.isChecked && !mMonthlySubscription.equals("0")) {
                        sParkingPlan.setSelection(0)
                        Util.showToast("Helmet option is not available for monthly bookings. If you want to book monthly, kindly uncheck the helmet option")
                    }
                } else {
                    mMonthlySubscription = "0"
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                return
            }
        }
    }

    private fun makeHelmetLayoutVisible(b: Boolean) {
        if (b) {
//            setupSpinner(`, "HELMET COUNT", helmetCountArrayList)
            cbHelmet.visibility = View.VISIBLE
//            etHelmetCount.visibility = View.VISIBLE
//            sHelmetCount.visibility = View.VISIBLE
        } else {
            cbHelmet.isChecked = false
//            etHelmetCount.setText("")
//            sHelmetCount.setSelection(0)
            cbHelmet.visibility = View.GONE
//            etHelmetCount.visibility = View.GONE
//            sHelmetCount.visibility = View.GONE
        }
    }

    private fun setUpCouponSpinner() {
        mEtCouponText?.setOnClickListener(View.OnClickListener {
            val promptsView = LayoutInflater.from(this@PublicParkingDetailsActivity)
                    .inflate(R.layout.layout_coupon_dialog, null)

            val alertDialogBuilder = AlertDialog.Builder(this@PublicParkingDetailsActivity)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            btnDone = promptsView.findViewById<Button>(R.id.bDone)
            rvCouponList = promptsView.findViewById<RecyclerView>(R.id.rvCouponsList)
            tvEmptyCoupons = promptsView.findViewById<TextView>(R.id.tvEmptyCoupons)

            aAdapter = CouponAdapter(this@PublicParkingDetailsActivity)
            aAdapter?.setOnCouponListener(this)
            rvCouponList?.apply {
                isNestedScrollingEnabled = false
                layoutManager = LinearLayoutManager(context)
                adapter = aAdapter
            }

            btnDone?.setOnClickListener(View.OnClickListener {
                alertDialogBuilder.dismiss()
                Handler().post(Runnable {
                    this@PublicParkingDetailsActivity.runOnUiThread(java.lang.Runnable {
                        mEtCouponText?.setText(mCouponSelectedText)
                    })
                })
            })
            alertDialogBuilder.show()
            getCoupons()
        })
    }

    fun setCouponDetails() {
        if (couponsArrayList.size > 0) {
            rvCouponList?.visibility = View.VISIBLE
            tvEmptyCoupons?.visibility = View.GONE
            aAdapter?.populate(couponsArrayList)
        } else {
            tvEmptyCoupons?.visibility = View.VISIBLE
            rvCouponList?.visibility = View.INVISIBLE
        }
    }

    override fun onCouponSelected(iCoupon: CouponsModel?) {
        mCouponSelected = iCoupon?.id
        mCouponSelectedText = iCoupon?.couponNo
    }


    private fun getCoupons() {
        showDialog()
        API.getClient().create(UserService::class.java).getCouponsForUser(
                TVPreferences.getInstance().userId,
                parkingDetailItem?.parkingId.toString()).enqueue(object : Callback<BaseResponse<CouponResponse>> {
            override fun onResponse(call: Call<BaseResponse<CouponResponse>>?, response: Response<BaseResponse<CouponResponse>>) {
                dismissDialog()
                couponsArrayList.clear()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body() != null && response.body()?.result != null)
                        response.body()?.result!!.apply {
                            couponsArrayList.addAll(availableCouponToUser)
                        }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
                setCouponDetails()
            }

            override fun onFailure(call: Call<BaseResponse<CouponResponse>>?, t: Throwable?) {
                dismissDialog()
                couponsArrayList.clear()
                setCouponDetails()
            }
        })
    }

    private fun setupSpinner(spinner: Spinner, title: String, arrayList: ArrayList<String>) {
        arrayList.add(0, title)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun serviceFetchVehiclesApi() {
        showDialog()
        API.getClient().create(UserService::class.java).fetchVehicles(
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<FetchVehiclesResponse>> {
            override fun onResponse(call: Call<BaseResponse<FetchVehiclesResponse>>?, response: Response<BaseResponse<FetchVehiclesResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()?.result!!.apply {
                        vehicleDetails = ArrayList()
                        vehicleToBeParkedArrayList.clear()

                        if (userDetails.isEmpty())
                            return@apply

                        for (userDetail in userDetails) {
                            val vehicleItem = userDetail.listVehicle!![0]
                            if (vehicleItem.vehicleNo!!.isEmpty())
                                continue
//                            if (isCarVacancyEmpty && "Four Wheeler" == vehicleItem.vehicleType)
//                                continue
//                            if (isBikeVacancyEmpty && "Two Wheeler" == vehicleItem.vehicleType)
//                                continue

                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleItem.VehicleID,
                                    vehicleItem.vehicleNo,
                                    vehicleItem.vehicleType!!,
                                    vehicleItem.vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleItem.vehicleNo)
                        }
                    }

                    initSpinners()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<FetchVehiclesResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun getInputValueForQrCode(): String {
        for (aVehicle in vehicleDetails) {
            if (aVehicle.vehicleNumber == mSelectedVehicleNo) {
                if (aVehicle.vehicleType.equals("Four Wheeler"))
                    mVehicleWheels = "4"
                else
                    mVehicleWheels = "2"
                break
            }
        }

        val iValue = mSelectedVehicleNo + "-" + parkingDetailItem?.parkingId + "-" + System.currentTimeMillis().toString()
        mQrCodeString = iValue

        return iValue
    }

    private fun getVehicleType(selectedVehicleNo: String): String {
        var selectedVehicleType = ""
        vehicleDetails.forEach {
            if (it.vehicleNumber == selectedVehicleNo) {
                selectedVehicleType = it.vehicleType
                return@forEach
            }
        }
        return selectedVehicleType
    }

    private fun generateQrCode(iInputValue: String, ivQrCode: ImageView) {
        mQrEncoder = QRGEncoder(iInputValue, null, QRGContents.Type.TEXT, 200);
        try {
            // Getting QR-Code as Bitmap
            mBitmap = mQrEncoder?.encodeAsBitmap();
            // Setting Bitmap to ImageView
            ivQrCode.setImageBitmap(mBitmap)
        } catch (e: WriterException) {
            Log.v(TAG, e.toString());
        }
    }

    private fun bookParking() {
        showDialog()
        getInputValueForQrCode()
        API.getClient().create(ParkingService::class.java).bookNowPublic(
                TVPreferences.getInstance().userId,
                intent.getStringExtra("parkingID"),
                mSelectedVehicleNo,
                mQrCodeString,
                mVehicleWheels!!,
                mMonthlySubscription,
                mHelmetCount!!,
                mCouponSelected
        ).enqueue(object : Callback<BaseResponse<BookNowPublicResponse>> {
            override fun onResponse(call: Call<BaseResponse<BookNowPublicResponse>>?, response: Response<BaseResponse<BookNowPublicResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()?.result!!.apply {
                        val promptsView = LayoutInflater.from(this@PublicParkingDetailsActivity)
                                .inflate(R.layout.dialog_congratulations, null)

                        val alertDialogBuilder = AlertDialog.Builder(this@PublicParkingDetailsActivity)
                                .setView(promptsView)
                                .setCancelable(false)
                                .create()

                        val btnNavigateNow = promptsView.findViewById<Button>(R.id.btnNavigateNow)
                        val btnNavigateLater = promptsView.findViewById<Button>(R.id.btnNavigateLater)
                        val ivQrCode = promptsView.findViewById<ImageView>(R.id.ivQrCode)
                        val rvBookedParkingDetails = promptsView.findViewById<RecyclerView>(R.id.rvBookedParkingDetails)
                        val ibParkingInfo = promptsView.findViewById<ImageButton>(R.id.ibPublicParkingInfo)

                        ivQrCode.visibility = View.VISIBLE

                        ibParkingInfo.setOnClickListener(View.OnClickListener {
                            AlertDialogHelper.showInfoAlertDialog(this@PublicParkingDetailsActivity,
                                    "THE QR code should be shown at the time of parking in the vehicle. Just go to “My booking” from widget bar and navigate to the desired parking , show the QR code, to parking attendant. \n" +
                                            "\n" +
                                            "Click on “CANCEL” scheduled parking or take SUPPORT and CHECK OUT or GET BILL for booking of private and public parking respectively from“My Booking” from widget bar side.\n" +
                                            "You can pay either by Cash or Paytm at the time of Checkout or Get Bill.\n" +
                                            "\n" +
                                            "Rate your parking experience and share us the views to move a level ahead.")
                        })

                        btnNavigateNow.setOnClickListener {
                            alertDialogBuilder.dismiss()
                            val geoURI: String = intent.getStringExtra("geoURI")
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(geoURI)))

                            val intent = Intent()
                            //intent.putExtra("shouldRefresh", true)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }
                        btnNavigateLater.setOnClickListener {
                            alertDialogBuilder.dismiss()
                            val intent = Intent(this@PublicParkingDetailsActivity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }

                        rvBookedParkingDetails.apply {
                            isNestedScrollingEnabled = false
                            layoutManager = LinearLayoutManager(context)
                            adapter = TitleValueAdaptor(this@PublicParkingDetailsActivity, R.layout.item_bold_title_value)
                        }

                        val dataList: List<TitleValuePojo> = arrayListOf(
                                TitleValuePojo("Booking ID:", bookingID!!))
//                                TitleValuePojo("Booking Time:", "Rs. 200"))
                        (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

                        generateQrCode(mQrCodeString!!, ivQrCode)
                        alertDialogBuilder.show()
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<BookNowPublicResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }
}
