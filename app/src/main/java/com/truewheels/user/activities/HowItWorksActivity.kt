package com.truewheels.user.activities

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.truewheels.user.R
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import kotlinx.android.synthetic.main.content_how_it_works.*

class HowItWorksActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_how_it_works)
        initToolbar()
        init()
    }

    private fun init() {
        rvHowItWorks.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@HowItWorksActivity, R.layout.item_title_description)
        }

        val dataList: List<TitleValuePojo> = arrayListOf(
                TitleValuePojo("How to Register for True wheel App?", "Step 1: Enter a valid mobile number and verify the OTP. \n" +
                        "Step 2: Sign up using Facebook, Gmail or manual sign up."))

        (rvHowItWorks.adapter as TitleValueAdaptor).populate(dataList)

        rvVehicleOwner.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@HowItWorksActivity, R.layout.item_title_description)
        }

        val vehicleDataList: List<TitleValuePojo> = arrayListOf(
                TitleValuePojo("How do I book parking?", "On home page there would be options to select either private or public parking. The booking is available for all private parking and selected public parking that would be shown in Red Color on Map."),
                TitleValuePojo("Public Parking:", "Booking is available for certain public parking which allows booking online parking. Select the public parking button in the bottom of the Map. Parking would be displayed on Map surroundings 3 KM area from your current or searched location. Select the parking and enter required details to book the parking."),
                TitleValuePojo("Private Parking:", "Booking is available for all Private parking. Parking location would be displayed on Map surroundings 3 KM area from your current or searched location. Just select the parking and enter the required details to book the parking."),
                TitleValuePojo("What are the booking status?", "There are 4 booking status. Description are given below:\n" +
                        "1) Scheduled: Booking done but the booked time not started.\n" +
                        "2) In Progress: Vehicle is parked inside the parking.\n" +
                        "3) Cancelled: If the booking is cancelled before the starting of booking time or actually parking the car.\n" +
                        "4) Parked Out: When the vehicle booking time is over.\n"),
                TitleValuePojo("How to cancel the booking?", "Open currently booked parking from “My Booking” option from the hamburger menu. If the parking status is scheduled then only you can cancel the parking. No Cancellation would be allowed if the booking status changes to “In-Progress”. For user convenience Cancellation is free."),
                TitleValuePojo("What are payment modes?", "You can pay the charges through cash or Paytm. Don’t worry if you paid more and parked for less time, we would refund it to your paytm wallet."),
                TitleValuePojo("How do I see my booked parking?", "Select the “My Booking” option from the hamburger menu icon.  All your bookings till date would be displayed in the list. You can open any booked entry from the list to view the details."),
                TitleValuePojo("How to use the QR code and where to find it?", "If you booked the parking through online mode. A QR code would be generated for you. You can find the QR code by selecting the booked parking in “My Booking”. Show the QR code at parking entrance to check in the parking."),
                TitleValuePojo("Why to press “Checkout” at parking exit?", "Pressing of checkout button would help you to pay the accurate amount and maintain proper billing. It also provide you an option to pay through Paytm."),
                TitleValuePojo("What is support option?", "Support option is for your convenience. You can raise a support ticket if you face any issue. We would resolve your concerns."))

        (rvVehicleOwner.adapter as TitleValueAdaptor).populate(vehicleDataList)


        rvParkingOwner.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@HowItWorksActivity, R.layout.item_title_description)
        }

        val parkingDataList: List<TitleValuePojo> = arrayListOf(
                TitleValuePojo("How to register a private parking space?", "Open the “Share you Space” option from the hamburger item list, and fill the form to register your space. Our agent will visit you for verification and activation of space within 24 hours. You need to have valid documents available related to you and your parking area for verification."),
                TitleValuePojo("How to I see/edit my registered space?", "Open the “My Space” from hamburger menu item list. You would get a list of all the registered parking till date. You can open and edit the parking details."),
                TitleValuePojo("How do I see the received booking?", "Open the “Booking Received” from the side list. All the booking you received till date would be visible there. You can also cancel the scheduled booking with proper reason and mark checkout to any ongoing booking, if the vehicle exit without checking out."),
                TitleValuePojo("What is support option?", "Support option is for your convenience. You can raise a support ticket if you face any issue. We would resolve your concerns."),
                TitleValuePojo("What to do in case of issue?", "In case of any issue you can use reach us option from side menu and send us message on whatsapp. We will get back to you immediately."))

        (rvParkingOwner.adapter as TitleValueAdaptor).populate(parkingDataList)
    }
}
