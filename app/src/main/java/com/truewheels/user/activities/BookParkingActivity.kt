package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.Editable
import android.text.TextPaint
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver
import android.widget.*
import com.google.android.material.textfield.TextInputEditText
import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.CouponAdapter
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.constants.Constants
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.helpers.CheckboxDialogHelper
import com.truewheels.user.helpers.MaterialDateTimeHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.add_delete_vehicle.AddDeleteVehicleResponse
import com.truewheels.user.server.model.book_parking.CalculateCostResponse
import com.truewheels.user.server.model.book_parking.CalculatedCostForBookNowPOJO
import com.truewheels.user.server.model.book_parking.book_now.BookNowResponse
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.server.model.coupons.CouponResponse
import com.truewheels.user.server.model.coupons.CouponsModel
import com.truewheels.user.server.model.parkings.FetchRWAParkingResponse
import com.truewheels.user.server.model.parkings.ParkingsItem
import com.truewheels.user.server.model.parkings.fetch_private_parking_detail.PrivateParkingDetailItem
import com.truewheels.user.server.model.paytm.Checksum
import com.truewheels.user.server.model.profile.FetchVehiclesResponse
import com.truewheels.user.server.model.profile.VehicleDetailsPojo
import com.truewheels.user.server.model.profile.VehicleType
import com.truewheels.user.server.model.sys.select.*
import com.truewheels.user.server.model.sys.systemcode.SystemCodeResponse
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.server.services.Paytm
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.DateUtil
import com.truewheels.user.util.PaytmUtil
import com.truewheels.user.util.Util
import com.truewheels.user.util.ValidationUtil
import com.truewheels.user.views.MyView
import kotlinx.android.synthetic.main.activity_book_parking.*
import kotlinx.android.synthetic.main.content_book_parking.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

@Suppress("ConvertToStringTemplate")
@SuppressLint("SetTextI18n")
class BookParkingActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener, CouponAdapter.CouponListener, MyView.OnToggledListener {
    var selectedidx:Int = -1
    var selectedidy:Int = -1
    var ParkingId:Int = -1
    override fun OnToggled(v: MyView?, touchOn: Boolean) {
        val idString = (v?.getIdX()).toString() + ":" + v?.getIdY()
        selectedidx= v?.idX!!
        selectedidy= v?.idY!!

           reinitGrid()
        Toast.makeText(this,
                "Toogled:\n" +
                idString + "\n" +
                touchOn,
            Toast.LENGTH_SHORT).show()
    }


    private lateinit var parkingDetailItemObj: PrivateParkingDetailItem
    private lateinit var vehicleDetails: ArrayList<VehicleDetailsPojo>
    private val vehicleToBeParkedArrayList: ArrayList<String> = ArrayList()
    private lateinit var facilitiesArr: BooleanArray
    private var calculatedCostForBookNowPojo = CalculatedCostForBookNowPOJO()
    private var isCarVacancyEmpty: Boolean = false
    private var isBikeVacancyEmpty: Boolean = false

    private var mTvPaytmBalance: TextView? = null
    private var duration: String? = ""
    private var mTotalAmount: String? = ""
    private var mPaymentMode: String? = ""

    private var btnDone: Button? = null
    private var rvCouponList: RecyclerView? = null
    private var tvEmptyCoupons: TextView? = null
    private var aAdapter: CouponAdapter? = null
    private val couponsArrayList: ArrayList<CouponsModel> = ArrayList()
    private val parkingArrayList: ArrayList<ParkingsItem?> = ArrayList()
    private var mCouponSelected: String? = ""
    private var mCouponSelectedText: String? = ""
    private lateinit var myViews: Array<MyView?>

    private lateinit var myGridLayout: GridLayout
    private lateinit var slots: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_parking)
        initToolbar()
        myGridLayout = findViewById<View>(R.id.mygrid) as GridLayout
        slots = findViewById<View>(R.id.slots) as TextView
        val carVacancy = intent.getStringExtra("carVacancy")
        isCarVacancyEmpty = (carVacancy == "0")
        val bikeVacancy = intent.getStringExtra("bikeVacancy")
        isBikeVacancyEmpty = (bikeVacancy == "0")

        serviceFetchVehiclesApi()
    }

    private fun init() {
        parkingDetailItemObj = intent.getSerializableExtra("parkingDetailItemObj") as PrivateParkingDetailItem
        getParkings()
        tvAddNewVehicle.setOnClickListener { v ->
            if (vehicleDetails.size >= 5) {
                Util.showToast("Cannot add more than 5 vehicles")
                return@setOnClickListener
            }
            val promptsView = LayoutInflater.from(v.context)
                    .inflate(R.layout.dialog_add_vehicle, null)

            val alertDialogBuilder = AlertDialog.Builder(v.context)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            val rbCar = promptsView.findViewById<RadioButton>(R.id.rbCar)
            val rbBike = promptsView.findViewById<RadioButton>(R.id.rbBike)
            val ivRadioCar = promptsView.findViewById<ImageView>(R.id.ivRadioCar)
            val ivRadioBike = promptsView.findViewById<ImageView>(R.id.ivRadioBike)
            val etDialogVehicleNo = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleNo)
            val etDialogVehicleModel = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleModel)
            val btnDialogAddVehicle = promptsView.findViewById<Button>(R.id.btnDialogAddVehicle)

            rbCar.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car)
                    rbBike.isChecked = false
                } else {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car_unchecked)
                }
            }
            rbBike.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_light)
                    rbCar.isChecked = false
                } else {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_unchecked)
                }
                // tn11z7860
                // tn11z6570 bike
            }
            btnDialogAddVehicle.setOnClickListener {
                var isValid = true
                if (!rbCar.isChecked && !rbBike.isChecked) {
                    Util.showToast("Please select vehicle type")
                    isValid = false
                } else if (etDialogVehicleNo.text.toString().isEmpty()) {
                    Util.showToast("Please enter vehicle number")
                    isValid = false
                } else if (!ValidationUtil.isValidVehicleNo(etDialogVehicleNo.text.toString())) {
                    Util.showToast(getString(R.string.vehicle_number_prompt))
                    isValid = false
                }

                if (isValid) {
                    serviceAddDeleteVehicleApi(true,
                            VehicleDetailsPojo(
                                    -1,
                                    etDialogVehicleNo.text.toString(),
                                    if (rbCar.isChecked) rbCar.text.toString() else rbBike.text.toString(),
                                    etDialogVehicleModel.text.toString()))
                    alertDialogBuilder.dismiss()
                }
            }
            alertDialogBuilder.show()
        }
        btnEstimatedCost.setOnClickListener {
            if (isFormValid())
                serviceCalculateCostApi()
        }
        btnParkingBookNow.setOnClickListener {
            showPaymentDialog(parkingDetailItemObj)
//            serviceBookNowApi()
        }
        rvAmountDenomination.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context) as RecyclerView.LayoutManager?
            adapter = TitleValueAdaptor(this@BookParkingActivity, R.layout.item_title_value)
        }

        etStartDate.setOnClickListener { MaterialDateTimeHelper.createDatePickerDialog(this@BookParkingActivity, "Start Date?", etStartDate, true) }
        etStartTime.setOnClickListener { MaterialDateTimeHelper.createTimePickerDialog(this@BookParkingActivity, "Start Time?", etStartTime) }
        etEndDate.setOnClickListener { MaterialDateTimeHelper.createDatePickerDialog(this@BookParkingActivity, "End Date?", etEndDate, true) }
        etEndTime.setOnClickListener { MaterialDateTimeHelper.createTimePickerDialog(this@BookParkingActivity, "End Time?", etEndTime) }

        initSpinners()
        initCheckboxGroups()

        sVehicleToBeParked.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                val parkingModeArrayList: ArrayList<String> = arrayListOf()

                if (position != 0) {
                    when (VehicleType.getSwappedMap()[vehicleDetails[position - 1].vehicleType]) {
                        "2" -> {
                            if (!parkingDetailItemObj.bikeBasicCharge.isNullOrEmpty() && parkingDetailItemObj.bikeBasicCharge != "NA")
                                parkingModeArrayList.add("Hourly")
                            if (!parkingDetailItemObj.bikedailycharge.isNullOrEmpty() && parkingDetailItemObj.bikedailycharge != "NA")
                                parkingModeArrayList.add("Daily")
                            if (!parkingDetailItemObj.bikemonthycharge.isNullOrEmpty() && parkingDetailItemObj.bikemonthycharge != "NA")
                                parkingModeArrayList.add("Monthly")

                            if (isBikeVacancyEmpty)
                                Util.showToast("No vacancy for bike available")
                            else {
                                if (parkingModeArrayList.size == 0)
                                    Util.showToast("No mode available for bike")
                                else
                                    setupSpinner(sParkingMode, "SELECT MODE", parkingModeArrayList)
                            }
                        }
                        "4" -> {
                            if (!parkingDetailItemObj.carBasicCharge.isNullOrEmpty() && parkingDetailItemObj.carBasicCharge != "NA")
                                parkingModeArrayList.add("Hourly")
                            if (!parkingDetailItemObj.carDailyCharge.isNullOrEmpty() && parkingDetailItemObj.carDailyCharge != "NA")
                                parkingModeArrayList.add("Daily")
                            if (!parkingDetailItemObj.carmonthlycharge.isNullOrEmpty() && parkingDetailItemObj.carmonthlycharge != "NA")
                                parkingModeArrayList.add("Monthly")

                            if (isCarVacancyEmpty)
                                Util.showToast("No vacancy for car available")
                            else {
                                if (parkingModeArrayList.size == 0)
                                    Util.showToast("No mode available for car")
                                else
                                    setupSpinner(sParkingMode, "SELECT MODE", parkingModeArrayList)
                            }
                        }
                    }
                }

                toggleVisibilityBookOrEstimate(true)
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                return
            }

        }
        sParkingMode.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                toggleVisibilityBookOrEstimate(true)
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                return
            }

        }
        val watcher: TextWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                return
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                return
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                toggleVisibilityBookOrEstimate(true)
            }

        }
        etStartDate.addTextChangedListener(watcher)
        etStartTime.addTextChangedListener(watcher)
        etEndDate.addTextChangedListener(watcher)
        etEndTime.addTextChangedListener(watcher)

        ivDeleteCoupon.setOnClickListener(View.OnClickListener {
            if (!TextUtils.isEmpty(mCouponSelected))
                toggleVisibilityBookOrEstimate(true)
            mCouponSelected = ""
            mCouponSelectedText = ""
            etSelectPrivateCoupon.setText("")
        })

        ibEstimateCostInfo.setOnClickListener(View.OnClickListener {
            AlertDialogHelper.showInfoAlertDialog(this@BookParkingActivity,
                    "Take a estimate of your booking:-\n" +
                            "\n" +
                            "Select a vehicle :- choose your registered vehicle in app and make sure you must go with same vehicle at parking. \n" +
                            "\n" +
                            "Select Mode:- In Hourly and Daily you can comapre the prices and book , as per the you requirement of parking timings once you checkout from parking , with in few min or with in few hour for Hourly and Daily respectively . \n" +
                            "You will not be allowed to parking on same Booking.\n" +
                            "\n" +
                            "For Monthly mode the booking would be valid for 30 days , you can move in and out from parking as many time as you want within your plan of 30 days. Booking will be valid for 30 days.\n" +
                            "\n" +
                            "Timings:- Your entered start and End time and Days which you selected for parking would be as per availability of timings and days of that parking . Otherwise system will restrict you to book that.\n" +
                            "\n" +
                            "Coupon :- Apply coupon to get the discount if available.\n" +
                            "\n" +
                            "Pay through PayTM :- To get the sure slot for your parking , in case of cash you might be not entertain by the owner in case if you not reached at parking on time. \n" +
                            "Depends on Parking Owner")
        })
        setUpCouponSpinner()
    }

    private fun setUpCouponSpinner() {
        etSelectPrivateCoupon.setOnClickListener(View.OnClickListener {
            val promptsView = LayoutInflater.from(this@BookParkingActivity)
                    .inflate(R.layout.layout_coupon_dialog, null)

            val alertDialogBuilder = AlertDialog.Builder(this@BookParkingActivity)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            btnDone = promptsView.findViewById<Button>(R.id.bDone)
            rvCouponList = promptsView.findViewById<RecyclerView>(R.id.rvCouponsList)
            tvEmptyCoupons = promptsView.findViewById<TextView>(R.id.tvEmptyCoupons)

            aAdapter = CouponAdapter(this@BookParkingActivity)
            aAdapter?.setOnCouponListener(this)
            rvCouponList?.apply {
                isNestedScrollingEnabled = false
                layoutManager = LinearLayoutManager(context)
                adapter = aAdapter
            }

            btnDone?.setOnClickListener(View.OnClickListener {
                alertDialogBuilder.dismiss()
                Handler().postDelayed(Runnable {
                    etSelectPrivateCoupon.setText(mCouponSelectedText)
                }, 300)
            })
            alertDialogBuilder.show()

            getCoupons()

        })
    }

    private fun getCoupons() {
        showDialog()
        API.getClient().create(UserService::class.java).getCouponsForUser(
                TVPreferences.getInstance().userId,
                parkingDetailItemObj?.parkingId.toString()).enqueue(object : Callback<BaseResponse<CouponResponse>> {
            override fun onResponse(call: Call<BaseResponse<CouponResponse>>?, response: Response<BaseResponse<CouponResponse>>) {
                dismissDialog()
                couponsArrayList.clear()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body() != null && response.body()?.result != null)
                        response.body()?.result!!.apply {
                            couponsArrayList.addAll(availableCouponToUser)
                        }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
                setCouponDetails()
            }

            override fun onFailure(call: Call<BaseResponse<CouponResponse>>?, t: Throwable?) {
                dismissDialog()
                couponsArrayList.clear()
                setCouponDetails()
            }
        })
    }


    private fun getParkings() {
        showDialog()
        API.getClient().create(ParkingService::class.java).fetchRWAParking(
                intent.getStringExtra("parkingID")).enqueue(object : Callback<BaseResponse<FetchRWAParkingResponse>> {
            override fun onResponse(call: Call<BaseResponse<FetchRWAParkingResponse>>?, response: Response<BaseResponse<FetchRWAParkingResponse>>) {
                dismissDialog()
                parkingArrayList.clear()
                myGridLayout = findViewById<View>(R.id.mygrid) as GridLayout
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body() != null && response.body()?.result != null)
                        response.body()?.result!!.apply {
                             parkingArrayList.addAll(parkings)
                            myGridLayout.visibility= View.VISIBLE
                            slots.visibility = View.VISIBLE


                        }
                    setGrid()
                } else {
                    myGridLayout.visibility= View.GONE
                    slots.visibility = View.GONE
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }


            }

            override fun onFailure(call: Call<BaseResponse<FetchRWAParkingResponse>>?, t: Throwable?) {
                dismissDialog()
                myGridLayout.visibility= View.GONE
                parkingArrayList.clear()
                //setCouponDetails()
            }
        })
    }

    fun reinitGrid()
    {
        if(parkingArrayList.size<=4) {
            myGridLayout.rowCount = 1
            myGridLayout.columnCount = parkingArrayList.size
        }
        else
        {
            if(parkingArrayList.size%4==0)
                myGridLayout.rowCount =parkingArrayList.size/4
            else
                myGridLayout.rowCount =parkingArrayList.size/4 +1
        }

        var s = parkingArrayList.size
        var numOfCol: Int = myGridLayout.columnCount
        val numOfRow: Int = myGridLayout.rowCount
        myViews = arrayOfNulls(numOfCol * numOfRow)
        for (yPos in 0 until numOfRow) {
            if(yPos>0) {
                s= s-4
                if(s<=4)
                    numOfCol = s
                else
                    numOfCol=4
            }
            for (xPos in 0 until numOfCol) {
                myViews[yPos * numOfCol + xPos] = myGridLayout.getChildAt(yPos * numOfCol + xPos) as MyView?
                if(xPos==selectedidx&&yPos==selectedidy) {
                    myViews[yPos * numOfCol + xPos]?.setTouch(true)

                    ParkingId = parkingArrayList[yPos * numOfCol + xPos]?.parkingId!!
                }
                else
                    myViews[yPos * numOfCol + xPos]?.setTouch(false)

                myGridLayout.invalidate()

            }
        }
    }



    fun setGrid()
    {


        if(parkingArrayList.size<=4) {
            myGridLayout.rowCount = 1
            myGridLayout.columnCount = parkingArrayList.size
        }
        else
        {
            if(parkingArrayList.size%4==0)
            myGridLayout.rowCount =parkingArrayList.size/4
            else
                myGridLayout.rowCount =parkingArrayList.size/4 +1
        }

        var s = parkingArrayList.size
        var numOfCol: Int = myGridLayout.columnCount
        val numOfRow: Int = myGridLayout.rowCount
        myViews = arrayOfNulls(numOfCol * numOfRow)
        for (yPos in 0 until numOfRow) {
            if(yPos>0) {
                s= s-4
                if(s<=4)
                numOfCol = s
                else
                    numOfCol=4
            }
            for (xPos in 0 until numOfCol) {
                var tView = MyView(this, xPos, yPos,parkingArrayList.get(yPos * numOfCol + xPos)?.spacename)
               /* if(xPos==selectedidx &&yPos ==selectedidy)
                tView = MyView(this, xPos, yPos,true)+
*/
                tView.setOnToggledListener(this)
                myViews[yPos * numOfCol + xPos] = tView
                myGridLayout.addView(tView)
            }
        }

        myGridLayout.viewTreeObserver.addOnGlobalLayoutListener {
            val MARGIN = 5

            val pWidth = myGridLayout.width
            val pHeight = myGridLayout.height
           // myGridLayout.rowCount=6
            val numOfCol = myGridLayout.columnCount
            val numOfRow = myGridLayout.rowCount
            val w = pWidth / numOfCol
            val h = pHeight / numOfRow

            for (yPos in 0 until numOfRow) {
                for (xPos in 0 until numOfCol) {
                    val params = myViews[yPos * numOfCol + xPos]?.layoutParams as GridLayout.LayoutParams
                    params.width = w - 2 * MARGIN
                    params.height = h - 2 * MARGIN
                    params.setMargins(MARGIN, MARGIN, MARGIN, MARGIN)
                    myViews[yPos * numOfCol + xPos]?.layoutParams = params
                }
            }
        }



    }

    fun setCouponDetails() {
        if (couponsArrayList.size > 0) {
            rvCouponList?.visibility = View.VISIBLE
            tvEmptyCoupons?.visibility = View.GONE
            aAdapter?.populate(couponsArrayList)
        } else {
            tvEmptyCoupons?.visibility = View.VISIBLE
            rvCouponList?.visibility = View.INVISIBLE
        }
    }

    override fun onCouponSelected(iCoupon: CouponsModel?) {
        mCouponSelected = iCoupon?.id
        mCouponSelectedText = iCoupon?.couponNo
        toggleVisibilityBookOrEstimate(true)
    }

    private fun showPaymentDialog(iParkingDetailItemObj: PrivateParkingDetailItem) {

        val promptsView = LayoutInflater.from(this@BookParkingActivity)
                .inflate(R.layout.payment_dialog, null)

        val alertDialogBuilder = AlertDialog.Builder(this@BookParkingActivity)
                .setView(promptsView)
                .setCancelable(true)
                .create()

        val aBPayNow = promptsView.findViewById<Button>(R.id.bPayNow)
        val aRbCash = promptsView.findViewById<RadioButton>(R.id.rbCash)
        val aRbPaytm = promptsView.findViewById<RadioButton>(R.id.rbPaytm)
        mTvPaytmBalance = promptsView.findViewById<TextView>(R.id.tvPaytmBal)
        val rvBookedParkingDetails = promptsView.findViewById<RecyclerView>(R.id.rvPaymentDetailsList)

        aRbCash.setOnCheckedChangeListener(this)
        aRbPaytm.setOnCheckedChangeListener(this)

        aBPayNow.setOnClickListener {
            if (!TextUtils.isEmpty(mPaymentMode)) {
                if (aRbPaytm.isChecked) {
                    getCheckSum(getBookingId(iParkingDetailItemObj)!!, (TVPreferences.getInstance().userId), alertDialogBuilder)
                } else {
                    payByCash(alertDialogBuilder)
                }
            } else
                Util.showToast("Please select a payment mode to continue")
        }


        rvBookedParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookParkingActivity, R.layout.item_bold_title_value)
        }

        val dataList: List<TitleValuePojo> = arrayListOf(
                TitleValuePojo("Duration:", duration!!),
                TitleValuePojo("Total cost:", mTotalAmount!!))
        (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

        alertDialogBuilder.show()

        alertDialogBuilder.setOnCancelListener(DialogInterface.OnCancelListener {
            mPaymentMode = ""
        })
    }

    private fun payByCash(iAlertDialogBuilder: AlertDialog) {
        serviceBookNowApi(iAlertDialogBuilder)
    }

    private fun getBookingId(iBookingId: PrivateParkingDetailItem): String? {
        return (iBookingId.parkingId.toString() + System.currentTimeMillis())
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        if (p1) {
            if (p0?.id == R.id.rbCash) {
                mTvPaytmBalance?.visibility = View.GONE
                mPaymentMode = "COD"
            } else if (p0?.id == R.id.rbPaytm) {
                mPaymentMode = "PAYTM"
                getPaytmBalForUser()
            }
        }
    }

    private fun getPaytmBalForUser() {
        mTvPaytmBalance?.text = "PAYTM balance: "
    }

    private fun initSpinners() {
        setupSpinner(sVehicleToBeParked, "SELECT VEHICLE", vehicleToBeParkedArrayList)

        val parkingModeArrayList: ArrayList<String> = arrayListOf("Hourly", "Daily", "Monthly")
        setupSpinner(sParkingMode, "SELECT MODE", parkingModeArrayList)
    }

    private fun setupSpinner(spinner: Spinner, title: String, arrayList: ArrayList<String>) {
        arrayList.add(0, title)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun initCheckboxGroups() {
        val facilitiesArrayList: ArrayList<String> = ArrayList(FacilityType.getMap().values)
        facilitiesArr = BooleanArray(facilitiesArrayList.size)
        etParkingFacilities.setOnClickListener { CheckboxDialogHelper.createCheckboxDialog(this@BookParkingActivity, etParkingFacilities, facilitiesArrayList, facilitiesArr) }
    }

    private fun isFormValid(): Boolean {
        var formValidity = true

        when {
            etStartDate.text.isEmpty() -> {
                Util.showToast("Enter start date")
                formValidity = false
            }
            etEndDate.text.isEmpty() -> {
                Util.showToast("Enter end date")
                formValidity = false
            }
            etStartTime.text.isEmpty() -> {
                Util.showToast("Enter start time")
                formValidity = false
            }
            etEndTime.text.isEmpty() -> {
                Util.showToast("Enter end time")
                formValidity = false
            }

            DateUtil.compareDateWithSystemDate("${etStartDate.text} ${etStartTime.text}",
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") < 0 -> {
                Util.showToast("Please enter upcoming time as start time")
                formValidity = false
            }

            DateUtil.compareUserDates("${etStartDate.text} ${etStartTime.text}",
                    "${etEndDate.text} ${etEndTime.text}",
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") < 0 -> {
                Util.showToast("End time should be after start time")
                formValidity = false
            }
            "${etStartDate.text} ${etStartTime.text}".equals("${etEndDate.text} ${etEndTime.text}") -> {
                Util.showToast("End time should be greater than start time")
                formValidity = false
            }
            sParkingMode.selectedItem.toString().substring(0, 1) == "H" &&
                    DateUtil.compareDates(etStartDate.text.toString() + " " + etStartTime.text.toString(),
                            etEndDate.text.toString() + " " + etEndTime.text.toString(),
                            "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") < 0 -> {
                Util.showToast("Enter correct end date time")
                formValidity = false
            }
            sParkingMode.selectedItemPosition == 0 -> {
                Util.showToast("Please select a parking mode")
                formValidity = false
            }
            sVehicleToBeParked.selectedItemPosition == 0 -> {
                Util.showToast("Please select a vehicle ")
                formValidity = false
            }
        // input consistency validations

            sParkingMode.selectedItem.toString().substring(0, 1) == "M" &&
                    DateUtil.getDifferenceInSeconds("${etStartDate.text} ${etStartTime.text}",
                            "${etEndDate.text} ${etEndTime.text}",
                            "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") < 60 * 60 * 24 * 30 -> {
                Util.showToast("In monthly mode, minimum 30 days duration is required")
                formValidity = false
            }

            !isDateRangeValid(etStartDate.text.toString(), etEndDate.text.toString()) -> {
                Util.showToast("This parking is not valid on selected days")
                formValidity = false
            }

            parkingDetailItemObj.timeFrom.isNotEmpty() && parkingDetailItemObj.timeTo.isNotEmpty() &&
                    DateUtil.compareStartAndEndDates(
                            parkingDetailItemObj.timeFrom,
                            parkingDetailItemObj.timeTo,
                            etStartTime.text.toString(),
                            etEndTime.text.toString(),
                            DateUtil.FORMAT_READABLE_TIME) < 0 -> {
                Util.showToast("Parking is not available at you selected timings. Please change the time and try again.")
                formValidity = false
            }

            checkDate(formValidity) < 0 -> {
                formValidity = false
                Util.showToast("Parking is not available at you selected timings. Please change the date and time and try again.")
            }
//            parkingDetailItemObj.timeTo.isNotEmpty() &&
//                    DateUtil.getDifferenceInSeconds(etEndTime.text.toString(), parkingDetailItemObj.timeFrom,
//                            parkingDetailItemObj.timeTo, DateUtil.FORMAT_READABLE_TIME) < 0 -> {
//                Util.showToast("This parking must be scheduled 30 minutes before its end time (${parkingDetailItemObj.timeTo})")
//                formValidity = false
//            }

            DateUtil.getDifferenceInSeconds(
                    DateUtil.getCurrentDate("${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}"),
                    "${etStartDate.text} ${etStartTime.text}",
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") > 5 * 24 * 60 * 60 -> {
                Util.showToast("Pre-booking after 5 days from now is not allowed")
                formValidity = false
            }
        }

        return formValidity
    }

    private fun checkDate(formValidity: Boolean): Int {
        if (formValidity) {
            if (parkingDetailItemObj.timeFrom.isEmpty() && parkingDetailItemObj.timeTo.isEmpty())
                return 1
            else if (!etStartDate.text.toString().equals(etEndDate.text.toString())) {
                return DateUtil.compareUserDates("${etEndDate.text} ${etEndTime.text}",
                        "${etEndDate.text} ${parkingDetailItemObj.timeTo}",
                        "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}")
            }
        }
        return 1;
    }

    private fun isDateRangeValid(startDateString: String, endDateString: String): Boolean {
        var startDate = DateUtil.getDate(DateUtil.FORMAT_READABLE_DATE, startDateString)
        val endDate = DateUtil.getDate(DateUtil.FORMAT_READABLE_DATE, endDateString)
        var result = true
        var count = 0

        val primeProduct = intent.getIntExtra("primeProduct", 19)
        val calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat(DateUtil.FORMAT_READABLE_DATE, Locale.getDefault())

        while (startDate <= endDate && count++ < 7) {
            // processing
            val dayOfWeek = DateUtil.convertFormat(
                    DateUtil.FORMAT_READABLE_DATE,
                    DateUtil.FORMAT_READABLE_DAY_OF_WEEK,
                    DateUtil.getDateAsString(DateUtil.FORMAT_READABLE_DATE, startDate))
                    .toUpperCase().substring(0, 2)

            val primeCode = AvailableDayType.getMapValue().getValue("D_$dayOfWeek")
            if (primeProduct % primeCode != 0) {
                result = false
                break
            }
            // adding one more day
            calendar.time = startDate
            calendar.add(Calendar.DAY_OF_YEAR, 1)
            val dateString = dateFormat.format(calendar.time)
            startDate = DateUtil.getDate(DateUtil.FORMAT_READABLE_DATE, dateString)
        }

        return result
    }

    private fun serviceAddDeleteVehicleApi(toAdd: Boolean, vehiclePojo: VehicleDetailsPojo) {
        showDialog()
        API.getClient().create(UserService::class.java).addDeleteVehicle(
                if (toAdd) "" else vehiclePojo.vehicleID.toString(),
                TVPreferences.getInstance().userId,
                if (toAdd) "1" else "0",
                vehiclePojo.vehicleNumber,
                vehiclePojo.vehicleModel,
                vehiclePojo.vehicleType
        ).enqueue(object : Callback<BaseResponse<AddDeleteVehicleResponse>> {
            override fun onResponse(call: Call<BaseResponse<AddDeleteVehicleResponse>>, response: Response<BaseResponse<AddDeleteVehicleResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    vehicleDetails = ArrayList()
                    vehicleToBeParkedArrayList.clear()

                    if (response.body() != null && response.body()?.result != null && response.body()?.result!!.userVehicleDetails?.size != 0) {
                        for ((vehicleID, vehicleNo, vehicleType, vehicleModel) in response.body()?.result!!.userVehicleDetails!!) {
                            if (vehicleNo!!.isEmpty())
                                continue
//                            if (isCarVacancyEmpty && "Four Wheeler" == vehicleType)
//                                continue
//                            if (isBikeVacancyEmpty && "Two Wheeler" == vehicleType)
//                                continue

                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleID.toInt(),
                                    vehicleNo,
                                    vehicleType!!,
                                    vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleNo)
                        }
                    }

                    initSpinners()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<AddDeleteVehicleResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    private fun serviceFetchVehiclesApi() {
        showDialog()

        API.getClient().create(UserService::class.java).fetchVehicles(
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<FetchVehiclesResponse>> {
            override fun onResponse(call: Call<BaseResponse<FetchVehiclesResponse>>?, response: Response<BaseResponse<FetchVehiclesResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()?.result!!.apply {
                        vehicleDetails = ArrayList()
                        vehicleToBeParkedArrayList.clear()

                        if (userDetails.isEmpty())
                            return@apply

                        for (userDetail in userDetails) {
                            val vehicleItem = userDetail.listVehicle!![0]
                            if (vehicleItem.vehicleNo!!.isEmpty())
                                continue
                            if (isCarVacancyEmpty && "Four Wheeler" == vehicleItem.vehicleType)
                                continue
                            if (isBikeVacancyEmpty && "Two Wheeler" == vehicleItem.vehicleType)
                                continue

                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleItem.VehicleID,
                                    vehicleItem.vehicleNo,
                                    vehicleItem.vehicleType!!,
                                    vehicleItem.vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleItem.vehicleNo)
                        }

                        /*FIXME old response

                        for (vehicleItem in userDetails[0].listVehicle!!) {
                            if (vehicleItem.vehicleNo!!.isEmpty())
                                continue
                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleItem.VehicleID,
                                    vehicleItem.vehicleNo,
                                    vehicleItem.vehicleType!!,
                                    vehicleItem.vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleItem.vehicleNo)
                        }*/
                    }

                    if (StateType.getMap().isEmpty())
                        serviceSystemCodeApi()
                    else
                        init()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<FetchVehiclesResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun toggleVisibilityBookOrEstimate(isEstimateCostVisible: Boolean) {
        if (isEstimateCostVisible) {
            btnEstimatedCost.visibility = View.VISIBLE
            btnParkingBookNow.visibility = View.GONE
            toggleVisibilityCalculatedResult(false)
            duration = ""
            mTotalAmount = ""
        } else {
            btnEstimatedCost.visibility = View.GONE
            btnParkingBookNow.visibility = View.VISIBLE
        }
    }

    private fun toggleVisibilityCalculatedResult(isVisible: Boolean) {
        val visibilityValue: Int = if (isVisible) View.VISIBLE else View.GONE

        tvPayTitle.visibility = visibilityValue
//        tvPayAmount.visibility = visibilityValue
        rvAmountDenomination.visibility = visibilityValue
        vBookParkingDivider.visibility = visibilityValue
        clExtraPayInfo.visibility = visibilityValue
    }

    private fun serviceCalculateCostApi() {
        showDialog()

        val selectedVehicleNo = sVehicleToBeParked.selectedItem.toString()
        var selectedVehicleType = ""
        vehicleDetails.forEach {
            if (it.vehicleNumber == selectedVehicleNo) {
                selectedVehicleType = it.vehicleType
                return@forEach
            }
        }
        if(ParkingId==-1)
            ParkingId = intent.getStringExtra("parkingID").toInt()
        API.getClient().create(ParkingService::class.java).calculateCost(
                ParkingId.toString()
                ,
                DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etStartDate.text.toString()) +
                        " " + DateUtil.convertFormat(DateUtil.FORMAT_READABLE_TIME, DateUtil.FORMAT_24_HOUR_TIME, etStartTime.text.toString()),
                DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etEndDate.text.toString()) +
                        " " + DateUtil.convertFormat(DateUtil.FORMAT_READABLE_TIME, DateUtil.FORMAT_24_HOUR_TIME, etEndTime.text.toString()),
                VehicleType.getSwappedMap()[selectedVehicleType],
                sParkingMode.selectedItem.toString().substring(0, 1),
                mCouponSelected).enqueue(object : Callback<BaseResponse<CalculateCostResponse>> {
            override fun onResponse(call: Call<BaseResponse<CalculateCostResponse>>?, response: Response<BaseResponse<CalculateCostResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    toggleVisibilityBookOrEstimate(false)
                    response.body()?.result!!.apply {
                        toggleVisibilityCalculatedResult(true)
//                        tvPayAmount.text = "Rs. " + totalAmount + "**"

                        val arrayList: ArrayList<TitleValuePojo> = arrayListOf()
//                        arrayList.add(TitleValuePojo("Payment Mode", "CASH"))


                        if (!totalMonths.isBlank() && totalMonths != "0")
                            duration += " $totalMonths month" + if (totalMonths != "1") "s" else ""
                        if (!totalDays.isBlank() && totalDays != "0")
                            duration += " $totalDays day" + if (totalDays != "1") "s" else ""
                        if (!totalHours.isBlank() && totalHours != "0")
                            duration += " $totalHours hr" + if (totalHours != "1") "s" else ""
                        if (!totalMinutes.isBlank() && totalMinutes != "0")
                            duration += " $totalMinutes min" + if (totalMinutes != "1") "s" else ""
                        if (duration == "")
                            duration = "0 minute"
                        duration = duration?.trim()

                        arrayList.add(TitleValuePojo("Duration", duration!!))
                        arrayList.add(TitleValuePojo("Parking Charges", "Rs. " + baseAmount))
                        arrayList.add(TitleValuePojo("Service Charges", "Rs. " + serviceCharge))
                        arrayList.add(TitleValuePojo("Coupon Discount", "Rs. " + couponDiscount))
                        tvTotalParkingCharges.text = "Rs. $totalAmount"

                        mTotalAmount = totalAmount

                        (rvAmountDenomination.adapter as TitleValueAdaptor).populate(arrayList)

                        calculatedCostForBookNowPojo.totalAmount = totalAmount
                        calculatedCostForBookNowPojo.vehicleNo = selectedVehicleNo
                        calculatedCostForBookNowPojo.vehicleType = selectedVehicleType
                        calculatedCostForBookNowPojo.dateIn = DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etStartDate.text.toString()) +
                                " " + DateUtil.convertFormat(DateUtil.FORMAT_READABLE_TIME, DateUtil.FORMAT_24_HOUR_TIME, etStartTime.text.toString())
                        calculatedCostForBookNowPojo.dateOut = DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etEndDate.text.toString()) +
                                " " + DateUtil.convertFormat(DateUtil.FORMAT_READABLE_TIME, DateUtil.FORMAT_24_HOUR_TIME, etEndTime.text.toString())
                        calculatedCostForBookNowPojo.subscriptionMode = sParkingMode.selectedItem.toString().substring(0, 1)
                        calculatedCostForBookNowPojo.baseAmount = baseAmount
                        calculatedCostForBookNowPojo.bookingTDSAmount = bookingTDSAmount
                        calculatedCostForBookNowPojo.serviceCharge = serviceCharge
                        calculatedCostForBookNowPojo.rysTDSAmount = rysTDSAmount
                        calculatedCostForBookNowPojo.payableToRYSAmount = payableToRYSAmount

                        Handler().postDelayed(
                                {
                                    val scrollBy = tilEndTime.bottom
                                    if(nsvBookParking!=null)
                                    nsvBookParking.smoothScrollTo(0, scrollBy)
                                }, 500
                        )
                    }
                } else {
                    toggleVisibilityBookOrEstimate(true)
                    toggleVisibilityCalculatedResult(false)
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CalculateCostResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun serviceBookNowApi(iAlertDialogBuilder: AlertDialog) {
        if (calculatedCostForBookNowPojo.totalAmount.isEmpty()) {
            Util.showToast("Please calculate estimated cost first")
            return
        }

        showDialog()
        if(ParkingId==-1)
            ParkingId = intent.getStringExtra("parkingID").toInt()
        API.getClient().create(ParkingService::class.java).bookNow(
                ParkingId.toString(),
                TVPreferences.getInstance().userId,
                calculatedCostForBookNowPojo.dateIn,
                calculatedCostForBookNowPojo.dateOut,
                calculatedCostForBookNowPojo.vehicleNo,
                calculatedCostForBookNowPojo.vehicleType,
                mPaymentMode,
                calculatedCostForBookNowPojo.totalAmount,
                sParkingMode.selectedItem.toString().substring(0, 1),
                calculatedCostForBookNowPojo.baseAmount,
                mCouponSelected,
                calculatedCostForBookNowPojo.bookingTDSAmount,
                calculatedCostForBookNowPojo.serviceCharge,
                calculatedCostForBookNowPojo.rysTDSAmount,
                calculatedCostForBookNowPojo.totalAmount,
                calculatedCostForBookNowPojo.payableToRYSAmount).enqueue(object : Callback<BaseResponse<BookNowResponse>> {
            override fun onResponse(call: Call<BaseResponse<BookNowResponse>>?, response: Response<BaseResponse<BookNowResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    callCouponApplied(iAlertDialogBuilder, response)
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<BookNowResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun callCouponApplied(iAlertDialogBuilder: AlertDialog, iResponse: Response<BaseResponse<BookNowResponse>>) {
        showDialog()

        API.getClient().create(UserService::class.java).couponApplied(
                mCouponSelected,
                "0",
                intent.getStringExtra("parkingID"),
                TVPreferences.getInstance().userId,
                iResponse.body()!!.result.bookingID,
                "0"
        ).enqueue(object : Callback<BaseResponse<CouponResponse>> {
            override fun onResponse(call: Call<BaseResponse<CouponResponse>>?, response: Response<BaseResponse<CouponResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()?.result!!.apply {
                        val promptsView = LayoutInflater.from(this@BookParkingActivity)
                                .inflate(R.layout.dialog_congratulations, null)

                        val alertDialogBuilder = AlertDialog.Builder(this@BookParkingActivity)
                                .setView(promptsView)
                                .setCancelable(false)
                                .create()

                        val btnNavigateNow = promptsView.findViewById<Button>(R.id.btnNavigateNow)
                        val btnNavigateLater = promptsView.findViewById<Button>(R.id.btnNavigateLater)
                        val rvBookedParkingDetails = promptsView.findViewById<RecyclerView>(R.id.rvBookedParkingDetails)
                        val ibParkingInfo = promptsView.findViewById<ImageButton>(R.id.ibPublicParkingInfo)


                        ibParkingInfo.setOnClickListener(View.OnClickListener {
                            AlertDialogHelper.showInfoAlertDialog(this@BookParkingActivity,
                                    "Private Parking can be booked online through “True Wheel App” at your favorite destination. These parking are owned by individual users.\n" +
                                            " \n" +
                                            "You can either pay through CASH or PAYTM as you like. If you paid more don't worry you would get the refund back to your account.\n" +
                                            " \n" +
                                            "You can CANCEL  the scheduled parking and take SUPPORT also check out the vehicle from the “My Booking” Section in side menu for the exact bill.\n" +
                                            " \n" +
                                            "You can rate the parking and share us your suggestion to server you better.")
                        })
                        btnNavigateNow.setOnClickListener {
                            alertDialogBuilder.dismiss()
                            val geoURI: String = intent.getStringExtra("geoURI")
                            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(geoURI)))

                            val intent = Intent()
                            //intent.putExtra("shouldRefresh", true)
                            setResult(Activity.RESULT_OK, intent)
                            iAlertDialogBuilder.dismiss()
                            finish()
                        }
                        btnNavigateLater.setOnClickListener {
                            alertDialogBuilder.dismiss()
                            iAlertDialogBuilder.dismiss()
                            val intent = Intent(this@BookParkingActivity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }

                        rvBookedParkingDetails.apply {
                            isNestedScrollingEnabled = false
                            layoutManager = LinearLayoutManager(context)
                            adapter = TitleValueAdaptor(this@BookParkingActivity, R.layout.item_bold_title_value)
                        }
                        val mList = ArrayList<TitleValuePojo>()
                        if (iResponse.body()!!.result.paymentTransactionID.length > 12)
                            mList.add(TitleValuePojo("Transaction ID:", (iResponse.body()!!.result.paymentTransactionID)))
                        else
                            mList.add(TitleValuePojo("Transaction ID:", (iResponse.body()!!.result.paymentTransactionID)))
                        if (mPaymentMode.equals("COD"))
                            mList.add(TitleValuePojo("Payment Mode:", "CASH"))
                        else
                            mList.add(TitleValuePojo("Payment Mode:", "PAYTM"))
                        if (iResponse.body()!!.result.ownerAddress != null)
                            mList.add(TitleValuePojo("Owner Address:", iResponse.body()!!.result.ownerAddress))
                        mList.add(TitleValuePojo("Total Charge:", "Rs. " + iResponse.body()!!.result.paidAmount))

                        (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(mList)

                        alertDialogBuilder.show()
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CouponResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun serviceBookNowPaytmApi(iResponse: Bundle, iAlertDialogBuilder: AlertDialog) {
        if (calculatedCostForBookNowPojo.totalAmount.isEmpty()) {
            Util.showToast("Please calculate estimated cost first")
            return
        }

        showDialog()

        API.getClient().create(ParkingService::class.java).bookNowPaytm(
                intent.getStringExtra("parkingID"),
                TVPreferences.getInstance().userId,
                calculatedCostForBookNowPojo.dateIn,
                calculatedCostForBookNowPojo.dateOut,
                getSelectedVehicleNo(),
                calculatedCostForBookNowPojo.vehicleType,
                mPaymentMode,
                calculatedCostForBookNowPojo.totalAmount,
                sParkingMode.selectedItem.toString().substring(0, 1),
                iResponse.getString("TXNID"),
                calculatedCostForBookNowPojo.baseAmount,
                calculatedCostForBookNowPojo.bookingTDSAmount,
                calculatedCostForBookNowPojo.serviceCharge,
                calculatedCostForBookNowPojo.rysTDSAmount,
                calculatedCostForBookNowPojo.totalAmount,
                calculatedCostForBookNowPojo.payableToRYSAmount,
                mCouponSelected,
                iResponse.getString("BANKNAME"),
                iResponse.getString("BANKTXNID"),
                iResponse.getString("GATEWAYNAME"),
                Constants.MID,
                iResponse.getString("ORDERID"),
                getPromoCampId(iResponse),
                getPromoRespCode(iResponse),
                getPromoStatus(iResponse),
                iResponse.getString("RESPMSG"),
                iResponse.getString("TXNAMOUNT"),
                iResponse.getString("TXNID"),
                iResponse.getString("TXNTYPE"),
//                iResponse.getString("STATUS"),
                iResponse.getString("RESPCODE")).enqueue(object : Callback<BaseResponse<BookNowResponse>> {
            override fun onResponse(call: Call<BaseResponse<BookNowResponse>>?, response: Response<BaseResponse<BookNowResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()?.result!!.apply {
                        callCouponApplied(iAlertDialogBuilder, response)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<BookNowResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun getSelectedVehicleNo(): String? {
        var aVehicelNo = ""
        for (aVehicle in vehicleDetails)
            if (aVehicle.vehicleNumber.equals(sVehicleToBeParked.selectedItem.toString())) {
                aVehicelNo = aVehicle.vehicleNumber
                break
            }
        return aVehicelNo
    }

    private fun getPromoStatus(iResponse: Bundle): String? {
        if (iResponse.getString("PROMO_STATUS") != null)
            return iResponse.getString("PROMO_STATUS")
        else
            return ""
    }

    private fun getPromoRespCode(iResponse: Bundle): String? {
        if (iResponse.getString("PROMO_RESPCODE") != null)
            return iResponse.getString("PROMO_RESPCODE")
        else
            return ""
    }

    private fun getPromoCampId(iResponse: Bundle): String? {
        if (iResponse.getString("PROMO_CAMP_ID") != null)
            return iResponse.getString("PROMO_CAMP_ID")
        else
            return ""
    }

    private fun serviceSystemCodeApi() {
        showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchSystemCodes()
                .enqueue(object : Callback<BaseResponse<SystemCodeResponse>> {
                    override fun onResponse(call: Call<BaseResponse<SystemCodeResponse>>?, response: Response<BaseResponse<SystemCodeResponse>>) {
                        dismissDialog()

                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                            response.body()?.result!!.apply {
                                for (systemCode in systemCodes) {
                                    when (systemCode?.category) {
                                        "StateType" -> StateType.setMap(systemCode.categoryProperty)
                                        "SpaceType" -> SpaceType.setMap(systemCode.categoryProperty)
                                        "PropertyType" -> PropertyType.setMap(systemCode.categoryProperty)
                                        "ParkingCategory" -> ParkingCategory.setMap(systemCode.categoryProperty)
                                        "Facility Type" -> FacilityType.setMap(systemCode.categoryProperty)
                                        "DescriptionType" -> DescriptionType.setMap(systemCode.categoryProperty)
                                        "DaysType" -> AvailableDayType.setMap(systemCode.categoryProperty)
                                    }
                                }

                                init()
                            }
                        } else {
                            if (null != response.body())
                                Util.showToast(response.body()!!.error)
                            else
                                Util.showToast("Error Code : ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<BaseResponse<SystemCodeResponse>>?, t: Throwable?) {
                        dismissDialog()
                    }
                })
    }

    private fun getCheckSum(iOrderId: String, iUserId: String, iAlertDialogBuilder: AlertDialog) {
        showDialog()
        val dec = DecimalFormat("#.00")
        val amt = mTotalAmount?.toDouble()
        mTotalAmount = dec.format(amt)
        API.getClient().create(Paytm::class.java).getChecksum(
                iOrderId,
                iUserId,
                mTotalAmount).enqueue(object : Callback<BaseResponse<Checksum>> {
            override fun onResponse(call: Call<BaseResponse<Checksum>>?, response: Response<BaseResponse<Checksum>>) {
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()!!.result.apply {
                        dismissDialog()
                        PaytmUtil().placeOrderOnPaytm(this@BookParkingActivity, iUserId, iOrderId, mTotalAmount, object : PaytmPaymentTransactionCallback {
                            override fun onTransactionResponse(inResponse: Bundle) {
                                serviceBookNowPaytmApi(inResponse, iAlertDialogBuilder)
                            }

                            override fun networkNotAvailable() {
                                Util.showToast("Please check your network and try again")
                            }

                            override fun clientAuthenticationFailed(inErrorMessage: String) {
                                Util.showToast("Please try again. Error: " + inErrorMessage)

                            }

                            override fun someUIErrorOccurred(inErrorMessage: String) {
                                Util.showToast("Please try again. Error: " + inErrorMessage)

                            }

                            override fun onErrorLoadingWebPage(iniErrorCode: Int, inErrorMessage: String, inFailingUrl: String) {
                                Util.showToast("Please try again. Error: " + inErrorMessage)

                            }

                            override fun onBackPressedCancelTransaction() {
                                Util.showToast("Transaction cancelled")

                            }

                            override fun onTransactionCancel(inErrorMessage: String, inResponse: Bundle) {
                                Util.showToast("Transaction cancelled")
                            }
                        }, checksumHash)
                    }

                } else {
                    dismissDialog()
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code Mint : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<Checksum>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }
}
