package com.truewheels.user.activities;

import android.annotation.SuppressLint;
import android.graphics.Rect;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.truewheels.user.R;
import com.truewheels.user.util.ProgressDialogUtil;
import com.truewheels.user.util.Util;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    protected final String TAG = this.getClass().getName();

    protected Toolbar mToolbar;

    public Toolbar getActivityToolbar() {
        return mToolbar;
    }

    protected void initToolbar(boolean isBackEnabled) {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(isBackEnabled);
    }

    protected void initToolbar() {
        initToolbar(true);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    Util.hideKeyboard(v.getContext(), v);
                    v.clearFocus();
                    findViewById(R.id.vTemp).requestFocus();
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    public void showDialog() {
        ProgressDialogUtil.showDialog(this);
    }

    public void dismissDialog() {
        ProgressDialogUtil.dismissDialog(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            Util.hideKeyboard(this, mToolbar);
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
