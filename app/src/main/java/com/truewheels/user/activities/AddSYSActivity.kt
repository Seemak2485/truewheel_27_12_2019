package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.content.res.AppCompatResources
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.location.places.Place
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.model.LatLng
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.helpers.CheckboxDialogHelper
import com.truewheels.user.helpers.ListDialogHelper
import com.truewheels.user.helpers.MaterialDateTimeHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.GenericResponse
import com.truewheels.user.server.model.sys.FetchParkingRWAResponse
import com.truewheels.user.server.model.sys.select.*
import com.truewheels.user.server.model.sys.systemcode.SystemCodeResponse
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.util.CheckBoxDialogHelper
import com.truewheels.user.util.DateUtil
import com.truewheels.user.util.MapsUtil
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.activity_add_sys.*
import kotlinx.android.synthetic.main.content_add_sys.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddSYSActivity : BaseActivity() {

    private lateinit var sysParkingItemObj: SYSParkingItem
    private var isEditMode: Boolean = false

    private lateinit var detailsDescArr: BooleanArray
    private lateinit var facilitiesArr: BooleanArray
    private lateinit var availableDaysArr: BooleanArray
    companion object {
        var isRWA:Boolean = false}
    private val PLACE_PICKER_REQUEST = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_sys)
        initToolbar()
        serviceRWAApi()
        if (StateType.getMap().isEmpty())
            serviceSystemCodeApi()
        else {
            init()
            if (isEditMode)
                autoFillForm()
        }
    }

    private fun serviceRWAApi() {
       // showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchRWAParkingList()
                .enqueue(object : Callback<BaseResponse<FetchParkingRWAResponse>> {
                    override fun onFailure(call: Call<BaseResponse<FetchParkingRWAResponse>>, t: Throwable) {
                       // dismissDialog()
                        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onResponse(call: Call<BaseResponse<FetchParkingRWAResponse>>?, response: Response<BaseResponse<FetchParkingRWAResponse>>) {
                      //  dismissDialog()

                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                            response.body()?.result!!.apply {
                                RWAList.setMap(response.body()!!.result.parkings)
                            }
                        }
                    }
                })
    }
    private fun serviceSystemCodeApi() {
        showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchSystemCodes()
                .enqueue(object : Callback<BaseResponse<SystemCodeResponse>> {
                    override fun onResponse(call: Call<BaseResponse<SystemCodeResponse>>?, response: Response<BaseResponse<SystemCodeResponse>>) {
                        dismissDialog()

                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                            response.body()?.result!!.apply {
                                for (systemCode in systemCodes) {
                                    when (systemCode?.category) {
                                        "StateType" -> StateType.setMap(systemCode.categoryProperty)
                                        "SpaceType" -> SpaceType.setMap(systemCode.categoryProperty)
                                        "PropertyType" -> PropertyType.setMap(systemCode.categoryProperty)
                                        "ParkingCategory" -> ParkingCategory.setMap(systemCode.categoryProperty)
                                        "Facility Type" -> FacilityType.setMap(systemCode.categoryProperty)
                                        "DescriptionType" -> DescriptionType.setMap(systemCode.categoryProperty)
                                        "DaysType" -> AvailableDayType.setMap(systemCode.categoryProperty)
                                    }
                                }

                                init()
                                if (isEditMode)
                                    autoFillForm()
                            }
                        } else {
                            if (null != response.body())
                                Util.showToast(response.body()!!.error)
                            else
                                Util.showToast("Error Code : ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<BaseResponse<SystemCodeResponse>>?, t: Throwable?) {
                        dismissDialog()
                    }
                })
    }

    @Suppress("UNCHECKED_CAST")
    private fun autoFillForm() {
        etParkingSpaceName.setText(sysParkingItemObj.parkSpaceName)
        etOwnerComments.setText(sysParkingItemObj.ownerComments)
        etAttendantNo.setText(sysParkingItemObj.parkingSupportNo)
        //if (!sysParkingItemObj.spaceType.isNullOrBlank()) sSpaceType.setSelection((sSpaceType.adapter as (ArrayAdapter<String>)).getPosition(SpaceType.getMap().getValue(sysParkingItemObj.spaceType)))
        if (!sysParkingItemObj.spaceType.isNullOrBlank()) etSpaceType.setText(SpaceType.getMap().getValue(sysParkingItemObj.spaceType))
        //if (!sysParkingItemObj.propertyType.isNullOrBlank()) sPropertyType.setSelection((sPropertyType.adapter as (ArrayAdapter<String>)).getPosition(PropertyType.getMap().getValue(sysParkingItemObj.propertyType)))
        if (!sysParkingItemObj.propertyType.isNullOrBlank()) etPropertyType.setText(PropertyType.getMap().getValue(sysParkingItemObj.propertyType))
        etPropertyAddress.setText(sysParkingItemObj.propertyAddress)
        //sState.setSelection((sState.adapter as (ArrayAdapter<String>)).getPosition(StateType.getMap().getValue(sysParkingItemObj.state)))
        etState.setText(StateType.getMap().getValue(sysParkingItemObj.state))
        etPincode.setText(sysParkingItemObj.propertyPinCode)
        etLandmark.setText(sysParkingItemObj.propertyLandMark)

        var temp = ""
        var tempArr: List<String>? = sysParkingItemObj.descriptionSelected?.split(",")
        tempArr?.let {
            for (x in tempArr!!) {
                if (x.isNotBlank()) {
                    temp = temp + ",  " + DescriptionType.getMap().getValue(x.trim())
                    detailsDescArr[DescriptionType.getMap().keys.indexOf(x.trim())] = true
                }
            }
            if (temp.isNotEmpty())
                etDetailsDesc.setText(temp.substring(3))
        }

        temp = ""
        tempArr = sysParkingItemObj.facilitySelected?.split(",")
        tempArr?.let {
            for (x in tempArr!!) {
                if (x.isNotBlank()) {
                    temp = temp + ",  " + FacilityType.getMap().getValue(x.trim())
                    facilitiesArr[FacilityType.getMap().keys.indexOf(x.trim())] = true
                }
            }
            if (temp.isNotEmpty())
                etFacilities.setText(temp.substring(3))
        }

        temp = ""
        tempArr = sysParkingItemObj.daysSelected?.split(",")
        tempArr?.let {
            for (x in tempArr) {
                if (x.isNotBlank()) {
                    temp = temp + ",  " + AvailableDayType.getMap().getValue(x.trim())
                    availableDaysArr[AvailableDayType.getMap().keys.indexOf(x.trim())] = true
                }
            }
            if (temp.isNotEmpty())
                etAvailableDays.setText(temp.substring(3))
        }

        val date = DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE, sysParkingItemObj.parkingAvailableDate)
        etAvailableFromDate.setText(date)
        etAvailableFromTime.setText(sysParkingItemObj.timeFrom)
        etAvailableSlotsCar.setText(sysParkingItemObj.noOfSpaceAvaiable)
        etAvailableSlotsBike.setText(sysParkingItemObj.noOfSpaceAvaiableBike)

        if (sysParkingItemObj.open24Hour == "Y" || sysParkingItemObj.open24Hour == "A")
            cbOpen24Hours.isChecked = true

        etAvailableToTime.setText(sysParkingItemObj.timeTo)

        etCarHourly.setText(sysParkingItemObj.carBasicCharge)
        etCarDaily.setText(sysParkingItemObj.carDailyCharge)
        etCarMonthly.setText(sysParkingItemObj.carmonthlycharge)
        etBikeHourly.setText(sysParkingItemObj.bikeBasicCharge)
        etBikeDaily.setText(sysParkingItemObj.bikedailycharge)
        etBikeMonthly.setText(sysParkingItemObj.bikemonthycharge)
        /* etCarWeekly.setText(sysParkingItemObj.carweeklycharge)
         etBikeWeekly.setText(sysParkingItemObj.bikeweeklycharge)*/
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        if ("edit" == intent.getStringExtra("mode")) {
            sysParkingItemObj = intent.getSerializableExtra("sysParkingItemObj") as SYSParkingItem
            isEditMode = true
            supportActionBar?.title = "Edit Your Space"

            tvIsActive.text = if (sysParkingItemObj.active == "Y" || sysParkingItemObj.active == "A")
                "ACTIVE" else "INACTIVE"
        } else
            tvIsActive.visibility = View.GONE

        btnDoneSys.setOnClickListener {
            if (isFormValid()) {
                if (isEditMode) {
                    serviceSYSEditApi(sysParkingItemObj.parkingId.toString())
                } else {
                    serviceSYSAddApi()
                }
            }
        }
        initEdittextLists()
        initCheckboxGroups()
        etAvailableFromDate.setOnClickListener { MaterialDateTimeHelper.createDatePickerDialog(this@AddSYSActivity, "Parking available date?", etAvailableFromDate, true) }
        etAvailableFromTime.setOnClickListener {
            MaterialDateTimeHelper.createTimePickerDialog(this@AddSYSActivity, "Parking available from?", etAvailableFromTime)
        }
        etAvailableToTime.setOnClickListener {
            MaterialDateTimeHelper.createTimePickerDialog(this@AddSYSActivity, "Parking available till?", etAvailableToTime)
        }
        cbOpen24Hours.setOnCheckedChangeListener { _, isChecked ->
            etAvailableFromTime.setText("")
            etAvailableToTime.setText("")
            etAvailableFromTime.isEnabled = !isChecked
            etAvailableToTime.isEnabled = !isChecked
        }

        initChargesInfoText()
        if (isEditMode) {
            etParkingSpaceName.isFocusable = false
            etPropertyAddress.isFocusable = false
            etState.setOnClickListener(null)
            etPincode.isFocusable = false
            etAvailableFromDate.setOnClickListener(null)
            etAvailableSlotsCar.isFocusable = false
            etAvailableSlotsBike.isFocusable = false
        }

        tilPropertyAddress.setOnClickListener(View.OnClickListener {
            startAutoSearch()
        })
        etPropertyAddress.setOnClickListener(View.OnClickListener {
            if (!isEditMode)
                startAutoSearch()
        })

        tvAddSysNotice.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@AddSYSActivity, TncActivity::class.java))

        })
    }

    private fun initChargesInfoText() {
        /* setupDrawableDialog(tvCarHourly, "Message for Car Hourly")
         setupDrawableDialog(tvCarDaily, "Message for Car Daily")
         setupDrawableDialog(tvCarMonthly, "Message for Car Monthly")
         setupDrawableDialog(tvBikeHourly, "Message for Bike Hourly")
         setupDrawableDialog(tvBikeDaily, "Message for Bike Daily")
         setupDrawableDialog(tvBikeMonthly, "Message for Bike Monthly")*/
        //setupDrawableDialog(etDetailsDesc, "Message for facilities")

        setupDrawableDialog(tvCarPricingLabel, "Please provide your expected charges for each car parked per hour, per day and per month")
        setupDrawableDialog(tvBikePricingLabel, "Please provide your expected charges for each bike parked per hour, per day and per month")
        setupDrawableDialog(tvCarLabel, "Please provide the number of spaces you have to park the car")
        setupDrawableDialog(tvBikeLabel, "Please provide the number of spaces you have to park the bike")

        setupDrawableDialog(etFacilities, "Message for services")
    }

    @SuppressLint("ClickableViewAccessibility")
    @Suppress("LocalVariableName")
    private fun setupDrawableDialog(textView: TextView, message: String) {
        textView.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this@AddSYSActivity, R.drawable.ic_info_outline_24dp), null)
        textView.setOnTouchListener { _: View, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_UP) {
                val DRAWABLE_RIGHT = 2
                // val DRAWABLE_LEFT = 0
                // val DRAWABLE_TOP = 1
                // val DRAWABLE_BOTTOM = 3

                if (event.rawX >= (textView.right - textView.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    AlertDialogHelper.showInfoAlertDialog(this@AddSYSActivity, message)
                }
            }
            return@setOnTouchListener true
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Suppress("LocalVariableName")
    private fun setupDrawableDialog(editText: EditText, message: String) {
        editText.setCompoundDrawablesWithIntrinsicBounds(null, null, AppCompatResources.getDrawable(this@AddSYSActivity, R.drawable.ic_info_outline_24dp), null)
        editText.setOnTouchListener { _: View, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_UP) {
                val DRAWABLE_RIGHT = 2
                // val DRAWABLE_LEFT = 0
                // val DRAWABLE_TOP = 1
                // val DRAWABLE_BOTTOM = 3

                if (event.rawX >= (editText.right - editText.compoundDrawables[DRAWABLE_RIGHT].bounds.width())) {
                    // your action here
                    AlertDialogHelper.showInfoAlertDialog(this@AddSYSActivity, message)
                    return@setOnTouchListener true
                } else {
                    return@setOnTouchListener false
                }
            }
            return@setOnTouchListener false
        }
    }

    private fun initEdittextLists() {
        val spaceTypeArrayList: ArrayList<String> = ArrayList(SpaceType.getMap().values)
        etSpaceType.setOnClickListener { ListDialogHelper.createListDialog(this@AddSYSActivity, etSpaceType, spaceTypeArrayList,tilRWASpaceList) }

        val rwaTypeArrayList: ArrayList<String> = ArrayList(RWAList.getMap().values)
        etRWASpaceList.setOnClickListener { ListDialogHelper.createListDialog(this@AddSYSActivity, etRWASpaceList, rwaTypeArrayList) }

        val propertyTypeArrayList: ArrayList<String> = ArrayList(PropertyType.getMap().values)
        etPropertyType.setOnClickListener { ListDialogHelper.createListDialog(this@AddSYSActivity, etPropertyType, propertyTypeArrayList) }

        val stateArrayList: ArrayList<String> = ArrayList(StateType.getMap().values)
        etState.setOnClickListener { ListDialogHelper.createListDialog(this@AddSYSActivity, etState, stateArrayList) }
    }

    /*private fun initSpinners() {
        val spaceTypeArrayList: ArrayList<String> = ArrayList(SpaceType.getMap().values)
        setupSpinner(sSpaceType, "SPACE TYPE", spaceTypeArrayList)

        val propertyTypeArrayList: ArrayList<String> = ArrayList(PropertyType.getMap().values)
        setupSpinner(sPropertyType, "PROPERTY TYPE", propertyTypeArrayList)

        val stateArrayList: ArrayList<String> = ArrayList(StateType.getMap().values)
        setupSpinner(sState, "STATE", stateArrayList)
    }

    private fun setupSpinner(spinner: Spinner, title: String, arrayList: ArrayList<String>) {
        arrayList.add(0, title)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }*/

    private fun initCheckboxGroups() {
        val detailsDescArrayList: ArrayList<String> = ArrayList(DescriptionType.getMap().values)
        detailsDescArr = BooleanArray(detailsDescArrayList.size)
        etDetailsDesc.setOnClickListener { CheckboxDialogHelper.createCheckboxDialog(this@AddSYSActivity, etDetailsDesc, detailsDescArrayList, detailsDescArr) }

        val facilitiesArrayList: ArrayList<String> = ArrayList(FacilityType.getMap().values)
        facilitiesArr = BooleanArray(facilitiesArrayList.size)
        etFacilities.setOnClickListener { CheckboxDialogHelper.createCheckboxDialog(this@AddSYSActivity, etFacilities, facilitiesArrayList, facilitiesArr) }

        val availableDaysArrayList: ArrayList<String> = ArrayList(AvailableDayType.getMap().values)
        availableDaysArr = BooleanArray(availableDaysArrayList.size)
//        etAvailableDays.setOnClickListener { CheckboxDialogHelper.createCheckboxDialog(this@AddSYSActivity, etAvailableDays, availableDaysArrayList, availableDaysArr) }
        etAvailableDays.setOnClickListener { CheckBoxDialogHelper().showDialog(this@AddSYSActivity, etAvailableDays, availableDaysArrayList, availableDaysArr) }
    }

    private fun isFormValid(): Boolean {
        var formValidity = true

        when {
            etParkingSpaceName.text.length < 3 -> {
                Util.showToast("Name is too short")
                formValidity = false
            }
            etPropertyAddress.text.length < 5 -> {
                Util.showToast("Property Address is too short")
                formValidity = false
            }
            etState.text.isEmpty() -> {
                Util.showToast("Please select a State")
                formValidity = false
            }
        /*sState.selectedItemPosition == 0 -> {
            Util.showToast("Please select a State")
            formValidity = false
        }*/
            etPincode.text.length < 6 -> {
                Util.showToast("Pincode is too short")
                formValidity = false
            }
            etLandmark.text.length < 3 -> {
                Util.showToast("Landmark is too short")
                formValidity = false
            }
            etAttendantNo.text.length in 1..9 -> {
                Util.showToast("Please enter correct attendant's number")
                formValidity = false
            }
            etAvailableDays.text.isEmpty() -> {
                Util.showToast("Please select parking available days")
                formValidity = false
            }
            etAvailableFromDate.text.isEmpty() -> {
                Util.showToast("Please select parking available from date")
                formValidity = false
            }
            !cbOpen24Hours.isChecked && (etAvailableFromTime.text.isEmpty() || etAvailableToTime.text.isEmpty()) -> {
                Util.showToast("Enter parking available time")
                formValidity = false
            }
            etCarHourly.text.isEmpty() && etBikeHourly.text.isEmpty() &&
                    etCarMonthly.text.isEmpty() && etBikeMonthly.text.isEmpty() &&
                    etCarDaily.text.isEmpty() && etBikeDaily.text.isEmpty() -> {
                Util.showToast("Please enter at least one price (hourly, daily or monthly)")
                formValidity = false
            }
        }

        return formValidity
    }

    private fun serviceSYSAddApi() {
        showDialog()

        var descTemp = ""
        var daysTemp = ""
        var facilityTemp = ""
        var temp = ""
        var tempArr: List<String>

        if (etDetailsDesc.text.isNotEmpty()) {
            tempArr = etDetailsDesc.text.toString().split(",")
            tempArr.let {
                for (x in tempArr)
                    temp = temp + ",  " + DescriptionType.getSwappedMap().getValue(x.trim())
                if (temp.isNotEmpty())
                    descTemp = temp.substring(3)
            }
        }
        if (etFacilities.text.isNotEmpty()) {
            temp = ""
            tempArr = etFacilities.text.toString().split(",")
            tempArr.let {
                for (x in tempArr)
                    temp = temp + ",  " + FacilityType.getSwappedMap().getValue(x.trim())
                if (temp.isNotEmpty())
                    facilityTemp = temp.substring(3)
            }
        }
        var primeProduct = 1
        if (etAvailableDays.text.isNotEmpty()) {
            temp = ""
            tempArr = etAvailableDays.text.toString().split(",")
            tempArr.let {
                for (x in tempArr) {
                    val dayCode = AvailableDayType.getSwappedMap().getValue(x.trim())
                    temp = temp + ",  " + dayCode
                    primeProduct *= AvailableDayType.getMapValue().getValue(dayCode)
                }
                if (temp.isNotEmpty())
                    daysTemp = temp.substring(3)
            }
        }

        val latlng: LatLng? = MapsUtil.getLocationFromAddress(
                this@AddSYSActivity,
                etPropertyAddress.text.toString() + "," + etState.text.toString() + "," + etPincode.text.toString())

        API.getClient().create(ShareYourSpaceService::class.java).addParking(
                TVPreferences.getInstance().userId,
                //if (sSpaceType.selectedItemPosition == 0) "" else SpaceType.getSwappedMap().getValue(sSpaceType.selectedItem.toString()),
                if (etSpaceType.text.isEmpty()) "" else SpaceType.getSwappedMap().getValue(etSpaceType.text.toString()),


                //if (sPropertyType.selectedItemPosition == 0) "" else PropertyType.getSwappedMap()[sPropertyType.selectedItem.toString()],
                if (etPropertyType.text.isEmpty()) "" else PropertyType.getSwappedMap()[etPropertyType.text.toString()],
                etAvailableSlotsCar.text.toString(),
                etAvailableSlotsBike.text.toString(),
                etPropertyAddress.text.toString(),
                etPincode.text.toString(),
                etLandmark.text.toString(),
                etOwnerComments.text.toString(),
//                if (latlng?.latitude.toString() != "null") latlng?.latitude.toString() else "",
//                if (latlng?.longitude.toString() != "null") latlng?.longitude.toString() else "",
                if (mSelectedPlace != null) mSelectedPlace?.latLng?.latitude.toString() else "",
                if (mSelectedPlace != null) mSelectedPlace?.latLng?.longitude.toString() else "",
                //StateType.getSwappedMap()[sState.selectedItem.toString()],
                StateType.getSwappedMap()[etState.text.toString()],
                etAvailableFromTime.text.toString(),
                etAvailableToTime.text.toString(),
                etBikeHourly.text.toString(),
                etCarHourly.text.toString(),
                if (cbOpen24Hours.isChecked) "Y" else "N",
                descTemp,
                daysTemp,
                facilityTemp,
                etParkingSpaceName.text.toString(),
                etBikeDaily.text.toString(),
                etBikeMonthly.text.toString(),
                etCarDaily.text.toString(),
                etCarMonthly.text.toString(),
                "N",
                DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etAvailableFromDate.text.toString()),
                primeProduct,
                if (etRWASpaceList.text.isEmpty()) 0 else RWAList.getSwappedMap().getValue(etRWASpaceList.text.toString()).toInt(),
                etAttendantNo.text.toString()
        ).enqueue(object : Callback<BaseResponse<GenericResponse>> {
            override fun onResponse(call: Call<BaseResponse<GenericResponse>>?, response: Response<BaseResponse<GenericResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    val promptsView = LayoutInflater.from(this@AddSYSActivity)
                            .inflate(R.layout.layout_success_alert_dialog, null)

                    val alertDialogBuilder = AlertDialog.Builder(this@AddSYSActivity)
                            .setView(promptsView)
                            .setCancelable(false)
                            .create()

                    val bOk = promptsView.findViewById<Button>(R.id.bOk)
                    val tvDesc = promptsView.findViewById<TextView>(R.id.tvDesc)
                    val ibParkingInfo = promptsView.findViewById<ImageButton>(R.id.ibRegisterParkingInfo)

                    tvDesc.setMovementMethod(LinkMovementMethod.getInstance());

                    tvDesc.setText("Thanks for registering your space with us.\n" +
                            "Soon a TrueWheel agent will visit your space to verify.")
                    ibParkingInfo.setOnClickListener(View.OnClickListener {
                        AlertDialogHelper.showInfoAlertDialog(this@AddSYSActivity,
                                "Congrats! Your space is registered with us. We will verify your space shortly.\n" +
                                        " \n" +
                                        "Once verified, available customers can book your registered space online as per their requirement.\n" +
                                        " \n" +
                                        "Edit the parking details:\n" +
                                        "Edit your booking details from \"My Space\" section of widget bar. A parking owner can edit its basic information. For additional help, we can be reached at: truewheel.in\n" +
                                        " \n" +
                                        "Booking Received:\n" +
                                        "Find your received bookings here or a parking owner can cancel it and checkout the vehicle and take support in case of any query.\n" +
                                        " \n" +
                                        "My earning:\n" +
                                        "Check your Earning and liability from registered parking by pressing \"My Earning\" in widget bar.")
                    })

                    bOk.setOnClickListener(View.OnClickListener {
                        val intent = Intent(this@AddSYSActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    })
                    alertDialogBuilder.show()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<GenericResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun startAutoSearch() {
        val aBuilder = PlacePicker.IntentBuilder()
        try {
            startActivityForResult(aBuilder.build(this), PLACE_PICKER_REQUEST)
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        } catch (e: GooglePlayServicesRepairableException) {
            e.printStackTrace()
        }
    }

    var mSelectedPlace: Place? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                mSelectedPlace = PlacePicker.getPlace(this@AddSYSActivity, data)
                etPropertyAddress.setText(mSelectedPlace?.address)
            }
        }
    }

    private fun serviceSYSEditApi(parkingId: String) {
        showDialog()

        var descTemp = ""
        var daysTemp = ""
        var facilityTemp = ""
        var temp = ""
        var tempArr: List<String>

        if (etDetailsDesc.text.isNotEmpty()) {
            tempArr = etDetailsDesc.text.toString().split(",")
            tempArr.let {
                for (x in tempArr)
                    temp = temp + ",  " + DescriptionType.getSwappedMap().getValue(x.trim())
                if (temp.isNotEmpty())
                    descTemp = temp.substring(3)
            }
        }
        if (etFacilities.text.isNotEmpty()) {
            temp = ""
            tempArr = etFacilities.text.toString().split(",")
            tempArr.let {
                for (x in tempArr)
                    temp = temp + ",  " + FacilityType.getSwappedMap().getValue(x.trim())
                if (temp.isNotEmpty())
                    facilityTemp = temp.substring(3)
            }
        }
        var primeProduct = 1
        if (etAvailableDays.text.isNotEmpty()) {
            temp = ""
            tempArr = etAvailableDays.text.toString().split(",")
            tempArr.let {
                for (x in tempArr) {
                    val dayCode = AvailableDayType.getSwappedMap().getValue(x.trim())
                    temp = temp + ",  " + dayCode
                    primeProduct *= AvailableDayType.getMapValue().getValue(dayCode)
                }
                if (temp.isNotEmpty())
                    daysTemp = temp.substring(3)
            }
        }

        API.getClient().create(ShareYourSpaceService::class.java).updateParking(
                TVPreferences.getInstance().userId,
                //if (sSpaceType.selectedItemPosition == 0) "" else SpaceType.getSwappedMap().getValue(sSpaceType.selectedItem.toString()),
                if (etSpaceType.text.isEmpty()) "" else SpaceType.getSwappedMap().getValue(etSpaceType.text.toString()),
                //if (sPropertyType.selectedItemPosition == 0) "" else PropertyType.getSwappedMap()[sPropertyType.selectedItem.toString()],
                if (etPropertyType.text.isEmpty()) "" else PropertyType.getSwappedMap()[etPropertyType.text.toString()],
                etAvailableSlotsCar.text.toString(),
                etAvailableSlotsBike.text.toString(),
                etPropertyAddress.text.toString(),
                etPincode.text.toString(),
                etLandmark.text.toString(),
                etOwnerComments.text.toString(),
                sysParkingItemObj.lattitude,
                sysParkingItemObj.longitude,
                "A",
                //StateType.getSwappedMap()[sState.selectedItem.toString()],
                StateType.getSwappedMap()[etState.text.toString()],
                etAvailableFromTime.text.toString(),
                etAvailableToTime.text.toString(),
                etBikeHourly.text.toString(),
                etCarHourly.text.toString(),
                if (cbOpen24Hours.isChecked) "Y" else "N",
                descTemp,
                daysTemp,
                facilityTemp,
                etParkingSpaceName.text.toString(),
                etBikeDaily.text.toString(),
                etBikeMonthly.text.toString(),
                etCarDaily.text.toString(),
                etCarMonthly.text.toString(),
                "N",
                DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etAvailableFromDate.text.toString()),
                parkingId,
                if (etRWASpaceList.text.isEmpty()) 0 else RWAList.getSwappedMap().getValue(etRWASpaceList.text.toString()).toInt(),
                primeProduct,

                etAttendantNo.text.toString()
        ).enqueue(object : Callback<BaseResponse<GenericResponse>> {
            override fun onResponse(call: Call<BaseResponse<GenericResponse>>?, response: Response<BaseResponse<GenericResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    Util.showToast("Updated successfully")
                    val intent = Intent()
                    intent.putExtra("shouldRefresh", true)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<GenericResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }
}
