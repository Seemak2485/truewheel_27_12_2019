package com.truewheels.user.activities

import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.BookingReceivedAdaptor
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.parkings.booking_received.BookingRecievedItem
import com.truewheels.user.server.model.parkings.booking_received.FetchBookingReceivedResponse
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.BookingsReceivedStringDateComparator
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.content_booking_received.*
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class BookingReceivedActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_received)
        initToolbar()
        init()
        serviceFetchBookingReceivedApi()
    }

    private fun init() {
        rvBookingReceived.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = BookingReceivedAdaptor(this@BookingReceivedActivity)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
    }

    private fun serviceFetchBookingReceivedApi() {
        showDialog()

        API.getClient().create(UserService::class.java).fetchBookingReceived(
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<FetchBookingReceivedResponse>> {
            override fun onResponse(call: retrofit2.Call<BaseResponse<FetchBookingReceivedResponse>>?, response: Response<BaseResponse<FetchBookingReceivedResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body()?.result!!.bookingRecieved!!.isEmpty()) {
                        tvEmptyBookingReceived.visibility = View.VISIBLE
                        (rvBookingReceived.adapter as BookingReceivedAdaptor).populate(null)
                    } else {
                        tvEmptyBookingReceived.visibility = View.GONE
                        var aBookingRecieved = response.body()?.result!!.bookingRecieved
                        Collections.reverse(aBookingRecieved)
                        (rvBookingReceived.adapter as BookingReceivedAdaptor)
                                .populate(sortList(aBookingRecieved))
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: retrofit2.Call<BaseResponse<FetchBookingReceivedResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun sortList(iBookingMade: List<BookingRecievedItem>?): List<BookingRecievedItem>? {
        Collections.sort(iBookingMade, BookingsReceivedStringDateComparator())
        return iBookingMade
    }
}
