package com.truewheels.user.activities;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.truewheels.user.R;
import com.truewheels.user.constants.Constants;

public class TncActivity extends BaseActivity {

    private WebView mWvTnc;
    private ProgressBar mPbLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tnc);

        initToolbar(true);
        initViews();
    }

    private void initViews() {
        mPbLoading = findViewById(R.id.mPbLoadingWebview);
        mWvTnc = findViewById(R.id.tncView);
        mWvTnc.getSettings().setJavaScriptEnabled(true);
        mWvTnc.loadUrl(Constants.BASE_URL+"/TrueWheelsUser/TermsCondition");

        mWvTnc.setWebViewClient(new HelloWebViewClient());
    }

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl("http://www.truewheelsuat.in/TrueWheelsUser/TermsCondition");
            return true;
        }

        public void onPageFinished(WebView view, String url) {
            try {
                mPbLoading.setVisibility(View.GONE);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}
