package com.truewheels.user.activities;

import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.truewheels.user.R;
import com.truewheels.user.TVPreferences;
import com.truewheels.user.adaptors.TitleValueAdaptor;
import com.truewheels.user.server.API;
import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.EarningRespone;
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo;
import com.truewheels.user.server.services.UserService;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyEarningsActivity extends BaseActivity {

    private RecyclerView mRvEarningsList;
    private TitleValueAdaptor mAdapter;
    private ArrayList<TitleValuePojo> mDatalist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_earnings);
        initToolbar(true);

        mRvEarningsList = findViewById(R.id.rvEarningsList);
        mRvEarningsList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new TitleValueAdaptor(this, R.layout.item_bold_title_value);
        mRvEarningsList.setAdapter(mAdapter);
        getEarnings();
    }

    private void getEarnings() {
        showDialog();
        Call<BaseResponse<EarningRespone>> aCall = API.getClient().create(UserService.class).getMyEarnings(TVPreferences.getInstance().getUserId());
        aCall.enqueue(new Callback<BaseResponse<EarningRespone>>() {
            @Override
            public void onResponse(Call<BaseResponse<EarningRespone>> call, Response<BaseResponse<EarningRespone>> response) {
                dismissDialog();
                try {
                    initViews(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<EarningRespone>> call, Throwable t) {
                dismissDialog();

            }
        });
    }

    private void initViews(Response<BaseResponse<EarningRespone>> iResponse) {
        if (iResponse.body().getResult().getBookingCharges() != null) {
            mDatalist.add(new TitleValuePojo("Earning on booking: ", iResponse.body().getResult().getBookingCharges()));
            mDatalist.add(new TitleValuePojo("", ""));
            mDatalist.add(new TitleValuePojo("Coupon Discount: ", iResponse.body().getResult().getCouponDiscount()));
            mDatalist.add(new TitleValuePojo("", ""));
            mDatalist.add(new TitleValuePojo("TrueWheel Commission: ", iResponse.body().getResult().getCommision()));
            mDatalist.add(new TitleValuePojo("", ""));
            mDatalist.add(new TitleValuePojo("You earned: ", iResponse.body().getResult().getLibilities()));
            mDatalist.add(new TitleValuePojo("", ""));
            mDatalist.add(new TitleValuePojo("Due Amount: ", iResponse.body().getResult().getLibilities()));
        } else
            mDatalist.add(new TitleValuePojo("Message: ", iResponse.body().getResult().getMessage()));
        mAdapter.populate(mDatalist);
    }
}
