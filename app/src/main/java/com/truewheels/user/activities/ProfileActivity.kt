package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle

import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.textfield.TextInputEditText
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.VehicleDetailsAdaptor
import com.truewheels.user.constants.RequestCodeConstants
import com.truewheels.user.helpers.MaterialDateTimeHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.SignUpResponse
import com.truewheels.user.server.model.add_delete_vehicle.AddDeleteVehicleResponse
import com.truewheels.user.server.model.profile.ProfileResponse
import com.truewheels.user.server.model.profile.UserDetailsItem
import com.truewheels.user.server.model.profile.VehicleDetailsPojo
import com.truewheels.user.server.model.signup.SignUpMode
import com.truewheels.user.server.model.signup.SignupResponse
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.DateUtil
import com.truewheels.user.util.Util
import com.truewheels.user.util.ValidationUtil
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProfileActivity : BaseActivity(), View.OnClickListener {

    private var isSignUp = false
    private var mNewUser = false
    private lateinit var userService: UserService

    private var isFb: Boolean = false
    private var socialNetworkId: String = ""
    private var mGoogleApiClient: GoogleApiClient? = null
    private var callbackManager: CallbackManager? = null

    private var mIbFacebookLogin: ImageButton? = null
    private var mIbGoogleLogin: ImageButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        initToolbar(false)

        init()
    }

    companion object {
        fun getBundle(isSignUp: Boolean, newUser: Boolean, mobile: String): Bundle {
            val bundle = Bundle()

            bundle.putBoolean("sign_up", isSignUp)
            bundle.putBoolean("new_user", newUser)
            bundle.putString("mobile", mobile)
            return bundle
        }
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        mIbFacebookLogin = findViewById(R.id.ibFacebookLogin)
        mIbGoogleLogin = findViewById(R.id.ibGoogleLogin)

        userService = API.getClient().create(UserService::class.java)

        isSignUp = intent.getBooleanExtra("sign_up", false)
        mNewUser = intent.getBooleanExtra("new_user", false)
        mToolbar.title = if (mNewUser) "Sign Up" else "Update Profile"

        if (!mNewUser)
            serviceFetchProfileApi()

        if (isSignUp) {
            btnProfileSave.text = "SignUp"
            etDob.visibility = View.GONE
            etPanCardNo.visibility = View.GONE
            etDrivingLicenseNo.visibility = View.GONE
            tvYourVehicles.visibility = View.GONE
            ivAddVehicles.visibility = View.GONE
            rvVehicleDetails.visibility = View.GONE
            tvEmptyVehicleDetails.visibility = View.GONE
        } else {
            ibFacebookLogin.visibility = View.GONE
            ibGoogleLogin.visibility = View.GONE
            tvSeparator.visibility = View.GONE
        }

        if (mNewUser) {
            val mobileNumber: String = intent.getStringExtra("mobile")
            etPhone.setText(mobileNumber)
        } else {
            if (supportActionBar != null)
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }

        etPhone.isFocusable = false
        etDob.setOnClickListener { MaterialDateTimeHelper.createDatePickerDialog(this@ProfileActivity, "DOB", etDob, 18) }
        rvVehicleDetails.isNestedScrollingEnabled = false
        val vehicleDetailsAdaptor = VehicleDetailsAdaptor(this)
        rvVehicleDetails.adapter = vehicleDetailsAdaptor
        rvVehicleDetails.layoutManager = LinearLayoutManager(this)
        rvVehicleDetails.itemAnimator = DefaultItemAnimator()

        ivAddVehicles.setOnClickListener { v ->
            if (null == (rvVehicleDetails.adapter as VehicleDetailsAdaptor).dataList)
                (rvVehicleDetails.adapter as VehicleDetailsAdaptor).populate(ArrayList())
            if (rvVehicleDetails.adapter!!.itemCount >= 5) {
                Util.showToast("Cannot add more than 5 vehicles")
                return@setOnClickListener
            }

            val promptsView = LayoutInflater.from(v.context)
                    .inflate(R.layout.dialog_add_vehicle, null)

            val alertDialogBuilder = AlertDialog.Builder(v.context)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            val rbCar = promptsView.findViewById<RadioButton>(R.id.rbCar)
            val rbBike = promptsView.findViewById<RadioButton>(R.id.rbBike)
            val ivRadioCar = promptsView.findViewById<ImageView>(R.id.ivRadioCar)
            val ivRadioBike = promptsView.findViewById<ImageView>(R.id.ivRadioBike)
            val etDialogVehicleNo = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleNo)
            val etDialogVehicleModel = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleModel)
            val btnDialogAddVehicle = promptsView.findViewById<Button>(R.id.btnDialogAddVehicle)

            rbCar.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car)
                    rbBike.isChecked = false
                } else {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car_unchecked)
                }
            }
            rbBike.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_light)
                    rbCar.isChecked = false
                } else {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_unchecked)
                }
            }
            btnDialogAddVehicle.setOnClickListener {
                var isValid = true
                if (!rbCar.isChecked && !rbBike.isChecked) {
                    Util.showToast(getString(R.string.prompt_vehicle_type))
                    isValid = false
                } else if (etDialogVehicleNo.text.toString().isEmpty()) {
                    Util.showToast(getString(R.string.prompt_empty_vehicle_number))
                    isValid = false
                } else if (!ValidationUtil.isValidVehicleNo(etDialogVehicleNo.text.toString())) {
                    Util.showToast(getString(R.string.vehicle_number_prompt))
                    isValid = false
                }
                else if (etDialogVehicleModel.getText().toString().isEmpty()) {
                   Util.showToast(getString(R.string.prompt_vehicle_model));
                   isValid = false
              }

                if (isValid) {
                    serviceAddDeleteVehicleApi(true,
                            VehicleDetailsPojo(
                                    -1,
                                    etDialogVehicleNo.text.toString(),
                                    if (rbCar.isChecked) rbCar.text.toString() else rbBike.text.toString(),
                                    etDialogVehicleModel.text.toString()))
                    alertDialogBuilder.dismiss()
                }
            }
            alertDialogBuilder.show()

        }
        btnProfileSave.setOnClickListener {
            if (isProfileFormValid()) {
                if (mNewUser) {
                    if (TextUtils.isEmpty(socialNetworkId)) {
                        if (isSignUpFormValid())
                            serviceSignUpApi()
                    } else {
                        if (isProfileFormValid()) {
                            if (mNewUser) {
                                showDialog()

                                userService.socialMediaSignUp(
                                        etFullName.text.toString(),
                                        "",
                                        etEmail.text.toString(),
                                        etPhone.text.toString(),
                                        socialNetworkId,
                                        "",
                                        if (isFb) SignUpMode.FACEBOOK.value else SignUpMode.GOOGLE.value,
                                        if (isFb) SignUpMode.FACEBOOK.value else SignUpMode.GOOGLE.value
                                ).enqueue(object : Callback<BaseResponse<SignUpResponse>> {
                                    override fun onResponse(call: Call<BaseResponse<SignUpResponse>>, response: Response<BaseResponse<SignUpResponse>>) {
                                        dismissDialog()
                                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                                            Log.i(TAG, "Social media signUp SUCCESS")
                                            TVPreferences.getInstance().fullName = etFullName.text.toString()
                                            TVPreferences.getInstance().userId = response.body()!!.result.transation
                                            TVPreferences.getInstance().authToken = response.body()!!.result.transation
                                            TVPreferences.getInstance().isProfileUpdated = true
                                            val i = Intent(this@ProfileActivity, MainActivity::class.java)
                                            startActivity(i)
                                            finish()
                                        } else {
                                            if (null != response.body())
                                                Util.showToast(response.body()!!.error)
                                            else
                                                Util.showToast("Error Code : ${response.code()}")
                                        }
                                    }

                                    override fun onFailure(call: Call<BaseResponse<SignUpResponse>>, t: Throwable) {
                                        dismissDialog()
                                    }
                                })
                            }
                        }
                    }

                } else {
                    serviceUpdateProfileAPI()
                }
            }
        }

        setupSocialLogin()
        setListener()
    }

    private fun setListener() {
        mIbFacebookLogin?.setOnClickListener(this)
        mIbGoogleLogin?.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ibFacebookLogin -> {
                btnActualFacebookLogin.performClick()
            }
            R.id.ibGoogleLogin -> {
                val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
                startActivityForResult(signInIntent, RequestCodeConstants.REQUEST_CODE_GOOGLE_LOGIN)
            }
        }
    }

    fun serviceAddDeleteVehicleApi(toAdd: Boolean, vehiclePojo: VehicleDetailsPojo) {
        showDialog()
        userService.addDeleteVehicle(
                if (toAdd) "" else vehiclePojo.vehicleID.toString(),
                TVPreferences.getInstance().userId,
                if (toAdd) "1" else "0",
                vehiclePojo.vehicleNumber,
                vehiclePojo.vehicleModel,
                vehiclePojo.vehicleType
        ).enqueue(object : Callback<BaseResponse<AddDeleteVehicleResponse>> {
            override fun onResponse(call: Call<BaseResponse<AddDeleteVehicleResponse>>, response: Response<BaseResponse<AddDeleteVehicleResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    val vehicleDetails = ArrayList<VehicleDetailsPojo>()

                    if (response.body() != null && response.body()?.result != null && response.body()?.result!!.userVehicleDetails?.size != 0) {
                        for ((vehicleID, vehicleNo, vehicleType, vehicleModel) in response.body()?.result!!.userVehicleDetails!!) {
                            if (vehicleNo!!.isEmpty())
                                continue
                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleID.toInt(),
                                    vehicleNo,
                                    vehicleType!!,
                                    vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)
                        }
                    }
                    (rvVehicleDetails.adapter as VehicleDetailsAdaptor).populate(vehicleDetails)

                    if (rvVehicleDetails.adapter!!.itemCount == 0) {
                        tvEmptyVehicleDetails.visibility = View.VISIBLE
                    } else {
                        tvEmptyVehicleDetails.visibility = View.GONE
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<AddDeleteVehicleResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    @Suppress("ObjectLiteralToLambda")
    private fun setupSocialLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestProfile().requestEmail()
                .requestIdToken(this.resources.getString(R.string.server_client_id))
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(/* FragmentActivity */ this, /* OnConnectionFailedListener */ object : GoogleApiClient.OnConnectionFailedListener {
                    override fun onConnectionFailed(connectionResult: ConnectionResult) {
                        Log.d(TAG, "onConnectionFailed:" + connectionResult.isSuccess)
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        callbackManager = CallbackManager.Factory.create()
        btnActualFacebookLogin.setReadPermissions("email", "public_profile")
        // If using in a fragment
        // Callback registration
        btnActualFacebookLogin.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            @SuppressLint("SetTextI18n")
            override fun onSuccess(loginResult: LoginResult) {
                // App code
                Log.i(TAG, "onSuccess : " + loginResult.toString())
                val request = GraphRequest.newMeRequest(loginResult.accessToken)
                { `object`, response ->
                    Log.i(TAG, "response : " + response)
                    // Application code
                    try {
                        var id = ""
                        if (`object`.has("id")) {
                            id = `object`.getString("id")
                        }
                        var firstName = ""
                        if (`object`.has("first_name")) {
                            firstName = `object`.getString("first_name")
                        }
                        var lastName = ""
                        if (`object`.has("last_name")) {
                            lastName = `object`.getString("last_name")
                        }

                        var email = ""
                        if (`object`.has("email")) {
                            email = `object`.getString("email")
                        }
                        socialNetworkId = id
                        isFb = true
                        updateUI(firstName + " " + lastName, email)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                val parameters = Bundle()
                parameters.putString("fields", "id, first_name, last_name, email,gender, birthday, location")
                request.parameters = parameters
                request.executeAsync()

            }

            override fun onCancel() {
                // App code
                Log.i(TAG, "onCancel : ")
            }

            override fun onError(exception: FacebookException) {
                // App code
                Log.i(TAG, "onError : " + exception.toString())
            }
        })
//        gSignInButton.setSize(SignInButton.SIZE_STANDARD);
    }

    private fun autoFillForm(result: UserDetailsItem) {
        etFullName.setText(result.fullName)
        etPhone.setText(result.phoneNo1)
        etAlternativePhone.setText(result.phoneNo2)
        etEmail.setText(result.emailId)
        var dob = result.dOB
        if (dob!!.isNotEmpty()) {
            dob = DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE, dob)
            etDob.setText(dob)
        }
        etPanCardNo.setText(result.pAN)
        etDrivingLicenseNo.setText(result.drivingLicense)

        val vehicleDetails = ArrayList<VehicleDetailsPojo>()
        for ((vehicleID, vehicleNo, vehicleType, vehicleModel) in result.listVehicle!!) {
            if (vehicleNo!!.isEmpty())
                continue
            val vehicleDetailsPojo = VehicleDetailsPojo(
                    vehicleID,
                    vehicleNo,
                    vehicleType!!,
                    vehicleModel!!)
            vehicleDetails.add(vehicleDetailsPojo)
        }
        (rvVehicleDetails.adapter as VehicleDetailsAdaptor).populate(vehicleDetails)

        if (rvVehicleDetails.adapter!!.itemCount == 0) {
            tvEmptyVehicleDetails.visibility = View.VISIBLE
        }
    }

    private fun serviceFetchProfileApi() {
        showDialog()

        userService.fetchProfile(TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<ProfileResponse>> {
            override fun onResponse(call: Call<BaseResponse<ProfileResponse>>, response: Response<BaseResponse<ProfileResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body()!!.result.userDetails.isEmpty())
                        return
                    //FIXME single object in response. No array
                    response.body()!!.result.apply {
                        autoFillForm(userDetails[0])
                    }
                } else {
                    if (null != response.body()) {
                        // Util.showToast(response.body().getError());
                    } else
                        Util.showToast("Error Code : " + response.code())
                }
            }

            override fun onFailure(call: Call<BaseResponse<ProfileResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    private fun serviceSignUpApi() {
        showDialog()

        userService.signUp(etFullName.text.toString(),
                etPhone.text.toString(),
                etEmail.text.toString(),
                etAlternativePhone.text.toString(),
                "Y").enqueue(object : Callback<BaseResponse<SignupResponse>> {
            override fun onResponse(call: Call<BaseResponse<SignupResponse>>, response: Response<BaseResponse<SignupResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    TVPreferences.getInstance().fullName = response.body()!!.result.fullName
                    TVPreferences.getInstance().userId = response.body()!!.result.userID
                    TVPreferences.getInstance().authToken = response.body()!!.result.userID
                    TVPreferences.getInstance().isProfileUpdated = true
                    val i = Intent(this@ProfileActivity, MainActivity::class.java)
                    startActivity(i)
                    finish()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : " + response.code())
                }
            }

            override fun onFailure(call: Call<BaseResponse<SignupResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    private fun serviceUpdateProfileAPI() {
        val capacity = 5
        val vehicleDetails = ArrayList<VehicleDetailsPojo>(capacity)
        for (i in 0 until capacity)
            vehicleDetails.add(VehicleDetailsPojo())

        val size = rvVehicleDetails.adapter?.itemCount
        if (size != 0) {
            for (i in 0 until size!!) {
                val view = rvVehicleDetails.getChildAt(i)
                val ivVehicleType = view.findViewById<ImageView>(R.id.ivVehicleType)
                val etVehicleNo = view.findViewById<EditText>(R.id.etVehicleNo)
                //val etVehicleModel = view.findViewById<EditText>(R.id.etVehicleModel)

                vehicleDetails[i].vehicleType = ivVehicleType.tag.toString()
                vehicleDetails[i].vehicleNumber = etVehicleNo.text.toString()
                //vehicleDetails[i].vehicleModel = etVehicleModel.text.toString()
                vehicleDetails[i].vehicleModel = ""
            }
        }

        showDialog()

        var dob = ""
        if (etDob.text.toString().trim().isNotEmpty())
            dob = DateUtil.convertFormat(DateUtil.FORMAT_READABLE_DATE, DateUtil.FORMAT_SQL_DATE, etDob.text.toString())

        userService.updateProfile(
                TVPreferences.getInstance().userId,
                etFullName.text.toString(),
                etEmail.text.toString(),
                dob,
                etAlternativePhone.text.toString(),
                etDrivingLicenseNo.text.toString(),
                etPanCardNo.text.toString(),
                etPhone.text.toString(),
                vehicleDetails[0].vehicleNumber,
                vehicleDetails[1].vehicleNumber,
                vehicleDetails[2].vehicleNumber,
                vehicleDetails[3].vehicleNumber,
                vehicleDetails[4].vehicleNumber,
                vehicleDetails[0].vehicleModel,
                vehicleDetails[1].vehicleModel,
                vehicleDetails[2].vehicleModel,
                vehicleDetails[3].vehicleModel,
                vehicleDetails[4].vehicleModel,
                vehicleDetails[0].vehicleType,
                vehicleDetails[1].vehicleType,
                vehicleDetails[2].vehicleType,
                vehicleDetails[3].vehicleType,
                vehicleDetails[4].vehicleType
        ).enqueue(object : Callback<BaseResponse<SignUpResponse>> {
            override fun onResponse(call: Call<BaseResponse<SignUpResponse>>, response: Response<BaseResponse<SignUpResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    Util.showToast("Updated Successfully")
                    TVPreferences.getInstance().fullName=(etFullName.text.toString())
                    TVPreferences.getInstance().email=( etEmail.text.toString())
                    TVPreferences.getInstance().phone = etPhone.text.toString()
                    finish()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : " + response.code())
                }
            }

            override fun onFailure(call: Call<BaseResponse<SignUpResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    private fun isProfileFormValid(): Boolean {
        var result = isSignUpFormValid()
        if (!result) return result

        val str = "Please enter "
        if (!ValidationUtil.isValidPanCard(etPanCardNo.text.toString()) && etPanCardNo.text.isNotEmpty()) {
            Util.showToast(str + "your valid PAN Card number")
            result = false
        } else if (!ValidationUtil.isValidLiscenceNo(etDrivingLicenseNo.text.toString()) && etDrivingLicenseNo.text.isNotEmpty()) {
            Util.showToast(str + "your valid Driver licence number")
            result = false
        }
        return result
    }

    private fun isSignUpFormValid(): Boolean {
        val str = "Please enter "
        if (TextUtils.isEmpty(etFullName.text.toString())) {
            Util.showToast(str + "your Full Name")
            return false
        } else if (TextUtils.isEmpty(etPhone.text.toString())) {
            Util.showToast(str + "your Phone Number")
            return false
        } else if (!TextUtils.isEmpty(etPhone.text.toString()) && etPhone.text.toString().length != 10) {
            Util.showToast(str + "your valid phone Number")
            return false
        } else if (!TextUtils.isEmpty(etAlternativePhone.text.toString()) && etAlternativePhone.text.toString().length != 10) {
            Util.showToast(str + "your valid alternate Number")
            return false
        } else if (!TextUtils.isEmpty(etAlternativePhone.text.toString()) && etPhone.text.toString() == etAlternativePhone.text.toString()) {
            Util.showToast(str + "different alternate Number")
            return false
        } else if (!ValidationUtil.isValidEmail(etEmail.text.toString()) || etEmail.text.isEmpty()) {
            Util.showToast(str + "your valid Email Address")
            return false
        } else if (!TextUtils.isEmpty(etAlternativePhone.text.toString()) && etAlternativePhone.text.toString().length != 10) {
            Util.showToast(str + "your valid alternative phone Number")
            return false
        }
        return true
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RequestCodeConstants.REQUEST_CODE_GOOGLE_LOGIN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            Log.d(TAG, "handleSignInResult:" + result.isSuccess)
            if (result.isSuccess) {
                // Signed in successfully, show authenticated UI.
                val acct = result.signInAccount
                val displayName = acct?.displayName
                var firstName = ""
                var lastName = ""
                val name: List<String> = displayName!!.split(" ")
                if (name.isNotEmpty())
                    firstName = name[0]
                if (name.size > 1)
                    lastName = displayName.substring(firstName.length).trim()

                updateUI(firstName + " " + lastName, acct.email)
                if (null != acct.id)
                    socialNetworkId = acct.id!!
                isFb = false
            } else {
                // Signed out, show unauthenticated UI.
                //            updateUI(false);
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateUI(fullName: String, emailId: String?) {
        Log.i(TAG, "isSignUp" + isSignUp)
        etFullName.setText(fullName)
        etEmail.setText(emailId)
        btnProfileSave.text = "Confirm Details"
    }

    override fun finish() {
        Util.hideKeyboard(this, etFullName)
        super.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
