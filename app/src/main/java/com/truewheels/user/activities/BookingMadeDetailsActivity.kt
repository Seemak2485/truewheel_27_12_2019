package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import com.google.zxing.WriterException
import com.paytm.pgsdk.Log
import com.paytm.pgsdk.PaytmPaymentTransactionCallback
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.constants.Constants
import com.truewheels.user.enums.BookingStatus
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.helpers.EdittextDialogHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.GenericResponse
import com.truewheels.user.server.model.SignUpResponse
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.server.model.parkings.CashCheckoutResponse
import com.truewheels.user.server.model.parkings.CheckoutBookingResponse
import com.truewheels.user.server.model.parkings.PublicCheckoutBookingResponse
import com.truewheels.user.server.model.parkings.booking_made.BookingMadeItem
import com.truewheels.user.server.model.paytm.Checksum
import com.truewheels.user.server.model.paytm.PaytmResponse
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.server.services.Paytm
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.DateUtil
import com.truewheels.user.util.PaytmUtil
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.activity_booking_made_details.*
import kotlinx.android.synthetic.main.content_booking_made_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DecimalFormat

class BookingMadeDetailsActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener {

    private lateinit var bookingMadeDetailObj: BookingMadeItem
    private var mIsPublic: Boolean = true
    private var mTotalAmount: String? = ""
    private var duration: String? = ""
    private var mTvPaytmBalance: TextView? = null
    private var mPaymentMode: String? = ""
    private var mRating: Float? = 0f
    private var mQrEncoder: QRGEncoder? = null
    private var mBitmap: Bitmap? = null
    private var aCbClean: CheckBox? = null
    private var aCbNice: CheckBox? = null
    private var aCbSafe: CheckBox? = null
    private var aCbValue: CheckBox? = null
    private var mCheckoutResponse: CheckoutBookingResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_made_details)
        initToolbar()
        init()
        autofill()

    }

    private fun init() {
        bookingMadeDetailObj = intent.getSerializableExtra("bookingMadeDetailObj") as BookingMadeItem
        if (bookingMadeDetailObj.parkingType.equals("Private"))
            mIsPublic = false
        else
            mIsPublic = true

        rvBookingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingMadeDetailsActivity, R.layout.item_bold_title_value)
        }
        rvParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingMadeDetailsActivity, R.layout.item_bold_title_value)
        }
        rvVehicleDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingMadeDetailsActivity, R.layout.item_bold_title_value)
        }
        rvPaymentDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingMadeDetailsActivity, R.layout.item_bold_title_value)
        }
        btnCheckoutBooking.setOnClickListener {
            AlertDialogHelper.showAlertDialog(this@BookingMadeDetailsActivity,
                    null,
                    "Are you sure want to Checkout your parked vehicle?\n" +
                            "You will be charged as per your Start and Checkout time only.",
                    object : AlertDialogHelper.Callback {
                        override fun onSuccess() {
                            serviceCheckoutBookingApi(true)
                        }

                        override fun onBack() {
                        }

                    })
        }
        bGetBill.setOnClickListener(View.OnClickListener {
            getBill()
        })

        btnCancelBooking.setOnClickListener {
            EdittextDialogHelper.showEdittextDialog(this@BookingMadeDetailsActivity,
                    "Reason for cancellation",
                    object : EdittextDialogHelper.Callback {
                        override fun onSuccess(message: String) {
                            AlertDialogHelper.showAlertDialog(this@BookingMadeDetailsActivity,
                                    null,
                                    getMessage(),
                                    object : AlertDialogHelper.Callback {
                                        override fun onSuccess() {
                                            serviceCancelBookingApi(message)
                                        }

                                        override fun onBack() {
                                        }

                                    })
                        }

                        override fun onBack() {
                            //Do nothing
                        }

                    })
        }
        btnSupportBooking.setOnClickListener {
            val intent = Intent(this@BookingMadeDetailsActivity, SupportActivity::class.java)
            intent.putExtra("bookingID", bookingMadeDetailObj.bookedId.toString())
            intent.putExtra("isFromBookingMade", true)
            startActivity(intent)
        }

        when (bookingMadeDetailObj.bookingStatus) {
            BookingStatus.SCHEDULED.status -> {
                btnCheckoutBooking.visibility = View.GONE
                if (mIsPublic) {
                    tvPaymentDetails.visibility = View.GONE
                    rvPaymentDetails.visibility = View.GONE
                    vPaymentDetails.visibility = View.GONE
                    bGetBill.visibility = View.VISIBLE
                } else {
                    bGetBill.visibility = View.GONE
                }
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingMadeDetailsActivity, R.color.color_primary))
            }
            BookingStatus.IN_PROGRESS.status -> {
                btnCancelBooking.visibility = View.GONE
                bGetBill.visibility = View.GONE
                if (mIsPublic) {
                    tvPaymentDetails.visibility = View.GONE
                    rvPaymentDetails.visibility = View.GONE
                    vPaymentDetails.visibility = View.GONE
                    bGetBill.visibility = View.VISIBLE
                } else {
                    if (!bookingMadeDetailObj.monthlySubscription.equals("M"))
                        btnCheckoutBooking.visibility = View.VISIBLE
                }
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingMadeDetailsActivity, android.R.color.black))
            }
            BookingStatus.CANCELLED.status -> {
                btnCancelBooking.visibility = View.GONE
                btnCheckoutBooking.visibility = View.GONE
                bGetBill.visibility = View.GONE
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingMadeDetailsActivity, android.R.color.holo_red_dark))
            }
            BookingStatus.PARKED_OUT.status -> {
                if (DateUtil.getDateDifference(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, bookingMadeDetailObj.checkedOutDateTime)) {
                    if (mIsPublic)
                        bGetBill.visibility = View.VISIBLE
                    else
                        btnCheckoutBooking.visibility = View.VISIBLE
                } else {
                    btnCheckoutBooking.visibility = View.GONE
                    bGetBill.visibility = View.GONE
                }
                btnCancelBooking.visibility = View.GONE
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingMadeDetailsActivity, android.R.color.holo_red_dark))
            }
        }

        if (bookingMadeDetailObj.checkedOutBy != null && bookingMadeDetailObj.checkedOutBy.equals("VO"))
            btnCheckoutBooking.visibility = View.GONE
        if (bookingMadeDetailObj.whoDidCheckout != null && bookingMadeDetailObj.whoDidCheckout.equals("VO"))
            btnCheckoutBooking.visibility = View.GONE
    }

    private fun getMessage(): String? {
        if (bookingMadeDetailObj.paymentMode.equals("COD")) {
            return "Are you sure want to Cancel your booked parking?\n" +
                    "You will not be charged any fee for this."
        } else {
            return "Are you sure want to Cancel your booked parking?\n" +
                    "You will not be charged any fee for this. Your " + bookingMadeDetailObj.totalAmount + " will be refunded back in your paytm wallet."
        }
    }

    private fun getBill() {
        showDialog()
        API.getClient().create(ParkingService::class.java).getBill(
                bookingMadeDetailObj.bookedId.toString()).enqueue(object : Callback<BaseResponse<PublicCheckoutBookingResponse>> {
            override fun onResponse(call: Call<BaseResponse<PublicCheckoutBookingResponse>>?, response: Response<BaseResponse<PublicCheckoutBookingResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        mCheckoutResponse = bill_Details!![0]
                        if (!mCheckoutResponse?.Paid.equals("Y")) {
                            if (!TextUtils.isEmpty(bill_Details!![0].amount))
                                showPaymentDialog(bill_Details!![0].amount!!)
                            else
                                showPaymentDialog(bill_Details!![0].extraAmount!!)
                        } else
                            showAlreadyCheckedOut(mCheckoutResponse)
                        Util.showToast(message)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<PublicCheckoutBookingResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun showPaymentDialog(iAmount: String) {
        mTotalAmount = iAmount

        val promptsView = LayoutInflater.from(this@BookingMadeDetailsActivity)
                .inflate(R.layout.payment_dialog, null)

        val alertDialogBuilder = AlertDialog.Builder(this@BookingMadeDetailsActivity)
                .setView(promptsView)
                .setCancelable(false)
                .create()

        val aBPayNow = promptsView.findViewById<Button>(R.id.bPayNow)
        val aRbCash = promptsView.findViewById<RadioButton>(R.id.rbCash)
        val aRbPaytm = promptsView.findViewById<RadioButton>(R.id.rbPaytm)
        mTvPaytmBalance = promptsView.findViewById<TextView>(R.id.tvPaytmBal)
        val rvBookedParkingDetails = promptsView.findViewById<RecyclerView>(R.id.rvPaymentDetailsList)
        val mTvPaymentModeLabel = promptsView.findViewById<TextView>(R.id.tvPaymentModeLabel)

        aRbCash.setOnCheckedChangeListener(this)
        aRbPaytm.setOnCheckedChangeListener(this)

        aBPayNow.setOnClickListener {
            if (iAmount == null || TextUtils.isEmpty(iAmount) || iAmount.equals("0")) {
                if (iAmount.equals("0")) {
                    showRatingDialog(false)
                } else {
                    alertDialogBuilder.dismiss()
                    val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                }
            } else if (!TextUtils.isEmpty(mPaymentMode)) {
                val orderId = getOrderId(bookingMadeDetailObj.bookedId.toString())!!
                if (aRbPaytm.isChecked)
                    getCheckSum(orderId, (TVPreferences.getInstance().userId), alertDialogBuilder)
                else
                    doCashTransaction(alertDialogBuilder, orderId)
            } else
                Util.showToast("Please select a payment mode to continue")
        }

        rvBookedParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingMadeDetailsActivity, R.layout.item_bold_title_value)
        }

        if (iAmount == null || TextUtils.isEmpty(iAmount) || iAmount.equals("0")) {
            aBPayNow.setText("Ok")
            aRbCash.visibility = View.GONE
            aRbPaytm.visibility = View.GONE
            mTvPaymentModeLabel.visibility = View.GONE
        }
        if (!TextUtils.isEmpty(bookingMadeDetailObj.checkedOutDateTime)) {
            val dataList: List<TitleValuePojo> = arrayListOf(
                    TitleValuePojo("Total cost:", iAmount),
                    TitleValuePojo("Checked out time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingMadeDetailObj.checkedOutDateTime)))
            (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

        } else if (!TextUtils.isEmpty(bookingMadeDetailObj.bookedOutTime)) {
            val dataList: List<TitleValuePojo> = arrayListOf(
                    TitleValuePojo("Total cost:", iAmount),
                    TitleValuePojo("Checked out time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingMadeDetailObj.bookingDateTime)))
            (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

        } else {
            if (mIsPublic) {
                val dataList: List<TitleValuePojo> = arrayListOf(
                        TitleValuePojo("Total cost:", iAmount),
                        TitleValuePojo("Checked in time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, mCheckoutResponse?.checkInPublic!!)))
                (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)
            } else {
                val dataList: List<TitleValuePojo> = arrayListOf(
                        TitleValuePojo("Total cost:", iAmount),
                        TitleValuePojo("Checked in time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingMadeDetailObj.bookedIntime)))
                (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)
            }

        }

        alertDialogBuilder.show()

        alertDialogBuilder.setOnCancelListener(DialogInterface.OnCancelListener {
            Log.d("Tag", "RatingDialogKilled")
            mPaymentMode = ""
        })
    }

    private fun doCashTransaction(alertDialog: AlertDialog, iOrderId: String) {
        showDialog()
        var parkingType = ""
        if (mIsPublic)
            parkingType = "Public"
        else
            parkingType = "Private"

        API.getClient().create(ParkingService::class.java).cashRequest(
                bookingMadeDetailObj.bookedId.toString(),
                iOrderId,
                "",
                parkingType,
                mPaymentMode,
                mTotalAmount).enqueue(object : Callback<BaseResponse<CashCheckoutResponse>> {
            override fun onResponse(call: Call<BaseResponse<CashCheckoutResponse>>?, response: Response<BaseResponse<CashCheckoutResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()

                    response.body()!!.result.apply {
                        alertDialog.dismiss()
                        showRatingDialog(true)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CashCheckoutResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

    private fun initiateRefund() {
        showDialog()

//        API.getClient().create(Paytm::class.java).initiateRefund(
//                iOrderId,
//                iUserId,
//                mTotalAmount).enqueue(object : Callback<BaseResponse<PaytmResponse>> {
//            override fun onResponse(call: Call<BaseResponse<PaytmResponse>>?, response: Response<BaseResponse<PaytmResponse>>) {
//                if (response.isSuccessful && response.body()!!.resultCode == 0) {
//                    response.body()!!.result.apply {
//                        dismissDialog()
//
//                    }
//
//                } else {
//                    dismissDialog()
//                    if (null != response.body())
//                        Util.showToast(response.body()!!.error)
//                    else
//                        Util.showToast("Error Code : ${response.code()}")
//                }
//            }
//
//            override fun onFailure(call: Call<BaseResponse<PaytmResponse>>?, t: Throwable?) {
//                dismissDialog()
//            }
//
//        })
    }

    private fun getCheckSum(iOrderId: String, iUserId: String, iAlertDialogBuilder: AlertDialog) {
        showDialog()

        val dec = DecimalFormat("#.00")
        val amt = mTotalAmount?.toDouble()
        mTotalAmount = dec.format(amt)
        API.getClient().create(Paytm::class.java).getChecksum(
                iOrderId,
                iUserId,mTotalAmount
                ).enqueue(object : Callback<BaseResponse<Checksum>> {
            override fun onResponse(call: Call<BaseResponse<Checksum>>?, response: Response<BaseResponse<Checksum>>) {
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()!!.result.apply {
                        dismissDialog()
                        PaytmUtil().placeOrderOnPaytm(this@BookingMadeDetailsActivity, iUserId, iOrderId, mTotalAmount, object : PaytmPaymentTransactionCallback {
                            override fun onTransactionResponse(inResponse: Bundle) {
                                sendPaytmResponseToServer(inResponse, iAlertDialogBuilder)
                            }

                            override fun networkNotAvailable() {
                                Util.showToast("Please check your network and try again")
                            }

                            override fun clientAuthenticationFailed(inErrorMessage: String) {
                                Util.showToast("Please try again. Error: " + inErrorMessage)

                            }

                            override fun someUIErrorOccurred(inErrorMessage: String) {
                                Util.showToast("Please try again. Error: " + inErrorMessage)

                            }

                            override fun onErrorLoadingWebPage(iniErrorCode: Int, inErrorMessage: String, inFailingUrl: String) {
                                Util.showToast("Please try again. Error: " + inErrorMessage)

                            }

                            override fun onBackPressedCancelTransaction() {
                                Util.showToast("Transaction cancelled")

                            }

                            override fun onTransactionCancel(inErrorMessage: String, inResponse: Bundle) {
                                Util.showToast("Transaction cancelled")
                            }
                        }, checksumHash);
                    }

                } else {
                    dismissDialog()
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<Checksum>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

    private fun sendPaytmResponseToServer(inResponse: Bundle, iAlertDialogBuilder: AlertDialog) {
        API.getClient().create(Paytm::class.java).transactionResponse(
                inResponse.getString("BANKNAME"),
                inResponse.getString("BANKTXNID"),
                bookingMadeDetailObj.bookedId.toString(),
                inResponse.getString("GATEWAYNAME"),
                Constants.MID,
                inResponse.getString("ORDERID"),
                inResponse.getString("PAYMENTMODE"),
                "0",
                inResponse.getString("PROMO_CAMP_ID"),
                inResponse.getString("PROMO_STATUS"),
                inResponse.getString("RESPMSG"),
                inResponse.getString("PROMO_RESPCODE"),
                inResponse.getString("STATUS"),
                inResponse.getString("TXNAMOUNT"),
                inResponse.getString("TXNDATE"),
                inResponse.getString("TXNID"),
                inResponse.getString("TXNTYPE"),
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<PaytmResponse>> {
            override fun onResponse(call: Call<BaseResponse<PaytmResponse>>?, response: Response<BaseResponse<PaytmResponse>>) {
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()!!.result.apply {
                        dismissDialog()
                        iAlertDialogBuilder.dismiss()
                        showRatingDialog(true)
                    }
                } else {
                    dismissDialog()
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<PaytmResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

    private fun getOrderId(iBookingId: String): String? {
        var aTime = System.currentTimeMillis()
        return iBookingId + aTime
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        if (p1) {
            if (p0?.id == R.id.rbCash) {
//                mTvPaytmBalance?.visibility = View.GONE
                mPaymentMode = "COD"
            } else if (p0?.id == R.id.rbPaytm) {
//                mTvPaytmBalance?.visibility = View.VISIBLE
                mPaymentMode = "PAYTM"
                getPaytmBalForUser()
            }
        }
    }

    private fun getPaytmBalForUser() {
        mTvPaytmBalance?.text = "PAYTM balance: 10000"
    }

    private fun showRatingDialog(iToShowPaymentReceived: Boolean) {
        val promptsView = LayoutInflater.from(this@BookingMadeDetailsActivity)
                .inflate(R.layout.rating_dialog, null)

        val alertDialogBuilder = AlertDialog.Builder(this@BookingMadeDetailsActivity)
                .setView(promptsView)
                .setCancelable(false)
                .create()

        val aRbParkingRating = promptsView.findViewById<RatingBar>(R.id.rbRateCall)
        val aTvPaymentRecieved = promptsView.findViewById<TextView>(R.id.tvPaymentRecieved)
        val aEtFeedback = promptsView.findViewById<EditText>(R.id.etFeedback)
        aCbClean = promptsView.findViewById<CheckBox>(R.id.cbClean)
        aCbNice = promptsView.findViewById<CheckBox>(R.id.cbBehaviour)
        aCbSafe = promptsView.findViewById<CheckBox>(R.id.cbSafe)
        aCbValue = promptsView.findViewById<CheckBox>(R.id.cbValue)
        val mBSubmit = promptsView.findViewById<Button>(R.id.bSubmit)

        aRbParkingRating.setOnRatingBarChangeListener(RatingBar.OnRatingBarChangeListener { ratingBar, v, b ->
            mRating = v
        })
        mBSubmit.setOnClickListener {
            if (mRating!! > 0) {
                if (mIsPublic)
                    submitResponse(alertDialogBuilder, aEtFeedback.text.toString().trim(), getFeedback())
                else
//                    submitResponse(alertDialogBuilder, iResult, aEtFeedback.text.toString().trim(), getFeedback())
                    submitPrivateResponse(alertDialogBuilder, aEtFeedback.text.toString().trim(), getFeedback())
            } else
                Util.showToast("Please rate the parking. It will help us in providing you better services.")
        }
        var aPaymentOptionUsed = ""
        if (mPaymentMode.equals("COD")) {
            aPaymentOptionUsed = "Cash"
        } else
            aPaymentOptionUsed = "PAYTM"
        aTvPaymentRecieved.text = "Rs " + mTotalAmount + " have been successfully recieved through " + aPaymentOptionUsed

        if (!iToShowPaymentReceived)
            aTvPaymentRecieved.visibility = View.GONE
        alertDialogBuilder.show()
    }

    private fun getFeedback(): String {
        var aFeedBackString: String? = ""
        if (aCbClean?.isChecked!!)
            aFeedBackString = aFeedBackString + "CL,"
        if (aCbNice?.isChecked!!)
            aFeedBackString = aFeedBackString + "NB,"
        if (aCbSafe?.isChecked!!)
            aFeedBackString = aFeedBackString + "SF,"
        if (aCbValue?.isChecked!!)
            aFeedBackString = aFeedBackString + "VL,"
        return aFeedBackString!!
    }

    private fun submitResponse(alertDialogBuilder: AlertDialog, iComment: String, iFeedBack: String) {
        API.getClient().create(UserService::class.java).ratePublicParking(
                "",
                TVPreferences.getInstance().userId,
                bookingMadeDetailObj.bookedId.toString(),
                iComment,
                mRating.toString(),
                iFeedBack,
                false).enqueue(object : Callback<BaseResponse<SignUpResponse>> {
            override fun onResponse(call: Call<BaseResponse<SignUpResponse>>?, response: Response<BaseResponse<SignUpResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        Util.showToast(message)
                    }
                    alertDialogBuilder.dismiss()

                    AlertDialogHelper.showInfoAlertDialog(this@BookingMadeDetailsActivity, "Thank you for rating the parking. We wish to see you again.") {
                        finish()
                        val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }
                } else {
                    alertDialogBuilder.dismiss()
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<SignUpResponse>>?, t: Throwable?) {
                alertDialogBuilder.dismiss()
                dismissDialog()
            }

        })
    }

    private fun submitPrivateResponse(alertDialogBuilder: AlertDialog, iComment: String, iFeedBack: String) {
        API.getClient().create(UserService::class.java).rateParking(
                0,
                0,
                TVPreferences.getInstance().userId.toInt(),
                mRating!!.toInt(),
                iComment,
                0,
                0,
                "",
                bookingMadeDetailObj.bookedId.toString()
        ).enqueue(object : Callback<BaseResponse<SignUpResponse>> {
            override fun onResponse(call: Call<BaseResponse<SignUpResponse>>?, response: Response<BaseResponse<SignUpResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        Util.showToast(message)
                    }
                    alertDialogBuilder.dismiss()

                    AlertDialogHelper.showInfoAlertDialog(this@BookingMadeDetailsActivity, "Thank you for rating the parking. We wish to see you again.") {
                        finish()
                        val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<SignUpResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }


    @SuppressLint("SetTextI18n")
    private fun autofill() {
        bookingMadeDetailObj.apply {
            tvBookingId.text = bookedId.toString()
            tvStatus.text = bookingStatus
            if (!TextUtils.isEmpty(parkingSpaceName))
                tvBookedParkingAddress.text = "$parkingSpaceName \n$parkingAddress"
            else
                tvBookedParkingAddress.text = "$parkingAddress"

            duration = ""
            if (!totalMonth.isNullOrBlank() && totalMonth != "0")
                duration += " $totalMonth month" + if (totalMonth != "1") "s" else ""
            if (!totalDays.isNullOrBlank() && totalDays != "0")
                duration += " $totalDays day" + if (totalDays != "1") "s" else ""
            if (!totalHour.isNullOrBlank() && totalHour != "0")
                duration += " $totalHour hr" + if (totalHour != "1") "s" else ""
            if (!totalMin.isNullOrBlank() && totalMin != "0")
                duration += " $totalMin min" + if (totalMin != "1") "s" else ""
            if (duration == "")
                duration = "0 minute"

            duration = duration?.trim()
            val bookingDataList: ArrayList<TitleValuePojo> = arrayListOf()
            val parkingDataList: ArrayList<TitleValuePojo> = arrayListOf()
            val vehicleDataList: ArrayList<TitleValuePojo> = arrayListOf()
            val paymentDataList: ArrayList<TitleValuePojo> = arrayListOf()

            bookingDataList.add(TitleValuePojo("Booking Mode", when (monthlySubscription) {
                "H" -> "Hourly"
                "D" -> "Daily"
                "0" -> "Regular"
                "M" -> "Monthly"
                else -> "Regular"
            }))

            if (!mIsPublic) {
                // For private booking
                bookingDataList.add(TitleValuePojo("Start Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedIntime)))
                if (checkedOutDateTime.isNotEmpty())
                    bookingDataList.add(TitleValuePojo("Checkout Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, checkedOutDateTime)))
                else
                    bookingDataList.add(TitleValuePojo("End Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedOutTime)))

                vehicleDataList.add(TitleValuePojo("Vehicle No.", "$vehicalNumber"))
                vehicleDataList.add(TitleValuePojo("Vehicle Type", "$vehicleType"))

                parkingDataList.add(TitleValuePojo("Parking Owner", "$fullName"))
                parkingDataList.add(TitleValuePojo("Parking Name", "$parkingSpaceName"))
                parkingDataList.add(TitleValuePojo("Parking Address", "$parkingAddress"))

                paymentDataList.add(TitleValuePojo("Payment Mode", "$paymentMode"))
                paymentDataList.add(TitleValuePojo("Base Cost", "$baseAmount"))
                paymentDataList.add(TitleValuePojo("Service Cost", "$serviceCharge"))

                if (!TextUtils.isEmpty(couponDiscount))
                    paymentDataList.add(TitleValuePojo("Coupon Discount", "$couponDiscount"))
                else
                    paymentDataList.add(TitleValuePojo("Coupon Discount", "0"))

                paymentDataList.add(TitleValuePojo("Total cost", totalAmount))
            } else {
                // For public booking
                if (!TextUtils.isEmpty(bookedIntime))
                    bookingDataList.add(TitleValuePojo("Start Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedIntime)))
                if (checkedOutDateTime.isNotEmpty())
                    bookingDataList.add(TitleValuePojo("Checkout Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, checkedOutDateTime)))
                else if (bookedOutTime.isNotEmpty())
                    bookingDataList.add(TitleValuePojo("End Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedOutTime)))

                parkingDataList.add(TitleValuePojo("Parking Owner", "$fullName"))
                parkingDataList.add(TitleValuePojo("Parking Name", "$parkingSpaceName"))
                parkingDataList.add(TitleValuePojo("Parking Address", "$parkingAddress"))

                vehicleDataList.add(TitleValuePojo("Vehicle No.", "$vehicalNumber"))
                if (vehicleType.equals("2"))
                    vehicleDataList.add(TitleValuePojo("Vehicle Type", "Two Wheeler"))
                else
                    vehicleDataList.add(TitleValuePojo("Vehicle Type", "Four Wheeler"))

                paymentDataList.add(TitleValuePojo("Payment Mode", "$paymentMode"))
                paymentDataList.add(TitleValuePojo("Base Cost", "$baseAmount"))
                paymentDataList.add(TitleValuePojo("Service Cost", "$serviceCharge"))

                if (!TextUtils.isEmpty(couponDiscount))
                    paymentDataList.add(TitleValuePojo("Coupon Discount", "$couponDiscount"))
                else
                    paymentDataList.add(TitleValuePojo("Coupon Discount", "0"))

                paymentDataList.add(TitleValuePojo("Total cost", totalAmount))
                if (TextUtils.isEmpty(checkedOutDateTime)) {
                    ivBigQrCode.visibility = View.VISIBLE
                    setQrCode(bookingMadeDetailObj.barCode)
                } else {
                    tvCancelMessage.visibility = View.VISIBLE
                    tvCancelMessage.text = "**Please use SUPPORT option for any issue faced during parking"
                }
            }
            (rvBookingDetails.adapter as TitleValueAdaptor).populate(bookingDataList)
            (rvParkingDetails.adapter as TitleValueAdaptor).populate(parkingDataList)
            (rvVehicleDetails.adapter as TitleValueAdaptor).populate(vehicleDataList)
            (rvPaymentDetails.adapter as TitleValueAdaptor).populate(paymentDataList)
        }
    }

    private fun getPlan(): String {
        if (bookingMadeDetailObj.monthlySubscription.equals("D"))
            return "Monthly Day"
        else if (bookingMadeDetailObj.monthlySubscription.equals("N"))
            return "Monthly Night"
        else
            return "Monthly Both"
    }

    private fun setQrCode(iInputValue: String) {
        mQrEncoder = QRGEncoder(iInputValue, null, QRGContents.Type.TEXT, 200);
        try {
            // Getting QR-Code as Bitmap
            mBitmap = mQrEncoder?.encodeAsBitmap();
            // Setting Bitmap to ImageView
            ivBigQrCode.setImageBitmap(mBitmap)
        } catch (e: WriterException) {
            android.util.Log.v(TAG, e.toString());
        }
    }

    private fun serviceCheckoutBookingApi(isPrivate: Boolean) {
        showDialog()
        API.getClient().create(ParkingService::class.java).checkoutBooking(
                bookingMadeDetailObj.bookedId.toString(),
                DateUtil.getCurrentDate(DateUtil.FORMAT_SQL_DATE_TIME),
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<CheckoutBookingResponse>> {
            override fun onResponse(call: Call<BaseResponse<CheckoutBookingResponse>>?, response: Response<BaseResponse<CheckoutBookingResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {

                    response.body()!!.result.apply {
                        if (checkedOutBy.equals("PO", true) || TextUtils.isEmpty(checkedOutBy) ||
                                whoDidCheckout.equals("PO", true) || TextUtils.isEmpty(whoDidCheckout)) {
                            if (isPrivate && bookingMadeDetailObj.paymentMode.equals("COD")) {
                                if (totalTxnAmount == null)
                                    showPaymentDialog(totalTxnAmount!!)
                                else
                                    showPaymentDialog(totalAmount!!)
                            } else {
                                if (Refund != null && Refund.equals("YES"))
                                    callRefundApi(extraAmount!!, false)
                                else
                                    showPaymentDialog(extraAmount!!)
                            }
                        } else
                            showAlreadyCheckedOut(this)
                       Util.showToast(message)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CheckoutBookingResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun showAlreadyCheckedOut(iResponse: CheckoutBookingResponse?) {
        val promptsView = LayoutInflater.from(this@BookingMadeDetailsActivity)
                .inflate(R.layout.payment_dialog, null)

        val alertDialogBuilder = AlertDialog.Builder(this@BookingMadeDetailsActivity)
                .setView(promptsView)
                .setCancelable(false)
                .create()

        val aBPayNow = promptsView.findViewById<Button>(R.id.bPayNow)
        val aRbCash = promptsView.findViewById<RadioButton>(R.id.rbCash)
        val aRbPaytm = promptsView.findViewById<RadioButton>(R.id.rbPaytm)
        mTvPaytmBalance = promptsView.findViewById<TextView>(R.id.tvPaytmBal)
        val rvBookedParkingDetails = promptsView.findViewById<RecyclerView>(R.id.rvPaymentDetailsList)
        val aTvPaymentModeTitle = promptsView.findViewById<TextView>(R.id.tvPaymentModeLabel)
        val aRgPaymentMode = promptsView.findViewById<RadioGroup>(R.id.rgCashMode)

        aRbCash.visibility = View.GONE
        aRbPaytm.visibility = View.GONE
        aTvPaymentModeTitle.visibility = View.GONE
        aRgPaymentMode.visibility = View.GONE

        aBPayNow.setText("Ok")

        aBPayNow.setOnClickListener {
            alertDialogBuilder.dismiss()
            val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        rvBookedParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingMadeDetailsActivity, R.layout.item_bold_title_value)
        }

        val dataList: List<TitleValuePojo> = arrayListOf(
                TitleValuePojo("Status:", "Already checked out!"),
                TitleValuePojo("Checkout time:", iResponse?.checkOutPublic!!),
                TitleValuePojo("Total cost:", iResponse?.amount!!)
        )
        (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

        alertDialogBuilder.show()

        alertDialogBuilder.setOnCancelListener(DialogInterface.OnCancelListener {
            Log.d("Tag", "RatingDialogKilled")
            mPaymentMode = ""
        })
    }

    private fun serviceCancelBookingApi(msg: String) {
        showDialog()

        API.getClient().create(ParkingService::class.java).cancelBooking(
                bookingMadeDetailObj.bookedId.toString(),
                msg,
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<GenericResponse>> {
            override fun onResponse(call: Call<BaseResponse<GenericResponse>>?, response: Response<BaseResponse<GenericResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        if (mIsPublic || bookingMadeDetailObj.paymentMode.equals("COD") || TextUtils.isEmpty(bookingMadeDetailObj.totalAmount) ||
                                bookingMadeDetailObj.totalAmount.equals("0")) {
                            Util.showToast(message)
                            val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        } else
                            callRefundApi(bookingMadeDetailObj.totalAmount, true)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<GenericResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun callRefundApi(iAmount: String, isCanceled: Boolean) {

        showDialog()
        mTotalAmount = iAmount
        val dec = DecimalFormat("#.00")
        val amt = mTotalAmount?.toDouble()
        mTotalAmount = dec.format(amt)
        API.getClient().create(Paytm::class.java).getChecksum(
                bookingMadeDetailObj.orderId,
                TVPreferences.getInstance().userId,mTotalAmount
        ).enqueue(object : Callback<BaseResponse<Checksum>> {
            override fun onResponse(call: Call<BaseResponse<Checksum>>?, response: Response<BaseResponse<Checksum>>) {
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()!!.result.apply {
                        dismissDialog()
                        API.getClient().create(Paytm::class.java).initiateRefund(
                                bookingMadeDetailObj.orderId,
                                TVPreferences.getInstance().userId,
                                bookingMadeDetailObj.TXNID,
                                mTotalAmount,
                                "REFUND",
                                bookingMadeDetailObj.Payment_ID,
                                checksumHash).enqueue(object : Callback<BaseResponse<PaytmResponse>> {
                            override fun onResponse(call: Call<BaseResponse<PaytmResponse>>?, response: Response<BaseResponse<PaytmResponse>>) {
                                dismissDialog()
                                if (response.isSuccessful &&response.body()!!.resultCode == 0&&( response.body()!!.body.resultInfo.respcode == "601" )  &&  (response.body()!!.body.resultInfo.status.equals("PENDING")||response.body()!!.body.resultInfo.status.equals("TXN_SUCCESS"))) {
                                    dismissDialog()

                                    if (isCanceled) {
                                        val alertDialogBuilder = AlertDialog.Builder(this@BookingMadeDetailsActivity)
                                        alertDialogBuilder.setTitle("Alert!")
                                        alertDialogBuilder.setMessage("Your refund of Rs." + mTotalAmount + " has been initiated.")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                                                    val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                    startActivity(intent)
                                                })
                                                .show()
                                    } else {
                                        val alertDialogBuilder = AlertDialog.Builder(this@BookingMadeDetailsActivity)
                                        alertDialogBuilder.setTitle("Alert!")
                                        alertDialogBuilder.setMessage("Your refund of Rs." + iAmount + " has been initiated.")
                                                .setCancelable(false)
                                                .setPositiveButton("OK", DialogInterface.OnClickListener { dialog, which ->
                                                    showRatingDialog(false)
                                                })
                                                .show()
                                    }
                                } else {
                                    if (null != response.body())
                                        Util.showToast(response.body()!!.error)
                                    else
                                        Util.showToast("Error Code : ${response.code()}")
                                    val alertDialogBuilder = AlertDialog.Builder(this@BookingMadeDetailsActivity)
                                    alertDialogBuilder.setTitle("Alert!")
                                    alertDialogBuilder.setMessage("Error occurred. Please try again. If the problem persist please contact support.")
                                            .setCancelable(false)
                                            .setPositiveButton("RETRY", DialogInterface.OnClickListener { dialog, which ->
                                                bookingMadeDetailObj.Payment_ID = bookingMadeDetailObj.Payment_ID + "1"
                                                callRefundApi(mTotalAmount.toString(), isCanceled)
                                            })
                                            .setNegativeButton("CANCEL", DialogInterface.OnClickListener { dialog, which ->
                                                finish()
                                                val intent = Intent(this@BookingMadeDetailsActivity, MainActivity::class.java)
                                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                startActivity(intent)
                                            })
                                            .show()
                                }
                            }

                            override fun onFailure(call: Call<BaseResponse<PaytmResponse>>?, t: Throwable?) {
                                dismissDialog()
                            }
                        })
                    }
                } else {
                    dismissDialog()
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }
            override fun onFailure(call: Call<BaseResponse<Checksum>>, t: Throwable) {
                dismissDialog()
            }})}
}
