package com.truewheels.user.activities

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.SYSParkingListAdaptor
import com.truewheels.user.constants.RequestCodeConstants
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.sys.select.FetchSYSParkingResponse
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.content_my_spaces.*
import retrofit2.Callback
import retrofit2.Response

class MySpacesActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_spaces)
        initToolbar()
        init()
        serviceFetchSYSParkingListingApi()
    }

    private fun init() {
        rvSYSParkingList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SYSParkingListAdaptor(this@MySpacesActivity)
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCodeConstants.REQUEST_CODE_EDIT_SYS && resultCode == RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data)

            data?.let {
                val shouldRefresh: Boolean = data.getBooleanExtra("shouldRefresh", false)
                if (shouldRefresh)
                    serviceFetchSYSParkingListingApi()
            }
        }
    }

    private fun serviceFetchSYSParkingListingApi() {
        showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchParkingList(
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<FetchSYSParkingResponse>> {
            override fun onResponse(call: retrofit2.Call<BaseResponse<FetchSYSParkingResponse>>?, response: Response<BaseResponse<FetchSYSParkingResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body()?.result!!.parkings!!.isEmpty()) {
                        tvEmptyNotification.visibility = View.VISIBLE
                        (rvSYSParkingList.adapter as SYSParkingListAdaptor).populate(null)
                    } else {
                        tvEmptyNotification.visibility = View.GONE
                        (rvSYSParkingList.adapter as SYSParkingListAdaptor)
                                .populate(response.body()?.result!!.parkings)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: retrofit2.Call<BaseResponse<FetchSYSParkingResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })


    }
}
