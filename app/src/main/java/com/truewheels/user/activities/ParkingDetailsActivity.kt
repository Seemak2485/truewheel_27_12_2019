package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.TextUtils
import com.truewheels.user.R
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.constants.RequestCodeConstants
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.server.model.parkings.fetch_private_parking_detail.FetchParkingDetailsResponse
import com.truewheels.user.server.model.parkings.fetch_private_parking_detail.PrivateParkingDetailItem
import com.truewheels.user.server.model.sys.select.*
import com.truewheels.user.server.model.sys.systemcode.SystemCodeResponse
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.activity_parking_details.*
import kotlinx.android.synthetic.main.content_parking_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ParkingDetailsActivity : BaseActivity() {

    var parkingDetailItem: PrivateParkingDetailItem? = null
    var geoURI: String? = null
    var carVacancy: String = "0"
    var bikeVacancy: String = "0"
    var primeProduct: Int = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_parking_details)
        initToolbar()
        init()
        if (StateType.getMap().isEmpty())
            serviceSystemCodeApi()
        else
            serviceFetchParkingDetailsApi()
    }

    private fun init() {
        btnContinue.setOnClickListener {
            if (null == parkingDetailItem) {
                Util.showToast("Error fetching details. Please select this parking again")
                return@setOnClickListener
            }
            val intentBookParking = Intent(this@ParkingDetailsActivity, BookParkingActivity::class.java)
            intentBookParking.putExtra("parkingID", intent.getStringExtra("parkingID"))
            intentBookParking.putExtra("geoURI", geoURI)
            intentBookParking.putExtra("carVacancy", carVacancy)
            intentBookParking.putExtra("bikeVacancy", bikeVacancy)
            intentBookParking.putExtra("primeProduct", primeProduct)
            intentBookParking.putExtra("parkingDetailItemObj", parkingDetailItem)
            startActivityForResult(intentBookParking, RequestCodeConstants.REQUEST_CODE_FINISH_ON_COMPLETETION)
        }

        rvPrivateParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@ParkingDetailsActivity, R.layout.item_bold_title_value)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RequestCodeConstants.REQUEST_CODE_FINISH_ON_COMPLETETION && resultCode == RESULT_OK) {
            super.onActivityResult(requestCode, resultCode, data)

            finish()
        }
    }

    private fun serviceFetchParkingDetailsApi() {
        showDialog()

        API.getClient().create(ParkingService::class.java).fetchParkingDetails(
                intent.getStringExtra("parkingID")).enqueue(object : Callback<BaseResponse<FetchParkingDetailsResponse>> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<BaseResponse<FetchParkingDetailsResponse>>?, response: Response<BaseResponse<FetchParkingDetailsResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    if (response.body()?.result!!.parkingDetail.size > 0)
                        response.body()?.result!!.parkingDetail[0]!!.apply {
                            parkingDetailItem = this
                            geoURI = "${intent.getStringExtra("geoURI")} ($parkingAddress)"
                            primeProduct = daysProduct
                            tvParkingName.text = parkingSpaceName
                            tvParkingAddress.text = parkingAddress
                            if (null != intent.getStringExtra("distance")) {
                                val distance: Double = intent.getStringExtra("distance").toDouble()
                                tvParkingDistance.text = String.format("%.2f KM away", distance)
                            }
                            if (carBasicCharge.isNullOrEmpty()) tvTitleHourlyCar.text = "NA"
                            else tvTitleHourlyCar.text = "Rs. $carBasicCharge"
                            if (carDailyCharge.isNullOrEmpty()) tvTitleDailyCar.text = "NA"
                            else tvTitleDailyCar.text = "Rs. $carDailyCharge"
                            if (carmonthlycharge.isNullOrEmpty()) tvTitleMonthlyCar.text = "NA"
                            else tvTitleMonthlyCar.text = "Rs. $carmonthlycharge"
                            if (bikeBasicCharge.isNullOrEmpty()) tvTitleHourlyBike.text = "NA"
                            else tvTitleHourlyBike.text = "Rs. $bikeBasicCharge"
                            if (bikedailycharge.isNullOrEmpty()) tvTitleDailyBike.text = "NA"
                            else tvTitleDailyBike.text = "Rs. $bikedailycharge"
                            if (bikemonthycharge.isNullOrEmpty()) tvTitleMonthlyBike.text = "NA"
                            else tvTitleMonthlyBike.text = "Rs. $bikemonthycharge"
                            tvTitleVacantSpaceCar.text = noOfSpaceAvaiable
                            tvTitleVacantSpaceBike.text = noOfSpaceAvaiableBike

                            carVacancy = noOfSpaceAvaiable
                            bikeVacancy = noOfSpaceAvaiableBike


                            val dataList: ArrayList<TitleValuePojo> = arrayListOf()

                            val delimiter = ", "
                            var temp = ""
                            var tempArr: List<String>? = descriptionSelected?.split(",")
                            tempArr?.let {
                                for (x in tempArr!!) {
                                    if (x.isNotBlank()) {
                                        temp = temp + delimiter + DescriptionType.getMap().getValue(x.trim())
                                        //detailsDescArr[DescriptionType.getMap().keys.indexOf(x.trim())] = true
                                    }
                                }
                                if (temp.isNotEmpty())
                                //etDetailsDesc.setText(temp.substring(3))
                                    dataList.add(TitleValuePojo("Description", temp.substring(delimiter.length)))
                            }

                            temp = ""
                            tempArr = facilitySelected?.split(",")
                            tempArr?.let {
                                for (x in tempArr!!) {
                                    if (x.isNotBlank()) {
                                        temp = temp + delimiter + FacilityType.getMap().getValue(x.trim())
                                        //facilitiesArr[FacilityType.getMap().keys.indexOf(x.trim())] = true
                                    }
                                }
                                if (temp.isNotEmpty())
                                //etFacilities.setText(temp.substring(3))
                                    dataList.add(TitleValuePojo("Facilities", temp.substring(delimiter.length)))
                            }

                            temp = ""
                            tempArr = daysSelected?.split(",")
                            tempArr?.let {
                                for (x in tempArr) {
                                    if (x.isNotBlank()) {
                                        temp = temp + delimiter + AvailableDayType.getMap().getValue(x.trim())
                                        //availableDaysArr[AvailableDayType.getMap().keys.indexOf(x.trim())] = true
                                    }
                                }
                                if (temp.isNotEmpty())
                                //etAvailableDays.setText(temp.substring(3))
                                    dataList.add(TitleValuePojo("Available Days", temp.substring(delimiter.length)))
                            }

                            dataList.add(TitleValuePojo("Owner Comment", parkingOwnerComment))

                            (rvPrivateParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

                            if (parkingDetailItem?.averageRating != null && !TextUtils.isEmpty(parkingDetailItem?.averageRating!!))
                                rbPrivateParkingRating.rating = parkingDetailItem?.averageRating!!.toFloat()
                            if (parkingDetailItem?.verified != null && parkingDetailItem?.verified.equals("A"))
                                tvPrivateVerifiedStatus.setText("Yes")
                            else
                                tvPrivateVerifiedStatus.setText("In Progress")

                            if (!TextUtils.isEmpty(timeFrom) && !TextUtils.isEmpty(timeTo))
                                tvAvailability.setText(timeFrom + " to " + timeTo)
                        }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<FetchParkingDetailsResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun serviceSystemCodeApi() {
        showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchSystemCodes()
                .enqueue(object : Callback<BaseResponse<SystemCodeResponse>> {
                    override fun onResponse(call: Call<BaseResponse<SystemCodeResponse>>?, response: Response<BaseResponse<SystemCodeResponse>>) {
                        dismissDialog()

                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                            response.body()?.result!!.apply {
                                for (systemCode in systemCodes) {
                                    when (systemCode?.category) {
                                        "StateType" -> StateType.setMap(systemCode.categoryProperty)
                                        "SpaceType" -> SpaceType.setMap(systemCode.categoryProperty)
                                        "PropertyType" -> PropertyType.setMap(systemCode.categoryProperty)
                                        "ParkingCategory" -> ParkingCategory.setMap(systemCode.categoryProperty)
                                        "Facility Type" -> FacilityType.setMap(systemCode.categoryProperty)
                                        "DescriptionType" -> DescriptionType.setMap(systemCode.categoryProperty)
                                        "DaysType" -> AvailableDayType.setMap(systemCode.categoryProperty)
                                    }
                                }
                            }
                            serviceFetchParkingDetailsApi()
                        } else {
                            if (null != response.body())
                                Util.showToast(response.body()!!.error)
                            else
                                Util.showToast("Error Code : ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<BaseResponse<SystemCodeResponse>>?, t: Throwable?) {
                        dismissDialog()
                    }
                })
    }
}