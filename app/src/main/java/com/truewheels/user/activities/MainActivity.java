package com.truewheels.user.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import androidx.annotation.NonNull;

import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.truewheels.user.R;
import com.truewheels.user.TVPreferences;
import com.truewheels.user.constants.Constants;
import com.truewheels.user.constants.RequestCodeConstants;
import com.truewheels.user.helpers.AlertDialogHelper;
import com.truewheels.user.helpers.FusedLocationProviderHelper;
import com.truewheels.user.helpers.GoogleMapHelper;
import com.truewheels.user.interfaces.ProceedCallback;
import com.truewheels.user.server.API;
import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.ClusterMarkerItem;
import com.truewheels.user.server.model.LocationData;
import com.truewheels.user.server.model.ParkingLocationData;
import com.truewheels.user.server.model.SearchRequest;
import com.truewheels.user.server.model.SearchResponse;
import com.truewheels.user.server.model.map_distance_matrix.MapDistanceMatrixResponse;
import com.truewheels.user.server.model.parkings.FetchPrivateParkingResponse;
import com.truewheels.user.server.model.parkings.ParkingsItem;
import com.truewheels.user.server.services.LocationService;
import com.truewheels.user.util.LocationUtils;
import com.truewheels.user.util.MapsUtil;
import com.truewheels.user.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.truewheels.user.R.id.full_name;
import static com.truewheels.user.enums.TriggerSearchApiReason.DEFAULT_TRIGGER;
import static com.truewheels.user.enums.TriggerSearchApiReason.GOOGLE_PLACES_AUTOCOMPLETE_RESULT_CALLBACK;
import static com.truewheels.user.enums.TriggerSearchApiReason.LOCATION_ENABLED_BTN_CLICK;
import static com.truewheels.user.enums.TriggerSearchApiReason.POPULAR_PARKING_API;

@SuppressWarnings({"NullableProblems", "ConstantConditions"})
public class MainActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        FusedLocationProviderHelper.FusedLocationCompleteCallback, GoogleMapHelper.OnClusterItemInfoWindowClickCallback, CompoundButton.OnCheckedChangeListener{

    private static final String TAG = MainActivity.class.getName();
    private static final LatLng DEFAULT_LOCATION = new LatLng(28.631883, 77.220024D);
    private static final int DEFAULT_ZOOM = 13;

    private GoogleMapHelper mGoogleMapHelper;
    FusedLocationProviderHelper mFusedLocationProviderHelper;

    private boolean mRequestingLocationUpdates = false;
    FusedLocationProviderClient mCurrentLocationProvider;
    private LocationCallback mLocationCallback;
    private LocationRequest mLocationRequest;

    public LatLng currentLatLng = DEFAULT_LOCATION;
    private String mDistance = "";
    private DrawerLayout mDrawer;
    private LocationService mLocationService;
    private boolean mIsPublic = true;
    private boolean mIsRWA = false;
    public HashMap<String, ParkingLocationData> mMarkerMap = new HashMap<>();
    private TextView mTvSearchLocation;
    private RadioButton mRbPrivateParking, mRbPublicParking;
    private TextView mFullName;
    private TextView mPhone;
    private TextView mTvLoading;
    private Float mRating;
    private ImageView mIvSearch;
    private ImageButton mIbHomeInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar(false);
        init();
        initCurrentLocationSettings();
        initMaps();
    }

    private void initCurrentLocationSettings() {
        mCurrentLocationProvider = LocationServices.getFusedLocationProviderClient(MainActivity.this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                }
            }

            ;
        };
        mLocationRequest = new LocationRequest();
        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);
        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    /**
     * Getting the current location of the user
     */
    private void getCurrentLocation() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mCurrentLocationProvider.getLastLocation().addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null)
                        currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());

                }
            });
        } else {
            Log.d("TAG", "Permission not granted for location services");
        }
    }

    /**
     * Check the user location periodically
     */
    private void startLocationUpdates() {
        if (checkLocationPermission()) {
            mCurrentLocationProvider.requestLocationUpdates(mLocationRequest, mLocationCallback, null /* Looper */);
            mRequestingLocationUpdates = true;
        } else
            Log.d("TAG", "Permssion not granted for location services");
    }

    /**
     * Stops the periodic location updates
     */
    private void stopLocationUpdates() {
        mCurrentLocationProvider.removeLocationUpdates(mLocationCallback);
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private boolean checkLocationPermission() {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void initMaps() {
        // Constructing FusedLocationProviderClient using FusedLocationProviderHelper
        mFusedLocationProviderHelper = new FusedLocationProviderHelper(this, this);

        getCurrentLocation();
        MapsUtil.askForGps(this, RequestCodeConstants.REQUEST_CODE_CHECK_GPS_SETTINGS);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mGoogleMapHelper = new GoogleMapHelper(this, mapFragment.getView());
        mGoogleMapHelper.setHelperView(findViewById(R.id.helperView));
        mGoogleMapHelper.setOnMapInitialisedCallback(new GoogleMapHelper.OnMapInitialisedCallback() {
            @Override
            public void onMapReady() {
                getDeviceLocation();
            }
        });
        mGoogleMapHelper.setOnCameraIdleCallback(new GoogleMapHelper.OnCameraIdleCallback() {
            @Override
            public void onCameraIdle(double latitude, double longitude) {
                LocationData locationData = new LocationData();
                locationData.setParkingClass("PC_A");
                locationData.setLat(latitude);
                locationData.setLng(longitude);
                serviceSearchLocationApi(locationData);
            }
        });
        mGoogleMapHelper.setShouldCluster(true);
        mGoogleMapHelper.setOnClusterItemInfoWindowClickCallback(this);
       // mGoogleMapHelper.setOnNonClusterMarkerClickCallback(this);
        mapFragment.getMapAsync(mGoogleMapHelper);

    }

    private void init() {
        initNavDrawer();
        mTvLoading = findViewById(R.id.tvLoading);
        mTvSearchLocation = findViewById(R.id.tv_search_location);
        mIvSearch = findViewById(R.id.ivSearch);
        mIbHomeInfo = findViewById(R.id.ibHomeInfo);
        FloatingActionButton fab = findViewById(R.id.fabDeviceLocation);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });
        fab.requestFocus();

        mRbPrivateParking = findViewById(R.id.rbPrivateParking);
        mRbPublicParking = findViewById(R.id.rbPublicParking);
        mRbPrivateParking.setOnCheckedChangeListener(this);
        mRbPublicParking.setOnCheckedChangeListener(this);

        mLocationService = API.getClient().create(LocationService.class);

        initSearchPlace();
    }

    private void initNavDrawer() {
        NavigationView navigationView = findViewById(R.id.nav_view);
        final View navigationHeaderView = navigationView.getHeaderView(0);
        mFullName = navigationHeaderView.findViewById(full_name);
        mPhone = navigationHeaderView.findViewById(R.id.contact_number);
        mDrawer = findViewById(R.id.drawer_layout);
        mFullName.setText(TVPreferences.getInstance().getFullName());
        mPhone.setText(TVPreferences.getInstance().getPhone());
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        mDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                mFullName.setText(TVPreferences.getInstance().getFullName());
                mPhone.setText(TVPreferences.getInstance().getPhone());
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initSearchPlace() {

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), "AIzaSyCJzn2EdCkIyWC122Au3sxePIGHngzOvMs");
        }

        final List<com.google.android.libraries.places.api.model.Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.ID,
                com.google.android.libraries.places.api.model.Place.Field.NAME,com.google.android.libraries.places.api.model.Place.Field.ADDRESS,com.google.android.libraries.places.api.model.Place.Field.LAT_LNG);


        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setCountry("IN")
                .build();

        final PlaceAutocomplete.IntentBuilder intentBuilder = new PlaceAutocomplete
                .IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                .setFilter(typeFilter);
        final Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN")
                .build(MainActivity.this);

        mTvSearchLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Intent intent = intentBuilder.build(MainActivity.this);
                startActivityForResult(intent, RequestCodeConstants.REQUEST_CODE_PLACE_AUTOCOMPLETE);
            }
        });
        mIvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent intent = intentBuilder.build(MainActivity.this);
                startActivityForResult(intent, RequestCodeConstants.REQUEST_CODE_PLACE_AUTOCOMPLETE);
            }
        });
        mIbHomeInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialogHelper.showInfoAlertDialog(MainActivity.this,
                        "Now book your public and private parking on the go with:\n" +
                        " \n" +
                        "Public Parking enables to park at public places like Mall, Metro, MCD, LDA etc. that you can book online and rest can be navigated.All Red colour icon parking on public parking section are available for online booking.\n" +
                        " \n" +
                        "Private Parking enables available private parking space nearest to your destination.Private parking will allow you to park your vehicle in colony ,society, building , high population density area.\n" +
                        "If you have vacant space, rent it on our app !! from \"Share your space\" option available in widget bar and you can earn enough bucks sitting at home.\n" +
                        "\n" +
                        "*Zoom In the Map to see the Parking distinct on Map in case icons are override to each other *");
            }
        });

    }

    @Override
    public void onClusterItemInfoWindowClick(final ClusterMarkerItem clusterMarkerItem) {
        ProceedCallback callback;
        if (mIsPublic) {
            callback = new ProceedCallback() {
                @Override
                public void process() {
                    Intent intent = new Intent(MainActivity.this, PublicParkingDetailsActivity.class);
                    ParkingLocationData pd = clusterMarkerItem.getParkingLocationData();
                    String geoURI = "geo:" + pd.getLattitude() + "," + pd.getLongitude() + "?q=" + pd.getLattitude() + "," + pd.getLongitude();
                    intent.putExtra("geoURI", geoURI);
                    if (mDistance.length() > 0)
                        intent.putExtra("distance", mDistance);
                    intent.putExtra("parkingID", pd.getParking_id());
                    startActivity(intent);
                }
            };
        }


        else {
            callback = new ProceedCallback() {
                @Override
                public void process() {
                    Intent intent = new Intent(MainActivity.this, ParkingDetailsActivity.class);
                    ParkingLocationData pd = clusterMarkerItem.getParkingLocationData();
                    String geoURI = "geo:" + pd.getLattitude() + "," + pd.getLongitude() + "?q=" + pd.getLattitude() + "," + pd.getLongitude();
                    intent.putExtra("geoURI", geoURI);
                    if (mDistance.length() > 0)
                        intent.putExtra("distance", mDistance);
                    intent.putExtra("parkingID", pd.getParking_id());
                    startActivity(intent);
                }
            };
        }

        LatLng source;
        ParkingLocationData pd = clusterMarkerItem.getParkingLocationData();
        LatLng dest = new LatLng(Double.valueOf(pd.getLattitude()), Double.valueOf(pd.getLongitude()));
        if (mGoogleMapHelper.getTriggerSearchApiReason() == POPULAR_PARKING_API)
            source = currentLatLng;
        else
            source = currentLatLng;
//        source = new LatLng(
//                    mGoogleMapHelper.getMap().getCameraPosition().target.latitude,
//                    mGoogleMapHelper.getMap().getCameraPosition().target.longitude);

        serviceMapDistanceMatrixApi(source, dest, callback);
    }

    private void serviceMapDistanceMatrixApi(LatLng source, LatLng destination, final ProceedCallback callback) {
        showDialog();
        mDistance = "";

        mLocationService.fetchMapDistanceMatrix(
                Constants.MAP_DISTANCE_MATRIX_URL,
                source.latitude + "," + source.longitude,
                destination.latitude + "," + destination.longitude,
                Constants.MAP_DISTANCE_MATRIX_KEY
        ).enqueue(new Callback<MapDistanceMatrixResponse>() {
            @Override
            public void onResponse(Call<MapDistanceMatrixResponse> call, Response<MapDistanceMatrixResponse> response) {
                dismissDialog();
                if (response.isSuccessful()) {

                    if (response.body().getStatus().equals("OK") && response.body().getRows().get(0)
                            .getElements().get(0).getStatus().equals("OK")) {
                        double dist = response.body()
                                .getRows().get(0)
                                .getElements().get(0)
                                .getDistance()
                                .getValue() / 1000.0;
                        mDistance = String.valueOf(dist);
                    }

                    callback.process();
                } else {
                    Util.showToast("Error Code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<MapDistanceMatrixResponse> call, Throwable t) {
                dismissDialog();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        hideKeyboard();
        switch (requestCode) {
            case RequestCodeConstants.REQUEST_CODE_CHECK_GPS_SETTINGS:
                // If the client is interested in which location providers are available, it can
                // retrieve a LocationSettingsStates from the Intent by calling fromIntent(Intent)
                //noinspection unused
                /*try {
                    final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user was asked to change settings, but chose not to
                        break;
                }
                break;

            case RequestCodeConstants.REQUEST_CODE_PLACE_AUTOCOMPLETE:
                Util.hideKeyboard(MainActivity.this, mToolbar);
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        mTvSearchLocation.setText(place.getAddress());
                        LocationData locationData = new LocationData();
                        locationData.setDestination(place.getName().toString());
                        locationData.setParkingClass("PC_A");
                        locationData.setLat(place.getLatLng().latitude);
                        locationData.setLng(place.getLatLng().longitude);
                        // map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                        //        place.getLatLng(), DEFAULT_ZOOM));
                        mGoogleMapHelper.setTriggerSearchApiReason(GOOGLE_PLACES_AUTOCOMPLETE_RESULT_CALLBACK);
                        serviceSearchLocationApi(locationData);
                        break;
                    case AutocompleteActivity.RESULT_ERROR:
                        Status status = Autocomplete.getStatusFromIntent(data);
                        // Handle the error.
                        Util.showToast("An error occurred. Please try again");
                        Log.i(TAG, status.getStatusMessage());
                        break;
                    case Activity.RESULT_CANCELED:
                        // The user canceled the operation.
                        break;
                }
        }
    }

    public void serviceSearchLocationApi(final LocationData data) {
        clearAllMarkers();
        mMarkerMap.clear();

        SearchRequest request = new SearchRequest();
        if (mGoogleMapHelper.getTriggerSearchApiReason() != POPULAR_PARKING_API) {
            request.setLat(data.getLat());
            request.setLong(data.getLng());
        } else {
            request.setLat(currentLatLng.latitude);
            request.setLong(currentLatLng.longitude);
        }

        if (mGoogleMapHelper.getTriggerSearchApiReason() == LOCATION_ENABLED_BTN_CLICK ||
                mGoogleMapHelper.getTriggerSearchApiReason() == GOOGLE_PLACES_AUTOCOMPLETE_RESULT_CALLBACK) {

            LatLng latLng = new LatLng(data.getLat(), data.getLng());
            mGoogleMapHelper.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
        } else if (mGoogleMapHelper.getTriggerSearchApiReason() == POPULAR_PARKING_API) {
            request.setPopularParking(true);
        }
        String locationAddress = MapsUtil.getAddressFromLocation(
                this, data.getLat(), data.getLng());
        mTvSearchLocation.setText(locationAddress);


        mTvLoading.setVisibility(View.VISIBLE);
        if (mIsPublic) {
            mRbPrivateParking.setEnabled(false);
            getPublicParking(request);
        } else {
            mRbPublicParking.setEnabled(false);
            getPrivateParking(data);
        }
       /* mGoogleMapHelper.getMap().addMarker(new MarkerOptions()
                .title("default")
                .position(currentLatLng)
                .icon(BitmapDescriptorFactory.fromResource((R.drawable.map_icon_red))));*/
      //  mGoogleMapHelper.getMap().setOnMarkerClickListener(this);
    }

    private void getPrivateParking(LocationData iData) {
        mLocationService.fetchPrivateParking(
                String.valueOf(iData.getLat()),
                String.valueOf(iData.getLng())).enqueue(new Callback<BaseResponse<FetchPrivateParkingResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<FetchPrivateParkingResponse>> call, Response<BaseResponse<FetchPrivateParkingResponse>> response) {
                mTvLoading.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body().getResultCode() == 0) {


                    List<ParkingsItem> nearestParkingData = response.body().getResult().getParkings();

                    for (ParkingsItem data : nearestParkingData) {
                        ParkingLocationData pd = new ParkingLocationData();
                        pd.setParking_id(data.getParkingId().toString());
                        pd.setDistance(data.getDistance());
                        pd.setLattitude(data.getLattitude());
                        pd.setLongitude(data.getLongitude());
                        pd.setCarBasicCharge(data.getCarBasicCharge());
                        pd.setBikeBasicCharge(data.getBikeBasicCharge());
                        pd.setSpace_type(data.getSpacetype());
                        addMarker(pd);
                    }
                    mGoogleMapHelper.getClusterManager().cluster();

                } else {
                    if (null != response.body())
                        Util.showToast(response.body().getError());
                    else
                        Util.showToast("Error Code : " + response.code());
                }
                mRbPublicParking.setEnabled(true);
            }

            @Override
            public void onFailure(Call<BaseResponse<FetchPrivateParkingResponse>> call, Throwable t) {
                mTvLoading.setVisibility(View.GONE);
                mRbPublicParking.setEnabled(true);
            }
        });
    }

    private void getPublicParking(SearchRequest iRequest) {
        mLocationService.fetchPublicParking(iRequest).enqueue(new Callback<BaseResponse<SearchResponse>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<SearchResponse>> call, @NonNull Response<BaseResponse<SearchResponse>> response) {
                mTvLoading.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body().getResultCode() == 0) {
                    ArrayList<ParkingLocationData> nearestParkingData = response.body().getResult().getParkingList();

                    if (mGoogleMapHelper.getTriggerSearchApiReason() != POPULAR_PARKING_API) {
                        for (ParkingLocationData data : nearestParkingData)
                            addMarker(data);
                    } else {
                        if (nearestParkingData.size() == 1) {
                            LatLng latLng = addMarker(nearestParkingData.get(0));
                            mGoogleMapHelper.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
                        } else {
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();

                            for (ParkingLocationData data : nearestParkingData) {
                                LatLng latLng = addMarker(data);
                                builder.include(latLng);
                            }
                            LatLngBounds bounds = builder.build();
                            mGoogleMapHelper.getMap().animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));
                        }
                    }
                    mGoogleMapHelper.getClusterManager().cluster();

                } else {
                    if (null != response.body())
                        Util.showToast(response.body().getError());
                    else
                        Util.showToast("Error Code : " + response.code());
                }
                mRbPrivateParking.setEnabled(true);
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<SearchResponse>> call, @NonNull Throwable t) {
                mTvLoading.setVisibility(View.GONE);
                mRbPrivateParking.setEnabled(true);
            }
        });
    }

    private LatLng addMarker(ParkingLocationData data) {
        LatLng latLng = new LatLng(Double.parseDouble(data.getLattitude()), Double.parseDouble(data.getLongitude()));
        ClusterMarkerItem markerItem = new ClusterMarkerItem(latLng.latitude, latLng.longitude);
        markerItem.setParkingLocationData(data);
        markerItem.setPublic(mIsPublic);
//        if (mIsPublic && data.getBookingAvailableFlag().equalsIgnoreCase("Y")) {
//            markerItem.setPublicAvailable(true);
////                //Set Custom InfoWindow Adapter
//            CustomSingleInfoViewAdapter adapter = new CustomSingleInfoViewAdapter(LayoutInflater.from(MainActivity.this));
//            mGoogleMapHelper.getMap().setInfoWindowAdapter(adapter);
//
//            MarkerOptions aMarkerOptions = new MarkerOptions().position(new LatLng(Double.parseDouble(data.getLattitude()),
//                    Double.parseDouble(data.getLongitude())));
//            Marker aMarker = mGoogleMapHelper.getMap().addMarker(aMarkerOptions);
//            mMarkerMap.put(aMarker.getId(), data);
//            Log.d("TAG", "A public available marker: " + aMarker);
//
////                aMarker.showInfoWindow();
//        } else {
        if (mIsPublic) {
            if (data.getBookingAvailableFlag().equalsIgnoreCase("Y")) {
                markerItem.setPublicAvailable(true);
            } else {
                markerItem.setPublicAvailable(false);
            }
        }

        if(data.getSpace_type()!=null && data.getSpace_type().equalsIgnoreCase("ST_RWA")) {
            markerItem.setRWAAvailable(true);
            mIsRWA = true;
        }
        else {
            markerItem.setRWAAvailable(false);
            mIsRWA =  false;
        }
        mGoogleMapHelper.getClusterManager().addItem(markerItem);
        return latLng;
    }

    private void clearAllMarkers() {
        mGoogleMapHelper.getMap().clear();
        mGoogleMapHelper.getClusterManager().clearItems();
        mGoogleMapHelper.getClusterManager().cluster();
    }

    public void getDeviceLocation() {
        mFusedLocationProviderHelper.getDeviceLocation();
    }

    @Override
    public void onFusedLocationComplete(@NonNull Task<Location> task) {
        if (task.isSuccessful()) {
            Location mLastKnownLocation = task.getResult();
            if (null == mLastKnownLocation)
                return;
            currentLatLng = new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            if (mGoogleMapHelper.getMap() == null) {
                return;
            }

          //  mGoogleMapHelper.getMap().setOnMarkerClickListener(this);
            // map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, DEFAULT_ZOOM));
            LocationData data = new LocationData();
            data.setLat(mLastKnownLocation.getLatitude());
            data.setLng(mLastKnownLocation.getLongitude());
            mGoogleMapHelper.setTriggerSearchApiReason(LOCATION_ENABLED_BTN_CLICK);
            serviceSearchLocationApi(data);

            String locationAddress = MapsUtil.getAddressFromLocation(
                    this, mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude());
            mTvSearchLocation.setText(locationAddress);
        } else {
            mGoogleMapHelper.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(DEFAULT_LOCATION, DEFAULT_ZOOM));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCodeConstants.REQUEST_CODE_PERMISSION_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getDeviceLocation();
                } else {
                    Util.showToast("Change your Settings to allow this app to access location");
                }
            }
            break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.popular_parking:
                mGoogleMapHelper.setTriggerSearchApiReason(POPULAR_PARKING_API);
                mIsPublic = true;
                serviceSearchLocationApi(null);
                break;
            case R.id.profile:
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                Bundle bundle = ProfileActivity.Companion.getBundle(false, false, TVPreferences.getInstance().getPhone());
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.my_bookings:
                startActivity(new Intent(this, MyBookingsActivity.class));
                break;
            case R.id.booking_received:
                startActivity(new Intent(this, BookingReceivedActivity.class));
                break;
            case R.id.how_it_works:
                startActivity(new Intent(this, HowItWorksActivity.class));
                break;
            case R.id.my_spaces:
                startActivity(new Intent(this, MySpacesActivity.class));
                break;
            case R.id.share_your_space:
                intent = new Intent(this, AddSYSActivity.class);
                intent.putExtra("mode", "add");
                startActivity(intent);
                break;
            case R.id.share:
                Util.shareApp(MainActivity.this);
                break;
            case R.id.reach_us:
                Util.openWhatsApp(MainActivity.this,
                        "919711018756",
                        TVPreferences.getInstance().getFullName() + "\n( Support-id: " +
                                "Ganeis" + TVPreferences.getInstance().getUserId() + System.currentTimeMillis() + " )\n" +
                                "Message :\n",
                        new Util.ErrorCallback() {
                            @Override
                            public void perform() {
                                Util.showToast("Error. Try our website!");
                                Util.openBrowser(MainActivity.this, "http://www.truewheelsuat.in/#contact");
                            }
                        });
                //startActivity(new Intent(this, ReachUsActivity.class));
                break;
            case R.id.about_us:
                startActivity(new Intent(this, AboutUsActivity.class));
                break;
            case R.id.my_earnings:
                startActivity(new Intent(this, MyEarningsActivity.class));
                break;
        }

        mDrawer.closeDrawers();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b) {
            mIsPublic = compoundButton.getId() == R.id.rbPublicParking;
            searchLocationApi();
        }
    }

    private void searchLocationApi() {
        mGoogleMapHelper.setTriggerSearchApiReason(DEFAULT_TRIGGER);
        LocationData locationData = new LocationData();
        locationData.setParkingClass("PC_A");
        locationData.setLat(mGoogleMapHelper.getMap().getCameraPosition().target.latitude);
        locationData.setLng(mGoogleMapHelper.getMap().getCameraPosition().target.longitude);
        serviceSearchLocationApi(locationData);
    }

    @Override
    public void onResume() {
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
        mRequestingLocationUpdates = false;
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



}

