package com.truewheels.user.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import android.view.View;
import android.widget.ProgressBar;

import com.truewheels.user.BuildConfig;
import com.truewheels.user.R;
import com.truewheels.user.TVPreferences;
import com.truewheels.user.helpers.AlertDialogHelper;
import com.truewheels.user.server.API;
import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.app_version.LatestAppVersionResponse;
import com.truewheels.user.server.services.AppService;
import com.truewheels.user.util.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    @SuppressWarnings("FieldCanBeLocal")
    private boolean mTesting = false;
//    private boolean mTesting = BuildConfig.DEBUG;

    private ProgressBar mPbSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initViews();

//        if (!mTesting)
//            serviceFetchLatestAppVersionApi();
//        else {
//            Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    openNextCorrespondingActivity();
//                }
//            }, 2000);
//        }
        openNextCorrespondingActivity();

    }

    private void initViews() {
        mPbSplash = findViewById(R.id.pSplash);
    }

    private void openNextCorrespondingActivity() {
        TVPreferences preferences = TVPreferences.getInstance();
        String authToken = preferences.getAuthToken();
        boolean isProfileUpdated = preferences.getIsProfileUpdated();
        if (null == authToken) {
            startActivity(new Intent(SplashActivity.this, LoginMobileNoActivity.class));
        } else if (isProfileUpdated) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        } else {
            Intent i = new Intent(SplashActivity.this, ProfileActivity.class);
            Bundle b = ProfileActivity.Companion.getBundle(true, true, TVPreferences.getInstance().getPhone());
            i.putExtras(b);
            startActivity(i);
        }
        finish();
    }

    private void serviceFetchLatestAppVersionApi() {
        mPbSplash.setVisibility(View.VISIBLE);
        API.getClient().create(AppService.class).fetchLatestAppVersion().enqueue(new Callback<BaseResponse<LatestAppVersionResponse>>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse<LatestAppVersionResponse>> call, @NonNull Response<BaseResponse<LatestAppVersionResponse>> response) {
                mPbSplash.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body().getResultCode() == 0) {
                    LatestAppVersionResponse res = response.body().getResult();
                    if (res.getShouldForceUpdate() && !BuildConfig.VERSION_NAME.equals(res.getCurrentLatestVersion())) {
                        AlertDialogHelper.showAlertDialog(SplashActivity.this,
                                null,
                                "Please update the app to continue",
                                "OK",
                                "Cancel",
                                new AlertDialogHelper.Callback() {
                                    @Override
                                    public void onBack() {
                                        finish();
                                    }

                                    @Override
                                    public void onSuccess() {
                                        Util.openPlaystore(SplashActivity.this);
                                    }
                                });
                    } else {
                        openNextCorrespondingActivity();
                    }

                } else {
                    if (null != response.body())
                        Util.showToast(response.body().getError());
                    else
                        Util.showToast("Error Code : " + response.code());
                    finish();
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse<LatestAppVersionResponse>> call, @NonNull Throwable t) {
                mPbSplash.setVisibility(View.GONE);
            }
        });
    }
}
