package com.truewheels.user.activities

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.SupportAdaptor
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.GenericResponse
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.activity_support.*
import kotlinx.android.synthetic.main.content_support.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SupportActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
        initToolbar()
        init()
    }

    private fun init() {
        rvSupport.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = SupportAdaptor()
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }

        val dataList: List<String> = if (intent.getBooleanExtra("isFromBookingMade", false))
            arrayListOf("Parking owner related issue",
                    "Billing related issue",
                    "Facility related issue in parking location",
                    "Parking related issue",
                    "Others")
        else
            arrayListOf("Vehicle owner related issue",
                    "Billing related issue",
                    "Vehicle related issue",
                    "Others")

        ((rvSupport.adapter as SupportAdaptor).populate(dataList))

        btnReportIssue.setOnClickListener {
            if (isFormValid())
                serviceSupportBookingApi()
        }
    }

    private fun isFormValid(): Boolean {
        var formValidity = true

        if ((rvSupport.adapter as SupportAdaptor).selectedPos == -1) {
            Util.showToast("Please select an option")
            formValidity = false
        } else if ((rvSupport.adapter as SupportAdaptor).getSupportMessage().isEmpty()) {
            Util.showToast("Please give a message")
            formValidity = false
        }

        return formValidity
    }


    private fun serviceSupportBookingApi() {
        showDialog()

        API.getClient().create(ParkingService::class.java).supportForBooking(
                intent.getStringExtra("bookingID"),
                (rvSupport.adapter as SupportAdaptor).getSupportTitle(),
                (rvSupport.adapter as SupportAdaptor).getSupportMessage(),
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<GenericResponse>> {
            override fun onResponse(call: Call<BaseResponse<GenericResponse>>?, response: Response<BaseResponse<GenericResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()!!.result.apply {
                        AlertDialogHelper.showInfoAlertDialog(this@SupportActivity, message) {
                            val intent = Intent(this@SupportActivity, MainActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(intent)
                        }
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<GenericResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

}
