package com.truewheels.user.activities

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.paytm.pgsdk.Log
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.enums.BookingStatus
import com.truewheels.user.helpers.AlertDialogHelper
import com.truewheels.user.helpers.EdittextDialogHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.GenericResponse
import com.truewheels.user.server.model.SignUpResponse
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import com.truewheels.user.server.model.parkings.CashCheckoutResponse
import com.truewheels.user.server.model.parkings.CheckoutBookingResponse
import com.truewheels.user.server.model.parkings.booking_received.BookingRecievedItem
import com.truewheels.user.server.services.ParkingService
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.DateUtil
import com.truewheels.user.util.Util
import kotlinx.android.synthetic.main.activity_booking_received_details.*
import kotlinx.android.synthetic.main.content_booking_received_details.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BookingReceivedDetailsActivity : BaseActivity(), CompoundButton.OnCheckedChangeListener {

    private lateinit var bookingReceivedDetailObj: BookingRecievedItem
    private var mTotalAmount: String? = ""
    private var mTvPaytmBalance: TextView? = null
    private var mPaymentMode: String? = ""
    private var duration = ""
    private var aCbClean: CheckBox? = null
    private var aCbNice: CheckBox? = null
    private var aCbSafe: CheckBox? = null
    private var aCbValue: CheckBox? = null
    private var mRating: Float? = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_received_details)
        initToolbar()
        init()
        autofill()
    }

    private fun init() {
        bookingReceivedDetailObj = intent.getSerializableExtra("bookingReceivedDetailObj") as BookingRecievedItem

        if (!TextUtils.isEmpty(bookingReceivedDetailObj.averageRating))
            rbUserRating.rating = bookingReceivedDetailObj.averageRating.toFloat()
        rvBookingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingReceivedDetailsActivity, R.layout.item_bold_title_value)
        }
        rvParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingReceivedDetailsActivity, R.layout.item_bold_title_value)
        }
        rvPaymentDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingReceivedDetailsActivity, R.layout.item_bold_title_value)
        }
        rvVehicleDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingReceivedDetailsActivity, R.layout.item_bold_title_value)
        }
        btnCheckoutBooking.setOnClickListener {
            if (bookingReceivedDetailObj.monthlySubscription.equals("M")) {
                serviceInOutMonthly()
            } else
                AlertDialogHelper.showAlertDialog(this@BookingReceivedDetailsActivity,
                        null,
                        "Are you sure want to Checkout this parked vehicle?\n" +
                                "User will be charged as per the Start and Checkout time only.",
                        object : AlertDialogHelper.Callback {
                            override fun onSuccess() {
                                serviceCheckoutBookingApi()
                            }

                            override fun onBack() {
                            }

                        })
        }
        btnCancelBooking.setOnClickListener {
            EdittextDialogHelper.showEdittextDialog(this@BookingReceivedDetailsActivity,
                    "Reason for cancellation",
                    object : EdittextDialogHelper.Callback {
                        override fun onSuccess(message: String) {
                            AlertDialogHelper.showAlertDialog(this@BookingReceivedDetailsActivity,
                                    null,
                                    "Are you sure want to Cancel this booked parking?\n" +
                                            "You will not be charged any fee for this.",
                                    object : AlertDialogHelper.Callback {
                                        override fun onSuccess() {
                                            serviceCancelBookingApi(message)
                                        }

                                        override fun onBack() {
                                        }

                                    })
                        }

                        override fun onBack() {
                            //Do nothing
                        }

                    })
        }
        btnSupportBooking.setOnClickListener {
            val intent = Intent(this@BookingReceivedDetailsActivity, SupportActivity::class.java)
            intent.putExtra("bookingID", bookingReceivedDetailObj.bookedId.toString())
            intent.putExtra("isFromBookingMade", false)
            startActivity(intent)
        }

        when (bookingReceivedDetailObj.bookingStatus) {
            BookingStatus.SCHEDULED.status -> {
                btnCheckoutBooking.visibility = View.GONE
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingReceivedDetailsActivity, android.R.color.holo_green_dark))
            }
            BookingStatus.IN_PROGRESS.status -> {
                btnCancelBooking.visibility = View.GONE
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingReceivedDetailsActivity, android.R.color.black))
            }
            BookingStatus.CANCELLED.status -> {
                btnCancelBooking.visibility = View.GONE
                btnCheckoutBooking.visibility = View.GONE
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingReceivedDetailsActivity, android.R.color.holo_red_dark))
            }
            BookingStatus.PARKED_OUT.status -> {
                btnCancelBooking.visibility = View.GONE
                btnCheckoutBooking.visibility = View.GONE
                tvStatus.setTextColor(ContextCompat.getColor(this@BookingReceivedDetailsActivity, android.R.color.holo_red_dark))
            }
        }

        var isMonthly = false
        if (bookingReceivedDetailObj.monthlySubscription.equals("M")) {
            btnCheckoutBooking.text = "Checked In/Out"
            isMonthly = true
        }

        if (!TextUtils.isEmpty(bookingReceivedDetailObj.checkedOutby) && !isMonthly && bookingReceivedDetailObj.checkedOutby != null)
            btnCheckoutBooking.visibility = View.GONE
    }

    @SuppressLint("SetTextI18n")
    private fun autofill() {
        bookingReceivedDetailObj.apply {
            tvBookingId.text = bookedId.toString()
            tvStatus.text = bookingStatus
            tvBookedParkingAddress.text = "$parkingSpaceName \n$parkingAddress"

            duration = ""
            if (!totalMonth.isNullOrBlank() && totalMonth != "0")
                duration += " $totalMonth month" + if (totalMonth != "1") "s" else ""
            if (!totalDays.isNullOrBlank() && totalDays != "0")
                duration += " $totalDays day" + if (totalDays != "1") "s" else ""
            if (!totalHour.isNullOrBlank() && totalHour != "0")
                duration += " $totalHour hr" + if (totalHour != "1") "s" else ""
            if (!totalMin.isNullOrBlank() && totalMin != "0")
                duration += " $totalMin min" + if (totalMin != "1") "s" else ""
            if (duration == "")
                duration = "0 minute"
            duration = duration?.trim()

            val bookingDataList: ArrayList<TitleValuePojo> = arrayListOf()
            val parkingDataList: ArrayList<TitleValuePojo> = arrayListOf()
            val vehicleDataList: ArrayList<TitleValuePojo> = arrayListOf()
            val paymentDataList: ArrayList<TitleValuePojo> = arrayListOf()

            bookingDataList.add(TitleValuePojo("Booking Mode", when (monthlySubscription) {
                "H" -> "Hourly"
                "D" -> "Daily"
                else -> "Monthly"
            }))

            bookingDataList.add(TitleValuePojo("Start Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedIntime)))
            if (checkedOutDateTime.isNotEmpty() && !bookingStatus.equals(BookingStatus.PARKED_OUT.status))
                bookingDataList.add(TitleValuePojo("Checkout Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, checkedOutDateTime)))
            else
                bookingDataList.add(TitleValuePojo("End Time", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookedOutTime)))

            vehicleDataList.add(TitleValuePojo("Vehicle No.", "$vehicalNumber"))
            vehicleDataList.add(TitleValuePojo("Vehicle Type", "$vehicleType"))
//            bookingDataList.add(TitleValuePojo("Vehicle Owner Name", "$fullName"))


            parkingDataList.add(TitleValuePojo("Parking Owner", "$fullName"))
            parkingDataList.add(TitleValuePojo("Parking Name", "$parkingSpaceName"))
            parkingDataList.add(TitleValuePojo("Parking Address", "$parkingAddress"))

            paymentDataList.add(TitleValuePojo("Payment Mode", "$paymentMode"))
            paymentDataList.add(TitleValuePojo("Base Amount", "$baseAmount"))
            paymentDataList.add(TitleValuePojo("Service Charge", "$serviceCharge"))
            if (!TextUtils.isEmpty(couponDiscount))
                paymentDataList.add(TitleValuePojo("Coupon Discount", "$couponDiscount"))
            else
                paymentDataList.add(TitleValuePojo("Coupon Discount", "0"))

            paymentDataList.add(TitleValuePojo("Total Cost", "$totalAmount"))

            (rvBookingDetails.adapter as TitleValueAdaptor).populate(bookingDataList)
            (rvParkingDetails.adapter as TitleValueAdaptor).populate(parkingDataList)
            (rvVehicleDetails.adapter as TitleValueAdaptor).populate(vehicleDataList)
            (rvPaymentDetails.adapter as TitleValueAdaptor).populate(paymentDataList)
        }

    }

    private fun serviceCheckoutBookingApi() {
        showDialog()

        API.getClient().create(ParkingService::class.java).checkoutBooking(
                bookingReceivedDetailObj.bookedId.toString(),
                DateUtil.getCurrentDate(DateUtil.FORMAT_SQL_DATE_TIME),
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<CheckoutBookingResponse>> {
            override fun onResponse(call: Call<BaseResponse<CheckoutBookingResponse>>?, response: Response<BaseResponse<CheckoutBookingResponse>>) {
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        //                        Util.showToast(message)
                        showPaymentDialog(totalAmount!!)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CheckoutBookingResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

    private fun showPaymentDialog(iAmount: String) {
        mTotalAmount = iAmount

        val promptsView = LayoutInflater.from(this@BookingReceivedDetailsActivity)
                .inflate(R.layout.payment_dialog, null)

        val alertDialogBuilder = AlertDialog.Builder(this@BookingReceivedDetailsActivity)
                .setView(promptsView)
                .setCancelable(false)
                .create()

        val aBPayNow = promptsView.findViewById<Button>(R.id.bPayNow)
        val aRbCash = promptsView.findViewById<RadioButton>(R.id.rbCash)
        val aRbPaytm = promptsView.findViewById<RadioButton>(R.id.rbPaytm)
        val aTvPaytmBal = promptsView.findViewById<TextView>(R.id.tvPaymentModeLabel)
        mTvPaytmBalance = promptsView.findViewById<TextView>(R.id.tvPaytmBal)
        val rvBookedParkingDetails = promptsView.findViewById<RecyclerView>(R.id.rvPaymentDetailsList)

        aRbCash.setOnCheckedChangeListener(this)
        aRbPaytm.setOnCheckedChangeListener(this)

        aRbPaytm.visibility = View.GONE
        aRbCash.visibility = View.GONE
        aTvPaytmBal.visibility = View.GONE

        aBPayNow.setOnClickListener {
            doCashTransaction(alertDialogBuilder)
        }

        rvBookedParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@BookingReceivedDetailsActivity, R.layout.item_bold_title_value)
        }

//        if (bookingReceivedDetailObj.checkedOutby == null)
//            aBPayNow.setText("Collect Cash")
//        else
            aBPayNow.setText("Ok")

        if (bookingReceivedDetailObj.checkedOutby == null) {
            val dataList: List<TitleValuePojo> = arrayListOf(
                    TitleValuePojo("Total cost:", iAmount),
                    TitleValuePojo("Checked in time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingReceivedDetailObj.bookedIntime)))
            (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)
        } else {
            if (bookingReceivedDetailObj.checkedOutby.equals("PO")) {
                val dataList: List<TitleValuePojo> = arrayListOf(
                        TitleValuePojo("Status:", "Already checked out"),
                        TitleValuePojo("Total cost:", iAmount),
                        TitleValuePojo("Checked in time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingReceivedDetailObj.bookedIntime)),
                        TitleValuePojo("Checked out time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingReceivedDetailObj.checkedOutDateTime)))
                (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

            } else {
                val dataList: List<TitleValuePojo> = arrayListOf(
                        TitleValuePojo("Status:", "Already checked out by vehicle owner"),
                        TitleValuePojo("Total cost:", iAmount),
                        TitleValuePojo("Checked in time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingReceivedDetailObj.bookedIntime)),
                        TitleValuePojo("Checked out time:", DateUtil.convertFormat(DateUtil.FORMAT_SQL_SLASH_DATE_TIME, DateUtil.FORMAT_READABLE_DATE_TIME_SHORT, bookingReceivedDetailObj.checkedOutDateTime)))
                (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)

            }
        }

        alertDialogBuilder.show()

        alertDialogBuilder.setOnCancelListener(DialogInterface.OnCancelListener {
            Log.d("Tag", "RatingDialogKilled")
            mPaymentMode = ""
        })
    }

    private fun doCashTransaction(alertDialogBuilder: AlertDialog?) {
        showDialog()

        API.getClient().create(ParkingService::class.java).collectCashRequest(
                bookingReceivedDetailObj.bookedId.toString(),
                mTotalAmount).enqueue(object : Callback<BaseResponse<CashCheckoutResponse>> {
            override fun onResponse(call: Call<BaseResponse<CashCheckoutResponse>>?, response: Response<BaseResponse<CashCheckoutResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()

                    response.body()!!.result.apply {
                        alertDialogBuilder?.dismiss()
                        showRatingDialog(response.body()!!.result)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CashCheckoutResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

    private fun showRatingDialog(iResult: CashCheckoutResponse) {
        val promptsView = LayoutInflater.from(this@BookingReceivedDetailsActivity)
                .inflate(R.layout.rating_dialog, null)

        val alertDialogBuilder = AlertDialog.Builder(this@BookingReceivedDetailsActivity)
                .setView(promptsView)
                .setCancelable(false)
                .create()

        val aRbParkingRating = promptsView.findViewById<RatingBar>(R.id.rbRateCall)
        val aTvPaymentRecieved = promptsView.findViewById<TextView>(R.id.tvPaymentRecieved)
        val aEtFeedback = promptsView.findViewById<EditText>(R.id.etFeedback)
        aCbClean = promptsView.findViewById<CheckBox>(R.id.cbClean)
        aCbNice = promptsView.findViewById<CheckBox>(R.id.cbBehaviour)
        aCbSafe = promptsView.findViewById<CheckBox>(R.id.cbSafe)
        aCbValue = promptsView.findViewById<CheckBox>(R.id.cbValue)
        val mBSubmit = promptsView.findViewById<Button>(R.id.bSubmit)
        val aTvParking = promptsView.findViewById<TextView>(R.id.tvRateParking)

        aTvParking.setText("Rate Vehicle Owner")
        aCbClean?.setText("Clean Vehicle")
        aCbNice?.setText("Nice behaviuor")
        aCbSafe?.visibility = View.GONE
        aCbValue?.visibility = View.GONE

        aRbParkingRating.setOnRatingBarChangeListener(RatingBar.OnRatingBarChangeListener { ratingBar, v, b ->
            mRating = v
        })
        mBSubmit.setOnClickListener {
            if (mRating!! > 0) {
                submitResponse(alertDialogBuilder, iResult, aEtFeedback.text.toString().trim(), getFeedback())
            } else
                Util.showToast("Please rate the parking. It will help us in providing you better services.")
        }

        aTvPaymentRecieved.text = "Rs " + mTotalAmount + " have been successfully recieved."
        alertDialogBuilder.show()
    }

    private fun getFeedback(): String {
        var aFeedBackString: String? = ""
        if (aCbClean?.isChecked!!)
            aFeedBackString = aFeedBackString + "CL,"
        if (aCbNice?.isChecked!!)
            aFeedBackString = aFeedBackString + "NB,"
        if (aCbSafe?.isChecked!!)
            aFeedBackString = aFeedBackString + "SF,"
        if (aCbValue?.isChecked!!)
            aFeedBackString = aFeedBackString + "VL,"
        return aFeedBackString!!
    }

    private fun submitResponse(alertDialogBuilder: AlertDialog, iResponse: CashCheckoutResponse, iComment: String, iFeedBack: String) {
        API.getClient().create(UserService::class.java).rateParking(
                0,
                0,
                0,
                0,
                "",
                TVPreferences.getInstance().userId.toInt(),
                mRating!!.toInt(),
                iComment,
                bookingReceivedDetailObj.bookedId.toString()
        ).enqueue(object : Callback<BaseResponse<SignUpResponse>> {
            override fun onResponse(call: Call<BaseResponse<SignUpResponse>>?, response: Response<BaseResponse<SignUpResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        Util.showToast(message)
                    }
                    alertDialogBuilder.dismiss()
                    AlertDialogHelper.showInfoAlertDialog(this@BookingReceivedDetailsActivity, "Thank you for rating the vehicle owner.") {
                        finish()
                        val intent = Intent(this@BookingReceivedDetailsActivity, MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(intent)
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<SignUpResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
        if (p1) {
            if (p0?.id == R.id.rbCash) {
                mPaymentMode = "COD"
            } else if (p0?.id == R.id.rbPaytm) {
                mPaymentMode = "PAYTM"
            }
        }
    }

    private fun serviceCancelBookingApi(msg: String) {
        showDialog()
        API.getClient().create(ParkingService::class.java).cancelBooking(
                bookingReceivedDetailObj.bookedId.toString(),
                msg,
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<GenericResponse>> {
            override fun onResponse(call: Call<BaseResponse<GenericResponse>>?, response: Response<BaseResponse<GenericResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()!!.result.apply {
                        Util.showToast(message)
                    }
                    val intent = Intent(this@BookingReceivedDetailsActivity, MainActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(intent)
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<GenericResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun serviceInOutMonthly() {
        showDialog()

        API.getClient().create(ParkingService::class.java).inOutMonthly(
                bookingReceivedDetailObj.bookedId.toString()
        ).enqueue(object : Callback<BaseResponse<CheckoutBookingResponse>> {
            override fun onResponse(call: Call<BaseResponse<CheckoutBookingResponse>>?, response: Response<BaseResponse<CheckoutBookingResponse>>) {
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    dismissDialog()
                    response.body()!!.result.apply {
                        AlertDialogHelper.showInfoAlertDialog(this@BookingReceivedDetailsActivity,
                                "Space Availability Updated Successfully.",
                                object : AlertDialogHelper.SuccessCallback {
                                    override fun onSuccess() {
                                        val intent = Intent(this@BookingReceivedDetailsActivity, MainActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        startActivity(intent)
                                    }

                                })
                    }
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<CheckoutBookingResponse>>?, t: Throwable?) {
                dismissDialog()
            }

        })
    }

}
