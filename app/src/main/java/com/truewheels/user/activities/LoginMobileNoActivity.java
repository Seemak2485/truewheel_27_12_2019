package com.truewheels.user.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.truewheels.user.R;
import com.truewheels.user.TVPreferences;
import com.truewheels.user.constants.RequestCodeConstants;
import com.truewheels.user.interfaces.SmsListener;
import com.truewheels.user.receivers.SmsReceiver;
import com.truewheels.user.server.API;
import com.truewheels.user.server.API_SMS;
import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.IsRegisterResponse;
import com.truewheels.user.server.services.UserService;
import com.truewheels.user.util.Util;

import okhttp3.ResponseBody;
import online.devliving.passcodeview.PasscodeView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.truewheels.user.R.id.etPhone;

@SuppressWarnings("NullableProblems")
@SuppressLint("SetTextI18n")
public class LoginMobileNoActivity extends BaseActivity implements View.OnClickListener, SmsListener {
    private ViewSwitcher simpleViewSwitcher; // get the reference of ViewSwitcher
    private EditText mEtPhone;
    private Button mBConfirmPhoneNo, mBConfirmOtp;
    private TextView mTvTermsAndCondition, mTvResendOtp, mTvCountdownTimer, mTvMobileNo;
    private ImageButton mIbEditNumber;
    private PasscodeView mPinLayout;
    private CountDownTimer mCountDownTimer;
    private ProgressBar mPbLogin;
    private LinearLayout mllAutoReading;

    private String mOtp = "";
    private String mPhoneNum;
    private int mCheckOTPCount = 0;
    private int mMaxOTPRetryCount = 3;
//    //    private boolean mTesting = BuildConfig.DEBUG;
//    private boolean mTesting = false;

    private void resetToFillNumberAgain() {
        mllAutoReading.setVisibility(View.GONE);
        simpleViewSwitcher.showPrevious();
        mPhoneNum = "";
        mCheckOTPCount = 0;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_mobile_no);
        initViews();
    }

    private void initViews() {
        initToolbar(false);

        mEtPhone = findViewById(etPhone);
        mBConfirmPhoneNo = findViewById(R.id.bConfirm);
        mBConfirmOtp = findViewById(R.id.bConfirmOtp);
        mTvTermsAndCondition = findViewById(R.id.tvTermsAndCondition);
        mTvResendOtp = findViewById(R.id.tvResendOtp);
        mTvCountdownTimer = findViewById(R.id.tvCountdownTime);
        mIbEditNumber = findViewById(R.id.ibEditNumber);
        mPinLayout = findViewById(R.id.etPinLayout);
        mTvMobileNo = findViewById(R.id.tvOtpString);
        mPbLogin = findViewById(R.id.pLogin);
        mllAutoReading = findViewById(R.id.llAutoReading);

        mBConfirmOtp.setEnabled(false);

        setListeners();
        setSwitcherAnimation();
        setTimer();
    }

    private void setTimer() {
        mCountDownTimer = new CountDownTimer(60 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                mTvCountdownTimer.setText(getString(R.string.sec_reamaining) + millisUntilFinished / 1000);
            }

            public void onFinish() {
                mllAutoReading.setVisibility(View.GONE);
                mTvCountdownTimer.setVisibility(View.GONE);
                mTvResendOtp.setVisibility(View.VISIBLE);
            }
        };
    }

    private void setListeners() {
        mBConfirmPhoneNo.setOnClickListener(this);
        mBConfirmOtp.setOnClickListener(this);
        mTvResendOtp.setOnClickListener(this);
        mTvTermsAndCondition.setOnClickListener(this);
        mIbEditNumber.setOnClickListener(this);
        mPinLayout.setPasscodeEntryListener(mPinListener);
    }

    PasscodeView.PasscodeEntryListener mPinListener = new PasscodeView.PasscodeEntryListener() {
        @Override
        public void onPasscodeEntered(String passcode) {
            if (!TextUtils.isEmpty(passcode.trim())) {
                if (passcode.length() == 4) {
                    Util.hideKeyboard(LoginMobileNoActivity.this, mPinLayout);
                    mBConfirmOtp.setEnabled(true);
                }
            }
        }
    };

    private void setSwitcherAnimation() {
        simpleViewSwitcher = findViewById(R.id.view_switcher);
        Animation in = AnimationUtils.loadAnimation(this, android.R.anim.slide_in_left);
        simpleViewSwitcher.setInAnimation(in);
        Animation out = AnimationUtils.loadAnimation(this, android.R.anim.slide_out_right);
        simpleViewSwitcher.setOutAnimation(out);
    }

    private void sendOTP(String num, final boolean isResend) {
        if (!TextUtils.isEmpty(num) && num.length() == 10) {
            Util.hideKeyboard(LoginMobileNoActivity.this, mEtPhone);

            String otpString = "One time password for True wheel is ";
            mOtp = getOtpNumber();
            mPhoneNum = num;
            otpString = otpString + mOtp;
            Log.i(TAG, otpString + " for num " + mPhoneNum);
            mPbLogin.setVisibility(View.VISIBLE);

            /*API_SMS.getClient().create(API_SMS.SMSService.class)
                    .sendSMS(API_SMS.USER_NAME,
                            API_SMS.PASSWORD,
                            API_SMS.SENDER,
                            num,
                            otpString,
                            "ndnd",
                            API_SMS.STYPE)*/

            API_SMS.getClient().create(API_SMS.SMSService.class)
                    .sendSMS(API_SMS.USER_NAME,
                            API_SMS.PASSWORD,
                            API_SMS.SENDER,
                            num,
                            "TA",
                            "1",
                            otpString
                            ).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mPbLogin.setVisibility(View.GONE);
                    try {
                        Log.i(TAG, " string : " + response.toString());
                        if (!isResend) {
                            simpleViewSwitcher.showNext();
                        }
                        mPinLayout.clearText();
                        mTvMobileNo.setText("+91 " + mPhoneNum);

                       verifySmsReadPermission();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                   //// mPinLayout.setText("1234");
                    mTvResendOtp.setVisibility(View.GONE);
                    mTvCountdownTimer.setVisibility(View.VISIBLE);
                    mCountDownTimer.start();
                    mBConfirmOtp.setEnabled(false);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.i(TAG, " failure", t);
                    mPbLogin.setVisibility(View.GONE);
                    Util.showToast(getString(R.string.prompt_please_try_again));
                }
            });

        } else {
            Util.showToast(getString(R.string.phone_no_prompt));
        }
    }

    private void callIsRegistered(final String num) {
        mPbLogin.setVisibility(View.VISIBLE);

        API.getClient().create(UserService.class)
                .isRegistered(num).enqueue(new Callback<BaseResponse<IsRegisterResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<IsRegisterResponse>> call, Response<BaseResponse<IsRegisterResponse>> response) {
                mPbLogin.setVisibility(View.GONE);

              if (response.isSuccessful() && response.body().getResultCode() == 0) {
                    TVPreferences preferences = TVPreferences.getInstance();
                    preferences.setIsProfileUpdated(true);
                    preferences.setFullName(response.body().getResult().getUsername());
                    preferences.setPhone(response.body().getResult().getMobile());
                    preferences.setEmail(response.body().getResult().getEmail());
                    preferences.setAuthToken(response.body().getResult().getUserId());
                    preferences.setUserId(response.body().getResult().getUserId());

                    startActivity(new Intent(LoginMobileNoActivity.this, MainActivity.class));
                } else {
                    Intent i = new Intent(LoginMobileNoActivity.this, ProfileActivity.class);
                    TVPreferences.getInstance().setPhone(num);
                    Bundle b = ProfileActivity.Companion.getBundle(true, true, num);
                    i.putExtras(b);
                    startActivity(i);
                }
                finish();
            }

            @Override
            public void onFailure(Call<BaseResponse<IsRegisterResponse>> call, Throwable t) {
                mPbLogin.setVisibility(View.GONE);
                Log.i(TAG, "some error");
            }
        });
    }

    private String getOtpNumber() {
        Double d = Math.random() * Math.random() * 10000;
        String a = ((int) (d * 10000)) + "";
        return a.substring(0, 4);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bConfirm:
                if (validatePhoneNumber() && checkSmsReadPermission()) {
                    proceedFurther();
                }
                break;
            case R.id.tvTermsAndCondition:
                startActivity(new Intent(LoginMobileNoActivity.this, TncActivity.class));
                break;
            case R.id.bConfirmOtp:
                String userOtp = mPinLayout.getText().toString().trim();
                if (/*mTesting ||*/ mOtp.equals(userOtp)) {
                    Util.hideKeyboard(LoginMobileNoActivity.this, mEtPhone);
                    TVPreferences.getInstance().setPhone(mPhoneNum);
                    callIsRegistered(mPhoneNum);
                } else {
                    mPinLayout.clearText();
                    Util.showToast(getString(R.string.invalid_otp_prompt));
                }
                break;
            case R.id.tvResendOtp:
                mCheckOTPCount++;
                if (mCheckOTPCount > mMaxOTPRetryCount) {
                    resetToFillNumberAgain();
                    Util.showToast(getString(R.string.prompt_max_attemp));
                    return;
                }
                sendOTP(mPhoneNum, true);
                break;
            case R.id.ibEditNumber:

                mCountDownTimer.cancel();
                resetToFillNumberAgain();
                break;
        }
    }

    private void proceedFurther() {
        Util.hideKeyboard(LoginMobileNoActivity.this, mEtPhone);

        sendOTP(mEtPhone.getText().toString().trim(), false);

        mPhoneNum = mEtPhone.getText().toString().trim();
    }

    private boolean checkSmsReadPermission() {
        final String[] permissions = new String[]{Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS};
        if (ActivityCompat.checkSelfPermission(LoginMobileNoActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(LoginMobileNoActivity.this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginMobileNoActivity.this, permissions, RequestCodeConstants.REQUEST_CODE_OTP);
            return false;
        } else {
            return true;
        }
    }

    private boolean verifySmsReadPermission() {
        if (ActivityCompat.checkSelfPermission(LoginMobileNoActivity.this, Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(LoginMobileNoActivity.this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED) {
            SmsReceiver.bindListener(this);
            mllAutoReading.setVisibility(View.VISIBLE);
        }
        return false;
    }

    private boolean validatePhoneNumber() {
        String aPhoneNo = mEtPhone.getText().toString().trim();
        if (TextUtils.isEmpty(aPhoneNo) || aPhoneNo.length() != 10) {
            Util.showToast(getString(R.string.phone_no_prompt));
            return false;
        }
        return true;
    }

    @Override
    public void messageReceived(String messageText) {
        try {
            if (mllAutoReading.getVisibility() == View.VISIBLE) {
                mPinLayout.setText(messageText);
                mllAutoReading.setVisibility(View.GONE);
                mBConfirmOtp.performClick();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case RequestCodeConstants.REQUEST_CODE_OTP:
                proceedFurther();
                break;
        }
    }
}
