package com.truewheels.user.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.truewheels.user.R;
import com.truewheels.user.TVPreferences;
import com.truewheels.user.server.API;
import com.truewheels.user.server.model.BaseResponse;
import com.truewheels.user.server.model.ContactUsRequest;
import com.truewheels.user.server.services.UserService;
import com.truewheels.user.util.Util;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReachUsActivity extends BaseActivity {

    private EditText mNameEt;
    private EditText mEmailEt;
    private EditText mSubjectEt;
    private EditText mMessageEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reach_us);
        initToolbar();

        String name = TVPreferences.getInstance().getFullName();
        mNameEt = findViewById(R.id.name);
        mNameEt.setText(name);

        mEmailEt = findViewById(R.id.email);
        mEmailEt.setText(TVPreferences.getInstance().getEmail());

        mSubjectEt = findViewById(R.id.subject);
        mMessageEt = findViewById(R.id.reach_us_text);
        findViewById(R.id.submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    ContactUsRequest request = new ContactUsRequest();
                    request.setName(mNameEt.getText().toString().trim());
                    request.setEmail(mEmailEt.getText().toString().trim());
                    request.setSubject(mSubjectEt.getText().toString().trim());
                    request.setMessage(mMessageEt.getText().toString().trim());
                    request.setPhone(TVPreferences.getInstance().getPhone());
                    request.setUserId(TVPreferences.getInstance().getUserId());
                    showDialog();

                    API.getClient().create(UserService.class)
                            .contactUs(request).enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            dismissDialog();
                            if (response.body().getResultCode() == 0) {
                                Util.showToast("Thanks for reaching us.");
                                finish();
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            dismissDialog();
                        }
                    });
                }
            }
        });

        findViewById(R.id.btn_Whatsapp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.openWhatsApp(ReachUsActivity.this,
                        "917030701191",
                        "YOLO",
                        new Util.ErrorCallback() {
                            @Override
                            public void perform() {
                                Util.showToast("Error. Try our website!");
                                Util.openBrowser(ReachUsActivity.this, "http://www.truewheelsuat.in/#contact");
                            }
                        });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSubjectEt.requestFocus();
    }

    private boolean validate() {
        String name = mNameEt.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Util.showToast("Please Enter your name");
            return false;
        }

        String email = mEmailEt.getText().toString();
        if (TextUtils.isEmpty(name) || !email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            Util.showToast("Please Enter valid name");
            return false;
        }

        String subject = mSubjectEt.getText().toString();
        if (TextUtils.isEmpty(subject)) {
            Util.showToast("Please Enter subject ");
            return false;
        }

        String message = mMessageEt.getText().toString();
        if (TextUtils.isEmpty(message)) {
            Util.showToast("Please write message for us");
            return false;
        }
        return true;
    }


}
