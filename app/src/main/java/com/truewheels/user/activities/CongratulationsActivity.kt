package com.truewheels.user.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.truewheels.user.R
import com.truewheels.user.adaptors.TitleValueAdaptor
import com.truewheels.user.server.model.book_parking.book_now.TitleValuePojo
import kotlinx.android.synthetic.main.activity_congratulations.*
import kotlinx.android.synthetic.main.content_congratulations.*

class CongratulationsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_congratulations)
        initToolbar()
        init()
    }

    private fun init() {
        btnNavigateNow.setOnClickListener {
            val geoURI: String = intent.getStringExtra("geoURI")
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(geoURI)))
        }
        btnNavigateLater.setOnClickListener {
            val intent = Intent(this@CongratulationsActivity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
        }

        rvBookedParkingDetails.apply {
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(context)
            adapter = TitleValueAdaptor(this@CongratulationsActivity, R.layout.item_title_value)
        }

        val dataList: List<TitleValuePojo> = arrayListOf(
                TitleValuePojo("Payment Transaction ID", intent.getStringExtra("paymentTransactionID")),
                /*TitleValuePojo("Attendant Name", intent.getStringExtra("attendantName")),
                TitleValuePojo("Attendant Phone No", intent.getStringExtra("attendantPhoneNo")),
                TitleValuePojo("Owner Name", intent.getStringExtra("ownerName")),
                TitleValuePojo("Owner Address", intent.getStringExtra("ownerAddress")),
                TitleValuePojo("Owner Number", intent.getStringExtra("ownerNumber")),*/
                TitleValuePojo("Payment Mode", "CASH"),
                TitleValuePojo("", ""),
                TitleValuePojo("Total Charge", "Rs. " + intent.getStringExtra("paidAmount")))
        (rvBookedParkingDetails.adapter as TitleValueAdaptor).populate(dataList)
    }

}
