package com.truewheels.user.activities

import android.app.AlertDialog
import android.os.Bundle

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.android.material.textfield.TextInputEditText
import com.truewheels.user.R
import com.truewheels.user.TVPreferences
import com.truewheels.user.helpers.CheckboxDialogHelper
import com.truewheels.user.helpers.MaterialDateTimeHelper
import com.truewheels.user.server.API
import com.truewheels.user.server.model.BaseResponse
import com.truewheels.user.server.model.add_delete_vehicle.AddDeleteVehicleResponse
import com.truewheels.user.server.model.parkings.fetch_public_parking_detail.PublicParkingDetailItem
import com.truewheels.user.server.model.profile.FetchVehiclesResponse
import com.truewheels.user.server.model.profile.VehicleDetailsPojo
import com.truewheels.user.server.model.sys.select.*
import com.truewheels.user.server.model.sys.systemcode.SystemCodeResponse
import com.truewheels.user.server.services.ShareYourSpaceService
import com.truewheels.user.server.services.UserService
import com.truewheels.user.util.DateUtil
import com.truewheels.user.util.Util
import com.truewheels.user.util.ValidationUtil
import kotlinx.android.synthetic.main.activity_book_parking.*
import kotlinx.android.synthetic.main.content_book_parking.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class BookPublicParkingActivity : BaseActivity() {

    private lateinit var parkingDetailItemObj: PublicParkingDetailItem
    private lateinit var vehicleDetails: ArrayList<VehicleDetailsPojo>
    private val vehicleToBeParkedArrayList: ArrayList<String> = ArrayList()
    private lateinit var facilitiesArr: BooleanArray

    private var isCarVacancyEmpty: Boolean = false
    private var isBikeVacancyEmpty: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_parking)
        initToolbar()

        val carVacancy = intent.getStringExtra("carVacancy")
        isCarVacancyEmpty = (carVacancy == "0")
        val bikeVacancy = intent.getStringExtra("bikeVacancy")
        isBikeVacancyEmpty = (bikeVacancy == "0")

        serviceFetchVehiclesApi()
    }

    private fun init() {
        parkingDetailItemObj = intent.getSerializableExtra("parkingDetailItemObj") as PublicParkingDetailItem

        tvAddNewVehicle.setOnClickListener { v ->
            if (vehicleDetails.size >= 5) {
                Util.showToast("Cannot add more than 5 vehicles")
                return@setOnClickListener
            }
            val promptsView = LayoutInflater.from(v.context)
                    .inflate(R.layout.dialog_add_vehicle, null)

            val alertDialogBuilder = AlertDialog.Builder(v.context)
                    .setView(promptsView)
                    .setCancelable(true)
                    .create()

            val rbCar = promptsView.findViewById<RadioButton>(R.id.rbCar)
            val rbBike = promptsView.findViewById<RadioButton>(R.id.rbBike)
            val ivRadioCar = promptsView.findViewById<ImageView>(R.id.ivRadioCar)
            val ivRadioBike = promptsView.findViewById<ImageView>(R.id.ivRadioBike)
            val etDialogVehicleNo = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleNo)
            val etDialogVehicleModel = promptsView.findViewById<TextInputEditText>(R.id.etDialogVehicleModel)
            val btnDialogAddVehicle = promptsView.findViewById<Button>(R.id.btnDialogAddVehicle)

            rbCar.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car)
                    rbBike.isChecked = false
                } else {
                    ivRadioCar.setImageResource(R.drawable.ic_directions_car_unchecked)
                }
            }
            rbBike.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_light)
                    rbCar.isChecked = false
                } else {
                    ivRadioBike.setImageResource(R.drawable.ic_motorcycle_unchecked)
                }
            }
            btnDialogAddVehicle.setOnClickListener {
                var isValid = true
                if (!rbCar.isChecked && !rbBike.isChecked) {
                    Util.showToast("Please select vehicle type")
                    isValid = false
                } else if (etDialogVehicleNo.text.toString().isEmpty()) {
                    Util.showToast("Please enter vehicle number")
                    isValid = false
                } else if (!ValidationUtil.isValidVehicleNo(etDialogVehicleNo.text.toString())) {
                    Util.showToast(getString(R.string.vehicle_number_prompt))
                    isValid = false
                }
//                else if (etDialogVehicleModel.getText().toString().isEmpty()) {
//                    Util.showToast("Please enter vehicle model");
//                    alertDialogBuilder.cancel()
//                    isValid = false
//                }

                if (isValid) {
                    serviceAddDeleteVehicleApi(true,
                            VehicleDetailsPojo(
                                    -1,
                                    etDialogVehicleNo.text.toString(),
                                    if (rbCar.isChecked) rbCar.text.toString() else rbBike.text.toString(),
                                    etDialogVehicleModel.text.toString()))
                    alertDialogBuilder.dismiss()
                }
            }
            alertDialogBuilder.show()
        }

        btnParkingBookNow.setOnClickListener {
            //            serviceBookNowApi()
        }

        btnEstimatedCost.setOnClickListener {
            //            if (isFormValid())
            generateQrCode()
            toggleVisibilityBookOrEstimate(false)
            toggleVisibilityCalculatedResult(true)
        }

        etStartDate.setOnClickListener { MaterialDateTimeHelper.createDatePickerDialog(this@BookPublicParkingActivity, "Start Date?", etStartDate, true) }
        etStartTime.setOnClickListener { MaterialDateTimeHelper.createTimePickerDialog(this@BookPublicParkingActivity, "Start Time?", etStartTime) }
        etEndDate.setOnClickListener { MaterialDateTimeHelper.createDatePickerDialog(this@BookPublicParkingActivity, "End Date?", etEndDate, true) }
        etEndTime.setOnClickListener { MaterialDateTimeHelper.createTimePickerDialog(this@BookPublicParkingActivity, "End Time?", etEndTime) }

        initSpinners()
        initCheckboxGroups()

        sVehicleToBeParked.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?, position: Int, id: Long) {
                toggleVisibilityBookOrEstimate(true)
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                return
            }

        }
        val watcher: TextWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                return
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                return
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                toggleVisibilityBookOrEstimate(true)
            }

        }
        etStartDate.addTextChangedListener(watcher)
        etStartTime.addTextChangedListener(watcher)
        etEndDate.addTextChangedListener(watcher)
        etEndTime.addTextChangedListener(watcher)

        btnEstimatedCost.text = "Confirm"
    }

    private fun getInputValueForQrCode(): String {

        var iValue = System.currentTimeMillis().toString()

        val selectedVehicleNo = sVehicleToBeParked.selectedItem.toString()
        var selectedVehicleType = ""
        vehicleDetails.forEach {
            if (it.vehicleNumber == selectedVehicleNo) {
                selectedVehicleType = it.vehicleType
                return@forEach
            }
        }
        iValue = iValue + selectedVehicleNo + selectedVehicleType

        return iValue
    }

    private fun generateQrCode() {

    }

    private fun initSpinners() {
        setupSpinner(sVehicleToBeParked, "SELECT VEHICLE", vehicleToBeParkedArrayList)

        sParkingMode.visibility = View.GONE
        tvAdvanceBookingLabel.visibility = View.GONE
    }

    private fun setupSpinner(spinner: Spinner, title: String, arrayList: ArrayList<String>) {
        arrayList.add(0, title)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, arrayList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

    private fun initCheckboxGroups() {
        val facilitiesArrayList: ArrayList<String> = ArrayList(FacilityType.getMap().values)
        facilitiesArr = BooleanArray(facilitiesArrayList.size)
        etParkingFacilities.setOnClickListener { CheckboxDialogHelper.createCheckboxDialog(this@BookPublicParkingActivity, etParkingFacilities, facilitiesArrayList, facilitiesArr) }
    }

    private fun isFormValid(): Boolean {
        var formValidity = true

        when {
            etStartDate.text.isEmpty() -> {
                Util.showToast("Enter start date")
                formValidity = false
            }
            etEndDate.text.isEmpty() -> {
                Util.showToast("Enter end date")
                formValidity = false
            }
            etStartTime.text.isEmpty() -> {
                Util.showToast("Enter start time")
                formValidity = false
            }
            etEndTime.text.isEmpty() -> {
                Util.showToast("Enter end time")
                formValidity = false
            }
            DateUtil.compareDateWithSystemDate("${etStartDate.text} ${etStartTime.text}",
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") < 0 -> {
                Util.showToast("Please enter upcoming time as start time")
                formValidity = false
            }
            DateUtil.compareDates(etStartDate.text.toString() + " " + etStartTime.text.toString(),
                    etEndDate.text.toString() + " " + etEndTime.text.toString(),
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") < 0 -> {
                Util.showToast("Enter correct end date time")
                formValidity = false
            }
            sVehicleToBeParked.selectedItemPosition == 0 -> {
                Util.showToast("Please select a vehicle ")
                formValidity = false
            }
        // input consistency validations
            !isDateRangeValid(etStartDate.text.toString(), etEndDate.text.toString()) -> {
                Util.showToast("This parking is not valid on selected days")
                formValidity = false
            }

            parkingDetailItemObj.timeFrom.isNotEmpty() &&
                    DateUtil.compareDates(
                            parkingDetailItemObj.timeFrom,
                            etStartTime.text.toString(),
                            DateUtil.FORMAT_READABLE_TIME) < 0 -> {
                Util.showToast("This parking starts at ${parkingDetailItemObj.timeFrom}")
                formValidity = false
            }
            parkingDetailItemObj.timeTo.isNotEmpty() &&
                    DateUtil.compareDates(
                            parkingDetailItemObj.timeTo,
                            etEndTime.text.toString(),
                            DateUtil.FORMAT_READABLE_TIME) > 0 -> {
                Util.showToast("This parking ends at ${parkingDetailItemObj.timeTo}")
                formValidity = false
            }
            parkingDetailItemObj.timeTo.isNotEmpty() &&
                    DateUtil.getDifferenceInSeconds(etEndTime.text.toString(),
                            parkingDetailItemObj.timeTo, DateUtil.FORMAT_READABLE_TIME) < 30 * 60 -> {
                Util.showToast("This parking must be scheduled 30 minutes before its end time (${parkingDetailItemObj.timeTo})")
                formValidity = false
            }
            parkingDetailItemObj.timeFrom != "" && DateUtil.getDifferenceInSeconds(
                    "${etStartDate.text} ${etStartTime.text}",
                    "${etEndDate.text} ${etEndTime.text}",
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}")
                    >
                    DateUtil.getDifferenceInSeconds(
                            parkingDetailItemObj.timeFrom,
                            parkingDetailItemObj.timeTo,
                            DateUtil.FORMAT_READABLE_TIME) -> {
                Util.showToast("This parking ends at ${parkingDetailItemObj.timeTo} on the same day")
                formValidity = false
            }
            DateUtil.getDifferenceInSeconds(
                    DateUtil.getCurrentDate("${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}"),
                    "${etStartDate.text} ${etStartTime.text}",
                    "${DateUtil.FORMAT_READABLE_DATE} ${DateUtil.FORMAT_READABLE_TIME}") > 5 * 24 * 60 * 60 -> {
                Util.showToast("Pre-booking after 5 days from now is not allowed")
                formValidity = false
            }
        }

        return formValidity
    }

    private fun isDateRangeValid(startDateString: String, endDateString: String): Boolean {
        var startDate = DateUtil.getDate(DateUtil.FORMAT_READABLE_DATE, startDateString)
        val endDate = DateUtil.getDate(DateUtil.FORMAT_READABLE_DATE, endDateString)
        var result = true
        var count = 0

        val primeProduct = intent.getIntExtra("primeProduct", 19)
        val calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat(DateUtil.FORMAT_READABLE_DATE, Locale.getDefault())

        while (startDate <= endDate && count++ < 7) {
            // processing
            val dayOfWeek = DateUtil.convertFormat(
                    DateUtil.FORMAT_READABLE_DATE,
                    DateUtil.FORMAT_READABLE_DAY_OF_WEEK,
                    DateUtil.getDateAsString(DateUtil.FORMAT_READABLE_DATE, startDate))
                    .toUpperCase().substring(0, 2)

            val primeCode = AvailableDayType.getMapValue().getValue("D_$dayOfWeek")
            if (primeProduct % primeCode != 0) {
                result = false
                break
            }
            // adding one more day
            calendar.time = startDate
            calendar.add(Calendar.DAY_OF_YEAR, 1)
            val dateString = dateFormat.format(calendar.time)
            startDate = DateUtil.getDate(DateUtil.FORMAT_READABLE_DATE, dateString)
        }

        return result
    }

    private fun serviceAddDeleteVehicleApi(toAdd: Boolean, vehiclePojo: VehicleDetailsPojo) {
        showDialog()
        API.getClient().create(UserService::class.java).addDeleteVehicle(
                if (toAdd) "" else vehiclePojo.vehicleID.toString(),
                TVPreferences.getInstance().userId,
                if (toAdd) "1" else "0",
                vehiclePojo.vehicleNumber,
                vehiclePojo.vehicleModel,
                vehiclePojo.vehicleType
        ).enqueue(object : Callback<BaseResponse<AddDeleteVehicleResponse>> {
            override fun onResponse(call: Call<BaseResponse<AddDeleteVehicleResponse>>, response: Response<BaseResponse<AddDeleteVehicleResponse>>) {
                dismissDialog()
                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    vehicleDetails = ArrayList()
                    vehicleToBeParkedArrayList.clear()

                    if (response.body() != null && response.body()?.result != null && response.body()?.result!!.userVehicleDetails?.size != 0) {
                        for ((vehicleID, vehicleNo, vehicleType, vehicleModel) in response.body()?.result!!.userVehicleDetails!!) {
                            if (vehicleNo!!.isEmpty())
                                continue
                            if (isCarVacancyEmpty && "Four Wheeler" == vehicleType)
                                continue
                            if (isBikeVacancyEmpty && "Two Wheeler" == vehicleType)
                                continue

                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleID.toInt(),
                                    vehicleNo,
                                    vehicleType!!,
                                    vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleNo)
                        }
                    }

                    initSpinners()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<AddDeleteVehicleResponse>>, t: Throwable) {
                dismissDialog()
            }
        })
    }

    private fun serviceFetchVehiclesApi() {
        showDialog()

        API.getClient().create(UserService::class.java).fetchVehicles(
                TVPreferences.getInstance().userId).enqueue(object : Callback<BaseResponse<FetchVehiclesResponse>> {
            override fun onResponse(call: Call<BaseResponse<FetchVehiclesResponse>>?, response: Response<BaseResponse<FetchVehiclesResponse>>) {
                dismissDialog()

                if (response.isSuccessful && response.body()!!.resultCode == 0) {
                    response.body()?.result!!.apply {
                        vehicleDetails = ArrayList()
                        vehicleToBeParkedArrayList.clear()

                        if (userDetails.isEmpty())
                            return@apply

                        for (userDetail in userDetails) {
                            val vehicleItem = userDetail.listVehicle!![0]
                            if (vehicleItem.vehicleNo!!.isEmpty())
                                continue
                            if (isCarVacancyEmpty && "Four Wheeler" == vehicleItem.vehicleType)
                                continue
                            if (isBikeVacancyEmpty && "Two Wheeler" == vehicleItem.vehicleType)
                                continue

                            val vehicleDetailsPojo = VehicleDetailsPojo(
                                    vehicleItem.VehicleID,
                                    vehicleItem.vehicleNo,
                                    vehicleItem.vehicleType!!,
                                    vehicleItem.vehicleModel!!)
                            vehicleDetails.add(vehicleDetailsPojo)

                            vehicleToBeParkedArrayList.add(vehicleItem.vehicleNo)
                        }
                    }

                    if (StateType.getMap().isEmpty())
                        serviceSystemCodeApi()
                    else
                        init()
                } else {
                    if (null != response.body())
                        Util.showToast(response.body()!!.error)
                    else
                        Util.showToast("Error Code : ${response.code()}")
                }
            }

            override fun onFailure(call: Call<BaseResponse<FetchVehiclesResponse>>?, t: Throwable?) {
                dismissDialog()
            }
        })
    }

    private fun toggleVisibilityBookOrEstimate(isEstimateCostVisible: Boolean) {
        if (isEstimateCostVisible) {
            btnEstimatedCost.visibility = View.VISIBLE
            btnParkingBookNow.visibility = View.GONE
            toggleVisibilityCalculatedResult(false)
        } else {
            btnEstimatedCost.visibility = View.GONE
            btnParkingBookNow.visibility = View.VISIBLE
        }
    }

    private fun toggleVisibilityCalculatedResult(isVisible: Boolean) {
        val visibilityValue: Int = if (isVisible) View.VISIBLE else View.GONE

        ivQrCode.visibility = visibilityValue
        tvQrCode.visibility = visibilityValue
    }

    private fun serviceSystemCodeApi() {
        showDialog()

        API.getClient().create(ShareYourSpaceService::class.java).fetchSystemCodes()
                .enqueue(object : Callback<BaseResponse<SystemCodeResponse>> {
                    override fun onResponse(call: Call<BaseResponse<SystemCodeResponse>>?, response: Response<BaseResponse<SystemCodeResponse>>) {
                        dismissDialog()

                        if (response.isSuccessful && response.body()!!.resultCode == 0) {
                            response.body()?.result!!.apply {
                                for (systemCode in systemCodes) {
                                    when (systemCode?.category) {
                                        "StateType" -> StateType.setMap(systemCode.categoryProperty)
                                        "SpaceType" -> SpaceType.setMap(systemCode.categoryProperty)
                                        "PropertyType" -> PropertyType.setMap(systemCode.categoryProperty)
                                        "ParkingCategory" -> ParkingCategory.setMap(systemCode.categoryProperty)
                                        "Facility Type" -> FacilityType.setMap(systemCode.categoryProperty)
                                        "DescriptionType" -> DescriptionType.setMap(systemCode.categoryProperty)
                                        "DaysType" -> AvailableDayType.setMap(systemCode.categoryProperty)
                                    }
                                }

                                init()
                            }
                        } else {
                            if (null != response.body())
                                Util.showToast(response.body()!!.error)
                            else
                                Util.showToast("Error Code : ${response.code()}")
                        }
                    }

                    override fun onFailure(call: Call<BaseResponse<SystemCodeResponse>>?, t: Throwable?) {
                        dismissDialog()
                    }
                })
    }
}
