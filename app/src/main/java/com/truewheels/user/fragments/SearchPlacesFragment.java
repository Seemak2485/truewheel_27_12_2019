package com.truewheels.user.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.truewheels.user.constants.Constants;
import com.truewheels.user.R;
import com.truewheels.user.server.model.LocationData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.facebook.FacebookSdk.getApplicationContext;


public class SearchPlacesFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = SearchPlacesFragment.class.getName();
    private static final String TASK_TAG = "TASK_TAG";
    private static final String LOCATION_TYPE = "LOCATION_TYPE";
    private static final String EDITABLE = "EDITABLE";
//    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(8.500000, 76.380000), new LatLng(16.7576556, 77.7972364));

    // TODO: Rename and change types of parameters
    private OnSearchLocationFragmentInteractionListener mListener;

    private EditText mSearchPlaceTv;
    private LinearLayout mSuggestionLayout;

    private LayoutInflater mInflater;
    private ArrayList<View> mSuggestionViewList;
    private boolean mGetSuggestionContinue;

    private GoogleApiClient mGoogleApiClient;
    private String mFetchingText;
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            Log.i(TAG, "places callback-- Thread-Name-" + Thread.currentThread().getName());
            if (!places.getStatus().isSuccess()) {
                Log.e("", "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            try {
                final Place place = places.get(0);
                String placeName = place.getName().toString();
                if (TextUtils.isEmpty(placeName))
                    placeName = "";
                LocationData d = new LocationData();
                d.setDestination(placeName.toString());
                d.setParkingClass("PC_A");
                d.setLat(place.getLatLng().latitude);
                d.setLng(place.getLatLng().longitude);
                mListener.finishWithLocationSearch(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() >= 3) {
                if (mSuggestionViewList == null) {
                    mSuggestionViewList = new ArrayList<>();
                    for (int i = 0; i < 5; i++) {
                        mSuggestionViewList.add(i, getView("", i));
                    }
                }
                getPredictions(s);

            } else {
                hideSuggestion();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public static SearchPlacesFragment newInstance() {
        SearchPlacesFragment fragment = new SearchPlacesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setGoogleApiClient();



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_serarch_places, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mSearchPlaceTv = (EditText) view.findViewById(R.id.search_location);
        mSuggestionLayout = (LinearLayout) view.findViewById(R.id.suggestion_box);


            /*mAddressClearButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(mSearchPlaceTv.getText())) {
                        mListener.discardSearch();
                    } else {
                        mSearchPlaceTv.setText("");
                    }
                }
            });*/

        mSearchPlaceTv.addTextChangedListener(textWatcher);

    }

    private void hideSuggestion() {
        mSuggestionLayout.removeAllViews();
        mSuggestionLayout.setVisibility(View.GONE);
    }

    private void showSuggestion() {
        mSuggestionLayout.removeAllViews();
        mSuggestionLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchLocationFragmentInteractionListener) {
            mListener = (OnSearchLocationFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private View getView(String text, int index) {
        if (mInflater == null) {
            mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        final View view = mInflater.inflate(R.layout.location_search_suggestion_row_layout, null, false);

        TextView tv = ((TextView) view.findViewById(R.id.suggested_address));
        tv.setText(text);
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlaceAutocomplete place = (((LocationSuggesitionViewHolder) view.getTag()).place);
                PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.placeId + "");
                mSearchPlaceTv.requestFocus();
                hideSuggestion();
                placeResult.setResultCallback(mUpdatePlaceDetailsCallback);
                mSearchPlaceTv.removeTextChangedListener(textWatcher);
                mSearchPlaceTv.setText(v.getTag().toString());
                mSearchPlaceTv.addTextChangedListener(textWatcher);
            }
        });
        LocationSuggesitionViewHolder holder = new LocationSuggesitionViewHolder();
        holder.textView = tv;
        holder.index = index;

        view.setTag(holder);
        return view;
    }



    private void getPredictions(final CharSequence constraint) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mGetSuggestionContinue = true;
                if (mGoogleApiClient != null) {
                    Log.i(TAG, "getPredictions() " + constraint + " ++++ mGoogleApiClient " + mGoogleApiClient.isConnected());
                    Log.i(TAG, "Current Thread 0: " + Thread.currentThread().getName());
                    AutocompleteFilter filter = new AutocompleteFilter.Builder().setCountry("IN")
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_ADDRESS).build();

                    PendingResult<AutocompletePredictionBuffer> results =
                            Places.GeoDataApi
                                    .getAutocompletePredictions(mGoogleApiClient, constraint.toString(),
                                            getBounds(Constants.CENTER_NCR_LATLNG, Constants.DISTANCE), filter);

                    // Wait for predictions, set the timeout.
                    AutocompletePredictionBuffer autocompletePredictions = results
                            .await(60, TimeUnit.SECONDS);
                    final Status status = autocompletePredictions.getStatus();
                    if (!status.isSuccess()) {
                        Log.e(TAG, "Error getting place predictions: " + status
                                .toString());
                        /*Toast.makeText(this, "Error: " + status.toString(),
                                Toast.LENGTH_SHORT).show();*/

                        autocompletePredictions.release();
                    }

                    Log.i(TAG, "Query completed. Received " + autocompletePredictions.getCount()
                            + " predictions.");
                    Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();


                    final ArrayList<PlaceAutocomplete> resultList = new ArrayList<>();


                    Log.e(TAG, "iterator data");

                    while (iterator.hasNext()) {
                        AutocompletePrediction prediction = iterator.next();

                        resultList.add(new PlaceAutocomplete(prediction.getPlaceId(),
                                getDescriptionOnUI(prediction.getFullText(null).toString())));
                    }
                    Log.e(TAG, "refill data");
                    // Buffer release
                    autocompletePredictions.release();
                    Log.e(TAG, "return data length : " + resultList.size());
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showSuggestion();
                            if (mGetSuggestionContinue) {
                                for (int i = 0; i < 5 && i < resultList.size(); i++) {
                                    PlaceAutocomplete item = resultList.get(i);
                                    View view = mSuggestionViewList.get(i);
                                    LocationSuggesitionViewHolder holder = (LocationSuggesitionViewHolder) view.getTag();
                                    holder.textView.setText(item.description.toString());
                                    holder.textView.setTag(item.description.toString());
                                    holder.place = item;
                                    mSuggestionLayout.addView(view);
                                }
                            }
                        }
                    });
                } else {
                    Log.e(TAG, "Google API client problem.");
                }
            }
        }).start();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "mGoogleApiClient  connected.... ");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "mGoogleApiClient  onConnectionSuspended.... ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "mGoogleApiClient  onConnectionFailed.... ");
    }

    private LatLngBounds getBounds(LatLng location, long mDistanceInMeters) {
        double latRadian = Math.toRadians(location.latitude);

        double degLatKm = 110.574235;
        double degLongKm = 110.572833 * Math.cos(latRadian);
        double deltaLat = mDistanceInMeters / 1000.0 / degLatKm;
        double deltaLong = mDistanceInMeters / 1000.0 / degLongKm;

        double minLat = location.latitude - deltaLat;
        double minLong = location.longitude - deltaLong;
        double maxLat = location.latitude + deltaLat;
        double maxLong = location.longitude + deltaLong;

        Log.d(TAG, "Min: " + Double.toString(minLat) + "," + Double.toString(minLong));
        Log.d(TAG, "Max: " + Double.toString(maxLat) + "," + Double.toString(maxLong));

        return new LatLngBounds(new LatLng(minLat, minLong), new LatLng(maxLat, maxLong));
    }

    private String getDescriptionOnUI(String desc) {

        try {
            String finalDisplay;
            String[] arr = desc.split(",");
            int len = arr.length;
            if (arr.length >= 3) {

                finalDisplay = arr[0] + "," + arr[1] + "\n" + arr[len - 3] + "," + arr[len - 2] + arr[len - 1];
            } else {
                finalDisplay = desc;
            }

            return finalDisplay;
        } catch (Exception e) {
            e.printStackTrace();
            return desc;
        }
    }


    public interface OnSearchLocationFragmentInteractionListener {
        void onBackPress();

        void startSearchingLocation();

        void finishWithLocationSearch(LocationData data);

        void discardSearch();
    }

    private class LocationSuggesitionViewHolder {
        TextView textView;
        int index;
        PlaceAutocomplete place;
    }

    public class PlaceAutocomplete {

        public CharSequence placeId;
        public CharSequence description;
        public CharSequence primaryText;
        public CharSequence secndText;


        PlaceAutocomplete(CharSequence placeId, CharSequence description) {
            this.placeId = placeId;
            this.description = description;
//            this.primaryText= p;
//            secndText= s;

        }

        @Override
        public String toString() {

            try {
                return description.toString();
            } catch (Exception e) {
                return "";
            }

        }
    }
}
